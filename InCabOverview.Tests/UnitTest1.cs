using InCabOverview;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace InCabOverview.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        [DataRow("York", "20072021", "VX19 YSO")]
        public void TestMethod1(string client, string date, string registration)
        {
            var allMessages = JsonConvert.DeserializeObject<List<Message>>(File.ReadAllText($@"data\{client}\AllMessages.{client}.{registration}.{date}.json"));
            var gpsTrail = JsonConvert.DeserializeObject<List<Coordinate>>(File.ReadAllText($@"data\{client}\GPSTrails.{client}.{registration}.{date}.json"));
            var propertiesOnRound = JsonConvert.DeserializeObject<List<Property>>(File.ReadAllText($@"data\{client}\PropertiesOnRound.{client}.{registration}.{date}.json"));

            var oldSample = new SOTN_Unit();
            var newSample = new SOTN_Unit();

            var oldResult = oldSample.GetCollectedProperties(allMessages, gpsTrail, propertiesOnRound);
            var newResult = newSample.GetCollectedProperties_NEW(allMessages, gpsTrail, propertiesOnRound);

            Assert.AreEqual(oldResult.Count, newResult.Count);
            Assert.AreEqual(oldSample.StreetsCollected, newSample.StreetsCollected);
        }
    }
}
