﻿
using incabOverview_v1._1;
using incabOverview_v1._1.Classes;
using System;
using System.Web;
using System.Web.Security;
using incabOverview_v1._1.Account;

namespace TestProduct1
{
    public partial class TestProduct1Landing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String Client = _DetailsFromURL.GetClient(Request.Url.AbsoluteUri);

            String Error = "";

            Boolean isPersistent = false;

            // Check if they've already logged in
            String authCookie = Request.QueryString["var1"];

            if (authCookie != null)
            {
                String Email = Request.QueryString["var2"];

                if (Authentication.VerifyCookie(Client, authCookie, Email))
                {
                    // Create the cookie.
                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, authCookie));

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                                                                                     Email,
                                                                                     DateTime.UtcNow,
                                                                                     DateTime.UtcNow.AddMinutes(360),
                                                                                     isPersistent,
                                                                                     authCookie,
                                                                                     FormsAuthentication.FormsCookiePath);

                    // Encrypt the ticket.
                    String encryptedTicket = FormsAuthentication.Encrypt(ticket);

                    // Create the cookie.
                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

                    // Redirect back to original URL.
                    Response.Redirect(FormsAuthentication.GetRedirectUrl(Email, isPersistent));
                }
            }

            //Get them to login
            if (IsPostBack)
            {
                String Email = Request["user"];

                String Password = Request["pass"];

                String Action = Request["action"];

                String Product = _DetailsFromURL.GetProduct(Request.Url.AbsoluteUri);


                if (Authentication.AuthenticateUser(Client, Email, Password, out Error))
                {
                    if (Authorisation.AccessToProductAllowed(Client, Email, Product, ref Error))
                    {
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                                                                                         Email,
                                                                                         DateTime.UtcNow,
                                                                                         DateTime.UtcNow.AddMinutes(360),
                                                                                         isPersistent,
                                                                                         "~#Direct#~",
                                                                                         FormsAuthentication.FormsCookiePath);

                        // Encrypt the ticket.
                        String encryptedTicket = FormsAuthentication.Encrypt(ticket);

                        //Store the cookie in the DDUser table.
                        Authentication.RecordCookie(Client, encryptedTicket, Email);

                        // Create the cookie.
                        Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

                        // Redirect back to original URL.
                        Response.Redirect(FormsAuthentication.GetRedirectUrl(Email, isPersistent));
                    }
                    else
                    {
                        invalidLogin.Text = Error;
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();

                    invalidLogin.Text = Error;
                }
            }
        }

        protected string GetGreeting()
        {
            return String.Format("Hello {0}", Context.User.Identity.Name);
        }

        protected bool GetRequestStatus()
        {
            return Request.IsAuthenticated;
        }


    }
}