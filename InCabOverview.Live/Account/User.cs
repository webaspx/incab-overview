﻿namespace incabOverview_v1._1.Account
{
    internal class User
    {
        /// <summary>
        /// The user's display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// The user's account email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The user account council.
        /// </summary>
        public string Council { get; set; }

        /// <summary>
        /// The account identifier.
        /// </summary>
        internal long ID { get; set; }

        /// <summary>
        /// The user's password hashed.
        /// </summary>
        internal string PasswordHash { get; set; }

        /// <summary>
        /// The salt added to the user's password before hashing.
        /// </summary>
        internal string PasswordSalt { get; set; }

        /// <summary>
        /// The account locked status.
        /// </summary>
        /// 
        private short lockStateValue;

        public short LockState
        {
            get { return lockStateValue; }
            set
            {
                lockStateValue = value;
                if (LockState == 0)
                {
                    IsLocked = false;

                }
                else
                {
                    IsLocked = true;
                }
            }
        }

        /// <summary>
        /// Is this person allowed to administrate other users and their access.
        /// </summary>
        internal bool AdminDD { get; set; }

        internal bool IsLocked { get; set; }

        public string ParentEmail { get; set; }
    }
}