﻿using incabOverview_v1._1.Account;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Webaspx.Database;
using Webaspx.Database.Encryption;
using incabOverview_v1._1.Classes;

namespace incabOverview_v1._1
{
    internal static class Authentication
    {
        internal static bool AuthenticateUser(String Client, String Email, String Password, out String Error)
        {
            Error = "";

            User user = GetAccount(Client, Email);

            if (user != null)
            {
                if (SHA256Crypto.GetSHA256Hash(Password + user.PasswordSalt) == user.PasswordHash)
                {
                    if (!user.IsLocked)
                    {
                        UpdateLastLogin(Client, Email);

                        return true;
                    }

                    Error = "This account is locked. Please contact your administrator.";

                    return false;
                }
            }

            Error = "Incorrect username or password.";

            return false;
        }

        private static void UpdateLastLogin(String Client, String Email)
        {
            String Query = @"UPDATE
                                 [DDUsers]
                             SET
                                 [LastLogin] = @LastLogin
                             WHERE
                                 [Email] = @Email";

            Dictionary<String, String> Parameters = new Dictionary<String, String>() {
                {"LastLogin", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")},
                {"Email", Email}
            };
            
            Database.Write(Query, Client, Parameters);
        }

        internal static void RecordCookie(String Client, String Value, String Email)
        {
            String Query = @"UPDATE
                                 [DDUsers]
                             SET
                                 [LoginToken] = @LoginToken
                             WHERE
                                 [Email] = @Email";

            Dictionary<String, String> Parameters = new Dictionary<String, String>() {
                {"LoginToken", Value},
                {"Email", Email}
            };

            Database.Write(Query, Client, Parameters);
        }

        internal static void DeleteCookie(String Client, String Email)
        {
            String Value = SHA256Crypto.GetSHA256Hash(new Random().ToString() + DateTime.Now.AddYears(-100).ToString());

            String Query = @"UPDATE
                                 [DDUsers]
                             SET
                                 [LoginToken] = @LoginToken
                             WHERE
                                 [Email] = @Email";

            Dictionary<String, String> Parameters = new Dictionary<String, String>() {
                {"LoginToken", Value},
                {"Email", Email}
            };

            Database.Write(Query, Client, Parameters);
        }

        internal static User GetAccount(String Client, String Email)
        {
            using (DatabaseConnection dbc = new DatabaseConnection(Client, Catalog.Collections))
            {
                String Query = @"SELECT
                                     [ID],
                                     [Email],
                                     [DisplayName],
                                     [PasswordHash],
                                     [PasswordSalt],
                                     [LockState],
                                     [AdminDD],
                                     [ParentEmail]
                                 FROM
                                     [DDUsers]
                                 WHERE
                                     [Email] = @Email";

                using (SqlDataReader reader = dbc.ExecuteReader(Query, new SqlParameter("Email", Email)))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            return new User {
                                ID = Convert.ToInt64(reader["ID"].ToString()),
                                DisplayName = reader["DisplayName"].ToString(),
                                Email = reader["Email"].ToString(),
                                PasswordHash = reader["PasswordHash"].ToString(),
                                PasswordSalt = reader["PasswordSalt"].ToString(),
                                LockState = Convert.ToInt16(reader["LockState"].ToString()),
                                AdminDD = Convert.ToBoolean(Convert.ToInt32(reader["AdminDD"])),
                                ParentEmail = reader["ParentEmail"].ToString(),
                            };
                        }
                    }
                }
            }

            return null;
        }


        internal static bool VerifyCookie(String Client, String Cookie, String Email)
        {
            if (!String.IsNullOrWhiteSpace(Cookie))
            {
                using (DatabaseConnection dbc = new DatabaseConnection(Client, Catalog.Collections))
                {
                    String CookieCount = @"SELECT
                                               COUNT(*)
                                           FROM
                                               [DDUsers]
                                           WHERE
                                                   [LoginToken] = @Cookie
                                               AND
                                                   [Email] = @Email";

                    if (Convert.ToInt16(dbc.ExecuteScalar(CookieCount, new SqlParameter("Cookie", Cookie), new SqlParameter("Email", Email))) == 1)
                    {
                        String LockStateQuery = @"SELECT
                                                      [LockState]
                                                  FROM
                                                      [DDUsers]
                                                  WHERE
                                                      [LoginToken] = @Cookie";

                        if (Convert.ToInt16(dbc.ExecuteScalar(LockStateQuery, new SqlParameter("Cookie", Cookie))) == 0)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}