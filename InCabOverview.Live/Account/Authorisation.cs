﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Webaspx.Database;

namespace incabOverview_v1._1.Account
{
    public class Authorisation
    {
        internal static List<Product> GetProducts(String Client, Int64 ID)
        {
            String Query = @"SELECT
                                 * 
                             FROM 
                                 [DDProductAccess]
                             WHERE
                                 [UserID] = @ID";

            List<Product> Products = new List<Product>();

            using (DatabaseConnection dbc = new DatabaseConnection(Client, Catalog.Collections))
            {
                using (SqlDataReader reader = dbc.ExecuteReader(Query, new SqlParameter("ID", ID)))
                {
                    while (reader.Read())
                    {
                        Products.Add(new Product() {
                            Name = reader["ProductName"].ToString(),
                            AuthorisationLevel = reader["AuthorisationLevel"].ToString(),
                            ScreenAccess = reader["ScreenAccess"].ToString(),
                            StartUpScreen = reader["StartUpScreen"].ToString(),
                            Accessible = true,
                        });
                    }
                }

                List<String> AllProducts = new List<String>();

                String ProductsQuery = @"SELECT 
                                             DISTINCT 
                                                 [ProductName] 
                                         FROM 
                                             [DDProductAccess]";

                using (SqlDataReader reader = dbc.ExecuteReader(ProductsQuery))
                {
                    while (reader.Read())
                    {
                        AllProducts.Add(reader["ProductName"].ToString());
                    }
                }

                List<String> AccessibleProducts = Products.Select(x => x.Name).ToList();

                List<String> notAccessible = AllProducts.Except(AccessibleProducts).ToList();

                for (int i = 0; i < notAccessible.Count; i++)
                {
                    Products.Add(new Product() { Name = notAccessible[i] });
                }
            }


            return Products.OrderBy(x => x.Name).ToList();
        }

        internal static Boolean AccessToProductAllowed(String Client, String Email, String Product, ref String Error)
        {
            using (DatabaseConnection dbc = new DatabaseConnection(Client, Catalog.Collections))
            {
                String Query = @"SELECT
                                     TOP 1
                                         [ID]
                                 FROM
                                     [DDUsers]
                                 WHERE
                                     [Email] = @Email";

                Int32 ID = Convert.ToInt32(dbc.ExecuteScalar(Query, new SqlParameter("Email", Email)));

                String ProductQuery = @"SELECT
                                            COUNT([UserID])
                                        FROM 
                                            [DDProductAccess]
                                        WHERE 
                                                [UserID] = @ID 
                                            AND 
                                                [ProductName] = @Product";

                Int32 ProductCount = Convert.ToInt32(dbc.ExecuteScalar(ProductQuery, new SqlParameter("ID", ID), new SqlParameter("Product", Product)));

                Error = "You do not have access to this module.";

                return (ProductCount == 1);
            }

        }
    }
}