﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incabOverview_v1._1.Account
{
    /// <summary>
    /// List of products we have e.g InCab , Collections, Disposal and whether the person has acess to this product or not
    /// </summary>
    public class Product
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                this.URL = String.Format("http://{0}-##council##.azurewebsites.net", name);
            }
        }

        public bool Accessible { get; set; }

        public string AuthorisationLevel { get; set; }

        public string ScreenAccess { get; set; }

        public string StartUpScreen { get; set; }

        public string URL { get; set; }
    }
}