﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using incabOverview_v1._1.Classes;
using System.Globalization;

namespace incabOverview_v1._1.Classes
{
    public enum MessageType
    {
        GPS, Issue, NoGPSFix, CSY, CPY, CPN, CSN, DriverCheck, LogIn, TT, UI, Message, Other
    }
    
    public enum IdentifierType
    {
        RegistrationRound, Registration, Round, None,
    }

    public struct Message
    {
        public Message(DataRow Row)
        {

        }
    }

    public struct Mes3sage
    {
        public String Data;

        public DateTime Sent;

        public Issue? Issue;

        public Coordinate Coordinate;

        public MessageType MessageType;

        public IdentifierType Identifier;

        public Mes3sage(DataRow Row, List<Issue> Issues)
        {
            this.Issue = null;

            this.Identifier = IdentifierType.None;

            String[] Data = Row.ItemArray.StringArray();

            Boolean CoordinateValid = false;

            if (Data[1].Contains("GPS:"))
            {
                MessageType = MessageType.GPS;

                Identifier = IdentifierType.Registration;

                CoordinateValid = true;
            }
            else if (Data[1].Contains("NoGPSFix"))
            {
                MessageType = MessageType.NoGPSFix;

                Identifier = IdentifierType.RegistrationRound;

                CoordinateValid = true;
            }
            else if (Data[1].Contains("Driver Check"))
            {
                MessageType = MessageType.DriverCheck;

                CoordinateValid = true;
            }
            else if (Data[1].Contains("$IN$"))
            {
                MessageType = MessageType.LogIn;

                Identifier = IdentifierType.Round;
            }
            else if (Data[1].Contains("$CSY$"))
            {
                MessageType = MessageType.CSY;

                Identifier = IdentifierType.Round;
            }
            else if (Data[1].Contains("$CSN$"))
            {
                MessageType = MessageType.CSN;

                Identifier = IdentifierType.Round;
            }
            else if (Data[1].Contains("$CPY$"))
            {
                MessageType = MessageType.CPY;

                Identifier = IdentifierType.Round;
            }
            else if (Data[1].Contains("$CPN$"))
            {
                MessageType = MessageType.CPN;

                Identifier = IdentifierType.Round;
            }
            else if (Data[1].Contains("$TT$"))
            {
                MessageType = MessageType.TT;
            }
            else if (Data[1].Contains("$UI$"))
            {
                MessageType = MessageType.UI;
            }
            else
            {
                Issue[] IssuesDetected = Issues.Where(x => Data[1].Contains(x.IssueCode) || Data[1].Contains(x.IssueText)).ToArray();

                //Issue[] IssuesDetected = Issues.Values.SelectMany(x => x.Where(y => Data[1].Contains(y.IssueCode) || Data[1].Contains(y.IssueText))).ToArray();

                if (IssuesDetected.Length > 0)
                {
                    this.Issue = IssuesDetected[0];

                    Identifier = IdentifierType.Round;

                    MessageType = MessageType.Issue;
                }
                else
                {
                    MessageType = (Data[1].Contains("$")) ? MessageType.Other : MessageType.Message;
                }
            }

            DateTime TimeSent;

            if ((Data[2].Length > 8))
            {
                TimeSent = DateTime.ParseExact(Data[2].RemoveLast(Data[2].Length - 8), "ddMMyyyy", CultureInfo.CurrentCulture) + TimeSpan.FromHours(Convert.ToDouble(Data[3]));
            }
            else
            {
                TimeSent = DateTime.ParseExact(Data[2], "ddMMyyyy", CultureInfo.CurrentCulture) + TimeSpan.FromHours(Convert.ToDouble(Data[3]));
            }

            this.Sent = TimeSent;

            this.Data = Row.ItemArray[1].ToString();

            this.Coordinate = (CoordinateValid) ? new Coordinate(Row.ItemArray) : null;

            if (CoordinateValid)
            {
                if (this.Coordinate.Latitude < 50 && this.Coordinate.Latitude != 0 && this.Coordinate.Longitude != 0)
                {

                }
            }
        }
    }
}