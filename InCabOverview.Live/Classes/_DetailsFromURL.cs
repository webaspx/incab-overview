﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webaspx.Database;

namespace incabOverview_v1._1.Classes
{
    public class _DetailsFromURL
    {
        internal static String GetClient(String URL)
        {
            String Client = "Webaspx";

            if (URL.Contains("localhost"))
            {
                return Client;
            }

            if (URL.Contains('-'))
            {
                Int32 firstHyphen = URL.IndexOf('-');

                if (URL.Contains('.') && firstHyphen > 0)
                {
                    Int32 nextFullstop = URL.IndexOf('.', firstHyphen);

                    if (nextFullstop > firstHyphen)
                    {
                        firstHyphen++;

                        String uncleanCouncil = URL.Substring(firstHyphen, nextFullstop - firstHyphen).Trim();

                        String[] listCouncils = DatabaseConnection.GetCollectionsAndInCabClientsList();

                        Client = listCouncils.Where(x => x.ToUpper() == uncleanCouncil.ToUpper()).SingleOrDefault();
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(Client))
            {
                Client = "Webaspx";
            }

            return Client;
        }

        internal static String GetProduct(String url)
        {
            String product = "";

            if (url.Contains("localhost"))
            {
                product = "InCabLive";

                if (String.IsNullOrEmpty(product))
                {
                    //  throw new Exception("Product is currently and empty string. Please define the product you're testing.");
                }

                return product;
            }
            else
            {
                Int32 start = url.IndexOf('/') + 2;

                Int32 firstHyphen = url.IndexOf('-');

                product = url.Substring(start, firstHyphen - start).Trim();

                return product;
            }
        }
    }
}