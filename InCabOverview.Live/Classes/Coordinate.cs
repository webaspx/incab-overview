﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web;

namespace incabOverview_v1._1.Classes
{
    public partial class Coordinate : GeoCoordinate
    {
        public DateTime Time { get; set; }

        public Coordinate(Double Longitude, Double Latitude, Double Time)
        {
            this.Longitude = Longitude;

            this.Latitude = Latitude;

            this.Time = DateTime.Today.Date + TimeSpan.FromHours(Time).StripMS();
        }

        public Coordinate(Double Longitude, Double Latitude, DateTime Time, Double Speed)
        {
            this.Longitude = Longitude;

            this.Latitude = Latitude;

            this.Time = Time;

            this.Speed = Speed;
        }

        public Coordinate(Double Longitude, Double Latitude, DateTime Time)
        {
            this.Longitude = Longitude;

            this.Latitude = Latitude;

            this.Time = Time;
        }

        public Coordinate(Double Longitude, Double Latitude)
        {
            this.Longitude = Longitude;

            this.Latitude = Latitude;
        }

        public Coordinate (Object[] Source)
        {
            String[] RawData = Source.StringArray();

            DateTime TimeSent = RawData.TimeSent();

            if (RawData[1].Contains("$NoGPSFix$"))
            {
                String Coordinate = RawData[1].Split(':')[3];

                this.Longitude = Convert.ToDouble(Coordinate.Split(' ')[1]);

                this.Latitude = Convert.ToDouble(Coordinate.Split(' ')[0]);

                this.Time = TimeSent;
            }
            else if (RawData[1].Contains("GPS:"))
            {
                this.Longitude = RawData[1].Split(':')[5].ToDouble();

                this.Latitude = RawData[1].Split(':')[4].ToDouble();

                this.Time = TimeSent;

                this.Speed = 5;
            }
            else if (RawData[1].Contains("$Driver Check"))
            {
                String[] MessageData = RawData[1].Split('$').Last().Replace("Driver Check ").Split(' ');

                if (MessageData.Length > 2)
                {
                    MessageData = MessageData.Skip(1).ToArray();
                }

                this.Longitude = Convert.ToDouble(MessageData[1]);

                this.Latitude = Convert.ToDouble(MessageData[0]);

                this.Time = TimeSent;
            }
            else
            {
                String MsgData;

                if (RawData[1].Split(':').Last()[0] == ' ')
                {
                    MsgData = RawData[1].Split(':').Last().Substring(1);
                }
                else
                {
                    MsgData = RawData[1].Split(':').Last();
                }

                this.Longitude = MsgData.Split(' ')[0].ToDouble();

                this.Latitude = MsgData.Split(' ')[1].ToDouble();

                this.Time = TimeSent;
            }
        }
    }
}