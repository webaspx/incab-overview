﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incabOverview_v1._1.Classes
{
    public class WorkdaySection
    {
        public DateTime Start;

        public DateTime Finish;

        public String DriverID;

        public Round Round;

        public String Registration;

        public WorkdaySection()
        {
            
        }
    }
}