﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Webaspx.Database;

namespace incabOverview_v1._1.Classes
{
    public static class Database
    {
        /// <summary>
        /// Executes SQL query with no or multiple parameters
        /// </summary>
        /// <param name="Query">SQL query to be executed by the database</param>
        /// <param name="Client">Specifies which client to connect to</param>
        /// <param name="Parameters">SQL parameters</param>
        /// <returns>Datatable with results of the query</returns>
        public static DataTable Read<K,V>(String Query, String Client, Dictionary<K, V> Parameters = null)
        {
            using (DatabaseConnection Connection = new DatabaseConnection(Client, Catalog.Incab))
            {
                using (SqlCommand SQLCommand = new SqlCommand(Query))
                {
                    SQLCommand.CommandTimeout = 30;

                    if (Parameters != null)
                    {
                        foreach (KeyValuePair<K, V> Parameter in Parameters)
                        {
                            SQLCommand.Parameters.AddWithValue(Parameter.Key.ToString(), Parameter.Value);
                        }
                    }

                    try
                    {
                        return Connection.ExecuteDataTable(SQLCommand);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Executes SQL query with a single parameter
        /// </summary>
        /// <param name="Query">SQL query to be executed by the database</param>
        /// <param name="Client">Specifies which client to connect to</param>
        /// <param name="ParameterKey">Parameter Key</param>
        /// <param name="ParameterValue">Parameter Value to assign to the ID</param>
        /// <returns>Datatable with results of the query</returns>
        public static DataTable Read(String Query, String Client, String ParameterKey, String ParameterValue)
        {
            using (DatabaseConnection Connection = new DatabaseConnection(Client, Catalog.Incab))
            {
                using (SqlCommand SQLCommand = new SqlCommand(Query))
                {
                    SQLCommand.CommandTimeout = 30;

                    SQLCommand.Parameters.AddWithValue(ParameterKey, ParameterValue);

                    try
                    {
                        //return Connection.ExecuteQuery(SQLCommand);

                        // Change to Daniels library
                        return null;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Executes SQL query without parameters
        /// </summary>
        /// <param name="Query">SQL query to be executed by the database</param>
        /// <param name="Client">Specifies which client to connect to</param>
        /// <returns>Datatable with results of the query</returns>
        public static DataTable Read(String Query, String Client)
        {
            using (DatabaseConnection Connection = new DatabaseConnection(Client, Catalog.Incab))
            {
                using (SqlCommand SQLCommand = new SqlCommand(Query))
                {
                    SQLCommand.CommandTimeout = 30;

                    try
                    {
                        //return Connection.ExecuteQuery(SQLCommand);

                        // Change to Daniels library
                        return null;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
        }


        public static void Write(String Query, String Client, Dictionary<String, String> Parameters = null)
        {
            using (DatabaseConnection Connection = new DatabaseConnection(Client, Catalog.Incab))
            {
                using (SqlCommand Command = new SqlCommand(Query))
                {
                    if (Parameters != null)
                    {
                        foreach (KeyValuePair<String, String> Parameter in Parameters)
                        {
                            Command.Parameters.AddWithValue(Parameter.Key, Parameter.Value);
                        }
                    }

                    try
                    {
                        Command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
    }
}