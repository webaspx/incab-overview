﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incabOverview_v1._1.Classes
{
    public struct Issue
    {
        public String IssueCode;

        public String IssueText;

        public String Category;
    }

    public struct LoggedIssue
    {
        public String House;

        public String Street;

        public String UPRN;

        public String Round;

        public DateTime Sent;

        public String User;
    }
}