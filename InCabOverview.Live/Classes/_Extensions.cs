﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace incabOverview_v1._1
{
    public static class Extensions
    {
        public static String Replace(this String Str, String Replace)
        {
            return Str.Replace(Replace, "");
        }

        public static Boolean Contains(this DataRow DTR, String Find)
        {
            return DTR.ItemArray[1].ToString().Contains(Find);
        }

        public static Boolean TimeComparison(DateTime D1, DateTime D2)
        {
            return ((D1 - D2) > TimeSpan.FromMilliseconds(1000)) ? true : false;
        }

        public static TimeSpan StripMS(this TimeSpan time)
        {
            return new TimeSpan(time.Days, time.Hours, time.Minutes, time.Seconds);
        }

        public static String[] StringArray(this Object[] Before)
        {
            return Before.Select(x => x.ToString()).ToArray();
        }

        public static Double ToDouble(this String Before)
        {
            return Convert.ToDouble(Before);
        }

        public static String RemoveLast(this String Before, int TakeX)
        {
            return Before.Substring(0, Before.Length - TakeX);
        }

        public static DateTime TimeSent(this Object[] Before)
        {
            String[] Data = Before.StringArray();

                Int32 StringLength = Data[2].Length - 8;

                return DateTime.ParseExact(Data[2].RemoveLast(StringLength), "ddMMyyyy", CultureInfo.InvariantCulture) + TimeSpan.FromHours(Data[3].ToDouble());
        }

        public static void InitialiseDictionary<K, V>(ref Dictionary<K, V> Data, K Key) where V : new()
        {
            if (!Data.ContainsKey(Key))
            {
                Data.Add(Key, new V());
            }
        }
    }
}