﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace incabOverview_v1._1.Classes
{
    public struct Round
    {
        public Int32 Week;

        public String Service;

        public String Day;

        public String Crew;

        public DateTime ItineraryDate;

        public List<Vehicle> Vehicles;

        public Int32 StreetsCollected;
        public Int32 StreetsPartial;
        public Int32 TotalStreets;

        public Int32 BinsCollected;
        public Int32 BinsPartial;
        public Int32 TotalBins;

        public Round(String[] Data)
        {
            this.Day = Data[3];

            this.Service = Data[2];

            this.Crew = Data[5];

            this.Week = Convert.ToInt32(Data[4].Replace("Week"));

            // Wrong
            this.ItineraryDate = DateTime.Today;

            // Wrong
            this.Vehicles = new List<Vehicle>();

            StreetsCollected = 20;
            StreetsPartial = 10;
            TotalStreets = 100;

            BinsCollected = 20;
            BinsPartial = 10;
            TotalBins = 100;
            
        }
    }
}