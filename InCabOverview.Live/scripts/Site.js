﻿if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
}

function createRoundCompletionChart(container, data) {
    var labels = [];
    var series = [];
    var colors = [];

    for (var i = 0, len = data.length; i < len; i++) {
        var label = "{0} {1}".format(data[i].Round.Service, data[i].Round.Crew);
        var completion = (data[i].Completion < 1) ? 2 : data[i].Completion;
        var color = "";

        if (completion <= 33) {
            color = "#e80c4d";
        } else if (completion > 33 && completion <= 66) {
            color = "#f2aa00";
        }
        else {
            color = "#52d29a";
        }

        labels.push(label);
        series.push(completion);
        colors.push(color);
    }



    var canvas = document.createElement('canvas');
    $(container).append(canvas);

    RoundCompletionChart = new Chart(canvas.getContext('2d'), {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                backgroundColor: colors,
                data: series,
            }],
        },
        options: {
            legend: {
                display: false,
            },
            maintainAspectRatio: false,
            responsive: true,
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: roundCompletionChartToolTips
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        drawBorder: false,
                        display: false,
                        tickMarkLength: 50,
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    },
                    gridLines: {
                        drawBorder: false,
                    }
                }]
            }
        }
    });
}

window.randomScalingFactor = function () {
    return Math.floor((Math.random() * 100) + 1);
};


function RoundStats(id) {
    var canvas = document.createElement('canvas');
    $(id).append(canvas);

    var RoundCompletionChart = new Chart(canvas.getContext('2d'), {
        type: 'horizontalBar',
        data: {
            labels: ["Bins", "Streets"],
            datasets: [{
                data: [727, 589],
                backgroundColor: "#52d29a",
            }, {
                data: [238, 553],
                backgroundColor: "#e80c4d",
            }]
        },
        options: {
            legend: {
                display: false,
            },
            tooltips: {
                enabled: false
            },
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        display: false,
                        tickMarkLength: 50,
                    },
                    
                }],
                yAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    },
                    gridLines: {
                        drawBorder: false,
                    },
                    
                }]
            }
        }
    });
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function RoundStatsSingle(id) {
    var canvas = document.createElement('canvas');
    $(id).append(canvas);

    var done = getRandomInt(0, 1200);
    var todo = getRandomInt(done, done + 1000);

    var RoundCompletionChart = new Chart(canvas.getContext('2d'), {
        type: 'horizontalBar',
        data: {
            labels: ["Bins"],
            datasets: [{
                data: [(done/todo)*100],
                backgroundColor: "#52d29a",
            }, {
                data: [100 - ((done / todo) * 100)],
                backgroundColor: "#e80c4d",
            }]
        },
        options: {
            legend: {
                display: false,
            },
            tooltips: {
                enabled: false
            },
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    },
                    stacked: true
                }],
                yAxes: [{
                    ticks: {
                        display: false,
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    },
                    stacked: true
                }]
            },
        }
    });
}