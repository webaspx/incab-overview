﻿var webSocketStatusSpan = document.getElementById("webSocketStatusSpan");
var webSocketReceiveDataSpan = document.getElementById("webSocketReceiveDataSpan");
var nameTextBox = document.getElementById("nameTextBox");

var webSocket;

//The address of our HTTP-handler 
var handlerUrl = "ws://localhost:50448/WebSocketHandler.ashx";

var reconnectionAttempts = 0;
var maxreconnectionAttempts = 5;

function SendData() {
    //Send data if WebSocket is opened. 
    if (webSocket.OPEN && webSocket.readyState == 1)
        webSocket.send(nameTextBox.value);

    //If WebSocket is closed, show message. 
    if (webSocket.readyState == 2 || webSocket.readyState == 3) {
        webSocketStatusSpan.innerText = "WebSocket closed, the data can't be sent.";

        $('#loader-title').text("Connection closed");
        $('#loader-subtitle').text("We'll attempt to reconnect you ({0}/{1})".format(reconnectionAttempts, maxreconnectionAttempts));

        $('.overlay').fadeIn();
    }
}

function CloseWebSocket() {
    webSocket.close();
}

function InitWebSocket() {

    //If the WebSocket object isn't initialized, we initialize it. 
    if (webSocket == undefined) {
        webSocket = new WebSocket(handlerUrl);

        //Open connection  handler. 
        webSocket.onopen = function () {
            $('#loader-title').text("Connection established");

            reconnectionAttempts = 0;

            webSocket.send("Announce user");
            $.ajax({
                type: "POST",
                url: 'incabOverview.aspx/WSTest',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (e) {
                    console.log("Success");
                },
                failure: function (e) {
                    $('#loader-title').text("Connection Failed");
                    $('#loader-subtitle').text("Error: " + e.message + "\r\nIf this error keeps occuring, please contact WMOperations@webaspx.com for support.");
                }
            });

            $('.overlay').fadeOut();
        };

        //Message data handler. 
        webSocket.onmessage = function (e) {
            var data = JSON.parse(e.data);

            if (data.InCabLiveUpdate) {
                $('#TipVisitsText').text(formatData(data, "TipVisits"));
                $('#BinNotOutText').text(formatData(data, "BinNotOut"));
                $('#DamagedBinsText').text(formatData(data, "DamagedBins"));
                $('#ContaminationText').text(formatData(data, "Contamination"));
                $('#IssuesText').text(formatData(data, "Issues"));
            }

            addToTable('#IssuesTableFeed', data.IssuesTable, ["Issue", "StreetName", "DateTimeYYYYMMDDhhmmss", "Round"]);

            $('#ServiceProgressionCard').empty();

            for (var i = 0; i < data.Rounds.length; i++) {
                createRoundChart(data.Rounds[i]);
            }

            LiveData.push(data);
        };

        //Close event handler. 
        webSocket.onclose = function () {
            reconnectWebSocket("Connection closed");
        };

        //Error event handler. 
        webSocket.onerror = function (e) {
            reconnectWebSocket("An error occured", e);
        }
    }
}

function reconnectWebSocket(title, e) {
    $('#loader-title').text(title);

    var subtitle = "We'll attempt to reconnect you ({0}/{1})".format(reconnectionAttempts, maxreconnectionAttempts);

    if (e != undefined && e.message != undefined) {
        subtitle = subtitle + "<br>{0}".format(e.message);
    }

    $('#loader-subtitle').text(subtitle);

    $('.overlay').fadeIn();

    (function myLoop(i) {
        setTimeout(function () {
            $('#loader-subtitle').text("We'll attempt to reconnect you ({0}/{1})".format(maxreconnectionAttempts-i, maxreconnectionAttempts));

            InitWebSocket();        //  your code here  

            console.log(i);

            if (--i && (webSocket.readyState == 2 || webSocket.readyState == 3)) {      //  decrement i and call myLoop again if i > 0
                myLoop(i);
            }
        }, 5000)
    })(maxreconnectionAttempts);
}


function formatData(data, item) {
    if (LiveData.length != 0) {
        var PreviousData = LiveData[LiveData.length - 1];

        var itemIncrease = (data[item] - PreviousData[item])

        CardIncrease($('#{0}Text'.format(item)).parent()[0], itemIncrease);

        //$('#{0}IncreaseText'.format(item)).css('color', (itemIncrease == 0) ? '#000000' : '');
        //
        //$('#{0}IncreaseText'.format(item)).text("+ {0}".format(itemIncrease));
    }

    return data[item];
}