﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using incabOverview_v1._1.Classes;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Net.WebSockets;
using Newtonsoft.Json.Linq;
using System.Web.Security;
using incabOverview_v1._1.Account;

namespace incabOverview_v1._1
{
    public partial class incabOverview : System.Web.UI.Page
    {
        static Dictionary<String, List<Mes3sage>> StoredData = new Dictionary<String, List<Mes3sage>>();

        static Dictionary<String, Int32> PageHitCounter = new Dictionary<String, Int32>();

        protected String Client;

        protected DateTime RequestedDate;

        private static Boolean isActive = false;

        private static TimeSpan LastIssueRecorded = DateTime.Now.TimeOfDay;

        [WebMethod]
        public static String WSTest()
        {
            if (!isActive)
            {
                isActive = true;

                Int32 i = 0;

                Int32 DriverChecksPass = 0;
                Int32 DriverChecksFail = 0;
                Int32 TipVisits = 0;
                Int32 BinNotOut = 0;
                Int32 DamagedBins = 0;
                Int32 Contamination = 0;
                Int32 Issues = 0;

                Round[] Rounds = new Round[] {
                    new Round() {
                        Service = "Refuse",
                        BinsCollected = 10,
                        TotalBins = 1420,
                        StreetsCollected = 9,
                        TotalStreets = 72
                    },
                    new Round() {
                        Service = "Recycling",
                        BinsCollected = 15,
                        TotalBins = 1200,
                        StreetsCollected = 3,
                        TotalStreets = 68
                    }
                };

                while (_Global.WebSocketClients.Count != 0)
                {
                    var id = Guid.NewGuid();

                    Random random = new Random();

                    DriverChecksPass = DriverChecksPass + random.Next(0, 10);
                    DriverChecksFail = DriverChecksFail + random.Next(0, 10);
                    TipVisits = TipVisits + random.Next(0, 10);
                    BinNotOut = BinNotOut + random.Next(0, 10);
                    DamagedBins = DamagedBins + random.Next(0, 10);
                    Contamination = Contamination + random.Next(0, 10);
                    Issues = Issues + random.Next(0, 10);

                    Rounds[0].StreetsCollected = Rounds[0].StreetsCollected + random.Next(0, 3);
                    Rounds[1].StreetsCollected = Rounds[1].StreetsCollected + random.Next(0, 3);

                    Rounds[0].BinsCollected = Rounds[0].BinsCollected + random.Next(10, 130);
                    Rounds[1].BinsCollected = Rounds[1].BinsCollected + random.Next(0, 13);

                    if (Rounds[0].BinsCollected > Rounds[0].TotalBins)
                    {
                        Rounds[0].BinsCollected = Rounds[0].TotalBins;
                    }


                    // Gets newest issues from 
                    DataTable IssuesTable = GetIssues("Scarborough");

                    String JSON = JObject.FromObject(new
                    {
                        InCabLiveUpdate = true,
                        DriverChecksPass = DriverChecksPass,
                        DriverChecksFail = DriverChecksFail,
                        TipVisits = TipVisits,
                        BinNotOut = BinNotOut,
                        DamagedBins = DamagedBins,
                        Contamination = Contamination,
                        Issues = Issues,
                        IssuesTable = IssuesTable,
                        Rounds = Rounds
                    }).ToString();

                    Byte[] data = System.Text.Encoding.UTF8.GetBytes(JSON);


                    foreach (WebSocket WebSocketClient in _Global.WebSocketClients)
                    {
                        var cancellationToken = new CancellationToken();

                        WebSocketClient.SendAsync(new ArraySegment<byte>(data), WebSocketMessageType.Text, true, cancellationToken);
                    }

                    i++;

                    Thread.Sleep(new Random().Next(2, 10) * 1000);

                    //Since = DateTime.Now.TimeOfDay;
                }

                isActive = false;
            }

            return "";
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            DigitalDepotLogin(sender, e);

            //DateTime RequestedDate = DateTime.Today;
            //
            //GetInCabData(RequestedDate);
            //
            //List<Mes3sage> CurrentClientData = StoredData[Client];
        }


        private void DigitalDepotLogin(object sender, EventArgs e)
        {
            String Client = _DetailsFromURL.GetClient(Request.Url.AbsoluteUri);

            String Error = "";

            Boolean isPersistent = false;

            // Check if they've already logged in
            String authCookie = Request.QueryString["var1"];

            if (authCookie != null)
            {
                String Email = Request.QueryString["var2"];

                if (Authentication.VerifyCookie(Client, authCookie, Email))
                {
                    // Create the cookie.
                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, authCookie));

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                                                                                     Email,
                                                                                     DateTime.UtcNow,
                                                                                     DateTime.UtcNow.AddMinutes(360),
                                                                                     isPersistent,
                                                                                     authCookie,
                                                                                     FormsAuthentication.FormsCookiePath);

                    // Encrypt the ticket.
                    String encryptedTicket = FormsAuthentication.Encrypt(ticket);

                    // Create the cookie.
                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

                    // Redirect back to original URL.
                    Response.Redirect(FormsAuthentication.GetRedirectUrl(Email, isPersistent));
                }
            }

            //Get them to login
            if (IsPostBack)
            {
                String Email = Request["user"];

                String Password = Request["pass"];

                String Action = Request["action"];

                String Product = _DetailsFromURL.GetProduct(Request.Url.AbsoluteUri);


                if (Authentication.AuthenticateUser(Client, Email, Password, out Error))
                {
                    if (Authorisation.AccessToProductAllowed(Client, Email, Product, ref Error))
                    {
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                                                                                         Email,
                                                                                         DateTime.UtcNow,
                                                                                         DateTime.UtcNow.AddMinutes(360),
                                                                                         isPersistent,
                                                                                         "~#Direct#~",
                                                                                         FormsAuthentication.FormsCookiePath);

                        // Encrypt the ticket.
                        String encryptedTicket = FormsAuthentication.Encrypt(ticket);

                        //Store the cookie in the DDUser table.
                        Authentication.RecordCookie(Client, encryptedTicket, Email);

                        // Create the cookie.
                        Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

                        // Redirect back to original URL.
                        Response.Redirect(FormsAuthentication.GetRedirectUrl(Email, isPersistent));
                    }
                    else
                    {
                        invalidLogin.Text = Error;
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();

                    invalidLogin.Text = Error;
                }
            }
        }

        #region Debug data
        
        DateTime Lastupdate = DateTime.Today;

        static TimeSpan FullTime;

        static TimeSpan PartialTime;

        static Int32 FullRows;

        static Int32 PartialRows;

        #endregion
        
        private void BreakdownByRound(List<Message> CurrentClientData)
        {
            // Step 1: 
            

        }

        private void BreakdownByVehicle()
        {

        }

        private void BreakdownByDriver()
        {

        }

        private void GetInCabData(DateTime RequestedDate)
        {
            ///* */ Lastupdate = DateTime.Now;
            //
            ///* */ Update.Text = Lastupdate.ToString();
            //
            //if (!StoredData.ContainsKey(Client))
            //{
            //    Extensions.InitialiseDictionary(ref StoredData, Client);
            //
            //    Extensions.InitialiseDictionary(ref PageHitCounter, Client);
            //}
            //
            //String Date = DateTime.Today.Date.ToString().Substring(0, 10).Replace("/");
            //
            //if (StoredData[Client].Count == 0)
            //{
            //    /* */ Stopwatch SW = new Stopwatch();
            //
            //    /* */ SW.Start();
            //
            //    DataTable DatabaseMessages = GetMessages(Client, RequestedDate);
            //
            //    List<Mes3sage> AllMessages = ParseMessages(DatabaseMessages, GetIssueTypes(Client));
            //
            //    StoredData[Client] = AllMessages;
            //
            //    /* */ SW.Stop();
            //
            //    /* */ FullTime = SW.Elapsed;
            //
            //    /* */ FullRows = AllMessages.Count;
            //}
            //else
            //{
            //    /* */ Stopwatch SW = new Stopwatch();
            //
            //    /* */ SW.Start();
            //
            //    Decimal LatestTime = Convert.ToDecimal(TimeSpan.Parse(StoredData[Client].First().Sent.TimeOfDay.ToString()).TotalHours);
            //
            //    DataTable NewMessages = GetMessages(Client, RequestedDate, LatestTime);
            //
            //    List<Mes3sage> RecentMessages = ParseMessages(NewMessages, GetIssueTypes(Client));
            //
            //    StoredData[Client].AddRange(RecentMessages);
            //
            //    /* */ SW.Stop();
            //
            //    /* */ PartialTime = SW.Elapsed;
            //
            //    /* */ PartialRows = RecentMessages.Count;
            //}
            //
            ///* */ Partial.Text = "Incremental Query: " + PartialTime + "<br/>Rows: " + PartialRows;
            //
            ///* */ Full.Text = "Full Query: " + FullTime + "<br/>Rows: " + FullRows;
        }

        /// <summary>
        /// Queries the database and returns all messages
        /// </summary>
        /// <param name="Client"></param>
        /// <param name="Time">Only get messages after this time</param>
        /// <returns>DataTable of messages</returns>
        private static DataTable GetMessages(String Client, DateTime DateTime, Decimal Time = 0)
        {
            Dictionary<String, DateTime> Parameters = new Dictionary<String, DateTime>() {
                {"@DateTime", DateTime},
                //{"@Time", Time.ToString()}
            };

            return Database.Read(@"SELECT 
                                       * 
                                   FROM 
                                       [MessagesSplit]
                                   WHERE 
                                       [DateTimeYYYYMMDDhhmmss] 
                                           LIKE 
                                               CONVERT(VARCHAR(255), @Date) 
                                           AND 
                                               [TIME1] > @Time 
                                   ORDER BY 
                                       [Time1] 
                                            DESC", Client, Parameters);
        }

        /// <summary>
        /// Gets a list of issues by type (contamination, hazard, issue, other)
        /// </summary>
        /// <param name="Client"></param>
        /// <returns>List of issue objects which contains: issueCode, issueText, issueType</returns>
        private static List<Issue> GetIssueTypes(String Client)
        {
            List<Issue> Issues = new List<Issue>();

            DataTable Result = Database.Read("SELECT DISTINCT [issueCode], [issueText], [issueType] FROM [InCabIssues]", Client);

            foreach (DataRow Row in Result.Rows)
            {
                String[] Data = Row.ItemArray.StringArray();

                Issues.Add(new Issue() {
                    Category = Data[2],
                    IssueCode = Data[0],
                    IssueText = Data[1]
                });
            }

            return Issues;
        }

        /// <summary>
        /// Parses messages from the databases into usable structs
        /// </summary>
        /// <param name="Data">DataTable of messages from the database</param>
        /// <param name="IssueTypes">Issue found in the incabIssues table</param>
        /// <returns>List of message objects parsed from the given datatable</returns>
        private List<Mes3sage> ParseMessages(DataTable Data, List<Issue> IssueTypes)
        {
            List<Mes3sage> AllMessages = new List<Mes3sage>();

            foreach (DataRow Row in Data.Rows)
            {
                AllMessages.Add(new Mes3sage(Row, IssueTypes));
            }

            return AllMessages;
        }

        /// <summary>
        /// Uses Keith's session system to check whether the user is valid or not.
        /// </summary>
        /// <param name="SessionID">SessionID can be found in the request string.</param>
        /// <returns>Boolean stating if the user is authorised to access the resource</returns>
        public Boolean ValidateUser(String SessionID)
        {
            if (SessionID != null)
            {
                DataTable Result = Database.Read("SELECT COUNT(*) FROM [Users] WHERE [Session] = @SessionID", Client, "@SessionID", SessionID);

                Int32 ValidUsers = Convert.ToInt32(Result.Rows[0].ItemArray[0].ToString());

                return (ValidUsers > 0);
            }
            else
            {
                return false;
            }
        }


        private static DataTable GetIssues(String Client)
        {
            String Query = @"SELECT
                                 [DateTimeYYYYMMDDhhmmss],
                                 [User1],
                                 [Lat],
                                 [Lon],
                                 [Registration],
                                 [DriverID],
                                 [CrewID],
                                 [RoundCrew],
                                 [RoundDay],
                                 [RoundWeek],
                                 [RoundService],
                                 [UPRN],
                                 [Issue],
                                 [HouseName],
                                 [StreetName]
                             FROM
                                 [MessagesSplit]
                             WHERE
                                     [MessageType] = @Issue
                                 AND
                                     [Date1YYYYMMDD] LIKE @Date
                                 AND
                                     [Time1Analogue] >= @Since";

            Dictionary<String, String> Parameters = new Dictionary<String, String>() {
                {"Issue", "Issue"},
                {"Date", DateTime.Today.ToString("yyyyMMdd") + "%"},
                {"Since", LastIssueRecorded.ToString("hh':'mm':'ss")}
            };

            DataTable Result = Database.Read(Query, Client, Parameters);

            if (Result != null & Result.Rows.Count > 0)
            {
                LastIssueRecorded = DateTime.Now.TimeOfDay;
            }

            return Result;
        }
    }
}