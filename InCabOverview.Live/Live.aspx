﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Live.aspx.cs" Inherits="incabOverview_v1._1.Live" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InCab Live</title>
    <script type="text/javascript" src="/scripts/jquery-live/jquery-latest.js"></script>
    <script src="scripts/chart.min.js"></script>
    <script src="scripts/charts.js"></script>
    <link href="css/Live.css" rel="stylesheet" />
    <script src="scripts/websocket.js"></script>
    <script type="text/javascript">
        var LiveData = [];

        $(document).ready(function () {
            setTimeout(function () { 
                InitWebSocket();
            }, 1000);
        });

        if (!String.prototype.format) {
            String.prototype.format = function () {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                      ? args[number]
                      : match
                    ;
                });
            };
        }

        function toTitleCase(str) {
            return (str == undefined) ? str : str.replace(/\w\S*/g,
                function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                }
            );
        }

        function addToTable(id, data, fields) {
            if (data != null) {
                $(id).find('tr').removeClass('new');

                for (var i = 0; i < data.length; i++) {
                    var row = document.createElement('tr');

                    for (var j = 0; j < fields.length; j++) {
                        var cell = document.createElement('td');

                        var text = toTitleCase(data[i][fields[j]]);

                        if (text != undefined) {
                            text = toTitleCase(text);
                        }

                        if (fields[j] == "Round") {
                            text = "{0} {1}".format(data[i]["RoundService"], data[i]["RoundDay"]);
                        } else if (fields[j] == "DateTimeYYYYMMDDhhmmss") {
                            text = text.substring(11, 19);
                        }

                        $(cell).text(text);

                        $(row).append(cell);
                    }

                    $(row).addClass('new');

                    if (Math.abs($(id).find('tr').length % 2) == 1) {
                        $(row).addClass('odd');
                    }

                    $(id).prepend(row).fadeIn('slow');
                }
            }
        }

        function CardIncrease(id, text) {
            var increase = $('<h2/>', {
                'text': (text == "0") ? text : "+ " + text,
                'style': 'float: right; padding: 0px 5px; font-size: 20px; font-weight: 500; margin: 0px; color: ' + ((text == "0") ? '#000000' : '#52d29a') + '; word-spacing: normal; transition: 5s;'
            });

            $(id).append(increase);

            setTimeout(function () {
                $(increase).css('transform', 'translate3d(0px, -1000%, 0px)');
                $(increase).fadeOut(1500, function () {
                    $(increase).remove();
                });
            }, 2500);
        }


        function createRoundChart(data) {
            var flexrow = $('<div/>', {
                'class': 'flex-row'
            });

            var serviceNameCol = $('<div/>', {
                'class': 'flex-column',
                'style': '/*background-color: hotpink*/'
            });

            $(serviceNameCol).append($('<h3/>', {
                'text': data.Service
            }));

            var serviceChartCol = $('<div/>', {
                'class': 'flex-column',
                'style': 'flex: 60%;',
            });

            $(flexrow).append(serviceNameCol);
            $(flexrow).append(serviceChartCol);


            var table = $('<table/>', {
                id: '{0}Progression'.format(data.Service),
                'class': 'roundBarTable'
            });

            var BinIcon = '<svg class="card-icon roundStatsIconBin black" style="width: 25px" viewBox="0 0 512 512"><path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z"></path></svg>';
            var StreetIcon = '<svg class="card-icon black" style="width: 25px" viewBox="0 0 576 512"><path fill="currentColor" d="M279.47 192h17.06c5.81 0 10.35-6.17 9.73-13.26l-3.75-43.58c-.35-4.07-3.15-7.16-6.49-7.16h-16.05c-3.34 0-6.14 3.09-6.49 7.16l-3.75 43.58c-.61 7.09 3.93 13.26 9.74 13.26zm-6.02 112h29.1c7.75 0 13.79-8.23 12.98-17.68l-4.13-48c-.7-8.14-6.3-14.32-12.98-14.32h-20.83c-6.68 0-12.28 6.18-12.98 14.32l-4.13 48c-.82 9.45 5.22 17.68 12.97 17.68zm10.66-208h7.79c3.88 0 6.9-4.12 6.49-8.84l-1.38-16c-.35-4.07-3.15-7.16-6.49-7.16h-5.03c-3.34 0-6.14 3.09-6.49 7.16l-1.38 16c-.41 4.72 2.61 8.84 6.49 8.84zm23.95 240h-40.12c-6.68 0-12.28 6.18-12.98 14.32l-6.89 80c-.81 9.45 5.23 17.68 12.98 17.68h53.89c7.75 0 13.79-8.23 12.98-17.68l-6.89-80c-.69-8.14-6.29-14.32-12.97-14.32zM157.35 64a7.99 7.99 0 0 0-7.38 4.92L1.25 425.85C-3.14 436.38 4.6 448 16.02 448h28c7.11 0 13.37-4.69 15.36-11.52L165.03 74.24c1.49-5.12-2.35-10.24-7.68-10.24zm417.4 361.85L426.04 68.92a8 8 0 0 0-7.38-4.92c-5.33 0-9.17 5.12-7.68 10.24l105.65 362.24A15.996 15.996 0 0 0 531.99 448h28c11.41 0 19.16-11.62 14.76-22.15z"></path></svg>';

            var BinChart = '<div id="{0}BinChart" style="max-height: 50px"></div>'.format(data.Service);
            var StreetChart = '<div id="{0}StreetChart" style="max-height: 50px"></div>'.format(data.Service);

            $(table).append('<tbody><tr><td>{0}<td/><td>{1}<td/></tr><tr><td>{2}<td/><td>{3}<td/></tr></tbody>'.format(BinIcon, BinChart, StreetIcon, StreetChart));

            $(serviceChartCol).append(table);

            $('#ServiceProgressionCard').append(flexrow);

            var BinIncrease = 0;
            var StreetIncrease = 0;

            if (LiveData.length != 0) {
                var PreviousData = LiveData[LiveData.length - 1];

                var index = 0;

                for (var i = 0; i < PreviousData.Rounds.length; i++) {
                    if (PreviousData.Rounds[i].Service == data.Service) {
                        index = i;

                        break;
                    }
                }

                BinIncrease = (data.BinsCollected - PreviousData.Rounds[index].BinsCollected)
                StreetIncrease = (data.StreetsCollected - PreviousData.Rounds[index].StreetsCollected)
            }

            ServiceCompletion(("#{0}BinChart".format(data.Service)), data.BinsCollected, data.TotalBins, BinIncrease);
            ServiceCompletion(("#{0}StreetChart".format(data.Service)), data.StreetsCollected, data.TotalStreets, StreetIncrease);
        }


        function ServiceCompletion(id, collected, uncollected, collectedIncrease) {
            var canvas = document.createElement('canvas');
            $(id).append(canvas);

            var PercentageCompleteTotal = (((collected / uncollected) * 100) - ((collectedIncrease / uncollected) * 100));
            var PercentageCompleteIncrease = ((collectedIncrease / uncollected) * 100);
            var PercentageIncomplete = (100 - ((collected / uncollected) * 100));
        
            var RoundCompletionChart = new Chart(canvas.getContext('2d'), {
                type: 'horizontalBar',
                data: {
                    labels: ["Bins"],
                    datasets: [{
                        data: [PercentageCompleteTotal],//[(((collected - collectedIncrease) / uncollected) * 100)],
                        backgroundColor: "#52d29a",
                    }, {
                        data: [PercentageCompleteIncrease], //[(collectedIncrease / uncollected) * 100],
                        backgroundColor: "#43ab7d"
                    },{
                        data: [PercentageIncomplete], //[100 - (((collected - collectedIncrease) / uncollected) * 100)],
                        backgroundColor: "#e80c4d",
                    }]
                },
                options: {
                    animation: {
                        duration: 0,
                        /*
                        onComplete: function () {
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'left';
                            ctx.textBaseline = 'center';

                            this.data.datasets.forEach(function (dataset) {
                                console.log(dataset);
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                        scale_max = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._yScale.maxHeight;
                                    left = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.left;
                                    offset = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._xScale.longestLabelWidth;
                                    ctx.fillStyle = '#444';
                                    var y_pos = model.y - 5;
                                    var label = model.label;
                                    // Make sure data value does not get overflown and hidden
                                    // when the bar's value is too close to max value of scale
                                    // Note: The y value is reverse, it counts from top down
                                    if ((scale_max - model.y) / scale_max >= 0.93)
                                        y_pos = model.y + 20;
                                    // ctx.fillText(dataset.data[i], model.x, y_pos);
                                    ctx.fillText(label, left + 10, model.y + 8);
                                }
                            });
                        }*/
                    },
                    legend: {
                        display: false,
                    },
                    tooltips: {
                        enabled: false
                    },
                    maintainAspectRatio: false,
                    responsive: true,
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false,
                            },
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            },
                            stacked: true
                        }],
                        yAxes: [{
                            ticks: {
                                display: false,
                            },
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            },
                            stacked: true
                        }]
                    },
                    layout: {
                        padding: {
                            bottom: -45,
                        }
                    },
                }
            });
        }
    </script>
</head>
<body>
    <div class="overlay">
        <div class="flex-center white">
            <div class="loader">
                <div style="background-color: rgb(239, 103, 36);"></div>
                <div style="background-color: #1e3663;"></div>
                <div style="background-color: #1e3663;"></div>
                <div style="background-color: #1e3663;"></div>
            </div>
            <h3 id="loader-title">
                Connecting to server
            </h3>
            <h4 id="loader-subtitle" style="color: #b5b5b5">
                10/10
            </h4>
        </div>
    </div>
    <asp:Label runat="server" ID="invalidLogin"></asp:Label>
    <form style="display: none" id="form1" runat="server">
        <div>
            <span id="webSocketStatusSpan"></span>
            <br />
            <span id="webSocketReceiveDataSpan"></span>
            <br />
            <span>Please enter a string</span>
            <br />
            <input id="nameTextBox" type="text" value="" />
            <input type="button" value="Send data" onclick="SendData();" />
            <input type="button" value="Close WebSocket" onclick="CloseWebSocket();" />
        </div>
    </form>

    <div class="flex-row" style="height: 100%;">
        <div class="flex-column" style="flex: 90%">
            <!-- 188 = 172 (card height + margins) -->
            <div id="top-row" class="flex-row" style="max-height: 188px; flex-wrap: wrap-reverse">
                <div class="card card-min">
                    <h3>Driver Checks</h3>
                    <div>
                        <table id="card-table">
                            <tr>
                                <td>
                                    <svg class="card-icon green" style="width: 45px" viewBox="0 0 448 512">
                                        <path fill="currentColor" d="M413.505 91.951L133.49 371.966l-98.995-98.995c-4.686-4.686-12.284-4.686-16.971 0L6.211 284.284c-4.686 4.686-4.686 12.284 0 16.971l118.794 118.794c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-11.314-11.314c-4.686-4.686-12.284-4.686-16.97 0z"></path>
                                    </svg>
                                </td>
                                <td>
                                    <svg class="card-icon red" style="width: 45px" viewBox="0 0 448 512">
                                        <path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z"></path>
                                    </svg>
                                </td>
                            </tr>
                            <tr>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="card card-min">
                    <h3>Tip Visits</h3>
                    <div>
                        <svg class="card-icon green" style="width: 45px" viewBox="0 0 512 512">
                            <path fill="currentColor" d="M201.728 62.049l-43.385 69.459c-3.511 5.622-10.916 7.332-16.537 3.819l-6.781-4.237c-5.619-3.511-7.329-10.912-3.819-16.533l43.387-69.48c37.575-60.12 125.263-60.084 162.816 0l46.217 74.015 12.037-52.14c1.491-6.458 7.934-10.484 14.392-8.993l7.794 1.799c6.458 1.491 10.484 7.934 8.993 14.392l-21.633 93.702c-1.491 6.458-7.934 10.484-14.392 8.993l-93.702-21.633c-6.458-1.491-10.484-7.934-8.993-14.392l1.799-7.794c1.491-6.458 7.934-10.484 14.392-8.993l52.202 12.052-46.251-74.047c-25.002-40.006-83.467-40.099-108.536.011zm295.56 239.071l-52.939-84.78c-3.511-5.623-10.916-7.334-16.538-3.821l-6.767 4.228c-5.62 3.512-7.329 10.913-3.819 16.534l52.966 84.798c26.605 42.568-4.054 97.92-54.272 97.92H310.627l37.858-37.858c4.686-4.686 4.686-12.284 0-16.97l-5.656-5.656c-4.686-4.686-12.284-4.686-16.97 0l-68 68c-4.686 4.686-4.686 12.284 0 16.971l68 68c4.686 4.686 12.284 4.686 16.97 0l5.656-5.657c4.686-4.686 4.686-12.284 0-16.971L310.627 448H415.88c75.274 0 121.335-82.997 81.408-146.88zM41.813 318.069l55.803-89.339 12.044 52.166c1.491 6.458 7.934 10.484 14.392 8.993l7.794-1.799c6.458-1.491 10.484-7.934 8.993-14.392l-21.633-93.702c-1.491-6.458-7.934-10.484-14.392-8.993l-93.702 21.633c-6.458 1.491-10.484 7.934-8.993 14.392l1.799 7.794c1.491 6.458 7.934 10.484 14.392 8.993l52.193-12.05-55.796 89.355C-25.188 364.952 20.781 448 96.115 448H196c6.627 0 12-5.373 12-12v-8c0-6.627-5.373-12-12-12H96.078c-50.199 0-80.887-55.335-54.265-97.931z"></path>
                        </svg>
                    </div>
                    <div id="TipVisitsText" class="tileDataDiv">
                        -
                    </div>
                </div>
                <div class="card card-min">
                    <h3>Bin Not Out</h3>
                    <div>
                        <div class="card-icon-parent">
                            <svg class="card-icon green" style="width: 45px" viewBox="0 0 512 512">
                                <path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z"></path>
                            </svg>
                            <svg class="card-icon card-icon-child red" style="width: 45px" viewBox="0 0 512 512">
                                <path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="BinNotOutText" class="tileDataDiv">
                        -
                    </div>
                </div>
                <div class="card card-min">
                    <h3>Damaged Bins</h3>
                    <div>
                        <div class="card-icon-parent">
                            <svg class="card-icon green" style="width: 45px" viewBox="0 0 512 512">
                                <path fill="currentColor" d="M296 432h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zm-160 0h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8zM440 64H336l-33.6-44.8A48 48 0 0 0 264 0h-80a48 48 0 0 0-38.4 19.2L112 64H8a8 8 0 0 0-8 8v16a8 8 0 0 0 8 8h24v368a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V96h24a8 8 0 0 0 8-8V72a8 8 0 0 0-8-8zM171.2 38.4A16.1 16.1 0 0 1 184 32h80a16.1 16.1 0 0 1 12.8 6.4L296 64H152zM384 464a16 16 0 0 1-16 16H80a16 16 0 0 1-16-16V96h320zm-168-32h16a8 8 0 0 0 8-8V152a8 8 0 0 0-8-8h-16a8 8 0 0 0-8 8v272a8 8 0 0 0 8 8z"></path>
                            </svg>
                            <svg class="card-icon card-icon-child black" style="width: 24px; right: 45%; top: 7px; transform: rotateZ(20deg);" viewBox="0 0 320 512">
                                <path fill="currentColor" d="M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.3 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36zM140.3 436.9l33.5-141.6 9.3-39.4h-150L63 32h125.9l-38.7 118-13.8 42h145.7L140.3 436.9z"></path>
                            </svg>
                        </div>
                    </div>
                    <div id="DamagedBinsText" class="tileDataDiv">
                        -
                    </div>
                </div>
                <div class="card card-min">
                    <h3>Contamination</h3>
                    <div>
                        <svg class="card-icon green" style="width: 45px" viewBox="0 0 496 512">
                            <path fill="currentColor" d="M247.9 320c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm0-96c17.6 0 32 14.4 32 32s-14.4 32-32 32-32-14.4-32-32 14.4-32 32-32zm51.2 113.2c-14.9 9.2-32.4 14.8-51.2 14.8s-36.3-5.6-51.2-14.8L133.8 437c-10.2 16.2-4.2 38.2 13.5 45.9C178 496.4 212 504 247.9 504s69.9-7.6 100.7-21.1c17.6-7.7 23.7-29.7 13.5-45.9l-63-99.8zM247.9 472c-30.4 0-59.8-6.2-86.8-18.1l47.9-76c12.6 4 25.7 6.1 38.8 6.1 13.3 0 26.4-2.1 39-6.1l48.6 75.8c-27.7 12.1-57.1 18.3-87.5 18.3zM32.5 256H151c0-34.3 18.3-64.2 45.7-81.2l-62.8-99.6c-6.2-9.8-17-15.3-27.8-15.3-7.1 0-14.2 2.3-20.1 7.3-45.2 38-76.7 91.7-85.7 152.5-2.9 19.2 12.6 36.3 32.2 36.3zm73.9-163.8l47.9 75.8c-15.2 15.7-26 35-31.5 56H32.5l-.3.4c7.6-51.6 34.2-98.7 74.2-132.2zM344.8 256h118.5c19.6 0 35.1-17.1 32.3-36.3-9-60.9-40.5-114.5-85.7-152.5-5.9-4.9-13-7.3-20.1-7.3-10.8 0-21.6 5.4-27.8 15.3l-62.8 99.6c27.3 17 45.6 46.9 45.6 81.2zm44.1-164.4c40.5 34.1 67 81.2 74.4 132.4H373c-5.5-21.1-16.4-40.3-31.6-56.1l47.5-76.3z"></path>
                        </svg>
                    </div>
                    <div id="ContaminationText" class="tileDataDiv">
                        -
                    </div>
                </div>
                <div class="card card-min">
                    <h3>Issues</h3>
                    <div>
                        <svg class="card-icon green" style="width: 45px" viewBox="0 0 576 512">
                            <path fill="currentColor" d="M270.2 160h35.5c3.4 0 6.1 2.8 6 6.2l-7.5 196c-.1 3.2-2.8 5.8-6 5.8h-20.5c-3.2 0-5.9-2.5-6-5.8l-7.5-196c-.1-3.4 2.6-6.2 6-6.2zM288 388c-15.5 0-28 12.5-28 28s12.5 28 28 28 28-12.5 28-28-12.5-28-28-28zm281.5 52L329.6 24c-18.4-32-64.7-32-83.2 0L6.5 440c-18.4 31.9 4.6 72 41.6 72H528c36.8 0 60-40 41.5-72zM528 480H48c-12.3 0-20-13.3-13.9-24l240-416c6.1-10.6 21.6-10.7 27.7 0l240 416c6.2 10.6-1.5 24-13.8 24z"></path>
                        </svg>
                    </div>
                    <div id="IssuesText" class="tileDataDiv">
                        -
                    </div>
                </div>
                <div class="card card-min" style="flex: 20%">
                    <h3>York Area</h3>
                    <h3>Monday Week 1 | 09:12:34</h3>
                </div>
            </div>
            <div class="flex-row">
                <div class="flex-column" style="flex: 50%;">
                    <div class="card" style="flex: 50%;">
                        <div id="Test" class="card-row-container">
                            <canvas id="RoundCompletionChart" style="width: 100%;"></canvas>
                        </div>
                    </div>
                    <div class="card" style="flex: 50%;">
                        <table id="IssuesTableFeed">
                            <thead>
                                <tr>
                                    <td>Issue
                                    </td>
                                    <td>Street
                                    </td>
                                    <td>Time
                                    </td>
                                    <td>Round
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="flex-column" style="flex: 15%;">
                    <div id="ServiceProgressionCard" class="card"></div>
                </div>
            </div>
        </div>

        <div class="flex-column" style="flex: 2%; display: flex; flex-direction: column;">
            <div class="card" style="">
                2
            </div>
            <div class="card" style="">
                2
            </div>
        </div>
    </div>


    <div class="flex-row">
        <!--Left Section-->

        <div class="flex-row">
            <!--Round Completion Chart -->
        </div>

        <div class="flex-row">
            <!-- Issue Table -->

        </div>
    </div>
</body>
</html>
