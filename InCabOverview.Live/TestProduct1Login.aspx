﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestProduct1Login.aspx.cs" Inherits="TestProduct1.TestProduct1Landing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InCab Live - Login</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div>
                    <h2>InCab Live</h2>
                    <asp:Label runat="server" ID="invalidLogin"></asp:Label>
                    <div>
                        <label>Email</label>
                        <input name="user" />
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" name="pass" />
                    </div>
                    <div class="w3-right">
                        <button name="action" value="login" type="submit">
                            Login
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <p class="w3-margin-right" style="float: right">
                &copy; Webaspx Ltd <%= DateTime.Now.Year %>
            </p>
        </footer>
    </form>
</body>
</html>
