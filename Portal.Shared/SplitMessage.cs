﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portal.Shared
{
    #region CLASSES
    public class SplitMessage
    {
        public int OriginalID { get; set; } // Int32 -- (-2,147,483,648 to +2,147,483,647)
        public string MessageID { get; set; }
        public string Date1 { get; set; }
        public double Time1Decimal { get; set; }
        public string Time1Analogue { get; set; }
        public string User1 { get; set; }
        public string Ticked { get; set; }
        public MessageType MsgType { get; set; }
        public string DateTime { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Reg { get; set; }
        public string DriverID { get; set; }
        public string CrewID { get; set; }
        public string Council { get; set; }
        public Direction MessageDirection { get; set; }
        public string RoundCrew { get; set; }
        public string RoundDay { get; set; }
        public string RoundWeek { get; set; }
        public string RoundService { get; set; }

        public string CStreetID { get; set; }
        public string CPropertyID { get; set; }
        public string USRN { get; set; }
        public string UPRN { get; set; }
        public int NumberOfBins { get; set; }

        public double TipGross { get; set; }
        public double TipTare { get; set; }
        public string TipUnits { get; set; }
        public string TipName { get; set; }

        public int FuelKey { get; set; }
        public double FuelMileage { get; set; }
        public double FuelLitres { get; set; }

        public string IssueType { get; set; }
        public string Issue { get; set; }
        public string IssueIcon { get; set; }
        public string IssueOtherMsg { get; set; }
        public string IssueCode { get; set; }

        public string DCAnswers { get; set; }
        public int DCMileage { get; set; }
        public int DCWMDRoundNumber { get; set; }

        public string HouseName { get; set; }
        public string StreetName { get; set; }

        public string SoftwareVers { get; set; }
        public string ItineraryDate { get; set; }
        
        public string DateMsgReceivedYYYYMMDD { get; set; }
        public double TimeMsgReceived { get; set; }

        public string Bearing { get; set; }
        public string SpeedKMH { get; set; }

        public SplitMessage()
        {
            MsgType = MessageType.Other;
            MessageDirection = Direction.ToBase;

        }

    }


    public  enum Direction
    {
        ToBase,
        FromBase,
    }

    public enum MessageType
    {
        GPS,
        INLogin,
        INLogout,
        CPY,
        CPN,
        CSY,
        CSN,
        DriverCheck,
        DriverCheckEOD,
        Issue,
        Message,
        Unissue,
        Panic,
        Fuel,
        Tip,
        WholeStreetIssue,
        ManualEntryIssue,
        Other,
        ErrorFailedToParse,
    };
    #endregion
}
