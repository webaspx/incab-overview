﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using Webaspx.Database;
using System.Web;
using System.Configuration;

namespace Portal.Shared
{
    /// Superclass AzureServer. Inherited by Council. 
    /// <summary>
    /// Holds core server connection strings and 
    /// database methods RunQuery, RunNonQuery, RunScalarQuery. 
    /// </summary>    
    public class AzureServer
    {
        protected Dictionary<string, string> Databases = new Dictionary<string, string>();

        // Parametered Constructor for standalone usage
        public AzureServer(Dictionary<string, string> Databases)
        {
            this.Databases = Databases;
        }
        public string serverString;

        // Parameterless Constructor for Inheritance
        public AzureServer() { }

        /// <summary>
        /// Runs an SQL Query against a database in the class' Database dictionary.
        /// Returns the result in a DataTable object.
        /// Queries that don't return any results will yield a null object.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        ///<param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        /// <returns></returns>
        public DataTable RunQuery(SqlCommand queryString, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (queryString == null) { return null; }

            // Search the Council's database dictionary for a 
            // match with the Database parameter
            SqlConnection myConnection;
            string serverStringToUse;
            // If the Database name has been manually specified
            if (customDatabase == true)
            {
                serverStringToUse = serverString.Replace("##Catalog##", Database);
            }
            else
            {
                // Else derive the Database Name from the DatabaseFields dictionary
                //if (Databases == null) 
                //{                    
                //    return null;
                //}
                if (Databases.ContainsKey(Database.ToLower()))
                {
                    serverStringToUse = serverString.Replace("##Catalog##", Databases[Database.ToLower()]);
                }
                else
                {
                    throw new ArgumentException("Council's dictionary has no such Database: ' " + Database + "'. " +
                        "Parameter given is not a valid key of this class's DatabaseFields dictionary");
                }
            }
            if (serverStringToUse == "NoValues") 
            {
                throw new ArgumentException("Council's dictionary has no such Database Du78' ");
            }
            myConnection = new SqlConnection(serverStringToUse);

            // Declare a DataTable to store the result of the query
            DataTable table = new DataTable();

            // Open the connection and run the query
            // Let this part Except if query is invalid - will need stack trace
            queryString.Connection = myConnection;
            try
            {
                myConnection.Open();
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message.ToString();
                string lineNumber = "";
                if (ex.StackTrace.ToString().Length > 0)
                {
                    lineNumber = ex.StackTrace.Split(' ').Last();
                    errorMessage += " Ln:" + lineNumber;
                }
                table.Rows.Add("DDUtils?" + errorMessage);
            }
            SqlDataAdapter adapter = new SqlDataAdapter(queryString);
            adapter.Fill(table);
            myConnection.Close();

            // If the query returned no rows return null 
            // instead of an empty DataTable which would make debugging harder
            if (table.Rows.Count == 0) { return null; }
            else { return (table); }
        }







        /// <summary>
        /// Runs an SQL Query against a database in the class' Database dictionary.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        /// <param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        public void RunNonQuery(SqlCommand queryString, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (queryString == null) { return; }

            // Search the Council's database dictionary for a 
            // match with the Database parameter
            SqlConnection myConnection;
            string serverStringToUse;
            // If the Database name has been manually specified
            // If the Database name has been manually specified
            if (customDatabase == true)
            {
                serverStringToUse = serverString.Replace("##Catalog##", Database);
            }
            else
            {
                // Else derive the Database Name from the DatabaseFields dictionary
                if (Databases.ContainsKey(Database.ToLower()))
                {
                    serverStringToUse = serverString.Replace("##Catalog##", Databases[Database.ToLower()]);
                }
                else
                {
                    throw new ArgumentException("Council's dictionary has no such Database: ' " + Database + "'. " +
                        "Parameter given is not a valid key of this class's DatabaseFields dictionary");
                }
            }

            myConnection = new SqlConnection(serverStringToUse);

            // Open the connection and run the query
            // Let this part Except if query is invalid - will need stack trace
            queryString.Connection = myConnection;
            myConnection.Open();
            int RowsAffected = queryString.ExecuteNonQuery();
            myConnection.Close();

            // ExecuteNonQuery returns -1 for anything other than an UPDATE, INSERT or DELETE
            if (RowsAffected == -1)
            {
                throw new ArgumentException("Query was not an UPDATE, INSERT or DELETE.");
            }
        }

        /// <summary>
        /// Runs a Scalar SQL Query against a database in the class' Database dictionary.
        /// Returns the result as a string. Excepts if query produces tabular results.
        /// Returns null if the query returns no result.
        /// </summary>
        /// <param name="queryString">The query to run</param>
        /// <param name="Database">Name of the database to run the query on</param>
        /// <param name="customDatabase">Whether Database parameter is the exact Database name entered manually, or a key to lookup in Database Fields, e.g. "incab"</param>
        /// <returns></returns>
        public string RunScalarQuery(SqlCommand queryString, string Database, bool customDatabase = false)
        {
            // Don't even attempt null queries
            if (queryString == null) { return null; }

            // Search the Council's database dictionary for a 
            // match with the Database parameter
            SqlConnection myConnection;
            string serverStringToUse;
            // If the Database name has been manually specified
            // If the Database name has been manually specified
            if (customDatabase == true)
            {
                serverStringToUse = serverString.Replace("##Catalog##", Database);
            }
            else
            {
                // Else derive the Database Name from the DatabaseFields dictionary
                if (Databases.ContainsKey(Database.ToLower()))
                {
                    serverStringToUse = serverString.Replace("##Catalog##", Databases[Database.ToLower()]);
                }
                else
                {
                    throw new ArgumentException("Council's dictionary has no such Database: ' " + Database + "'. " +
                        "Parameter given is not a valid key of this class's DatabaseFields dictionary");
                }
            }

            myConnection = new SqlConnection(serverStringToUse);

            // Declare a DataTable to store the result of the query
            DataTable table = new DataTable();

            // Open the connection and run the query
            // Let this part Except if query is invalid - will need stack trace
            queryString.Connection = myConnection;
            myConnection.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(queryString);
            adapter.Fill(table);
            myConnection.Close();

            // If the query returned no rows return null 
            // instead of an empty DataTable which would make debugging harder
            // If the query returned more than one throw an exception
            // Else return the first value of the first row 
            if (table.Rows.Count == 0) { return null; }
            else if (table.Rows.Count > 1 || table.Columns.Count > 1)
            {
                throw new ArgumentException("Query returned more than one result. Use RunQuery for non-scalar queries, else re-factor query.");
            }
            else if (table.Rows[0][0] == null) { return null; }
            else { return table.Rows[0][0].ToString(); }
        }
    }
    //==================================================================================================================================================
    /// <summary>
    /// Stores information and methods about a Client.
    /// Inherits from AzureServer which handles the core database querying methods.
    /// </summary>
    public class Council : AzureServer
    {
        #region Attributes and Instantiators
        /// <summary>
        /// The pretty name e.g. "Oadby and Wigston"
        /// </summary>
        public readonly string displayName;
        /// <summary>
        /// The database name e.g. "oadbyandwigston"
        /// </summary>
        public readonly string urlName;

        /// <summary>
        /// Names of the products the council is subscribed to
        /// </summary>
        public List<string> availableProducts;

        /// <summary>
        /// List of domains client e-mails may have. 
        /// Only e-mails ending in these domains appear in User Admin.
        /// </summary>
        public List<string> associatedDomains;

        /// <summary>
        /// E-mails that may appear to the user in User Admin.
        /// </summary>
        public List<string> clientsideEmails;

        /// <summary>
        /// Whether the Council has subscribed to Trade.
        /// </summary>
        public bool hasTrade;

        /// <summary>
        /// Stores database fields so user can simply write "Collections" to get the right database
        /// </summary>
        public Council(string displayName, string URLname, string serverString, Dictionary<string, string> Databases)
        {
            this.displayName = displayName;
            this.urlName = URLname;

            // Inherited from AzureServer
            this.serverString = serverString;
            this.Databases = Databases;

            // I was going to re-factor these into properties, but due to how properties are handled this actually 
            // slows down the site quite considerably
            availableProducts = GetSubscribedProductNames();
            associatedDomains = GetDomains();
            clientsideEmails = GetAssociatedEmails();
            hasTrade = GetTradeFlag();
        }

        /// <summary>
        /// Called after a change to update the class attributes.
        /// Ugly but runs faster than using Properties.
        /// </summary>
        public void UpdateCouncilInfo()
        {
            availableProducts = GetSubscribedProductNames();
            associatedDomains = GetDomains();
            clientsideEmails = GetAssociatedEmails();
            hasTrade = GetTradeFlag();
        }
        #endregion 

        #region Getter methods. Retrieve information about the Council.
        /// <summary>
        /// Lists the names of the products the council has bought
        /// </summary>
        /// <returns></returns>
        public List<string> GetSubscribedProductNames()
        {
            // Define the query to run
            SqlCommand getProducts = new SqlCommand("SELECT DISTINCT Name FROM DDProducts WHERE CouncilSubscribed = '1'");

            // Run the query and store results in a DataTable
            DataTable result = RunQuery(getProducts, "Collections");

            // Add the Name column of each row to a list of strings
            List<string> Products = new List<string>();
            foreach (DataRow row in result.Rows)
            {
                Products.Add(row["Name"].ToString());
            }

            return Products;
        }

        /// <summary>
        /// Gets a full list of all products in the DDProducts table
        /// whether the client is subscribed or not.
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProducts()
        {
            // Define the query to run
            SqlCommand getProducts = new SqlCommand("SELECT DISTINCT * FROM DDProducts");

            // Run the query and store results in a DataTable
            DataTable result = RunQuery(getProducts, "Collections");

            // For each row of the result
            List<Product> Products = new List<Product>();
            foreach (DataRow row in result.Rows)
            {
                // Create a new instance of the Product Class from its values
                // and add it to a list of Products
                Products.Add(new Product()
                {
                    Name = row["Name"].ToString(),
                    DemoURL = row["demoUrl"].ToString(),
                    Live = Convert.ToBoolean(row["Live"]),
                    Subscribed = Convert.ToBoolean(row["CouncilSubscribed"]),
                    NotLiveMessage = row["Message"].ToString(),
                });
            }
            return Products;
        }

        /// <summary>
        /// Returns a length 2 list containing
        /// (ParentEmail, Displayname) only 
        /// for user matching given id
        /// </summary>
        /// This is called from the User Admin table so don't re-factor it over to the User class
        /// <param name="id">ID of the user to get details for</param>
        /// <returns></returns>
        public List<string> GetSingleUserInfo(string id)
        {
            // Create the query 
            SqlCommand GetSingleUser = new SqlCommand("SELECT TOP(1) [ParentEmail], [DisplayName] FROM [DDUsers] WHERE [ID] = @id");
            // Add the ID as a parameter to avoid SQL injections
            GetSingleUser.Parameters.Add(new SqlParameter(parameterName: "@id", value: id));
            // Store the result in a datatable
            DataTable result = RunQuery(GetSingleUser, "Collections");

            // Create a list and add the ParentEmail, then the DisplayName
            List<string> userValues = new List<string>();
            userValues.Add(result.Rows[0]["ParentEmail"].ToString()); // DBNull.ToString() returns the empty string
            userValues.Add(result.Rows[0]["DisplayName"].ToString()); // cannot be Null in DB

            return userValues;
        }

        /// <summary>
        /// Gets the list of domains the client uses.
        /// e.g. "halton.gov.uk", "dorsetcc.gov.uk"
        /// </summary>
        /// <returns></returns>
        public List<string> GetDomains()
        {
            // Define the query to run
            SqlCommand query = new SqlCommand("SELECT TOP(1) associatedDomains FROM colClient");

            // Get the domains from the database
            // They come hash delimited e.g. "domain1.gov.uk#domain2.gov.uk"
            string hash_delimited_domains = RunScalarQuery(query, "Collections");

            if (hash_delimited_domains != null)
            {
                // So unsplit them and return as a list
                return hash_delimited_domains.Split('#').ToList();
            }
            else
            {
                return new List<string>();
            }
        }

        /// <summary>
        /// Gets e-mails in the DDUsers table the user may see in User Admin. 
        /// Filters by Webaspx local-parts and Client associated domains. 
        /// </summary>
        /// <returns></returns>
        public List<string> GetAssociatedEmails()
        {
            // Get all e-mails from DDUsers
            DataTable result = RunQuery(new SqlCommand("SELECT Email from DDUsers"), "Collections");

            // Get current Webaspx Local-parts
            List<string> WebaspxLocalParts = Utilities.getWebaspxLocalParts();

            // Initialise an empty list to populate with e-mails that meet the requirements
            List<string> Emails = new List<string>();

            // If DDUsers is not empty
            if (result != null)
            {
                foreach (DataRow row in result.Rows)
                {
                    string email = row["Email"].ToString();
                    /// Ensure e-mail contains an @ to split on
                    /// If it doesn't it doesn't want adding to the list anyway
                    if (email.Contains("@"))
                    {
                        // Get first half of e-mail e.g. "fred.smith"
                        string LocalPart = email.Split('@')[0];
                        // Get last half of e-mail e.g. "gmail.com"
                        string Domain = email.Split('@')[1];

                        // If the client's associated Domains includes the domain in question
                        // AND the Webaspx Local parts DOES NOT include the Local Part in question
                        // OR if the Council is Webaspx
                        if ((associatedDomains.Contains(Domain) && !WebaspxLocalParts.Contains(LocalPart)) || urlName.ToLower() == "webaspx")
                        {
                            // Add the e-mail to ClientSideEmails
                            Emails.Add(row["Email"].ToString());
                        }
                    }
                }
            }

            return Emails;
        }

        /// <summary>
        /// Retrieves the User account for a given e-mail.
        /// </summary>
        /// <param name="email">The e-mail of the user to retrieve.</param>
        /// <returns></returns>
        public User GetUserAccount(string email)
        {
            // Define the query to run
            SqlCommand GetUser = new SqlCommand("SELECT TOP(1) * FROM DDUsers WHERE Email = @Email");
            // Add email as a parameter to avoid SQL injections
            GetUser.Parameters.Add(new SqlParameter(parameterName: "@Email", value: email));

            // Store the top row of the result (there will only be one row anyway)
            // in a DataRow
            DataTable result = RunQuery(GetUser, "Collections");

            if (result == null)
            {
                return null;
            }

            // Create a User from the data retrieved
            User user = new User(serverString, Databases)
            {
                ID = Convert.ToInt64(result.Rows[0]["ID"].ToString()),
                DisplayName = result.Rows[0]["DisplayName"].ToString(),
                Email = result.Rows[0]["Email"].ToString(),
            };
            user.PasswordHash = result.Rows[0]["PasswordHash"].ToString();
            user.PasswordSalt = result.Rows[0]["PasswordSalt"].ToString();
            user.LockState = Convert.ToInt16(result.Rows[0]["LockState"].ToString());
            user.AdminDD = Convert.ToBoolean(result.Rows[0]["AdminDD"]);
            user.ParentEmail = result.Rows[0]["ParentEmail"].ToString();
            user.LastLogin = DateTime.UtcNow;
            user.EmailWhat = result.Rows[0]["EmailWhat"].ToString();
            user.FirstName = result.Rows[0]["Firstname"].ToString();
            user.LastName = result.Rows[0]["Lastname"].ToString();
            user.Client = result.Rows[0]["Client"].ToString();

            if (DateTime.TryParse(result.Rows[0]["ProfileEditDate"].ToString(), out DateTime editdate))
            {
                user.ProfileEditDate = editdate;
            }

            if (!string.IsNullOrEmpty(result.Rows[0]["ProfileEditedBy"].ToString()))
            {
                user.ProfileEditedBy = result.Rows[0]["ProfileEditedBy"].ToString();
            }

            return user;
        }

        /// <summary>
        /// Returns whether the council has subscribed to Trade or not.
        /// </summary>
        /// <returns></returns>
        public bool GetTradeFlag()
        {
            // Define query to run
            SqlCommand getTrade = new SqlCommand("SELECT TOP 1 [hasTrade] from [colClient]");

            // Run the query and store the result. Should be "Y" or "N"
            string hasTrade = RunScalarQuery(getTrade, "Collections");

            return hasTrade == "Y";
        }

        /// <summary>
        /// Returns a string indicating if a given ID has Admin Rights.
        /// Called from UserAdmin using an ID from the UserAdmin table. 
        /// </summary>
        /// <param name="id">ID of the account in question</param>
        /// <returns></returns>
        public string GetAdminInfo(string id)
        {
            // Create the query and add the ID as a parameter to avoid SQL injections
            SqlCommand query = new SqlCommand("SELECT TOP(1) AdminDD FROM DDUsers WHERE ID = @SqlID");
            query.Parameters.Add(new SqlParameter(parameterName: "@SqlID", value: id));
            string result = RunScalarQuery(query, "Collections").ToLower();

            // If the result was exactly "true" return it
            // if it was any other value return the string "false"
            if (result == "true") { return result; }
            else { return "false"; }
        }

        /// <summary>
        /// Converts a comma delimited list of IDs into a list
        /// of theirs corresponding e-mails.
        /// </summary>
        /// <param name="IDList">A comma delimited list of IDs</param>
        /// <returns></returns>
        public List<string> GetEmailsFromIds(string IDList)
        {
            // Create a list of IDs from IDList by splitting on ','
            List<String> IDs = IDList.Split(',').ToList();

            // Get all IDs and Emails in DDUsers
            DataTable DDUsers = RunQuery(new SqlCommand("SELECT ID, Email FROM DDUsers"), "Collections");

            // Foreach ID in the list
            for (int i = 0; i < IDs.Count(); i++)
            {
                // Select the DataRow where the ID column matches the ID in the list 
                // then replace the ID in the list with the Email column of that DataRow
                IDs[i] = DDUsers.Select("ID = " + IDs[i])[0]["Email"].ToString();
            }

            return IDs;
        }
        #endregion

        #region Authentication Functions. Deals with cookies and validating passwords.
        /// <summary>
        /// Returns whether a given Username and Password exist and match. 
        /// If they don't explains why in the out parameter.
        /// </summary>
        /// <param name="Email">The email entered by the user</param>
        /// <param name="Password">The password entered in plain text</param>
        /// <param name="error">Reason for failing authentication. Empty if successful.</param>
        /// <returns></returns>
        //public bool AuthenticateUser(string Email, string Password, out string error)
        public bool AuthenticateUser(string Email, string Password)        
        {
            // Initialise error as the empty string
            string error = "";

            // Create a query to get account information matching the entered e-mail address
            SqlCommand GetUserCommand = new SqlCommand("SELECT TOP(1) PasswordHash, PasswordSalt, LockState FROM DDUsers WHERE Email = @SQLEmail");
            // Add Email as a parameter to avoid SQL injections
            GetUserCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));
            // Run the query and store the results in a DataTable
            DataTable UserTable = RunQuery(GetUserCommand, "Collections");

            // If there was no Email address matching the one entered 
            if (UserTable == null)
            {
                // return false and explain why
                error += "No user found for this e-mail address.";
                return false;
            }

            // For readability extract the authentication data from the DataTable
            string PasswordHash = UserTable.Rows[0]["PasswordHash"].ToString();
            // Password Salts are always cast to Upper to avoid any case ambiguity when hashing
            // The User Class Get method for password salt returns in Upper too
            string PasswordSalt = UserTable.Rows[0]["PasswordSalt"].ToString();
            string LockState = UserTable.Rows[0]["LockState"].ToString();

            // If the account is locked 
            if (LockState == "1")
            {
                // return false and explain why
                error = "This account is locked. Please contact your administrator.";
                return false;
            }

            // If the hashed password and salt match the hash in the table
            string hashedEnteredPassword=Utilities.ComputeSha256Hash(Password + PasswordSalt);
            if (hashedEnteredPassword == PasswordHash)
            {
                // return success, leaving error as the empty string
                return true;
            }
            else
            {
                // return false and explain why
                error = "Incorrect username or password.";
                return false;
            }
        }

        /// <summary>
        /// Validates a password reset request key and 
        /// produces the corresponding e-mail if valid.
        /// </summary>
        /// <param name="email">The e-mail address the reset is for. Empty if key is invalid. </param>
        /// <param name="key">The password reset key retrieved from the URL</param>
        /// <returns></returns>
        public bool VerifyPasswordResetURL(string key, out string email)
        {
            // Initialises email as the empty string
            email = "";

            // Defines the query and adds key as a parameter to avoid SQL injections
            SqlCommand query = new SqlCommand("SELECT TOP 1 [Email], [PasswordRequestID], [PasswordRequestDate] FROM [DDUsers] WHERE PasswordRequestID = @Key");
            query.Parameters.Add(new SqlParameter(parameterName: "Key", value: key));
            // Runs the query and stores the result in a DataTable
            DataTable result = RunQuery(query, "Collections");

            // If the result was null the key was not found in the table
            if (result != null)
            {
                // Else the key was found and now needs validating

                // Extract the ID and expiration date from the query result
                string storedPWID = result.Rows[0]["PasswordRequestID"].ToString();
                DateTime pwIDExpiration = Convert.ToDateTime(result.Rows[0]["PasswordRequestDate"]);

                // If the keys match and the reset was made less than 30 minutes ago
                if (pwIDExpiration > DateTime.Now.ToUniversalTime().AddMinutes(-30) && storedPWID.Equals(key))
                {
                    // Then the request is valid. 
                    // Return true and write the corresponding email address to the out parameter
                    email = result.Rows[0]["Email"].ToString();
                    return true;
                }
            }
            // So return false (leaving email as the empty string)
            return false;
        }
        #endregion

        #region UserAdmin Functions. Makes actual changes.
        /// <summary>
        /// Creates a new user account using the minimum necessary data set. 
        /// Most fields not nullable in DB so should all be created at once.
        /// Randomises password hash and salt.
        /// Sets registration date to the current date time.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Firstname"></param>
        /// <param name="Lastname"></param>
        /// <param name="ParentEmail"></param>
        /// <param name="DisplayName"></param>
        /// <param name="LockState"></param>
        /// <param name="AdminDD"></param>
        /// <param name="InactiveDaysAllowed"></param>
        public void CreateUser(string Email,
            string Firstname = "", string Lastname = "",
            string ParentEmail = "", string DisplayName = "",
            int LockState = 0, int AdminDD = 0, int InactiveDaysAllowed = 60)
        {
            /// If both Firstname AND Lastname are left blank, estimate from other values
            TextInfo textInfo = new CultureInfo("en-UK", false).TextInfo;
            if (Firstname == "" & Lastname == "")
            {
                // If the localpart contains a '.' split on it and 
                // take the first two elements of that list as First and Last name respectively
                string localpart = Email.Split('@')[0];
                if (localpart.Contains('.'))
                {
                    Firstname = textInfo.ToTitleCase(localpart.Split('.')[0]);
                    Lastname = textInfo.ToTitleCase(localpart.Split('.')[1]);
                }
            }

            // If DisplayName was left blank estimate from first and last name
            if (DisplayName == "")
            {
                DisplayName = Firstname + " " + Lastname;
                DisplayName = DisplayName.Trim();
            }

            // Create the SQL Command
            SqlCommand CreateUserCommand = new SqlCommand("IF EXISTS (SELECT * FROM DDUsers WHERE Email = @SQLEmail) " +
                "RAISERROR('User already exists', 16, 1) " +
                "ELSE INSERT INTO DDUsers " +
                "(Email, Firstname, Lastname, PasswordHash, PasswordSalt, " +
                "RegistrationDate, ParentEmail, DisplayName, LockState, AdminDD, InactiveDaysAllowed, Client) " +
                "VALUES (@SQlEmail, @SQlFirstname, @SQlLastname, NEWID(), NEWID(), @SQLRegistrationDate, " +
                "@SQLParentEmail, @SQLDisplayName, @SQLLockState, @SQlAdminDD, @SQLInactiveDaysAllowed, @SQlClient);");

            // Add all the values as parameters to avoid SQL injections
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLEmail", value: Email));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLClient", value: urlName));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLLockState", value: LockState));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLAdminDD", value: AdminDD));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLInactiveDaysAllowed", value: InactiveDaysAllowed));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLRegistrationDate", value: DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLDisplayName", value: DisplayName));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLParentEmail", value: ParentEmail));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLFirstname", value: Firstname));
            CreateUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLLastname", value: Lastname));

            // Run the query, creating the user.
            RunNonQuery(CreateUserCommand, "Collections");
            // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
            UpdateCouncilInfo();
        }

        /// <summary>
        /// For a given Email, removes that User from DDUsers and all entries for that user in DDProductAccess.
        /// </summary>
        /// <param name="Email">String, matches the Email of the user in DDUsers table</param>
        public void RazeUser(string Email)
        {
            // Create a query to find the UserID for the given Email
            SqlCommand GetUserID = new SqlCommand("SELECT TOP(1) ID FROM DDUsers WHERE Email = @SQLEmail");
            GetUserID.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));

            string ID = RunScalarQuery(GetUserID, "Collections");
            if (ID != null)
            {
                // THIS IS ONLY SAFE BECAUSE we know ID came directly from the DDUsers table and can't possibly be an injection
                RunNonQuery(new SqlCommand("DELETE FROM DDUsers WHERE ID = " + ID), "Collections");
                RunNonQuery(new SqlCommand("DELETE FROM DDProductAccess WHERE UserID = " + ID), "Collections");

                // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
                UpdateCouncilInfo();
            }
        }

        /// <summary>
        /// For a given UserID, removes that User from DDUsers and all entries for that user in DDProductAccess.
        /// </summary>
        /// <param name="ID">Int, matches ID of user in DDUsers table.</param>
        public void RazeUser(int ID)
        {
            // Create a query to remove user from DDUsers
            SqlCommand DeleteUserCommand = new SqlCommand("DELETE FROM DDUsers WHERE ID = @SQlID");
            DeleteUserCommand.Parameters.Add(new SqlParameter(parameterName: "SQLID", value: ID));

            // Create a query to remove user from DDProductAccess
            SqlCommand DeleteProdCommand = new SqlCommand("DELETE FROM DDProductAccess WHERE UserID =  @SQlUserID");
            DeleteProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLUserID", value: ID));

            // Run the queries
            RunNonQuery(DeleteProdCommand, "Collections");
            RunNonQuery(DeleteUserCommand, "Collections");

            // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
            UpdateCouncilInfo();
        }


        /// <summary>
        /// Updates a user's profile with values available to the User Admin table.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="AdminDD"></param>
        /// <param name="ParentEmail"></param>
        /// <param name="DisplayName"></param>
        public void UpdateUserFromAdmin(string Email, int AdminDD, string ParentEmail, string DisplayName)
        {
            // Create the query and add the values as parameters to avoid SQL injections
            SqlCommand UpdateUser = new SqlCommand("UPDATE DDUsers SET AdminDD = @SQLAdminDD , ParentEmail = @SQLParentEmail, DisplayName = @SQLDisplayName WHERE Email = @SQlEmail; " +
                "UPDATE DDUsers SET ProfileEditDate = GETDATE() WHERE Email = @SQLEmail;");
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "@SQLAdminDD", value: AdminDD));
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "@SQLDisplayName", value: DisplayName));
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "@SQLParentEmail", value: ParentEmail));

            // Run the query
            RunNonQuery(UpdateUser, "Collections");

            // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
            UpdateCouncilInfo();
        }

        /// <summary>
        /// Updates a user's profile with values available to the User Admin table.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="AdminDD"></param>
        public void UpdateUserFromAdmin(string Email, int AdminDD)
        {
            // Create the query and add the values as parameters to avoid SQL injections
            SqlCommand UpdateUser = new SqlCommand("UPDATE DDUsers SET AdminDD = @SQLAdminDD WHERE Email = @SQlEmail; " +
                "UPDATE DDUsers SET ProfileEditDate = GETDATE() WHERE Email = @SQLEmail;");
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "@SQLAdminDD", value: AdminDD));

            // Run the query
            RunNonQuery(UpdateUser, "Collections");

            // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
            UpdateCouncilInfo();
        }

        /// <summary>
        /// Grants a user access to a product. Has no effect if user does not exist.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="ProductName">Name of the product. Caller is responsible for this being correct.</param>
        /// <param name="AuthorisationLevel">Valid values vary according to the Product being added. Defaults to the empty string.</param>
        /// <param name="ScreenAccess">Valid values vary according to the Product being added. Defaults to the empty string.</param>
        /// <param name="StartUpScreen">Valid values vary according to the Product being added. Defaults to the empty string.</param>
        public void GiveUserProduct(string Email, string ProductName, string AuthorisationLevel = "", string ScreenAccess = "", string StartUpScreen = "")
        {
            // Create a query to find the UserID for the given Email
            SqlCommand GetUserID = new SqlCommand("SELECT TOP(1) ID FROM DDUsers WHERE Email = @SQLEmail");
            GetUserID.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));

            // Retrieve the ID for the given Email
            string ID = RunScalarQuery(GetUserID, "Collections");

            // If the email does exist in the same and thus has an ID
            if (ID != null)
            {
                // Write an UPSERT query - will update values if an entry already exists
                // but insert a new line if one doesn't 
                SqlCommand GrantProdCommand = new SqlCommand(
                    "IF EXISTS(SELECT * FROM DDProductAccess WHERE UserID = @SQLUserID AND " +
                    "Name = @SQLProductName) UPDATE DDProductAccess SET Name = @SQLProductName, " +
                    "AuthorisationLevel = @SQLAuthorisationLevel, ScreenAccess = @SQLScreenAccess, " +
                    "StartUpScreen = @SQLStartUpScreen WHERE UserID = @SQLUserID AND Name = @SQLProductName " +
                    "ELSE INSERT INTO DDProductAccess (UserID, Name, AuthorisationLevel, ScreenAccess, StartUpScreen) VALUES " +
                    "(@SQlUserID, @SQLProductName, @SQLAuthorisationLevel, @SQLScreenAccess, @SQLStartUpScreen);");

                GrantProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLUserID", value: ID));
                GrantProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLProductName", value: ProductName));
                GrantProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLAuthorisationLevel", value: AuthorisationLevel));
                GrantProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLScreenAccess", value: ScreenAccess));
                GrantProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLStartUpScreen", value: StartUpScreen));
                GrantProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));

                // Run the query
                RunNonQuery(GrantProdCommand, "Collections");

                // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
                UpdateCouncilInfo();
            }
        }

        /// <summary>
        /// Removes a user's access to a product. 
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="ProductName"></param>
        public void TakeProductAway(string Email, string ProductName)
        {
            // Create a query to find the UserID for the given Email
            SqlCommand GetUserID = new SqlCommand("SELECT TOP(1) ID FROM DDUsers WHERE Email = @SQLEmail");
            GetUserID.Parameters.Add(new SqlParameter(parameterName: "@SQLEmail", value: Email));

            // Retrieve the ID for the given Email
            string ID = RunScalarQuery(GetUserID, "Collections");

            // If the email does exist in the same and thus has an ID
            if (ID != null)
            {
                // Write a query to delete entries in ProductAccess that match both the user's ID and the Product Name
                SqlCommand DeleteProdCommand = new SqlCommand("DELETE FROM DDProductAccess " +
                    "WHERE UserID = @SQlUserID AND Name = @SQLProductName;");
                DeleteProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLUserID", value: ID));
                DeleteProdCommand.Parameters.Add(new SqlParameter(parameterName: "@SQLProductName", value: ProductName));

                // Run the query
                RunNonQuery(DeleteProdCommand, "Collections");

                // Refresh the Council's data - I dislike this but it actually runs faster than using Properties with getters/setters
                UpdateCouncilInfo();
            }
        }
        #endregion
    }
    //==================================================================================================================================================
    /// <summary>
    /// Stores information about a Webaspx roduct.
    /// </summary>
    public class Product
    {
        // The database name of the Product, so not "Cloud Sequencer" but "CloudSequencer"
        private string name;

        // The database name of the Product, so not "Cloud Sequencer" but "CloudSequencer"
        // Setting this computes the first part of the url of the product, so 
        // "cloudsequencer-##council##.azurewebsites.net"
        // ##council## is switched out for the Council name on the Modules page
        public string Name
        {
            get { return name; }
            set
            {
                // Set the Attribute name to the value given
                name = value;
                // Set this product's URL to a value containing name
                this.URL = $"https://{name}-##council##.azurewebsites.net";

                // If the product is InCab or Collections:
                if (name.ToLower() == "collections" ||
                    name.ToLower() == "incab")
                {
                    // Add a "/Default.aspx" to the URL
                    this.URL = $"https://{name}-##council##.azurewebsites.net/Default.aspx";
                }
            }
        }

        /// <summary>
        /// Whether or not the Council has subscribed to this product
        /// </summary>
        public bool Subscribed { get; set; } = false;

        /// <summary>
        /// The URL of this product's Web App.
        /// Usually in form productname-council.azurewebsites.net e.g. incab-southnorfolk.azurewebsites.net/
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// The URL of this product's demo site. Navigating straight here won't work as the Demo sites require a password variable in the Request. 
        /// This is computed on the Modules page 
        /// </summary>
        public string DemoURL { get; set; }

        /// <summary>
        /// Whether or not the product is currently live. 
        /// </summary>
        public bool Live { get; set; } = false;

        /// <summary>
        /// If the product is not live, the reason is given here. e.g. "Down for maintenance. Will be back by 4pm x/x/20xx"
        /// </summary>
        public string NotLiveMessage { get; set; }
    }
    //==================================================================================================================================================
    /// <summary>
    /// Stores information and methods about a User.
    /// Inherits from AzureServer which handles the core database querying methods.
    /// </summary>
    public class User : AzureServer
    {
        #region Attributes, Properties and Instantiators. 
        public User(string serverString, Dictionary<string, string> Databases)
        {
            // This attribute is inheited from AzureServer and holds information on the database connection
            this.Databases = Databases;
            this.serverString = serverString;
        }

        /// <summary>
        /// The user's display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// The user's account email address.
        /// </summary>
        public string Email { get; set; }

        public string Client { get; set; }

        /// <summary>
        /// The account identifier.
        /// </summary>
        internal long ID { get; set; }

        /// <summary>
        /// The user's password hashed.
        /// </summary>
        internal string PasswordHash { get; set; }

        /// <summary>
        /// The salt added to the user's password before hashing, returns it capitalised.
        /// </summary>
        private string salt;

        /// <summary>
        /// The salt used to create the hashed password. 
        /// Must be returned capitalised to ensure no ambiguity when hashing
        /// </summary>
        public string PasswordSalt
        {
            get { return salt.ToUpper(); }
            set { salt = value; }
        }

        /// <summary>
        /// Whether the account is locked or not. 
        /// 0 means it is not, 1 means it is. 
        /// Any other value is invalid. 
        /// </summary>
        public short LockState { get; set; }

        /// <summary>
        /// Is this person allowed to Administrate other users and their access.
        /// </summary>
        public bool AdminDD { get; set; }

        /// <summary>
        /// The User's parent email. 
        /// Password Reset Emails will be sent to this if it is set.
        /// </summary>
        public string ParentEmail { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        /// <summary>
        /// Date and time of last login. 
        /// Updated to current system upon login.
        /// </summary>
        public DateTime LastLogin { get; set; }

        /// <summary>
        /// Synonym for ParentEmail. 
        /// </summary>
        public string EmailWhat { get; set; }

        public DateTime? ProfileEditDate { get; set; }

        public string ProfileEditedBy { get; set; } = "Webaspx";

        public List<Product> Products { get; set; }
        #endregion

        /// <summary>
        /// Sets the LastLogin field for the user account to the current system time.
        /// </summary>
        /// <param name="email"></param>
        public void UpdateLastLogin()
        {
            SqlCommand UpdateUser = new SqlCommand("UPDATE [DDUsers] SET LastLogin ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' WHERE EMAIL = @Email");
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "Email", value: Email));

            RunQuery(UpdateUser, "Collections");
        }

        /// <summary>
        /// Returns whether the User has access to that Product
        /// i.e. if there is an entry in ProductAccess matching their UserID and with Name = {the product name}
        /// </summary>
        /// <param name="productname">Name to match with products in the database.</param>
        /// <returns></returns>
        public bool hasAccess(string productname)
        {
            // Create the query and add variables as SQL parameters to prevent injections
            // Looks for any ProductAccess entry with a matching Productname so could return multiple rows
            SqlCommand hasAccess = new SqlCommand("SELECT * FROM DDProductAccess WHERE UserID = @ID AND Name = @ProdName");
            hasAccess.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));
            hasAccess.Parameters.Add(new SqlParameter(parameterName: "ProdName", value: productname));

            // Store the result in a DataTable
            DataTable result = RunQuery(hasAccess, "Collections");

            // If there are any results return true, else return false
            if (result == null)
            {
                return false;
            }
            else
            {
                // Note the database could is incorrect if multiple rows are returned, but that just means they have access twice so 
                // this function should still return true. It could cause a permissions conflict though.
                return true;
            }
        }

        /// <summary>
        /// Writes an encrypted ticket to the DB so the ticket in the session can be validated against it.
        /// </summary>
        /// <param name="Ticket">An encrypted ticket to write to the DB</param>
        public void StoreCookie(string Ticket)
        {
            // Create the query with SQL parameters to avoid injections
            SqlCommand StoreCookie = new SqlCommand("UPDATE DDUsers SET LoginToken = @Ticket WHERE ID = @ID");
            StoreCookie.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));
            StoreCookie.Parameters.Add(new SqlParameter(parameterName: "@Ticket", value: Ticket));

            // Run the query on the Collections database
            RunNonQuery(StoreCookie, "Collections");
        }

        /// <summary>
        /// Retrieves the value of LoginToken so other Web Apps can create cookies using the same value
        /// So if they return from DPO to Digital Depot their Digital Depot cookie is still valid
        /// </summary>
        /// <returns></returns>
        public string RetrieveCookie()
        {
            // Create the query with SQL parameters to avoid injections
            SqlCommand GetToken = new SqlCommand("SELECT TOP(1) LoginToken FROM DDUsers WHERE ID = @ID");
            GetToken.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));

            // Run the query and return the token
            return RunScalarQuery(GetToken, "Collections");
        }

        /// <summary>
        /// Creates a page transition key in the DDUsers table. 
        /// This key can be used to authenticate exactly 1 page transition (e.g. from Digital Depot to InCab)
        /// And then is obfuscated when verified
        /// </summary>
        /// <returns></returns>
        public string CreateTransitionKey()
        {
            // Create the query with SQL parameters to avoid injections
            SqlCommand StoreKey = new SqlCommand("UPDATE DDUsers SET TransitionKey = NEWID() WHERE ID = @ID; " +
                "SELECT TOP(1) TransitionKey FROM DDUsers WHERE ID = @ID");
            StoreKey.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));

            // Run the query to create, store, then retrieve the key
            return (RunScalarQuery(StoreKey, "Collections"));
        }

        /// <summary>
        /// Obfuscates a transition key and returns whether it was valid or not. 
        /// Should be called the first time a user transitions to a product, 
        /// e.g. from Digital Depot to InCab.
        /// </summary>
        /// <param name="givenKey">The one-time key. Obfuscated immediately.</param>
        /// <returns></returns>
        public bool VerifyTransitionKey(string givenKey)
        {
            // Create the query with SQL parameters to avoid injections
            SqlCommand GetKey = new SqlCommand("SELECT TOP(1) TransitionKey FROM DDUsers WHERE ID = @ID");
            GetKey.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));

            string storedKey = RunScalarQuery(GetKey, "Collections");

            SqlCommand ObfuscateKey = new SqlCommand("UPDATE DDUsers SET TransitionKey = NEWID() WHERE ID = @ID");
            ObfuscateKey.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));
            RunNonQuery(ObfuscateKey, "Collections");

            if (storedKey.ToLower() == givenKey.ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares a cookie from the Session with one in the Database for a match.
        /// </summary>
        /// <param name="cookie"></param>
        /// <returns></returns>
        public bool VerifyCookie(string cookie)
        {
            // If the cookie is null or not a string return false
            if (!string.IsNullOrWhiteSpace(cookie))
            {
                // Write a query to retrieve the cookie from the DB
                SqlCommand SeekCookie = new SqlCommand("SELECT TOP(1) LoginToken FROM [DDUsers] WHERE ID = @ID");
                SeekCookie.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));

                // Write a query to retrieve the LockState of the User
                SqlCommand isLocked = new SqlCommand("SELECT TOP(1) LockState FROM DDUsers WHERE ID = @ID");
                isLocked.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));

                string CookieDB = RunScalarQuery(SeekCookie, "Collections").ToLower();
                string lockState = RunScalarQuery(isLocked, "Collections");

                // Return true if the cookies match and the user is not locked
                if ((CookieDB == cookie.ToLower() && lockState == "0"))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Removes a cookie from the DB upon logging out or requesting a password reset. 
        /// </summary>
        public void DeleteCookie()
        {
            // Create the query with SQL parameters to avoid injections
            SqlCommand UpdateUser = new SqlCommand("UPDATE [DDUsers] SET LoginToken = NULL WHERE ID = @ID");
            UpdateUser.Parameters.Add(new SqlParameter(parameterName: "ID", value: ID));

            // Run the query against the Collections database
            RunNonQuery(UpdateUser, "Collections");
        }

        /// <summary>
        /// Resets the password of the user.
        /// </summary>
        /// <param name="password">The new password</param>
        /// <returns></returns>
        public bool ResetPassword(string password)
        {
            // Only attempt if the password is not null or empty
            if (!string.IsNullOrWhiteSpace(password))
            {
                // Get a new salt from the database
                string salt = RunScalarQuery(new SqlCommand("SELECT NEWID()"), "Collections");

                // compute a hashed password using that salt
                // We always use Upper case salt for Hashing to avoid any ambiguities. The User Class' Salt property returns capitalised too.
                string hashPassword = Utilities.ComputeSha256Hash(password + salt.ToUpper());

                // Create the query to reset the password:
                // PasswordRequestID is obfuscated with a new random id so the old key can't be used to reset a password again
                SqlCommand resetPassword = new SqlCommand("UPDATE DDUsers SET " +
                    "[PasswordHash] = @PasswordHash, " +
                    "[PasswordSalt] = @PasswordSalt, " +
                    "[PasswordRequestID]= NEWID() " +
                    "WHERE [Email] =@Email; ");
                resetPassword.Parameters.Add(new SqlParameter(parameterName: "PasswordHash", value: hashPassword));
                resetPassword.Parameters.Add(new SqlParameter(parameterName: "PasswordSalt", value: salt));
                resetPassword.Parameters.Add(new SqlParameter(parameterName: "Email", value: Email));

                // Run the query and reset the password
                RunNonQuery(resetPassword, "Collections");

                // return true if we've gotten here without errors
                return true;
            }
            return false; // because password was null or white space
        }

        /// <summary>
        /// Updates the User's Display Name and records the time of the edit. 
        /// Called from User Account page so also sets EditedBy to self.
        /// </summary>
        /// <param name="DisplayName">The new Display Name</param>
        public void UpdateDisplayName(string DisplayName)
        {
            // Create the query using SQL parameters to avoid SQL injections
            SqlCommand Update = new SqlCommand("UPDATE [DDUsers] SET " +
                "DisplayName = @DisplayName, " +
                "ProfileEditedBy = @ProfileEditor, " +
                "ProfileEditDate = '" +
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                "' WHERE Email = @ProfileEditor");
            Update.Parameters.Add(new SqlParameter(parameterName: "DisplayName", value: DisplayName));
            Update.Parameters.Add(new SqlParameter(parameterName: "ProfileEditor", value: Email));

            // Run the query against the Collections Database.
            RunNonQuery(Update, "Collections");
        }

        /// <summary>
        /// Updates the User's Parent Email and records the time of the edit. 
        /// Called from User Account page so also sets EditedBy to self.
        /// </summary>
        /// <param name="ParentEmail">The new Parent Email. </param>
        public void UpdateParentEmail(string ParentEmail)
        {
            // Create the query using SQL parameters to avoid SQL injections
            SqlCommand Update = new SqlCommand("UPDATE [DDUsers] SET " +
                "ParentEmail = @ParentEmail, " +
                "ProfileEditedBy = @ProfileEditor, " +
                "ProfileEditDate = '" +
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                "' WHERE Email = @ProfileEditor");
            Update.Parameters.Add(new SqlParameter(parameterName: "ParentEmail", value: ParentEmail));
            Update.Parameters.Add(new SqlParameter(parameterName: "ProfileEditor", value: Email));

            // Run the query against the Collections Database.
            RunNonQuery(Update, "Collections");
        }

        /// <summary>
        /// Updates the User's First Name and records the time of the edit. 
        /// Called from User Account page so also sets EditedBy to self.
        /// </summary>
        /// <param name="FirstName">The new First Name.</param>
        public void UpdateFirstName(string FirstName)
        {
            // Create the query using SQL parameters to avoid SQL injections
            SqlCommand Update = new SqlCommand("UPDATE [DDUsers] SET " +
                "firstname = @FirstName, " +
                "ProfileEditedBy = @ProfileEditor, " +
                "ProfileEditDate = '" +
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                "' WHERE Email = @ProfileEditor");
            Update.Parameters.Add(new SqlParameter(parameterName: "FirstName", value: FirstName));
            Update.Parameters.Add(new SqlParameter(parameterName: "ProfileEditor", value: Email));

            // Run the query against the Collections Database.
            RunNonQuery(Update, "Collections");
        }

        /// <summary>
        /// Updates the User's Last Name and records the time of the edit. 
        /// Called from User Account page so also sets EditedBy to self.
        /// </summary>
        /// <param name="LastName">The new Last Name.</param>
        public void UpdateLastName(string LastName)
        {
            // Create the query using SQL parameters to avoid SQL injections
            SqlCommand Update = new SqlCommand("UPDATE [DDUsers] SET " +
                "Lastname = @LastName, " +
                "ProfileEditedBy = @ProfileEditor, " +
                "ProfileEditDate = '" +
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                "' WHERE Email = @ProfileEditor");
            Update.Parameters.Add(new SqlParameter(parameterName: "LastName", value: LastName));
            Update.Parameters.Add(new SqlParameter(parameterName: "ProfileEditor", value: Email));

            // Run the query against the Collections Database.
            RunNonQuery(Update, "Collections");
        }
    }
    //==================================================================================================================================================
    public class Utilities
    {
        /// <summary>
        /// Extracts the client name from the URL.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetURLNameFromURL(string url)
        {

            //add a line for nothing
            url = url.ToLower();

            if (url.Contains("localhost"))
                return Misc.GetDigitalDepotLocalHostTestCouncil();

            if (!(url.Contains("-") && url.Contains(".azurewebsites.net")))
                return url;

            // Split the string between the first hyphen and the next full stop after it
            // So https://digitaldepot-carlisle.azurewebsites becomes "carlisle"
            int firstHyphen = url.IndexOf('-');
            int nextFullstop = url.IndexOf('.', firstHyphen);
            firstHyphen++;
            string councilFromURL = url.Substring(firstHyphen, nextFullstop - firstHyphen).Trim();

            return councilFromURL;
        }

        public static string SetLocalHost()
        {
            throw new NotImplementedException("Please use function above ^^ (GetURLNameFromURL)");
            //string councilFromURL = "carlisle";
            //councilFromURL = "Rhondda";
            //councilFromURL = "torridge";
            //councilFromURL = "Ryedale";
            //councilFromURL = "OadbyAndWigston";

            //councilFromURL = "webaspx";
            //councilFromURL = "york";
            //return councilFromURL;
        }


        /// <summary>
        ///  Takes the council name from the URL string and create an instance of the Council object based on it.
        ///  Uses Azure Web App Settings to find the correct details. If running in Localhost these aren't available
        ///  so will use Webaspx_Master instead.
        /// </summary>
        /// <param name="councilFromURL"></param>
        /// <returns></returns>
        //public static Council getCouncilFromURL(string url)
        //{
        //    // If running LIVE:
        //    if (!url.Contains("localhost"))
        //    {
        //        // First try to return council from Azure Configuration Manager
        //        try { return getCouncilFromAzure(url); }
        //        catch (Exception Ex)
        //        {
        //            // If that fails send Sam an e-mail and get Council from Webaspx_Master as a redundancy
        //            //SendEmail(Ex.Message, "DigitalDepot Diagnostic: " + url + "\\n" + Ex.ToString(), "philip.bowler@webaspx.com");
        //            return getCouncilFromWebaspxMaster(url);
        //        }

        //    }
        //    // If running LOCALHOST
        //    else
        //    {
        //        return getCouncilFromWebaspxMaster(url);
        //    }
        //}

        public static Council getCouncilFromURL(string url)
        {
            //try
            //{
                if (url.Contains("localhost"))
                {
                    //ONLY incab code will call this - not collections PB-We need to remove the use of this table from in cab code
                    return getCouncilFromWebaspxMaster(url);
                }
                return getCouncilFromAzure(url);
            //}
            //catch (Exception Ex) 
            //{
            //    string errorMessage = Ex.Message.ToString();
            //   string lineNumber = "";
            //    if (Ex.StackTrace.ToString().Length > 0)
            //    {
            //        lineNumber = Ex.StackTrace.Split(' ').Last();
            //        errorMessage += " Ln:" + lineNumber;
            //    }
            //    throw new NotImplementedException("GCFU:"+errorMessage);
            //}
        }


        /// <summary>
        /// Derives an instance of the Council class from the settings stored in Azure App Settings. 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Council getCouncilFromAzure(string url)
        {
            //New KP
            // Get the ClientName from the URL
            string debugPass = "";
            string URLName = "";
            string DisplayName = "";
            string connection = "";
            string connectionStringKP = "";
            string ConnectionString = "";
            string[,] hasIncab = new string[1, 1];
            try
            {
                if (url.StartsWith("https://collections-") || url.StartsWith("https://workflow-")|| url.StartsWith("https://incab-"))
                {
                    URLName = Misc.getClientFromURL(url);
                }
                else
                {
                    URLName = GetURLNameFromURL(url);
                }
                debugPass += "1";
                // Get the Display Name from the URL Name                
                //if (ConfigurationManager.AppSettings["DisplayName"] != null)
                //    DisplayName = ConfigurationManager.AppSettings["DisplayName"].ToString();
                //else
                //    DisplayName = EstimateDisplayName(URLName);
                debugPass += "2";
                // Get the ConnectionString from the Azure ConnectionString environment variables (configured from within the Azure Portal)
                connection = Misc.getDbnameDbSourceDbUser(URLName, "Collections");
                debugPass += "2a";
                string sqlQueryHasIncab = "Select hasIncab from colClient";
                hasIncab = Misc.getDataArrayExactFromDB(sqlQueryHasIncab, URLName, connection);
                debugPass += "2b";
                string[] splitConnection = connection.Split('£');
                debugPass += "2c";
                connectionStringKP = BuildConnectionString(URLName, "Collections", splitConnection[1], splitConnection[2]);

                //ConnectionString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
                debugPass += "3";

                // Fill the dictionary of available databases from the same place
                Dictionary<string, string> Databases = new Dictionary<string, string> { };
                //if (ConfigurationManager.ConnectionStrings["Collections"] != null)
                //Databases.Add("collections", ConfigurationManager.ConnectionStrings["Collections"].ConnectionString);
                Databases.Add("collections", "Collections_" + URLName);
                debugPass += "4";
                //if (ConfigurationManager.ConnectionStrings["InCab"] != null)//KP 04May2020
                if (hasIncab[1, 0] == "Y")
                    //Databases.Add("incab", ConfigurationManager.ConnectionStrings["InCab"].ConnectionString);
                    Databases.Add("incab", "incab_" + URLName);
                debugPass += "5";
                return new Council(URLName, URLName, connectionStringKP, Databases);
                //return new Council(DisplayName, URLName, ConnectionString, Databases);
            }
            catch (Exception Ex)
            {
                string errorMessage = Ex.Message.ToString();
                string lineNumber = "";
                if (Ex.StackTrace.ToString().Length > 0)
                {
                    lineNumber = Ex.StackTrace.Split(' ').Last();
                    errorMessage += " Ln:" + lineNumber;
                }
                //throw new NotImplementedException("GCFA:" + URLName + ":" + DisplayName + ":" + "::Con" + connection + "::" + ConnectionString + ":" + debugPass + ":" + errorMessage + ":" + url);
                throw new NotImplementedException("GCFA:" + URLName + ":" + DisplayName + ":" + "::Con::" + "" + ":" + debugPass + ":" + errorMessage + ":" + url);
            }
        }
      
        private static string BuildConnectionString(string council, string dbSelector, string dataSource, string username)
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.DataSource = dataSource;
            if (dbSelector.ToLower() == "incab") { dbSelector = "incab"; }
            if (dbSelector.ToLower() == "collections") { dbSelector = "Collections"; }
            if (dbSelector.ToLower() == "dpo") { dbSelector = "DPO"; }
            if (dbSelector.ToLower() == "disposal") { dbSelector = "Disposal"; }
            sb.InitialCatalog = String.Format("{0}_{1}", dbSelector, council);
            sb.Encrypt = true;
            sb.TrustServerCertificate = true;
            sb.UserID = username;
            sb.Password = "Web08#580";
            return sb.ToString();
        }


        public static Council getCouncilFromWebaspxMaster(string url)
        {
            // Get the ClientName from the URL            
            string URLName = "";
            if (url.StartsWith("https://collections-"))
            {
                URLName = Misc.getClientFromURL(url);
            }
            else
            {
                URLName = GetURLNameFromURL(url);
            }

            // Get the Display Name from the URL Name
            string DisplayName = EstimateDisplayName(URLName); // gets nicely formatted display name (eg "ardsandnorthdown" becomes "Ards And North Down")

            if (url.Contains("localhost"))
            {
                URLName = Misc.GetDigitalDepotLocalHostTestCouncil();// if running in local host, this determines the council to test on ONLY INCAB code
                DisplayName = URLName;
            }

            // Specify the connection to Webaspx_Master
            SqlConnection councilGetter = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable councilTable = new DataTable();

            // Fill the table with the Client details matching the URLname
            councilGetter.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT TOP(1) * FROM ClientInfo WHERE URLname = @SQLUrlName", councilGetter);
            getCouncils.Parameters.Add(new SqlParameter(parameterName: "@SQLUrlName", URLName));
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(councilTable);
            councilGetter.Close();

            // Create an instance of the Council class from the appropriate row
            //string ConnectionString = "";
            //DataRow row = null;
            //if (councilTable.Rows.Count ==0) 
            //{
            //    return new Council("Error", "NoSuchClientOnlientInfo", null, null);
            //} 
            DataRow row;
            if (councilTable.Rows.Count == 0)
            {
                Dictionary<string, string> DatabaseError = new Dictionary<string, string>();
                DatabaseError.Add("collections", "NoValue");
                DatabaseError.Add("incab", "NoValue");
                return new Council("NoValues", "NoValues", "NoValues", DatabaseError);
            }
            row = councilTable.Rows[0];
            // Derive the ConnectionString from the serverString field
            string ConnectionString ="Data Source=" + row["serverString"].ToString() + ".database.windows.net;" +
                "Initial Catalog=##Catalog##;" +
                "User ID=mike.devine@webaspx.com@" + row["serverString"].ToString() + ";" +
                "password=Web08#580;";

            // Fill the Database dictionary
            Dictionary<string, string> Databases = new Dictionary<string, string>();
            Databases.Add("collections", row["CollectionsDBName"].ToString());
            Databases.Add("incab", row["InCabDBName"].ToString());

            return new Council(DisplayName, URLName, ConnectionString, Databases);
        }

        /// <summary>
        /// Sends an email from noreply@webaspx.com to a comma delimited string of recipients
        /// Subject is pre-set to Client Website Trawler. Refactor as a parameter if code is re-used.
        /// </summary>
        /// <param name="body">This must be valid HTML and will be the body of the e-mail</param>
        /// <param name="emailList">This must be a single string of comma delimited e-mail addresses</param>
        public static void SendEmail(string body, string subject, string emailList)
        {
            var noreplyEmail = "nreply@routeware.com";//Gmail [WDD-88]
            // Create an SMTP client with appropriate configuration and credentials 
            using (var EmailClient = new SmtpClient() { Port = 587, Host = "smtp.gmail.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential(noreplyEmail, "qxHb5419NRz%") })//Gmail [WDD-88]
            //using (SmtpClient EmailClient = new SmtpClient() { Port = 587, Host = "smtp.office365.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential("noreply@webaspx.com", "slowSc@rf22") })//Outlook
            {
                // Create a new Message from noreply addressed to the list of recipients
                using (MailMessage Email = new MailMessage(noreplyEmail, emailList)) //Gmail [WDD-88]
                //using (MailMessage Email = new MailMessage("noreply@webaspx.com", emailList)) // //Outlook
                {
                    // Set subject and body 
                    Email.Subject = subject;
                    Email.Body = body;
                    Email.IsBodyHtml = true;

                    // Attempt to send the mail, report error and hang if any exception is encountered
                    Console.WriteLine("Finished. Sending e-mail...");
                    try
                    {
                        EmailClient.Send(Email);
                        Console.WriteLine("E-mail sent successfully.");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Could not send E-mail.\n" + e.ToString());
                        while (true) ;
                    }
                }
            }
        }

        /// <summary>
        /// Sends a plain text email from noreply@webaspx.com to a comma delimited string of recipients
        /// Subject is pre-set to Client Website Trawler. Refactor as a parameter if code is re-used.
        /// </summary>
        /// <param name="body">This must be valid HTML and will be the body of the e-mail</param>
        /// <param name="emailList">This must be a single string of comma delimited e-mail addresses</param>
        public static void SendPlainTextEmail(string body, string subject, string emailList)
        {
            // Create an SMTP client with appropriate configuration and credentials 
            using (SmtpClient EmailClient = new SmtpClient() { Port = 587, Host = "smtp.office365.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential("noreply@webaspx.com", "slowSc@rf22") })
            {
                // Create a new Message from noreply addressed to the list of recipients
                using (MailMessage Email = new MailMessage("noreply@webaspx.com", emailList)) // 
                {
                    // Set subject and body 
                    Email.Subject = subject;
                    Email.Body = body;
                    Email.IsBodyHtml = false;

                    // Attempt to send the mail, report error and hang if any exception is encountered
                    Console.WriteLine("Finished. Sending e-mail...");
                    try
                    {
                        EmailClient.Send(Email);
                        Console.WriteLine("E-mail sent successfully.");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Could not send E-mail.\n" + e.ToString());
                        while (true) ;
                    }
                }
            }
        }

        /// <summary>
        /// Converts a client's urlname into their display name.
        /// Only used as a backup if DisplayName isn't available from Azure App Settings. 
        /// </summary>
        /// <param name="URLName"></param>
        /// <returns></returns>
        public static string EstimateDisplayName(string URLName)
        {
            URLName = URLName.ToLower();

            Dictionary<string, string> ClientNameExceptions = new Dictionary<string, string>
            {
                { "abc", "ABC" },
                { "ardsandnorthdown", "ArdsAndNorthDown" },
                { "causewaycoast", "CausewayCoast" },
                { "dwp", "DWP" },
                { "eastayrshire", "EastAyrshire" },
                { "eastrenfrewshire", "EastRenfrewshire" },
                { "falkstreets", "FalkStreets" },
                { "northayrshire", "NorthAyrshire" },
                { "oadbyandwigston", "OadbyAndWigston" },
                { "scottishborders", "ScottishBorders" },
                { "southnorfolk", "SouthNorfolk" },
                { "urbaserbournemouth", "UrbaserBournemouth" },
                { "westlancs", "WestLancashire" }                
            };

            if (ClientNameExceptions.ContainsKey(URLName))
                return ClientNameExceptions[URLName];

            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(URLName);
        }

        /// <summary>
        /// Computes an unserialised SHA256 encryption. 
        /// Takes an input string to encrypt, ideally a password concatenated with a unique salt cast to a string
        /// </summary>
        /// <param name="rawData">The string to be encrypted.</param>
        /// <returns>String representing a SHA256 hash</returns>
        public static string ComputeSha256Hash(string rawData)
        {
            if (string.IsNullOrEmpty(rawData))
                throw new ArgumentException("Cannot compute the hash of a null or empty string.");

            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        /// <summary>
        /// Retrieves the list of current Webaspx staff e-mail LocalParts from the Webaspx_Master database.
        /// Used for telling if an e-mail belongs to Webaspx staff if it ends in a client-accepted domain.
        /// </summary>
        /// <returns></returns>
        public static List<string> getWebaspxLocalParts()
        {
            // Define the connection to Webaspx Master
            SqlConnection connection = new SqlConnection(
                "Data Source=h2pkj6jgdx.database.windows.net;" +
                "Initial Catalog=Webaspx_Master;" +
                "User ID=mike.devine@webaspx.com@h2pkj6jgdx;" +
                "password=Web08#580;");

            // Create a Datatable to receive the result
            DataTable WebaspxLocalParts = new DataTable();

            // Fill the table with the Client details matching the URLname
            connection.Open();
            SqlCommand getCouncils = new SqlCommand("SELECT LocalPart FROM WebaspxEmails", connection);
            SqlDataAdapter adapter = new SqlDataAdapter(getCouncils);
            adapter.Fill(WebaspxLocalParts);
            connection.Close();

            // Convert the column to a list and return it
            List<string> result = new List<string>();
            foreach (DataRow row in WebaspxLocalParts.Rows)
            {
                result.Add(row["LocalPart"].ToString().ToLower());
            }
            return result;
        }

        /// <summary>
        /// Sends a test email from noreply@webaspx.com to the Dev (sam.biram@webaspx.com)
        /// Used for debugging published sites that you can't step through
        /// </summary>
        /// <param name="body">The string you want to send</param>
        public static void SendTestEmail(string body)
        {
            // Create an SMTP client with appropriate configuration and credentials 
            using (SmtpClient EmailClient = new SmtpClient() { Port = 587, Host = "smtp.office365.com", EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, Timeout = 120000, Credentials = new NetworkCredential("noreply@webaspx.com", "slowSc@rf22") })
            {
                // Create a new Message from noreply addressed to the list of recipients
                using (MailMessage Email = new MailMessage("noreply@webaspx.com", "keith.percival@webaspx.com")) // 
                {
                    // Set subject and body 
                    Email.Subject = "DPO - Test Email";
                    Email.Body = body;
                    Email.IsBodyHtml = false;

                    // Attempt to send the mail, report error and hang if any exception is encountered
                    Console.WriteLine("Finished. Sending e-mail...");
                    try
                    {
                        EmailClient.Send(Email);
                        Console.WriteLine("E-mail sent successfully.");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Could not send E-mail.\n" + e.ToString());
                        while (true) ;
                    }
                }
            }
        }

        public static void UpdateUsersLastLogin(string email, string council)
        {
            using (var conn = new DatabaseConnection(council, Catalog.Collections))
            {
                SqlCommand UpdateUser = new SqlCommand("UPDATE [DDUsers] SET LastLogin ='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' WHERE EMAIL = @Email");
                UpdateUser.Parameters.Add(new SqlParameter(parameterName: "Email", value: email));
                conn.ExecuteNonQuery(UpdateUser);
            }
        }

        public static void StoreCookie(string email, string key, string council, string portal)
        {
            using (var conn = new DatabaseConnection(council, Catalog.Collections))
            {
                // Get current token data
                SqlCommand getToken = new SqlCommand("SELECT LoginToken FROM DDUsers WHERE Email = @Email");
                getToken.Parameters.Add(new SqlParameter(parameterName: "Email", value: email));
                string tokens = conn.ExecuteScalar(getToken).ToString();
                string newTokens = "";

                //if (tokens.Contains("Collections"))
                if (tokens.Contains(portal))
                {
                    string[] splitTokens = tokens.Split(',');
                    for (int i = 0; i < splitTokens.Length; i++)
                    {
                        if (splitTokens[i].Split(':')[0] == portal)
                        {
                            newTokens += $"{portal}:{key}";
                        }
                        else
                        {
                            newTokens += splitTokens[i];
                        }

                        if (i < splitTokens.Length)
                        {
                            newTokens += ",";
                        }
                    }

                    key = newTokens;
                }
                else
                {
                    tokens += $",{portal}:{key}";
                    key = tokens;
                }
                if (key != null) 
                {
                    if (key.Contains(",,"))
                    {
                        key = key.Replace(",,,,,,", ",");
                        key = key.Replace(",,,,,", ",");
                        key = key.Replace(",,,,", ",");
                        key = key.Replace(",,,", ",");
                        key = key.Replace(",,", ",");
                    }
                    if (key.EndsWith(",")) { key = key.TrimEnd(','); }
                }

                SqlCommand storeCookie = new SqlCommand("UPDATE DDUsers SET LoginToken = @Key WHERE  Email = @Email");
                storeCookie.Parameters.Add(new SqlParameter(parameterName: "Email", value: email));
                storeCookie.Parameters.Add(new SqlParameter(parameterName: "Key", value: key));
                conn.ExecuteNonQuery(storeCookie);
            }
        }

        public static bool ValidateCookie(string email, string cookie, string council, string portal)
        {
            // If the cookie is null or not a string return false
            if (!string.IsNullOrWhiteSpace(cookie))
            {
                using (var conn = new DatabaseConnection(council, Catalog.Collections))
                {
                    // Write a query to retrieve the cookie from the DB
                    SqlCommand SeekCookie = new SqlCommand("SELECT TOP(1) LoginToken FROM [DDUsers] WHERE ID = @ID");
                    SeekCookie.Parameters.Add(new SqlParameter(parameterName: "ID", value: GetUserId(email, council)));
                    string cookieDB = Convert.ToString(conn.ExecuteScalar(SeekCookie)).ToLower();
                    string[] splitToken = cookieDB.Split(',');

                    if(cookieDB.Contains(portal))
                    {
                        string[] splitKeys = cookieDB.Split(',');
                        for (int i = 0; i < splitKeys.Length; i++)
                        {
                            if (splitKeys[i].Split(':')[0] == portal)
                            {
                                cookieDB = splitKeys[i].Split(':')[1];
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }


                    // Write a query to retrieve the LockState of the User
                    SqlCommand isLocked = new SqlCommand("SELECT TOP(1) LockState FROM DDUsers WHERE ID = @ID");
                    isLocked.Parameters.Add(new SqlParameter(parameterName: "ID", value: GetUserId(email, council)));

                   
                    string lockState = Convert.ToString(conn.ExecuteScalar(isLocked));

                    if (cookieDB == cookie.ToLower() && lockState == "0")
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------
        public static string GetUserId(string email, string council)
        {
            try
            {
                using (var conn = new DatabaseConnection(council, Catalog.Collections))
                {
                    SqlCommand getID = new SqlCommand("SELECT TOP(1) ID FROM DDUsers WHERE Email = @Email");
                    getID.Parameters.Add(new SqlParameter(parameterName: "Email", value: email));
                    string storedID = Convert.ToString(conn.ExecuteScalar(getID));
                    return storedID;
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static bool VerifyTransitionKey(string givenKey, string email, string council)
        {
            // Create the query with SQL parameters to avoid injectionss
            if (!string.IsNullOrEmpty(givenKey) && !string.IsNullOrEmpty(email))
            {
                using (var conn = new DatabaseConnection(council, Catalog.Collections))
                {
                    string userID = GetUserId(email, council);

                    SqlCommand GetKey = new SqlCommand("SELECT TOP(1) TransitionKey FROM DDUsers WHERE ID = @ID");
                    GetKey.Parameters.Add(new SqlParameter(parameterName: "ID", value: userID));
                    string storedKey = Convert.ToString(conn.ExecuteScalar(GetKey));

                    SqlCommand ObfuscateKey = new SqlCommand("UPDATE DDUsers SET TransitionKey = NEWID() WHERE ID = @ID");
                    ObfuscateKey.Parameters.Add(new SqlParameter(parameterName: "ID", value: userID));
                    conn.ExecuteNonQuery(ObfuscateKey);

                    if (storedKey.ToLower() == givenKey.ToLower())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }

            return false;
        }

        public static bool UsesDigitalDepot(string council)
        {
            //patch for Sams code to cater for if a council has more than one word
            if (council.ToUpper().Contains("ARDS")      && council.ToUpper().Contains("DOWN"))          { council = "ArdsAndNorthDown"; }            
            if (council.ToUpper().Contains("BLAENAU")   && council.ToUpper().Contains("GWENT"))         { council = "blaenaugwent"; }
            if (council.ToUpper().Contains("CAUSEWAY")  && council.ToUpper().Contains("COAST"))         { council = "CausewayCoast"; }
            if (council.ToUpper().Contains("EAST")      && council.ToUpper().Contains("AYRSHIRE"))      { council = "EastAyrshire"; }
            if (council.ToUpper().Contains("EAST")      && council.ToUpper().Contains("RENFREWSHIRE"))  { council = "EastRenfrewshire"; }
            if (council.ToUpper().Contains("FALK")      && council.ToUpper().Contains("STREETS"))       { council = "FalkStreets"; }
            if (council.ToUpper().Contains("MID")       && council.ToUpper().Contains("ULSTER"))        { council = "MidUlster"; }
            if (council.ToUpper().Contains("NORTH")     && council.ToUpper().Contains("AYRSHIRE"))      { council = "NorthAyrshire"; }
            if (council.ToUpper().Contains("OADBY")     && council.ToUpper().Contains("WIGSTON"))       { council = "OadbyAndWigston"; }
            if (council.ToUpper().Contains("SCOTTISH")  && council.ToUpper().Contains("BORDERS"))       { council = "ScottishBorders"; }
            if (council.ToUpper().Contains("SOUTH")     && council.ToUpper().Contains("NORFOLK"))       { council = "SouthNorfolk"; }
            if (council.ToUpper().Contains("TEST")      && council.ToUpper().Contains("FALKIRK"))       { council = "TestFalk"; }
            if (council.ToUpper().Contains("TEST")      && council.ToUpper().Contains("RYEDALE"))       { council = "TestRye"; }
            if (council.ToUpper().Contains("WEST")      && council.ToUpper().Contains("LANC"))          { council = "WestLancs"; }
                                                
            //try
            //{
            using (
                    var conn = new DatabaseConnection(council, Catalog.Collections))
                {
                    string usingDD = Convert.ToString(conn.ExecuteScalar("SELECT TOP(1) [useDigitalDepot] FROM [colClient]"));

                    if (usingDD == "Y")
                        return true;
                }
            //}
            //catch (Exception Ex)
            //{
            //    string errorMessage = Ex.Message.ToString();
            //    string lineNumber = "";

            //    if (Ex.StackTrace.ToString().Length > 0)
            //    {
            //        lineNumber = Ex.StackTrace.Split(' ').Last();
            //        errorMessage += " Ln:" + lineNumber;
            //    }
            //    try
            //    {
            //        string connection1 = Misc.getDbnameDbSourceDbUser(council, "Collections");
            //        string[,] colClient = Misc.getDataArrayExactFromDB("SELECT TOP(1) [useDigitalDepot] FROM [colClient]", council, connection1);
            //        if (colClient[1, 0] == "Y") { return true; }
            //    }
            //    catch
            //    {
            //        throw new NotImplementedException(errorMessage + ":" + council);
            //    }
            //}
            return false;
        }



    }
        //==================================================================================================================================================
        
        //==================================================================================================================================================
        //==================================================================================================================================================
        //==================================================================================================================================================
        //==================================================================================================================================================
        //==================================================================================================================================================
 }
