﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

    internal static class Database
    {
        private static Dictionary<string, DBConnection> connections = new Dictionary<string, DBConnection>(StringComparer.OrdinalIgnoreCase)
        {            
            { "ABC",                new DBConnection("tcp:yn19k5t3s4.database.windows.net", "mike.devine@webaspx.com@yn19k5t3s4", "Web08#580", @"2%.P|;YVSX£[]%£Z*@,TYC3&M4}./W?.?B]£$[UULG5?Z3<Y&8¦|X3[|Y^]0]5N£") },            
            { "ArdsAndNorthDown",   new DBConnection("tcp:an20170606.database.windows.net", "mike.devine@webaspx.com@an20170606", "Web08#580", @"U,*HB@,^4!*D_>¬B37!~U]B0?<<`IM.J[@3O/{&Z/L&/%¦>E£E?/1}H|]*`O¬1$|") },
            { "Bournemouth",        new DBConnection("tcp:bm20160121.database.windows.net", "mike.devine@webaspx.com@bm20160121", "Web08#580", @"<BEU8A}V¦D%K.^{¦0*¦X£^?C^¬-S5~QT¬H%,4T<AG%¦{0T9VTCC(/[X6J&~¦BQ6,") },
            { "Bromsgrove",         new DBConnection("tcp:al41tc1vv6.database.windows.net", "mike.devine@webaspx.com@al41tc1vv6", "Web08#580", @"`(JVEP¦BB%[.[MC2G(BX2MQD,?`3~/8¬!{GW914|4->_AF2EKG£3¦$EZ[P;,2*R-") },            
            { "Carlisle",           new DBConnection("tcp:cl20170906.database.windows.net", "mike.devine@webaspx.com@cl20170906", "Web08#580", @"/]ZAF!KZ|3IOX/8IGV¦~C&@$F]SOS_¬43TZ4~[<R}1%;05RT¬G.B]I5XUTAP£!T]") },
            { "Carmarthenshire",    new DBConnection("tcp:cm20160712.database.windows.net", "mike.devine@webaspx.com@cm20160712", "Web08#580", @"(@^#MGNNHP>4KKD{PNHTJ3;IRS%/V¬YK6*12]WG8]%¬5A.T>C[U8K$_UM¬4779OC") },
            { "Copeland",           new DBConnection("tcp:cp20160121.database.windows.net", "mike.devine@webaspx.com@cp20160121", "Web08#580", @"-¦E&$ISYS5}_]~W£@QA!3_,¦$?G%MLK0SM#L$TW/YG¬$&`!*2I#&X24R[`Z`X0?9") },
            { "DumfriesAndGalloway", new DBConnection("tcp:dg20210715.database.windows.net", "mike.devine@webaspx.com@dg20210715", "Web08#580", @"") },
            { "DWP",                new DBConnection("tcp:psxm02t85i.database.windows.net", "mike.devine@webaspx.com@psxm02t85i", "Web08#580", @"8Z`!2L,A^¬^9_B1£#BAL¦0B_T?@!2`#;SN|TUR@UR1U`DSY0CW£H5KZ42U@45U19") },
            { "EastAyrshire",       new DBConnection("tcp:ea20190129.database.windows.net", "mike.devine@webaspx.com@ea20190129", "Web08#580", @"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") },
            { "EastLothian",        new DBConnection("tcp:el20181101.database.windows.net", "mike.devine@webaspx.com@el20181101", "Web08#580", @"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE") },
            { "TestDWP",            new DBConnection("tcp:h2pkj6jgdx.database.windows.net", "mike.devine@webaspx.com@h2pkj6jgdx", "Web08#580", @"8Z`!2L,A^¬^9_B1£#BAL¦0B_T?@!2`#;SN|TUR@UR1U`DSY0CW£H5KZ42U@45U19") },
            { "Falkirk",            new DBConnection("tcp:al41tc1vv6.database.windows.net", "mike.devine@webaspx.com@al41tc1vv6", "Web08#580", @"{V[U/-BK5*I`4|*5D9¬M]#^]6K23E0%^O7^1MAE8!03#6.3FC<{VGY($EN%K~|`O") },
            { "FalkStreets",        new DBConnection("tcp:fs03032020.database.windows.net", "mike.devine@webaspx.com@fs03032020", "Web08#580", @"") },
            { "TestFalk",           new DBConnection("tcp:al41tc1vv6.database.windows.net", "mike.devine@webaspx.com@al41tc1vv6", "Web08#580", @"") },
            { "Glamorgan",          new DBConnection("tcp:gl20170906.database.windows.net", "mike.devine@webaspx.com@gl20170906", "Web08#580", @"_%<¦07A,%{]7QW#B83J8JPOKHJWYT|{AI{Z.#RWK~{$SO?{NP$9<I5I<6£¦6J8.U") },
            { "Halton",             new DBConnection("tcp:gyqaecvtl4.database.windows.net", "mike.devine@webaspx.com@gyqaecvtl4", "Web08#580", @"!-<YLJYG¦NV7U`]Q&3¦@.!;7/YW@Q~9SCP2F^WA0#>{¦ECU>>19TW¬T#8&L76¦}V") },
            { "Liverpool",          new DBConnection("tcp:lv20190299.database.windows.net", "mike.devine@webaspx.com@lv20190299", "Web08#580", @"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL") },
            { "MidAndEastAntrim",   new DBConnection("tcp:ma20210723.database.windows.net", "mike.devine@webaspx.com@ma20210723", "Web08#580", @"") },
            { "Monmouthshire",      new DBConnection("tcp:mm20160712.database.windows.net", "mike.devine@webaspx.com@mm20160712", "Web08#580", @"Q>?Y]#>%9M#7NM{_/?7O{$#I*,H;(D1MW}MVF[^_P&YJE,I¬0/SU160XABNB#^4K") },
            { "NorthAyrshire",      new DBConnection("tcp:na20171220.database.windows.net", "mike.devine@webaspx.com@na20171220", "Web08#580", @"_&S-3&3-<5K0??3^^/-C]MC2M[0T7*U,;`4}H1O%6LSJ{2N8GG5QV|}S6NA}<>P3") },
            { "OadbyAndWigston",    new DBConnection("tcp:ow20180904.database.windows.net", "mike.devine@webaspx.com@ow20180904", "Web08#580", @"") },
            { "Pembbrokeshire",      new DBConnection("tcp:pm20190327.database.windows.net", "mike.devine@webaspx.com@pm20190327", "Web08#580", @"") },
            { "Pembrokeshire",      new DBConnection("tcp:pm20190327.database.windows.net", "mike.devine@webaspx.com@pm20190327", "Web08#580", @"") },
            { "Rhondda",            new DBConnection("tcp:rh20171214.database.windows.net", "mike.devine@webaspx.com@rh20171214", "Web08#580", @"N>1-U2*,¬^*T¦`R¬{J59ERS¬HH(BZ<DP5;C6GXKFH¦-6BWP~Y|@0E@J98||[R.7.") },
            { "Richmondshire",      new DBConnection("tcp:rc20160712.database.windows.net", "mike.devine@webaspx.com@rc20160712", "Web08#580", @"¬V8R&S(}?SSJX4X^09V-2NQ7¬@%6J/-M(L332;*QT4[N]G^?[>{DV~O|I}2,}@I|") },
            { "Rugby",              new DBConnection("tcp:rg20170316.database.windows.net", "mike.devine@webaspx.com@rg20170316", "Web08#580", @"¦D]>VQL_6PKU_WG1(6&]#.$`£`L6Q|9;EW*1G$<&C#0I{~WZA¦;,M7^/.]R@V{¦L") },
            { "Rutland",            new DBConnection("tcp:rl20160121.database.windows.net", "mike.devine@webaspx.com@rl20160121", "Web08#580", @"RJL-YFXA`!`|]/1L@3R~.FRS££(GI1&160[?ZP5W,Q|4GBWM¦{EP#5C[G;B$~O87") },
            { "Ryedale",            new DBConnection("tcp:ry20160524.database.windows.net", "mike.devine@webaspx.com@ry20160524", "Web08#580", @"/(`0]P]*R99V£RMW.XJ-*!CL05/GA99/;/5<M{8PGX*¦J,-/S5I5S4<G?;AO>9O1") },
            { "RWTestDemo",         new DBConnection("tcp:rw20190905.database.windows.net", "mike.devine@webaspx.com@rw20190905", "Web08#580", @"") },
            { "TestRye",            new DBConnection("tcp:ry20160524.database.windows.net", "mike.devine@webaspx.com@ry20160524", "Web08#580", @"/(`0]P]*R99V£RMW.XJ-*!CL05/GA99/;/5<M{8PGX*¦J,-/S5I5S4<G?;AO>9O1") },
            { "Scarborough",        new DBConnection("tcp:sb20170320.database.windows.net", "mike.devine@webaspx.com@sb20170320", "Web08#580", @"D£5_$T2[¬JJ£%/]¦L20^DDV_P0<IX>@0#4U2M2R0Y¬N5|W>`8%66|F%1?U2-;H.9") },
            { "ScottishBorders",    new DBConnection("tcp:ens2meugcp.database.windows.net", "mike.devine@webaspx.com@ens2meugcp", "Web08#580", @"0|17$O@6J¦_-(/M¬R,&4ZA-TO¦>A,CQ}5<((]*£>2W_^@LG8NV]N!^]G*#*7AC,7") },
            { "SharedCam",          new DBConnection("tcp:nqlvv7huh8.database.windows.net", "mike.devine@webaspx.com@nqlvv7huh8", "Web08#580", @"IC¬,O.!6L]XFA{X$&(R$£*H.S¬&C<%?TSND[0SL6Z2V2#ZO<@&1DV6B!RYX]%>~N") },
            { "SouthAyrshire",      new DBConnection("tcp:sa20171214.database.windows.net", "mike.devine@webaspx.com@sa20171214", "Web08#580", @"U?TIN/%HW,>P/,C7]XK$7$£>%`¬GVS{N8$B2_`}`OBG¬UK*Z6XC31Z9S2<[DKNH$") },
            { "SouthLanarkshire",   new DBConnection("tcp:sl20171214.database.windows.net", "mike.devine@webaspx.com@sl20171214", "Web08#580", @".4|?M|-{3#@#`4&G[`PNDQY£LC6O#O<,^%#3T£Q^9¬*W/7@KP<V0/$(@_I7[,B$K") },
            { "SouthNorfolk"    ,   new DBConnection("tcp:sn20190906.database.windows.net", "mike.devine@webaspx.com@sn20190906", "Web08#580", @"") },
            { "Stirling",           new DBConnection("tcp:st20160712.database.windows.net", "mike.devine@webaspx.com@st20160712", "Web08#580", @"^;CL7H~U2KR08U%{VL¦/L_0!#T;33F#IBN}O£.3#SUH>!*P&¬£7``£L/B¬^R6M#G") },
            { "Swansea",            new DBConnection("tcp:sw20170320.database.windows.net", "mike.devine@webaspx.com@sw20170320", "Web08#580", @"$|[$AO[E*JZI/<$3?XMB<|85R.S¦/!`R#L3W/]<A54O(/3£M`_[;G,],589{Q{`P") },
            { "Swindon",            new DBConnection("tcp:sw20160712.database.windows.net"," mike.devine@webaspx.com@sw20160712", "Web08#580", @";[E4M}H9-,{1X[7MUVV<I¦E£9H]T!£%T¬B|7T7.,P7Q9~4^06@TO#E4]#2MHV4W$") },
            { "SwnTest",            new DBConnection("tcp:sw20160712.database.windows.net"," mike.devine@webaspx.com@sw20160712", "Web08#580", @";[E4M}H9-,{1X[7MUVV<I¦E£9H]T!£%T¬B|7T7.,P7Q9~4^06@TO#E4]#2MHV4W$") },
            { "Testing",            new DBConnection("tcp:ts20190821.database.windows.net", "mike.devine@webaspx.com@ts20190821", "Web08#580", @"4(R0$0&K*MT]R?B`IVMO!%?5[>X{W/G/@V!¦41%,A505-/]*V#}B¦.>~¦0*Q``£>") },
            { "Torridge",           new DBConnection("tcp:tr20180206.database.windows.net", "mike.devine@webaspx.com@tr20180206", "Web08#580", @"4(R0$0&K*MT]R?B`IVMO!%?5[>X{W/G/@V!¦41%,A505-/]*V#}B¦.>~¦0*Q``£>") },
            { "TestTor",            new DBConnection("tcp:tr20180206.database.windows.net", "mike.devine@webaspx.com@tr20180206", "Web08#580", @"4(R0$0&K*MT]R?B`IVMO!%?5[>X{W/G/@V!¦41%,A505-/]*V#}B¦.>~¦0*Q``£>") },
            { "UrbaserBournemouth", new DBConnection("tcp:ub20161121.database.windows.net", "mike.devine@webaspx.com@ub20161121", "Web08#580", @"E_(¦B¦?^C7ITM!U~4XMQ%!W8<[¦R5¦P%VRBG8D*@~£]£9KH>2{R<JAQHX>,;£327") },
            { "Veolia",             new DBConnection("tcp:h2pkj6jgdx.database.windows.net", "mike.devine@webaspx.com@h2pkj6jgdx", "Web08#580", "UDZQE;P?_3|OM%{`G{£VVUO7H{?G`W;LY{T}\".LP-2DGK}F¦$]!,M8^MT#||/KYE") },
            { "WestLancs",          new DBConnection("tcp:wl20160121.database.windows.net", "mike.devine@webaspx.com@wl20160121", "Web08#580", @"¦[9^AD!>1@%;~X6?!#!£D17<^(D[8.P$G9JR&M$@1£¦LMG&?$9-S!|2P.Y[0<KMX") },
            { "Wrexham",            new DBConnection("tcp:yx3plf20le.database.windows.net", "mike.devine@webaspx.com@yx3plf20le", "Web08#580", @".YJF>$D£¬^P;MN]4LJX}RDA%(J`*VF|-126ALXQ826VV<Z#0F8LL.¦W20H|SGN_V") },
            { "York",               new DBConnection("tcp:yk20190821.database.windows.net", "mike.devine@webaspx.com@yk20190821", "Web08#580", @".YJF>$D£¬^P;MN]4LJX}RDA%(J`*VF|-126ALXQ826VV<Z#0F8LL.¦W20H|SGN_V") },
            { "WMCloud",            new DBConnection("tcp:wmcoct2017.database.windows.net", "mike.devine@webaspx.com@wmcoct2017", "Web08#580", @"V%G}¬N_ZP}HO4T*£6!RP6£-;OZMRE6W&%B67L/Q9ZKB@E{*1>/9QG87^K}EOPJ~%") },
        };

        internal static bool Contains(string council)
        {
            return connections.ContainsKey(council);
        }

        internal static string GetKeyPassword(string council)
        {
            return connections[council].KeyPassword;
        }

        internal static SqlConnection GetSqlConnection(string council, string catalog)
        {
            DBConnection data = connections[council];
            var builder = new SqlConnectionStringBuilder();
            builder.UserID = data.User;
            builder.TrustServerCertificate = true;
            builder.InitialCatalog = catalog;
            builder.Password = data.Password;
            builder.DataSource = data.Server;
            return new SqlConnection(builder.ConnectionString);
        }

        internal static string[] GetEncryptedColumns(string council, string catalog, string q)
        {
            string tables = "";

            var columns = new List<string>();
            StringBuilder query = new StringBuilder(q.ToUpper());

            int i = query.ToString().IndexOf("'");

            // we need to remove the quoted items otherwise they will be picked up if they contain SELECT...
            while (i != -1)
            {
                int ii = query.ToString().IndexOf("'", i + 1);
                if (ii != -1)
                {
                    query.Remove(i, ii - i + 1);
                    i = query.ToString().IndexOf("'");
                }
            }

            i = query.ToString().IndexOf("FROM");

            while (i != -1)
            {
                int ii = -1;
                if (query.Length > i + 15)
                {
                    ii = query.ToString().IndexOf("SELECT", i, 15);
                }
                else
                {
                    ii = query.ToString().IndexOf("SELECT", i);
                }

                int vi = -1;
                vi = query.ToString().IndexOf(";", i);

                if (ii != -1)
                {
                    i = query.ToString().IndexOf("FROM", i + 1);
                }
                else
                {
                    int I = query.ToString().IndexOf("WHERE", i);
                    if (I != -1)
                    {
                        tables += "," + query.ToString().Substring(i + 4, I - 4 - i).Trim();
                        i = query.ToString().IndexOf("FROM", I + 1);
                    }
                    else
                    {
                        if (vi == -1)
                        {
                            tables += "," + query.ToString().Substring(i + 4).Trim();
                        }
                        else
                        {
                            tables += "," + query.ToString().Substring(i + 4, vi - (i + 4)).Trim();
                        }
                        i = query.ToString().IndexOf("FROM", i + 1);
                    }
                }
            }

            // insert ----------------------------------------
            i = query.ToString().IndexOf("INSERT");
            while (i != -1)
            {
                if (query.Length > 14)
                {
                    i = query.ToString().IndexOf("INTO", i, 14);
                }
                else
                {
                    i = query.ToString().IndexOf("INTO", i);
                }

                if (i != -1)
                {
                    int ii = query.ToString().IndexOf("(", i);
                    if (ii != -1)
                    {
                        tables += "," + query.ToString().Substring(i + 4, ii - (i + 4)).Trim();
                    }

                    i = query.ToString().IndexOf("INSERT", i);
                }
            }

            // update --------------------------------------
            i = query.ToString().IndexOf("UPDATE");
            while (i != -1)
            {
                int ii = query.ToString().IndexOf("SET", i);
                if (ii != -1)
                {
                    tables += "," + query.ToString().Substring(i + 6, ii - i - 6).Trim();
                }
                i = query.ToString().IndexOf("UPDATE", i + 1);
            }

            if (tables.StartsWith(","))
            {
                tables = tables.Remove(0, 1);
            }

            /*query = query.ToUpper();
            int I = query.IndexOf("FROM");

            string tables = "";
            while (I != -1)
            {
                int II = -1;

                if (query.Length > I + 15)
                {
                    II = query.IndexOf("SELECT", I, 15);
                }
                else
                {
                    II = query.IndexOf("SELECT", I);
                }

                if (II != -1)
                {
                    I = query.IndexOf("FROM", I + 1);
                }
                else
                {
                    int III = query.IndexOf("WHERE", I);
                    if (III != -1)
                    {
                        tables = query.Substring(I + 4, III - 4 - I);
                        I = query.IndexOf("FROM", I + 1);
                    }
                    else
                    {
                        tables = query.Substring(I + 4);
                        I = query.IndexOf("FROM", I + 1);
                    }
                }
            }*/

            if (!String.IsNullOrWhiteSpace(tables))
            {
                if (tables.EndsWith(";"))
                {
                    tables = tables.Remove(tables.Length - 1);
                }

                tables = tables.Trim();
                StringBuilder qu = new StringBuilder("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME= ");

                string[] tables2 = tables.Split(',');
                for (int iii = 0; iii < tables2.Length; iii++)
                {
                    qu.Append(String.Format(" '{0}' OR TABLE_NAME=", tables2[iii]));
                }

                qu.Remove(qu.Length - 14, 14);

                using (SqlConnection conn = Database.GetSqlConnection(council, catalog))
                {
                    conn.Open();
                    using (var command = new SqlCommand(qu.ToString(), conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string col = Convert.ToString(reader[0]);
                                    if (col.StartsWith("¦"))
                                    {
                                        columns.Add(col.Remove(0, 1));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return columns.ToArray();
        }
    }

    internal class DBConnection
    {
        public DBConnection(string server, string user, string password, string keyPassword)
        {
            Server = server;
            User = user;
            Password = password;
            KeyPassword = keyPassword;
        }

        public string Server { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public string KeyPassword { get; set; }
    }

