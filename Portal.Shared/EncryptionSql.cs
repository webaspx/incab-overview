﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using Webaspx.Database.Rebuild;
using Webaspx.Database;



public static class EncryptionSql
{
    public static string RebuildUpdateQuery(string query, string c, string ca)
    {
        //var rebuilder = new SqlRebuilder(query, c, ca);   //Edit out KP 05Aug2019
        //return rebuilder.RebuildEncryption();             //Edit out KP 05Aug2019
        return query;

        /*

        if (String.IsNullOrEmpty(query)) { return query; }
        if (!Database.Contains(c)) { return query; }
        if (query.Contains("OPEN SYMMETRIC KEY")) { return query; }

        string[] ecols = Database.GetEncryptedColumns(c, ca, query);

        if (ecols.Length == 0)
        {
            return query;
        }

        var q = new StringBuilder(query);

        string column = null;
        bool quote = false;
        int brackets = 0;
        bool create = false;
        bool encrypted = false;
        bool where = false;

        int ii = 0;
        int iii = 0;

        // UPDATE TABLE SET Column = (SELECT
        StringBuilder term = new StringBuilder();
        StringBuilder value = new StringBuilder();

        for (int i = 0; i < q.Length; i++)
        {
            // we need to create the query value.
            if (q[i] == ' ')
            {
                if (create)
                {
                    value.Append(" ");
                }
            }
            else
            {
                if (create)
                {
                    value.Append(q[i]);
                }
            }

            if (q[i] == '\'')
            {
                bool run = true;
                if (quote)
                {
                    if (q.Length > i + 1 && q[i + 1] == '\'')
                    {
                        run = false;
                    }
                    else if (i - 1 > 0 && q[i - 1] == '\'')
                    {
                        run = false;
                    }
                }

                if (run)
                {
                    quote = !quote;
                }
            }
            else
            {
                if (q[i] == '(')
                {
                    brackets++;
                }

                if ((q[i] == ';' || i == q.Length - 1) && where && !quote && brackets == 0)
                {
                    UpdateRebuildWhere(value.ToString(), c, ca, q, ecols, ref ii, ref iii, ref i, ref encrypted);
                    where = false;
                }
                else if ((q[i] == '=' || q[i] == ',' || q[i] == ';' || term.ToString().ToUpper() == "WHERE") && column != null && !quote && brackets == 0)
                {
                    // handle the built query.
                    if (q[i] == ',' || (q[i] == ';' && !where) || term.ToString().ToUpper() == "WHERE")
                    {
                        create = false;

                        string val = value.ToString().Trim();
                        if (term.ToString().ToUpper() == "WHERE")
                        {
                            // there was a where we don't need to check for semi-colon.
                            where = true;
                            create = true; // create the where
                            val = val.Remove(val.Length - 5, 5);
                        }
                        else
                        {
                            val = val.Remove(val.Length - 1, 1);
                        }

                        // here is the value.
                        RebuildValue(column, val, c, ca, q, ecols, ref ii, ref iii, ref i, ref encrypted);

                        if (where == true && term.ToString().ToUpper() == "WHERE")
                        {
                            // we are here.
                            iii = i;
                        }
                    }
                    else if (q[i] == ';' && where)
                    {
                        // this is the end of our current statement.
                        // where = false;
                    }
                    else
                    {
                        create = true;
                    }

                    value.Remove(0, value.Length);
                }

                // build the column name.
                if ((term.ToString().ToUpper() == "SET" || q[i] == ',') && !quote)
                {
                    ii = i;
                    iii = q.ToString().IndexOf("=", ii);

                    if (q[i] == ',')
                    {
                        ii++;
                    }

                    column = q.ToString().Substring(ii, iii - ii);
                }


                if (q[i] == ')')
                {
                    brackets--;
                }

                // clear the term.
                if (q[i] == ' ')
                {
                    term.Remove(0, term.Length);
                }
                else
                {
                    term.Append(q[i]);
                }
            }
        }

        if (encrypted)
        {
            return String.Format(@"OPEN SYMMETRIC KEY SSN_Key_1 DECRYPTION BY PASSWORD = '{0}'; {1}", Database.GetKeyPassword(c), q.ToString());
        }
        else
        {
            return query;
        }*/
    }

    /*private static void UpdateRebuildWhere(string where, string c, string ca, StringBuilder q, string[] ecols, ref int ii, ref int iii, ref int i, ref bool encrypted)
    {
        // remove the quotes
        int iv = where.IndexOf('\'');
        while (iv != -1)
        {
            int v = where.IndexOf('\'', iv + 1);
            if (v != -1)
            {
                where = where.Remove(iv, v - iv + 1);
            }
            iv = where.IndexOf('\'');
        }

        var term = new StringBuilder();
        for (int v = 0; v < where.Length - 1; v++)
        {
            for (int vi = 0; vi < ecols.Length; vi++)
            {
                string col = term.ToString().ToLower().Trim();

                if (col == "like" || col == "not" || col == "and" || col == "or")
                {
                    continue;
                }

                if (col.StartsWith("[") && col.EndsWith("]"))
                {
                    col.Remove(0, 1);
                    col.Remove(col.Length - 1, 1);
                }

                if (col == ecols[vi].ToLower())
                {
                    string ncol = String.Format("CONVERT(varchar(MAX), DECRYPTBYKEY([¦{0}]))", ecols[vi]);
                    q.Replace(term.ToString(), ncol, iii, iii + v + 1 - iii);

                    int d = ncol.Length - col.Length;
                    i += d;
                }
            }

            if (where[v] == ' ')
            {
                term.Remove(0, term.Length);
            }
            else
            {
                term.Append(where[v]);
            }
        }
    }
    */
    /*private static void RebuildValue(string col, string val, string council, string catalog, StringBuilder q, string[] ecols, ref int ii, ref int iii, ref int i, ref bool encrypted)
    {
        string val_temp = val;
        val = val.Trim();

        string s = val.Replace(" ", "").ToUpper();
        if (s.StartsWith("(SELECT") || s.StartsWith("SELECT"))
        {
            if (s.StartsWith("(") && s.EndsWith(")"))
            {
                val = val.Remove(val.Length - 1);
            }

            int iv = val.IndexOf("SELECT");
            val = val.Remove(0, iv);

            string select = RebuildSelectQuery(val, council, catalog, false);

            if (select.Contains("DECRYPT") || select.Contains("ENCRYPT"))
            {
                encrypted = true;
            }

            select = "(" + select + ") ";
            select = String.Format("ENCRYPTBYKEY(Key_GUID('SSN_Key_1'), CONVERT(varchar(MAX), {0}))", select);

            q.Replace(val_temp, select, ii + col.Length, i - ii - col.Length);

            int d = 0;

            // select should be higher but may not be due to the trim.
            if (select.Length > val_temp.Length)
            {
                d = select.Length - val_temp.Length;
            }

            i += d;

        }
        else
        {
            // not a select
            for (int vi = 0; vi < ecols.Length; vi++)
            {
                if (ecols[vi].ToLower() == col.ToLower().Trim())
                {
                    // encrypt the value
                    val_temp = String.Format("ENCRYPTBYKEY(Key_GUID('SSN_Key_1'), CONVERT(varchar(MAX), {0}))", val);

                    q.Replace(val, val_temp, ii + col.Length, i - ii - col.Length);

                    int d = val_temp.Length - val.Length;
                    i += d;

                    encrypted = true;

                    break;
                }
            }
        }

        col = col.Trim();

        // change the column name.
        for (int vi = 0; vi < ecols.Length; vi++)
        {
            if (ecols[vi].ToLower() == col.ToLower().Trim())
            {
                string ncol = "[¦" + col + "]";
                q.Replace(col, ncol, ii, iii - ii);

                i += ncol.Length - col.Length;

                break;
            }
        }
    }*/

    public static string RebuildSelectQuery(string query, string council, string catalog, bool key = true)
    {
        //var rebuilder = new SqlRebuilder(query, council, catalog);    //KP edit Out 05Aug19
        //return rebuilder.RebuildEncryption();                         //KP edit Out 05Aug19
        return query;

        /*if (String.IsNullOrEmpty(query)) { return query; }
        if (!Database.Contains(council)) { return query; }
        if (query.Contains("OPEN SYMMETRIC KEY")) { return query; }

        string[] ecols = Database.GetEncryptedColumns(council, catalog, query);

        if (ecols.Length == 0)
        {
            return query;
        }

        bool enc = false;
        var done = new List<string>();
        var sbquery = new StringBuilder(query);
        var qchars = new StringBuilder();
        bool quote = false;
        bool where = false;
        int I = 0;
        int II = 0;

        for (int i = 0; i < sbquery.Length; i++)
        {
            if (sbquery[i] == '\'')
            {
                I = i + 1;
                II = i + 1;

                bool run = true;
                if (quote)
                {
                    if (sbquery.Length > i + 1 && sbquery[i + 1] == '\'')
                    {
                        run = false;
                    }
                    else if (i - 1 > 0 && sbquery[i - 1] == '\'')
                    {
                        run = false;
                    }
                }

                if (run)
                {
                    quote = !quote;
                }

            }
            else if (qchars.ToString().ToUpper() == "WHERE")
            {
                where = true;

                qchars.Remove(0, qchars.Length);

                II++;
                I = II;
            }
            else if (qchars.ToString().ToUpper() == "SELECT")
            {
                // another select means the where is over.
                where = false;
                qchars.Remove(0, qchars.Length);

                II++;
                I = II;
            }
            else if (qchars.ToString() == "*")
            {
                ReplaceAsterisk(sbquery, I, ref II, ref i, council, catalog, ecols, ref enc);

                II++;
                I = II;
                qchars.Remove(0, qchars.Length);
            }
            else
            {
                // we don't want anything inside of quote marks.
                if (!quote)
                {
                    if (sbquery[i] == ',' || sbquery[i] == ' ' || sbquery[i] == ')' || sbquery[i] == '(' || sbquery[i] == '=')
                    {
                        string term = qchars.ToString();
                        if (!String.IsNullOrEmpty(term))
                        {
                            int iii = term.IndexOf(".");
                            string col = term.Substring(iii + 1, term.Length - iii - 1);
                            ReplaceEncryptedColumn(term, col, sbquery, I, ref II, term.Contains("."), ref i, where, ecols, done, ref enc);
                        }

                        II++;
                        I = II;
                        qchars.Remove(0, qchars.Length);
                    }
                    else
                    {
                        qchars.Append(sbquery[i]);
                        II++;
                    }
                }
            }
        }

        // only open the key if an encryption was done.
        if (enc)
        {
            if (key)
            {
                return String.Format(@"OPEN SYMMETRIC KEY SSN_Key_1 DECRYPTION BY PASSWORD = '{0}'; {1}", Database.GetKeyPassword(council), sbquery.ToString());
            }
            else
            {
                return sbquery.ToString();
            }
        }
        else
        {
            return sbquery.ToString();
        }*/
    }
    public static string RebuildDeleteQuery(string query, string council, string catalog, bool key = true)
    {
        //var rebuilder = new SqlRebuilder(query, council, catalog);///KP edit Out 05Aug19
        //return rebuilder.RebuildEncryption();///KP edit Out 05Aug19
        return query;

        /*if (String.IsNullOrEmpty(query)) { return query; }
        if (!Database.Contains(council)) { return query; }
        if (query.Contains("OPEN SYMMETRIC KEY")) { return query; }
        return query;*/
    }

    /*private static void ReplaceEncryptedColumn(string term, string col, StringBuilder q, int I, ref int II, bool dot, ref int i, bool where, string[] ecols, List<string> done, ref bool enc)
    {
        col = col.Replace(" ", "");

        // lets go through each word.
        for (int ii = 0; ii < ecols.Length; ii++)
        {
            string ecol = ecols[ii].ToLower();

            if (ecol == col.ToLower() || (ecol.Contains(".") && dot && ecol == term.ToLower()))
            {
                if (!where)
                {
                    enc = true;
                    if (dot)
                    {
                        // does dot need square brackets for sql terms?
                        string newterm = term.Replace(col, "[¦" + ecols[ii] + "]");
                        if (ecols[ii].Contains("."))
                        {
                            newterm = ecols[ii].Replace(col, "[¦" + col + "]");
                        }

                        newterm = String.Format("CONVERT(varchar(MAX), DECRYPTBYKEY({0})) as [{1}]", newterm, col);

                        int iii = newterm.Length - term.Length;
                        q.Replace(term, newterm, I, II - I);
                        i += iii;
                        II += iii;

                    }
                    else
                    {
                        string newterm = String.Format("CONVERT(varchar(MAX), DECRYPTBYKEY([{0}])) as [{1}]", "¦" + ecols[ii], col);
                        int iii = newterm.Length - term.Length;
                        q.Replace(term, newterm, I, II - I);
                        i += iii;
                        II += iii;

                    }

                    done.Add(col.ToLower());
                }
                else
                {
                    // we need to replace where statements but only if they haven't been obtained in the select.
                    enc = true;
                    if (!done.Contains(col.ToLower()))
                    {
                        string newterm = String.Format("CONVERT(varchar(MAX), DECRYPTBYKEY([{0}]))", "¦" + ecols[ii], col);
                        int iii = newterm.Length - term.Length;
                        q.Replace(term, newterm, I, II - I);
                        i += iii;
                        II += iii;
                    }
                }

                return;
            }

        }
    }
    */
    /* private static void ReplaceAsterisk(StringBuilder q, int I, ref int II, ref int i, string council, string catalog, string[] ecols, ref bool enc)
     {
         // check if the table is encrypted.
         bool tb_enc = true;
         if (tb_enc)
         {
             string tbls = "";

             // we will use 10 to prevent other selects / wheres from been found.
             int III = q.ToString().ToUpper().IndexOf("FROM", II, 10);

         retry2:
             if (III != -1)
             {
                 // if theres a select near by then it is a from (select.
                 int V = -1;
                 if (III + 10 < q.Length)
                 {
                     V = q.ToString().ToUpper().IndexOf("SELECT", III, 14);
                 }
                 int VI = q.ToString().ToUpper().IndexOf("WHERE", III);

                 if (V != -1)
                 {
                     // DON'T REPLACE THE asterisk
                     return;

                     // go to the next from
                     //III = q.ToString().ToUpper().IndexOf("FROM", III + 4);
                     //goto retry2;
                 }
                 else
                 {
                     int ii = q.ToString().IndexOf(";", III);

                     if (VI != -1)
                     {
                         tbls = q.ToString().Substring(III + 4, VI - III - 4);
                     }
                     else
                     {
                         if (ii == -1)
                         {
                             tbls = q.ToString().Substring(III + 4);
                         }
                         else
                         {
                             tbls = q.ToString().Substring(III + 4, ii - (III + 4));
                         }
                     }
                 }
             }

             tbls = tbls.Trim();
             if (tbls.EndsWith(";"))
             {
                 tbls = tbls.Remove(tbls.Length - 1);
             }

             string[] tables = tbls.Split(',');

             // create the where conditions
             string qtbl = "";
             for (int ii = 0; ii < tables.Length; ii++)
             {
                 if (!String.IsNullOrEmpty(tables[ii]))
                 {
                     qtbl += "'" + tables[ii] + "' OR";
                 }
             }

         retry:
             if (qtbl.Length > 0)
             {
                 qtbl = qtbl.Remove(qtbl.Length - 2);
             }
             //goto retry;

             var cols = new List<string>();
             StringBuilder ask = new StringBuilder();
             using (SqlConnection conn = Database.GetSqlConnection(council, catalog))
             {
                 conn.Open();

                 // string tbls = "";
                 using (var command = new SqlCommand(String.Format("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME= {0}", qtbl), conn))
                 {
                     using (SqlDataReader reader = command.ExecuteReader())
                     {
                         if (reader.HasRows)
                         {
                             while (reader.Read())
                             {
                                 string col = Convert.ToString(reader[0]);
                                 if (col.StartsWith("¦"))
                                 {
                                     col = col.Remove(0, 1);
                                 }

                                 if (!cols.Contains(col))
                                 {
                                     cols.Add(col);

                                     if (ecols.Contains(col))
                                     {
                                         col = String.Format("CONVERT(varchar(MAX), DECRYPTBYKEY([¦{0}])) as [{0}]", col);
                                         enc = true;
                                     }
                                     ask.Append(col + ",");
                                 }
                             }

                             ask.Remove(ask.Length - 1, 1);
                         }
                     }
                 }
             }

             int iii = ask.Length - (II - I);
             q.Replace("*", ask.ToString(), I, II - I);
             i += iii;
             II += iii;
         }
     }
     */
    public static string RebuildInsertQuery(string query, string council, string catalog)
    {
        //var rebuilder = new SqlRebuilder(query, council, catalog);    ///KP edit Out 05Aug19
        //return rebuilder.RebuildEncryption();                         ///KP edit Out 05Aug19
        return query;
        /*
        if (String.IsNullOrEmpty(query)) { return query; }
        if (!Database.Contains(council)) { return query; }
        if (query.Contains("OPEN SYMMETRIC KEY")) { return query; }

        string[] ecols = Database.GetEncryptedColumns(council, catalog, query);

        if (ecols.Length == 0)
        {
            return query;
        }

        bool is_enc = false;
        bool quote = false;
        bool values = false;
        int ii = 0;
        int iii = 0;

        int ienc = 0;
        int brackets = 0;

        var enc = new List<int>();
        var q = new StringBuilder(query);
        var term = new StringBuilder();

        for (int i = 0; i < q.Length; i++)
        {
            if (q[i] == '\'')
            {
                bool run = true;
                if (quote)
                {
                    if (q.Length > i + 1 && q[i + 1] == '\'')
                    {
                        run = false;
                    }
                    else if (i - 1 > 0 && q[i - 1] == '\'')
                    {
                        run = false;
                    }
                }

                if (run)
                {
                    quote = !quote;
                }
            }
            else
            {
                if (q[i] == '(')
                {
                    brackets++;
                }

                if (!quote)
                {
                    string col = null;
                    if (term.ToString().ToUpper() == "INSERT")
                    {
                        values = false;

                        ienc = -1;
                        iii = q.ToString().IndexOf(')', i);
                        ii = q.ToString().IndexOf("(", i);
                    }
                    else if (term.ToString().ToUpper() == "VALUES")
                    {
                        values = true;

                        ienc = -1;
                        iii = q.ToString().IndexOf(')', i);
                        ii = q.ToString().IndexOf("(", i);
                    }

                    if (q[i] == ')' && !values)
                    {
                        if (ii != -1 && brackets == 1)
                        {
                            ienc++;

                            // get last
                            col = q.ToString().Substring(ii + 1, i - ii - 1);

                            iii = -1;
                        }
                    }
                    else if (q[i] == ',' && !values)
                    {
                        ienc++;
                        col = q.ToString().Substring(ii + 1, i - ii - 1);
                    }

                    if (!String.IsNullOrEmpty(col))
                    {
                        int d = 0;

                        string col2 = col.Trim();
                        for (int vi = 0; vi < ecols.Length; vi++)
                        {
                            if (ecols[vi].ToLower() == col2.ToLower())
                            {
                                is_enc = true;

                                string newt = "[¦" + col2 + "]";
                                q.Replace(col2, newt, ii + 1, i - ii - 1);

                                d = newt.Length - col.Length;
                                i += d;

                                enc.Add(ienc);

                                break;
                            }
                        }


                        if (iii == -1)
                        {
                            ii = -1;
                        }
                        else
                        {
                            ii += col.Length + 1 + d;
                        }

                    }
                }


                // here we replace the values with the encryption command.
                if (values)
                {
                    int d = 0;
                    string value = null;
                    if (q[i] == ',')
                    {
                        if (brackets == 1)
                        {
                            value = q.ToString().Substring(ii + 1, i - ii - 1);
                            ienc++;
                        }
                        else if (brackets == 0)
                        {
                            values = true;

                            ienc = -1;
                            iii = q.ToString().IndexOf(')', i);
                            ii = q.ToString().IndexOf("(", i);
                        }
                    }
                    else if (q[i] == ')')
                    {
                        // we need to check we are in a single bracket as inserts may contain commands which also use brackets.
                        if (brackets == 1)
                        {
                            value = q.ToString().Substring(ii + 1, i - ii - 1);
                            ienc++;
                        }
                    }

                    if (!String.IsNullOrEmpty(value))
                    {
                        if (enc.Contains(ienc))
                        {
                            string newt = String.Format("ENCRYPTBYKEY(Key_GUID('SSN_Key_1'), CONVERT(varchar(MAX), {0}))", value);

                            q.Replace(value, newt, ii + 1, i - ii - 1);

                            d = newt.Length - value.Length;
                            i += d;
                        }

                        if (iii == -1)
                        {
                            ii = -1;
                        }
                        else
                        {
                            ii += value.Length + 1 + d;
                        }
                    }


                }

                if (q[i] == ')')
                {
                    brackets--;
                }

                // clear the term.
                if (q[i] == ' ')
                {
                    term.Remove(0, term.Length);
                }
                else
                {
                    term.Append(q[i]);
                }
            }
        }

        if (is_enc)
        {
            return String.Format(@"OPEN SYMMETRIC KEY SSN_Key_1 DECRYPTION BY PASSWORD = '{0}'; {1}", Database.GetKeyPassword(council), q.ToString());
        }
        else
        {
            return query;
        }

        return null;*/
    }
}


