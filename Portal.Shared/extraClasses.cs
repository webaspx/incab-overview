﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Net;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Text;
using System.IO;
//using System.Web.UI.HtmlControls;
//using System.Web.Services;
//using Newtonsoft.Json.Linq;
using System.Data;
using System.Globalization;

/// <summary>
/// Extra Classes shared functions between the pages
/// </summary>
public class extraClasses
{
    public extraClasses()
    {

    }
    //================================================================================================================================================================================================================
    /// <summary>
    /// <para>reads data and generates the CalendarAndCostsData class for the Calendar and trade info</para>
    /// <para>for the number of Months specified with noOfMonths or between start and end dates</para>    
    /// <para>The results are the converted to HTML for return and population of Divs on the form from Ajax</para>
    /// <para>WILL ONLY WORK FOR WAXROUNDNEW</para>
    /// </summary>          
    public static string getCollectionsDatesForUPRN(string uprn, string email, string councilName, string calendarStartDDMMYYYY, string calendarEndDDMMYYYY, bool returnBreakdownByContainer, bool returnBreakdownByService, bool returnMismatches, bool returnDateSelect, bool returnTradeCosts, bool returnFriendlyNextCollection)
    {
        //This is also used by exteranl windows Forms app for Bulk trade invoices
        colClient clientDetails = colClient.GetClientParameters(councilName);
        string connection = null;
        CalendarAndCostsData calendarInfo = getCalendarAndInvoiceData(uprn, email, councilName, calendarStartDDMMYYYY, calendarEndDDMMYYYY, false, clientDetails, connection, returnFriendlyNextCollection);
        
        string[,] calendarArray   = calendarInfo.breakDownByContainerArray;
        string[,] calendarArr     = calendarInfo.breakDownByServiceArray;
        string[,] tradeCosts      = calendarInfo.tradeCostsArray;
        string[,] additionalCosts = calendarInfo.additionalCostsArray;
        bool hasTradeContainer    = calendarInfo.hasTradeContainer;
        string[] clientServices   = calendarInfo.clientServices;
        DateTime startDate        = calendarInfo.startDate;
        DateTime endDate          = calendarInfo.endDate;
        string origCalStart       = calendarInfo.origCalStart;
        string[,] rounds          = calendarInfo.rounds;
        string[,] containers      = calendarInfo.containers;
        string[] nextCollections  = calendarInfo.nextCollectionDates;
        string[,] tData           = calendarInfo.tradeData;
        string[] otherInfo        = calendarInfo.otherInfo;

        //Write out HTML from Arrays
        StringBuilder html = new StringBuilder();
        //string HTML = "";
        #region Write tradeCosts To HTML
        if (hasTradeContainer && returnTradeCosts&&clientDetails.hasTrade && tradeCosts[0,0]!=null)
        {
            html.Append("<div id='divCosts'>");
            if (returnTradeCosts)
            {
                string section = "TradeCosts";
                string infoDivName = "divInfo" + section;
                string infoButton = "<img width='20' height='20' title='Click for more info' style='vertical-align: middle;' onclick=\"showInfo('" + section + "');\" src='img/Buttons/infoYellow.png'>";
                html.Append("<div id='" + infoDivName + "' class='infoDiv'></div>");
                html.Append("<table>");
                html.Append("<tr><th colSpan='" + (tradeCosts.GetLength(1)) + "'>Trade Costs " + infoButton + "</th></tr>");
                html.Append("<tr>");
                html.Append("<td class='th2' colSpan='2'></td>");
                html.Append("<td class='th2' colSpan='4'>Containers</td>");
                html.Append("<td class='th2' colSpan='1'>Rounds</td>");
                html.Append("<td class='th2' colSpan='6'>Property</td>");
                html.Append("<td class='th2' colSpan='7'>Pricing</td>");                
                html.Append("<td class='th2' colSpan='2'>Tot Dispose Collect</td>");
                html.Append("<td class='th2' colSpan='1'></td>");
                html.Append("<td class='th2' colSpan='2'>Range</td>");
                html.Append("<td class='th2' colSpan='5'></td>");                
                connection = Misc.getDbnameDbSourceDbUser(clientDetails.clientName, "Collections");
                string[,] tBinsData = Misc.getDataArrayExactFromDB("Select id, Payment_ref from bins where uprn='" + uprn + "' and bin_Number like '%TRADE%'", clientDetails.clientName, connection);
                html.Append("</tr>");
                for (int c = 0; c < tradeCosts.GetLength(0); c++)
                {
                    string payRef = "";
                    html.Append("<tr>");
                    for (int j = 0; j < tradeCosts.GetLength(1); j++)
                    {
                        html.Append("<td");
                        if (c == 0) { html.Append(" class=\"thMid\""); }
                        html.Append(">");
                        if (tradeCosts[c, j] == null) { tradeCosts[c, j] = ""; }
                        if (j == 0)
                        {
                            //append payref if present re Ryedale Ampleforth College 010007634301
                            
                            for (int b = 1; b < tBinsData.GetLength(0); b++)
                            {                                
                                if (tradeCosts[c, 0] == tBinsData[b,0]) 
                                {
                                    if (tBinsData[b,1] != "NotRequestedInSQL")
                                    {
                                        payRef = "<span style='font-size:9px'>" + tBinsData[b,1] + "</span>";
                                        break;
                                    }
                                }
                            }
                            tradeCosts[c, 0] += "#" + payRef;
                        }
                        html.Append(tradeCosts[c, j].Replace("#", "<br/>"));
                        
                        html.Append("</td>");
                    }
                    html.Append("</tr>");
                }
                if (tradeCosts.GetLength(0) > 12) 
                {
                    //add header row again
                    html.Append("<tr>");
                    for (int j = 0; j < tradeCosts.GetLength(1); j++) 
                    {
                        html.Append("<td class=\"thMid\">");
                        html.Append(tradeCosts[0, j].Replace("#", "<br/>"));
                        html.Append("</td>");
                    }
                    html.Append("</tr>");
                }
                html.Append("<tr><td colSpan='" + (tradeCosts.GetLength(1)) + "' class='tableFoot'>Trade Costs " + infoButton + "</td></tr>");
                html.Append("</table>");
                //populate additional collections
                if (additionalCosts.GetLength(0) > 1) 
                {
                    section = "AdditionalTradeCollections";
                    infoDivName = "divInfo" + section;
                    infoButton = "<img width='20' height='20' title='Click for more info' style='vertical-align: middle;' onclick=\"showInfo('" + section + "');\" src='img/Buttons/infoYellow.png'>";
                    html.Append("<div id='" + infoDivName + "' class='infoDiv'></div>");
                    html.Append("<table>");                    
                    html.Append("<tr><th colSpan='" + (additionalCosts.GetLength(1)) + "'>Additional Trade Costs " + infoButton + "</th></tr>");                    
                    
                    for (int c = 0; c < additionalCosts.GetLength(0); c++)
                    {
                        html.Append("<tr>");
                        for (int j = 0; j < additionalCosts.GetLength(1); j++)
                        {
                            html.Append("<td");
                            if (c == 0) { html.Append(" class=\"thMid\""); }
                            html.Append(">");
                            if (additionalCosts[c, j] == null) { additionalCosts[c, j] = ""; }                            
                            html.Append(additionalCosts[c, j].Replace("#", "<br/>"));
                            html.Append("</td>");
                        }
                        html.Append("</tr>");
                    }
                    html.Append("</table>");
                }
            }
            html.Append("</div>");//end tradeCosts div
        }        
        #endregion

        if (returnFriendlyNextCollection) 
        {
            for (int n = 0; n < nextCollections.Length; n++)
            {
                html.Append(nextCollections[n] + "<br>");
            }
            if (calendarInfo.depot != "") { html.Append("Your collection depot is " + calendarInfo.depot + "<br>"); }
        }
        #region Write Calendar Header Key To HTML
        html.Append("<b>The Calendar</b><img src='img/Gif-Spacer.gif' alt='spacer' height='1' width='30'/>");
        //Calendar key         
        html.Append("<div><B>Key:<img src='img/Gif-Spacer.gif' height='1' alt='spacer' width='20'/>");
        //keys
        //01 10
        //001 010 100
        //0001 0010 0100 1000
        //00001 00010 00100 01000 10000
        string keyGraphic = "0000000000";
        for (int i = 0; i < clientServices.Length; i++)
        {
            keyGraphic = "0000000000";
            keyGraphic += (Math.Pow(10, i + 1)).ToString();
            keyGraphic = (clientServices.Length).ToString() + "-" + keyGraphic.Substring(keyGraphic.Length - clientServices.Length - 1, clientServices.Length) + ".png";
            if (clientDetails.customCalendarColours)
            {
                keyGraphic = clientDetails.clientName3L + "-" + keyGraphic;
            }
            string srvNameFull = clientServices[i];
            //replace 3L Serv with AltServName
            srvNameFull = Misc.GetBinServiceFullFromService3L(clientServices[i], clientDetails.clientName3L);
            if (clientDetails.altServiceNames != "")
            {
                string[] splitAltNames = clientDetails.altServiceNames.Split('#');
                for (int a = 0; a < splitAltNames.Length; a++)
                {
                    string[] serv3LAndAlt = splitAltNames[a].Replace(")", "").Split('(');
                    if (clientServices[i] == serv3LAndAlt[0])
                    {
                        srvNameFull = serv3LAndAlt[1];
                        break;
                    }
                }
            }

            if (clientDetails.altServiceNames != "")
            {
                string[] altServNames = clientDetails.altServiceNames.Split('#');
                for (int a = 0; a < altServNames.Length; a++)
                {
                    string[] splitSevAndAltName = altServNames[a].Split('$');
                    string ser3L = splitSevAndAltName[0];
                    if (splitSevAndAltName.Length > 1)
                    {
                        string altName = splitSevAndAltName[1];
                        if (clientServices[i] == ser3L) { srvNameFull = altName; break; }
                    }
                }
            }
            
            html.Append("<img src='img/Pie/" + keyGraphic + "' alt='" + srvNameFull + "' height='25' width='25' style=\"vertical-align:middle\"/>" + srvNameFull);

            html.Append("<img src='img/Gif-Spacer.gif' height='1' alt='spacer' width='25'/>");
        }
        html.Append("</B>");        
        for (int o = 0; o < otherInfo.Length; o++)
        {
            string servicesThatUseParentContainers = "";
            if (otherInfo[o].StartsWith("ParentChildInfo"))
            {
                string[] splitParentServices = otherInfo[o].Replace("ParentChildInfo$","").Split('$');
                for (int s = 0; s < splitParentServices.Length - 1; s++)
                {
                    servicesThatUseParentContainers += Misc.GetServiceFullFromService3L(splitParentServices[s], clientDetails.clientName3L) + " ";
                }
            }
            html.Append("(" +  "<img height='15' width='15' style=\"vertical-align:middle\" title='Parent Property containers used for this service'  src='img/Icons/ParentChildIcon.png\'>"+ servicesThatUseParentContainers+" use parents' containers)");
        }
        html.Append("</div>");
        //End Calendar Key
        #endregion

        #region Write Calendar To HTML one month at a time from CalendarArray (all dates -1 month and +1 month) and CalendarArr (dates in range required summurised by service - last row is combined graphic)
        string iconBin2 = "";
        int noOfServices = clientDetails.services.Split('#').Length;
        for (int i = 0; i < noOfServices; i++)
        {
            iconBin2 += "0";
        }
        string blankGraphic = "<img src='img/Pie/" + clientServices.Length + "-" + iconBin2 + "";
        if (clientDetails.customCalendarColours)
        {
            blankGraphic = blankGraphic.Replace("Pie/", "Pie/" + clientDetails.clientName3L + "-");
        }
        string calStartYYYYMMDD = startDate.ToString("yyyyMMdd");
        string calEndYYYYMMDD = endDate.ToString("yyyyMMdd");
        string calStartDate = "01/" + calendarArr[0, 1].Substring(3, 7);
        int lastDayOfLastMonth = DateTime.DaysInMonth(Convert.ToInt16(calendarArr[0, calendarArr.GetLength(1) - 1].Substring(6, 4)), Convert.ToInt16(calendarArr[0, calendarArr.GetLength(1) - 1].Substring(3, 2)));
        string calEndDate = lastDayOfLastMonth.ToString("00") + "/" + calendarArr[0, calendarArr.GetLength(1) - 1].Substring(3, 7);
        int startMonth = Convert.ToInt16(calStartDate.Substring(3, 2));
        int endMonth = Convert.ToInt16(calEndDate.Substring(3, 2));
        int startYear = Convert.ToInt16(calStartDate.Substring(6, 4));
        int endYear = Convert.ToInt16(calEndDate.Substring(6, 4));
        int noOfCalMonths = ((endYear - startYear) * 12) + (startMonth - endMonth) - 1;
        noOfCalMonths = Math.Abs((endMonth - startMonth) + 12 * (endYear - startYear)) + 1;
        int currentCalMonth = startMonth;
        int currentCalYear = startYear;
        html.Append("<div id='NewCalendar'>");
        string dayDD = "";
        int thisDayyyyyMMdd = 0;
        for (int i = 0; i < noOfCalMonths; i++)//create a table for each month
        {
            string[,] monthArray = new string[8, 8];//array to store calendar for this month
            currentCalMonth = startMonth + i;
            if (currentCalMonth > 12) { currentCalMonth -= 12; currentCalYear = startYear + 1; }//end year 1
            if (currentCalMonth > 12) { currentCalMonth -= 12; currentCalYear = startYear + 2; }//end year 2
            if (currentCalMonth > 12) { currentCalMonth -= 12; currentCalYear = startYear + 3; }//end year 3
            if (currentCalMonth > 12) { currentCalMonth -= 12; currentCalYear = startYear + 4; }//end year 4
            for (int r = 0; r <= 7; r++)//blank the days
            {
                for (int w = 0; w <= 7; w++)
                {
                    monthArray[r, w] = "";
                }
            }
            monthArray[0, 1] = Misc.GetMonthFullFromMorMMorMMM(currentCalMonth.ToString("00")) + " " + currentCalYear.ToString("0000");
            monthArray[1, 0] = "Wk";
            monthArray[1, 1] = "Mon";
            monthArray[1, 2] = "Tue";
            monthArray[1, 3] = "Wed";
            monthArray[1, 4] = "Thu";
            monthArray[1, 5] = "Fri";
            monthArray[1, 6] = "Sat";
            monthArray[1, 7] = "Sun";

            int daysInCurrentMonth = DateTime.DaysInMonth(currentCalYear, currentCalMonth);
            int calRow = 2;
            //CalendarArray - all dates 1 month previous 1 month after split by container
            //CalendarArr - dates in range required summurised by service - last row is combined graphic
            string todayddMMYYYY = DateTime.Now.ToString("ddMMyyyy");
            int today_yyyyMMdd = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
            for (int d = 1; d <= daysInCurrentMonth; d++)//step through each day in current month
            {
                DateTime thisDay = new DateTime(currentCalYear, currentCalMonth, d);
                string thisDayDDD = thisDay.ToString("ddd");
                for (int w = 1; w <= 7; w++)//step through each day on this row to get the day number or CollectionPie
                {
                    if (thisDayDDD == monthArray[1, w])
                    {
                        monthArray[calRow, w] = d.ToString();//populate day number 1>28/29/30/31
                        //}

                        string cellDate = d.ToString("00") + currentCalMonth.ToString("00") + currentCalYear.ToString("0000");
                        for (int j = 1; j < calendarArr.GetLength(1); j++)//check for a round entry, if found replace the month number with this graphic
                        {
                            if (calendarArr[0, j] == thisDay.ToString("dd/MM/yyyy"))//find corresponding date
                            {
                                if (monthArray[calRow, 0] == "")
                                {
                                    for (int l = j + 28; l < calendarArray.GetLength(1) - 28; l++)//Get Week Number from date headers of CalendarArray ... dd/MM/yyyy#yyyyMMdd#ddd#MMM#Wkx#MWx
                                    {
                                        if (calendarArray[0, l].Contains(thisDay.ToString("dd/MM/yyyy")))
                                        {
                                            string[] splitDates = calendarArray[0, l].Split('#');
                                            monthArray[calRow, 0] = splitDates[4].Replace("Wk", "");//Week Number
                                            break;//matching date found for WeekNo so exit
                                        }
                                    }
                                }
                                if (!calendarArr[calendarArr.GetLength(0) - 1, j].StartsWith(blankGraphic))
                                {
                                    monthArray[calRow, w] = calendarArr[calendarArr.GetLength(0) - 1, j];//graphic for combined pie chart
                                    break; // goto next day
                                }
                            }
                        }
                        if (cellDate == todayddMMYYYY) { monthArray[calRow, w] += "**TODAY**"; }
                    }
                }
                if (thisDayDDD == "Sun") { calRow++; }
            }
            //Write out Calendar as a table
            html.Append("<div class='tryFixAlignLeft'>");
            html.Append("<table id='CalTab" + i.ToString("00") + "' class='newCalendarTable'>");
            string[] monthSpaceYearSplit = monthArray[0, 1].Split(' ');
            string yyyyMM = monthSpaceYearSplit[1] + Misc.GetMMfromMonthFullOrMMM(monthSpaceYearSplit[0]);
            if (!returnFriendlyNextCollection)//don't out put week if public look up
            {
                html.Append("<tr><td>");
            }
            string houseStyle = "thMid";
            string highContrast = "thMidHC";
            string thStyle = houseStyle;
            if (councilName.ToLower() == "southnorfolk") 
            {
                html.Append("</td><th class='" + highContrast + "' colspan='7'><b>" + monthArray[0, 1] + "</b></th></tr>");
            }
            else
            {
                html.Append("</td><th colspan='7'><b>" + monthArray[0, 1] + "</b></th></tr>");
            }
            for (int r = 1; r <= 7; r++)
            {
                html.Append("<tr>");
                if (!returnFriendlyNextCollection) //only output week number of non public calendar
                { 
                    html.Append("<td class='tdVSmallVLightGrey'>" + monthArray[r, 0] + "</td>");
                }
                for (int d = 1; d <= 7; d++)
                {
                    if (monthArray[r, d] == "") //cater for blank cells - set back ground to white
                    {
                        html.Append("<td class='tdWhiteLightGrey'>");
                    }
                    else
                    {
                        if (r == 1) //Day row - Set background to white
                        {
                            html.Append("<td class='tdWhiteDarkGrey'>");
                        }
                        else
                        {
                            string addBorderStyle = "";
                            if (monthArray[r, d].Contains("**TODAY**"))
                            {
                                monthArray[r, d] = monthArray[r, d].Replace("**TODAY**", "");
                                //addBorderStyle = " style='color:white;bgcolor:red'";
                                addBorderStyle = " style='background:#e7c41e' ";
                            }
                            if (monthArray[r, d].Length == 1 || monthArray[r, d].Length == 2)
                            {
                                dayDD = (Convert.ToInt16(monthArray[r, d])).ToString("00");
                                thisDayyyyyMMdd = Convert.ToInt32(yyyyMM + dayDD);
                                if (thisDayyyyyMMdd > Convert.ToInt32(endDate.ToString("yyyyMMdd")) || thisDayyyyyMMdd < Convert.ToInt32(startDate.ToString("yyyyMMdd")))
                                {
                                    addBorderStyle = " style='color:white';";
                                }
                            }
                            if (councilName.ToLower() == "southnorfolk") 
                            {
                                html.Append("<td class='tdlightGreyGreyHC' " + addBorderStyle + ">");/// **Css handles the Wk column
                            }
                            else
                            {
                                html.Append("<td class='tdlightGreyGrey' " + addBorderStyle + ">");/// **Css handles the Wk column
                            }
                        }
                    }
                    if (r == 1 && returnFriendlyNextCollection) 
                    {
                        string day2L = monthArray[r, d].Substring(0,2);
                        html.Append(day2L); 
                        //html.Append(monthArray[r, d]); 
                    }
                    else
                    {
                        if (councilName.ToLower() == "southnorfolk") 
                        {
                            if (monthArray[r, d].Length == 1 || monthArray[r, d].Length == 2)
                            {
                                dayDD = (Convert.ToInt16(monthArray[r, d])).ToString("00");
                                thisDayyyyyMMdd = Convert.ToInt32(yyyyMM + dayDD);
                                if (thisDayyyyyMMdd < today_yyyyMMdd)
                                {
                                    html.Append("");
                                }
                                else
                                {
                                    html.Append(monthArray[r, d]);
                                }
                            }
                            else
                            {
                                html.Append(monthArray[r, d]);
                            }
                        }
                        else
                        {
                            html.Append(monthArray[r, d]);
                        }
                    }
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }
            html.Append("</table>");
            html.Append("</div>");
        }
        html.Append("</div>");
        #endregion

        #region Write DateRange to HTML

        //Add Start Date and End Date Filter
        if (returnDateSelect)
        {
            html.Append("<div>");
            html.Append("<input type='text' tabindex='0' id='txtCalSD' size='8' readonly='readonly' value='" + origCalStart + "' onchange=\"slideup(this,'NewCalendar','300','DIV');updateCalendar('False','False','New Start Date Range Selected');\"/>");
            html.Append("Calendar Range");
            html.Append("<input type='text' tabindex='0' id='txtCalED' size='8' readonly='readonly' value='" + endDate.ToString("dd/MM/yyyy") + "' onchange=\"slideup(this,'newCalendar','300','DIV');updateCalendar('False','False','New End Date Range Selected');\"/>");
            //html.Append("<img title='Start Date + 3 Months' onmouseover=\"this.src='img/Buttons/Bin2_Delete_Hover.png'\" onmouseout=\"this.src='img/Buttons/Bin2_Delete.png'\" onclick=\"ajxDeleteBin('132047','010000905912','True','trConID1');\" src='img/Buttons/Bin2_Delete.png'>");
            html.Append("</div>");
        }
        #endregion

        #region Write Mismatches To HTML - RoundsNoBins BinsNoRounds
        if (returnMismatches)
        {
            //write mismatches - Rounds No Containers and Containers No Rounds
            bool hasMismatches = false;
            //         get rounds with no containers         
            string servicesForRounds = "";
            for (int r = 1; r < rounds.GetLength(0); r++)
            {
                string[] splitRoundName = rounds[r, 0].Split(' ');
                string roundServ3L = splitRoundName[splitRoundName.Length - 1];
                if (clientDetails.useWaxRoundNew)
                {
                    if (rounds[r, 4].ToLower().Contains("t"))
                    {
                        if (roundServ3L == "Ref") { roundServ3L = "TRf"; }
                        if (roundServ3L == "Rec") { roundServ3L = "TRc"; }
                        if (roundServ3L == "Grn") { roundServ3L = "TGr"; }
                    }
                }
                if (!servicesForRounds.Contains(roundServ3L))
                {
                    //check not admin round - if not - then add
                    bool adminRound = false;
                    if (clientDetails.collectsOnASunday)
                    {
                        if (rounds[r, 0].StartsWith("Admin"))
                        {
                            adminRound = true;
                        }
                    }
                    else
                    {
                        if (rounds[r, 0].Contains(" Sun Wk"))
                        {
                            adminRound = true;
                        }
                    }

                    if (!adminRound)
                    {
                        servicesForRounds += roundServ3L + "~";
                    }
                }
            }
            servicesForRounds += "zzXXzz";
            servicesForRounds = servicesForRounds.Replace("~zzXXzz", "");
            servicesForRounds = servicesForRounds.Replace("zzXXzz", "");
            if (clientDetails.clientName == "Falkirk"||clientDetails.clientName=="TestFalk") { servicesForRounds = servicesForRounds.Replace("~Gls", ""); }//Gls are the Falkirk Garden Monthly rounds
            string[] roundServices = servicesForRounds.Split('~');
            string[,] roundHasBin = new string[roundServices.Length, 2];
            for (int r = 0; r < roundServices.Length; r++)
            {
                roundHasBin[r, 0] = roundServices[r];
                roundHasBin[r, 1] = "False";
            }
            if (servicesForRounds == "")
            {
                roundHasBin[0, 0] = "";
                roundHasBin[0, 1] = "True";
            }
            for (int r = 0; r < roundServices.Length; r++)
            {
                for (int c = 1; c < containers.GetLength(0); c++)
                {
                    string containerService3L = Misc.GetService3LForBinType(containers[c, 1]);
                    if (roundServices[r] == containerService3L)
                    {
                        roundHasBin[r, 1] = "True";
                        break;
                    }
                }
            }
            //get containers with no Rounds
            string servicesForBins = "";
            for (int c = 1; c < containers.GetLength(0); c++)
            {
                string ser3L = Misc.GetService3LForBinType(containers[c, 1]);
                if (!servicesForBins.Contains(ser3L)) { servicesForBins += ser3L + "~"; }
            }
            servicesForBins += "zzXXzz";
            servicesForBins = servicesForBins.Replace("~zzXXzz", "");
            servicesForBins = servicesForBins.Replace("zzXXzz", "");
            string[] binServices = servicesForBins.Split('~');
            string[,] binHasRound = new string[binServices.Length, 2];
            for (int b = 0; b < binServices.Length; b++)
            {
                binHasRound[b, 0] = binServices[b];
                binHasRound[b, 1] = "False";
            }
            if (servicesForBins == "")
            {
                binHasRound[0, 0] = "";
                binHasRound[0, 1] = "True";
            }
            for (int b = 0; b < binServices.Length; b++)
            {
                for (int r = 1; r < rounds.GetLength(0); r++)
                {
                    string[] splitRoundName = rounds[r, 0].Split(' ');
                    string roundServ3L = splitRoundName[splitRoundName.Length - 1];
                    if (clientDetails.useWaxRoundNew)
                    {
                        if (rounds[r, 4].ToLower().Contains("t"))
                        {
                            if (roundServ3L == "Ref") { roundServ3L = "TRf"; }
                            if (roundServ3L == "Rec") { roundServ3L = "TRc"; }
                            if (roundServ3L == "Grn") { roundServ3L = "TGr"; }
                        }
                    }
                    if (binServices[b] == roundServ3L)
                    {
                        binHasRound[b, 1] = "True";
                        break;
                    }
                }
            }
            for (int r = 0; r < roundHasBin.GetLength(0); r++)
            {
                if (roundHasBin[r, 1] == "False") { hasMismatches = true; }
            }
            for (int b = 0; b < binHasRound.GetLength(0); b++)
            {
                if (binHasRound[b, 1] == "False") { hasMismatches = true; }
            }
            if (hasMismatches)
            {
                html.Append("<div>");
                html.Append("<Table>");
                html.Append("<tr><th class='thWarning'>Round / Container Mismatches</th></tr>");
                html.Append("<tr>");
                html.Append("<td><b>Warning - The following will need correcting to show entries on the calendar</b></td>");
                html.Append("</tr>");
                for (int r = 0; r < roundHasBin.GetLength(0); r++)
                {
                    if (roundHasBin[r, 1] == "False")
                    {
                        html.Append("<tr>");
                        html.Append("<td>The " + Misc.GetServiceFullFromService3L(roundHasBin[r, 0], clientDetails.clientName3L) + " service has no containers for this property</td>");
                        html.Append("</tr>");
                    }
                }
                for (int b = 0; b < binHasRound.GetLength(0); b++)
                {
                    if (binHasRound[b, 1] == "False")
                    {
                        html.Append("<tr>");
                        html.Append("<td>The " + Misc.GetServiceFullFromService3L(binHasRound[b, 0], clientDetails.clientName3L) + " bin has no rounds for this property</td>");
                        html.Append("</tr>");
                    }
                }
                html.Append("</Table>");
                html.Append("</div>");
            }
            //trade warnings                        
            if (hasTradeContainer) 
            {
                if (tData.GetLength(0) > 1) 
                {
                    TradeInfo tINfo = TradeInfo.GetTradeParameters(tData, 1);
                    StringBuilder contractDateWarningHTML = new StringBuilder();
                    for (int c = 1; c < containers.GetLength(0); c++)
                    {
                        ContainerData binData = ContainerData.GetContainerParameters(containers, c);
                        if (binData.tradeContainer)
                        {
                            if (binData.StartDateYYYYMMDD != 0)
                            {
                                if (tINfo.ContractStartDateYYYYMMDD != 0)
                                {
                                    if (binData.StartDateYYYYMMDD < tINfo.ContractStartDateYYYYMMDD)
                                    {
                                        contractDateWarningHTML.Append("<tr>");
                                        contractDateWarningHTML.Append("<td>The " + binData.Bin_number + "(" + binData.ID + ") Start Date ("+binData.Start_date+") is before the trade contract start date ("+tINfo.ContractStartDate+")</td>");
                                        contractDateWarningHTML.Append("</tr>");
                                    }
                                }
                            }
                            if (binData.EndDateYYYYMMDD != 0)
                            {
                                if (tINfo.ContractEndDateYYYYMMDD != 0)
                                {
                                    if (binData.EndDateYYYYMMDD > tINfo.ContractEndDateYYYYMMDD)
                                    {
                                        contractDateWarningHTML.Append("<tr>");
                                        contractDateWarningHTML.Append("<td>The " + binData.Bin_number + "(" + binData.ID + ") End Date (" + binData.End_date + ") is after the trade contract end date (" + tINfo.ContractStartDate + ")</td>");
                                        contractDateWarningHTML.Append("</tr>");
                                    }
                                }
                            }
                        }
                    }
                    if (contractDateWarningHTML.ToString() != "") 
                    {
                        html.Append("<div>");
                        html.Append("<Table>");
                        html.Append("<tr><th class='thWarning'>Trade Start End Contract Warnings</th></tr>");
                        html.Append("<tr>");
                        html.Append("<td><b>Warning - The following may need correcting to show entries on the calendar</b></td>");
                        html.Append("</tr>");
                        html.Append(contractDateWarningHTML);
                        html.Append("</Table>");
                        html.Append("</div>");
                    }
                }
            }
        }
        #endregion

        #region Calendar Breakdown write HTML - By Service - from CalendarArr
        //Write out Calendar by Service  this will be hidden on return and shown on clciking the info button         
        if (returnBreakdownByService)
        {
            html.Append("Calendar by Service");
            html.Append("<div class='table-wrapper' id='divCalendarByService'>");
            html.Append("<Table>");
            calendarArr[0, 0] = "-";
            for (int i = 0; i < calendarArr.GetLength(0); i++)
            {
                html.Append("<tr>");
                for (int j = 0; j < calendarArr.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        if (i == 0) { html.Append("<th class='first-col'>"); } else { html.Append("<td class='first-col'>"); }

                    }
                    else
                    {
                        if (i == 0) { html.Append("<th>"); } else { html.Append("<td>"); }
                    }
                    html.Append(calendarArr[i, j].Replace("#", "<br/>"));
                    if (i == 0) { html.Append("</th>"); } else { html.Append("</td>"); }
                }
                html.Append("</tr>");
            }
            html.Append("</Table>");
        }
        else
        {
            html.Append("<div  id='divCalendarByService'>");
        }
        html.Append("</div>");
        //End Write out Calendar by Service  this will be hidden on return and shown on clciking the info button
        #endregion

        #region Calendar Breakdown write HTML - By Container from CalendarArray
        //Write out Calendar by Container for month before / month after         
        html.Append("<div id='divCalendarByContainer'>");
        if (returnBreakdownByContainer)
        {
            html.Append("Calendar by Container with Reasons");
            html.Append("<div class='table-wrapper'>");
            html.Append("<Table>");
            for (int i = 0; i < calendarArray.GetLength(0); i++)
            {
                html.Append("<tr>");
                for (int j = 0; j < calendarArray.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        if (i == 0) { html.Append("<th class='first-col'>"); } else { html.Append("<td class='first-col'>"); }
                    }
                    else
                    {
                        if (i == 0) { html.Append("<th>"); } else { html.Append("<td>"); }
                    }
                    html.Append(calendarArray[i, j].Replace("#", "<br/>"));
                    if (i == 0) { html.Append("</th>"); } else { html.Append("</td>"); }
                }
                html.Append("</tr>");
            }
            html.Append("</Table>");
            html.Append("</div>");
            html.Append("<Table id='CalKey' style=\"text-align:left;max-width:900px;\" >");
            string[,] calReasonKey = new string[20, 6];
            for (int r = 0; r < calReasonKey.GetLength(0); r++)
            {
                for (int c = 0; c < calReasonKey.GetLength(1); c++)
                {
                    calReasonKey[r, c] = "";
                }
            }
            calReasonKey[0, 0] = "Value";
            calReasonKey[0, 1] = " Collection Reason Key";
            calReasonKey[1, 0] = "[0]";
            calReasonKey[1, 1] = "No Collection";
            calReasonKey[2, 0] = "[1]";
            calReasonKey[2, 1] = "Collection";
            calReasonKey[3, 0] = "SD";
            calReasonKey[3, 1] = "StartDate of Container";
            calReasonKey[4, 0] = "ED";
            calReasonKey[4, 1] = "EndDate Of Container";
            calReasonKey[5, 0] = "RN";
            calReasonKey[5, 1] = "Collection based on RoundName for this service";
            calReasonKey[6, 0] = "RW";
            calReasonKey[6, 1] = "Collection based on Weekly RoundName for this service";
            calReasonKey[7, 0] = "MR";
            calReasonKey[7, 1] = "Missing or Admin round - so no Collection";
            calReasonKey[8, 0] = "AF";
            calReasonKey[8, 1] = "Collection date adjusted (DateFrom)";
            calReasonKey[9, 0] = "AT";
            calReasonKey[9, 1] = "Collection date adjusted (DateTo)";
            calReasonKey[10, 0] = "NC";
            calReasonKey[10, 1] = "Collection date adjusted based on NoCollect in Adjusted Dates Table";
            calReasonKey[11, 0] = "LV";
            calReasonKey[11, 1] = "Collection date left as original contradictory to rest of service";
            calReasonKey[12, 0] = "-";
            calReasonKey[12, 1] = "-";
            if (hasTradeContainer)
            {
                calReasonKey[0, 2] = "Value";
                calReasonKey[0, 3] = "Trade Collection Reason";
                calReasonKey[1, 2] = "CD";
                calReasonKey[1, 3] = "TRADE - Collections Based On Collection Day of Container";
                calReasonKey[2, 2] = "PD";
                calReasonKey[2, 3] = "TRADE - Collections Based On Collection Day Of Property";
                calReasonKey[3, 2] = "DF";
                calReasonKey[3, 3] = "TRADE - Collections Based On CollectionDays and Frequency Of Container";
                calReasonKey[4, 2] = "PF";
                calReasonKey[4, 3] = "TRADE - Collections Based On CollectionDays and Frequency of Property";
                calReasonKey[5, 2] = "AR";
                calReasonKey[5, 3] = "TRADE - Collections Based On Also Collected by of Container";
                calReasonKey[6, 2] = "AW";
                calReasonKey[6, 3] = "TRADE - Collections Based On Also Collected by of Container (Weekly)";
                calReasonKey[7, 2] = "CP";
                calReasonKey[7, 3] = "TRADE - Collections Based On Pattern of Container";
                calReasonKey[8, 2] = "PP";
                calReasonKey[8, 3] = "TRADE - Collections Based On Pattern of Property";
                calReasonKey[9, 2] = "CS";
                calReasonKey[9, 3] = "TRADE - Collections before contract Start";
                calReasonKey[10, 2] = "CE";
                calReasonKey[10, 3] = "TRADE - Collections after contract end";
                calReasonKey[11, 2] = "SS";
                calReasonKey[11, 3] = "TRADE - Collection suspension";
            }
            for (int r = 0; r < calReasonKey.GetLength(0); r++)
            {
                if (calReasonKey[r, 0] == "") { break; }
                html.Append("<tr>");
                for (int c = 0; c < calReasonKey.GetLength(1); c++)
                {
                    if (calReasonKey[r, c] == "") { break; }
                    if (r == 0) { html.Append("<th>"); } else { html.Append("<td>"); }
                    html.Append(calReasonKey[r, c]);
                    if (r == 0) { html.Append("</th>"); } else { html.Append("</td>"); }
                }
                html.Append("</tr>");
            }
            html.Append("</Table>");
        }
        html.Append("</div>");
        //End Write out Calendar by Container for month before / month after
        #endregion
        return html.ToString();
        //return HTML;
    }
    //================================================================================================================================================================================================================    
    public class CalendarAndCostsData
    {        
        public string[,] breakDownByContainerArray  { get; set; }
        public string[,] breakDownByServiceArray    { get; set; }
        public string[,] tradeCostsArray            { get; set; }
        public string[,] additionalCostsArray       { get; set; }
        public bool hasTradeContainer { get; set; }
        public string[] clientServices { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string origCalStart { get; set; }
        public string[,] rounds { get; set; }
        public string[,] containers { get; set; }
        public string[,] addressLLPG { get; set; }
        public string[,] tradeData { get; set; }
        public string depot { get; set; }
        public string[] nextCollectionDates { get; set; }        
        public string[] otherInfo { get; set; }
    }
    //=================================================================================================================================================================================
    /// <summary>
    /// <para>This method returns Calendar, Break Down by Service, BreakDown by Container, TradeCosts, DateRange HTML Selector as a CalendarAndCostsData class</para>
    /// <para>for the number of Months specified with noOfMonths or between start and end dates</para>
    /// <para>One row for each Service Collection</para>
    /// <para>One column for each Day</para>
    /// <para>WILL ONLY WORK FOR WAXROUNDNEW</para>
    /// </summary>          
    public static CalendarAndCostsData getCalendarAndInvoiceData(string uprn, string email, string councilName, string calendarStartDDMMYYYY, string calendarEndDDMMYYYY, bool forWebservice, colClient clientDetails, string connection, bool returnFriendlyNextCollection)
    {
        //This is also used by exteranl windows Forms app for Bulk trade invoices
        string[,] tradeCosts = new string[1, 1];
        string[,] additionalCosts = new string[1, 1];
        List<string> other_info = new List<string>();
        if (connection == null)
        {
            connection = Misc.getDbnameDbSourceDbUser(councilName, "Collections");
        }
        if (clientDetails.clientName == null)
        {
            clientDetails = colClient.GetClientParameters(councilName);
        }
        //check for parent child relationsship
        string sqlParentCheck = "Select UPRN,Parent_UPRN from LLPGXtra where UPRN='" + uprn + "';";
        string[,] parentChildResults = Misc.getDataArrayExactFromDB(sqlParentCheck, councilName, connection);
        string parentUPRN = "";
        if (parentChildResults.GetLength(0) > 1) 
        {
            parentUPRN = parentChildResults[1, 1]; 
        }

        string[] services = clientDetails.services.Split('#');
        //get normal rounds from waxroundNew
        string ignoreAdmin = " and roundname not like '%Sun Wk%' ";
        if (clientDetails.collectsOnASunday)
        {
            ignoreAdmin = " and roundname not like 'Admin%' ";
        }
        string getRoundsSQL = "";
        string[,] rounds = new string[1, 1];
        if (clientDetails.useWaxRoundNew)
        {
            getRoundsSQL = "Select roundname,Pattern,numOfColWeeks,Frequency,CollectionType,roundBand,Day_DDD,Week,ServiceName from waxRoundNew where UPRN='" + uprn + "' " + ignoreAdmin + "";
            rounds = Misc.getDataArrayExactFromDB(getRoundsSQL, councilName, connection);
            if (rounds.GetLength(0) > 1)
            {
                if (rounds[1, 0] == "") //no rounds
                {
                    string[,] blankRounds = new string[1, rounds.GetLength(0)];
                    for (int j = 0; j < rounds.GetLength(1); j++)
                    {
                        blankRounds[0, j] = rounds[0, j];
                    }
                    rounds = blankRounds;
                }
            }
        }
        else
        {
            #region get Rounds for Waxround      **ONLY DWP **       
            //"Select roundname,Pattern,numOfColWeeks,Frequency from waxRoundNew where UPRN='" + uprn + "' " + ignoreAdmin + "";
            string roundsSQL = "select  * from WaxRound where UPRN='" + uprn + "'";
            string[,] waxrounds = Misc.getDataArrayExactFromDB(roundsSQL, councilName, connection);
            //parse waxround data into waxroundnew format - NO WAX ROUND Clients have trade - if they need it then will have to convert them to Waxround new
            string roundList = "";
            string[] headers = "roundname#Pattern#numOfColWeeks#Freqency".Split('#');
            if (waxrounds.GetLength(0) == 1)
            {
                //no UPRN found
                rounds = new string[1, headers.Length];
                for (int j = 0; j < headers.Length; j++)
                {
                    rounds[0, j] = headers[j];
                }
            }
            else
            {
                //find rounds
                for (int j = 2; j < waxrounds.GetLength(1); j++)
                {
                    if (waxrounds[1, j] == null) { waxrounds[1, j] = ""; }
                    if (waxrounds[1, j] != "")
                    {
                        if (!roundList.Contains(waxrounds[1, j]))
                        {
                            roundList += waxrounds[1, j] + "~";
                        }
                    }
                }
                if (roundList == "")
                {
                    //no Rounds Found
                    rounds = new string[1, headers.Length];
                    for (int j = 0; j < headers.Length; j++)
                    {
                        rounds[0, j] = headers[j];
                    }
                }
                else
                {
                    //rounds found -parse and populate rounds table                         
                    string[] roundsSplit = roundList.Split('~');
                    rounds = new string[roundsSplit.Length, headers.Length];
                    for (int j = 0; j < headers.Length; j++)
                    {
                        rounds[0, j] = headers[j];
                    }
                    for (int r = 0; r < roundsSplit.Length - 1; r++)
                    {
                        string[] splitRoundname = roundsSplit[r].Split(' ');
                        string serv3L = splitRoundname[splitRoundname.Length - 1];
                        string week = splitRoundname[splitRoundname.Length - 2];
                        string day = splitRoundname[splitRoundname.Length - 3];
                        //     0       1      2                3
                        //roundname#Pattern#numOfColWeeks#Freqency
                        rounds[r + 1, 0] = roundsSplit[r];
                        rounds[r + 1, 1] = "";
                        rounds[r + 1, 2] = "";
                        rounds[r + 1, 3] = "";
                    }
                }
            }
            #endregion
        }
        bool hasTradeContainer = false;
        //get Containers and initialise
        string getContainersSQL = "Select ID,Bin_number,Start_date,End_date,Pattern,numColWeeks,Frequency,CollectionDays,AlsoCollectedBy,discount,discountType,binBand from Bins where UPRN='" + uprn + "'  ORDER BY CONVERT(DATE,End_date,103),Bin_number;";
        string[,] containers = new string[1, 1];
        string parentsContainersUsedForTheseServices = "";
        if (parentUPRN != "" && parentUPRN != uprn)
        {
            #region cater for parent uprn containers if no containers for this service at the UPRN
            //parent UPRN to consider - get containers from parent and child - if no containers for service for child populate with parent. (Ignore subscriberd garden, AHP and trade)
            //                         0   1           2           3         4        5            6          7               8                9         10            11       12    
            getContainersSQL = "Select ID, Bin_number, Start_date, End_date, Pattern, numColWeeks, Frequency, CollectionDays, AlsoCollectedBy, discount, discountType, binBand, UPRN from Bins where UPRN='" + uprn + "' or UPRN='" + parentUPRN + "'  ORDER BY UPRN,CONVERT(DATE,End_date,103),Bin_number;";
            containers = Misc.getDataArrayFromDB(getContainersSQL, councilName, connection);//containers from parent UPRNs to consider
            //get all services
            List<string> containerServices = new List<string>();
            for (int c = 1; c < containers.GetLength(0); c++)
            {
                string containerService = Misc.GetService3LForBinType(containers[c, 1]);
                if (!containerServices.Contains(containerService))
                {
                    if (containerService.Contains("TR") || (containerService == "Grn" && containers[c, 2] != "") || containerService == "AHP")
                    {
                        //ignore trade containers, AHP containers and subscribed garden as these should not be communal
                    }
                    else
                    {
                        containerServices.Add(containerService);
                    }
                }
            }
            //we now have a list of all services for which a container is present for either the child or parent
            // build a new list of containers and assign all parents to child for missing services
            string[,] serviceParentChild = new string[containerServices.Count, 2];
            for (int s = 0; s < containerServices.Count; s++)
            {
                serviceParentChild[s, 0] = containerServices[s];
                serviceParentChild[s, 1] = "";
                for (int c = 1; c < containers.GetLength(0); c++)
                {
                    if (uprn == containers[c, 12] && Misc.GetService3LForBinType(containers[c, 1]) == containerServices[s])
                    {
                        //matching child uprn and container service - use this
                        if (!serviceParentChild[s, 1].Contains("Child"))
                        {
                            serviceParentChild[s, 1] += "Child$";
                        }
                    }
                    if (parentUPRN == containers[c, 12] && Misc.GetService3LForBinType(containers[c, 1]) == containerServices[s])
                    {
                        //matching child uprn and container service - use this
                        if (!serviceParentChild[s, 1].Contains("Parent"))
                        {
                            serviceParentChild[s, 1] += "Parent$";
                        }
                    }
                }
            }
            //we now have an array list of all the services with an entry of parent of child            
            //Go through containers - change UPRN of parent only containers to child UPRN - remove other parent containers
            int parentAndChildContainers = 0;
            for (int c = 1; c < containers.GetLength(0); c++)
            {
                string bin3LService = Misc.GetService3LForBinType(containers[c, 1]);
                if (containers[c, 12] == parentUPRN)
                {
                    if (bin3LService.Contains("TR") || (bin3LService == "Grn" && containers[c, 2] != "") || bin3LService == "AHP")
                    {
                        //delete this                                    
                        containers[c, 0] = "DELETE THIS";
                        parentAndChildContainers++;
                    }
                    else
                    {
                        for (int s = 0; s < serviceParentChild.GetLength(0); s++)
                        {
                            if (serviceParentChild[s, 0] == bin3LService)
                            {
                                if (serviceParentChild[s, 1].Contains("Child") && serviceParentChild[s, 1].Contains("Parent"))//if containers for both child and parent then delete the parents
                                {
                                    //delete this                                    
                                    containers[c, 0] = "DELETE THIS";
                                    parentAndChildContainers++;
                                }
                                if (!serviceParentChild[s, 1].Contains("Child") && serviceParentChild[s, 1].Contains("Parent"))//if containers are only for parent add this service to a paramter for use when displaying ht calendar
                                {
                                    parentsContainersUsedForTheseServices += serviceParentChild[s, 0] + "$";
                                }
                            }
                        }
                    }
                }
            }
            //remove blanks entries
            string[,] newContainers = new string[containers.GetLength(0) - parentAndChildContainers, containers.GetLength(1) - 1];//ignore UPRN field on repopulation
            int line = -1;
            for (int c = 0; c < containers.GetLength(0); c++)
            {
                if (containers[c, 0] != "DELETE THIS")
                {
                    line++;
                    for (int j = 0; j < newContainers.GetLength(1); j++)
                    {
                        newContainers[line, j] = containers[c, j];
                    }
                }
            }
            containers = newContainers;
            if (parentsContainersUsedForTheseServices != "")
            {
                other_info.Add("ParentChildInfo$" + parentsContainersUsedForTheseServices);
            }
            #endregion
        }
        else
        {
            containers = Misc.getDataArrayFromDB(getContainersSQL, councilName, connection);//normal containers - no parent UPRNs to consider
        }

        for (int j = 0; j < containers.GetLength(1); j++)
        {
            for (int c = 1; c < containers.GetLength(0); c++)
            {
                if (containers[c, j] == null) { containers[c, j] = ""; }
            }
        }

        //**Get Patterns Start
        //tradePatterns[,] /  bool tradeRoundHasPattern  / bool containerHasPattern     /containerPatterns string delim by $     / roundPatterns string delim by $
        #region Patterns - Get and Store for Container, Rounds and Property
        //Check for a store Patterns for Containers, Rounds and Property - also collate all so as to only get required used patterns from the database
        bool containerHasPattern = false;
        string patternsRequired = "";
        string containerPatterns = "";
        string roundPatterns = "";
        //int patternCnt = 0;//KP edit out 02Aug19
        for (int s = 1; s < containers.GetLength(0); s++)
        {
            ContainerData contrLine = ContainerData.GetContainerParameters(containers, s);
            if (contrLine.tradeContainer)
            {
                hasTradeContainer = true;
                if (contrLine.Pattern == null || contrLine.Pattern.ToLower() == "default") { contrLine.Pattern = ""; }
                if (contrLine.hasPattern)
                {
                    containerHasPattern = true;
                    //if (!patternsRequired.Contains(containers[s, 4])) { patternsRequired += "'" + containers[s, 4] + "',"; }
                    //if (!containerPatterns.Contains(containers[s, 4])) { containerPatterns += containers[s, 4] + "$"; }
                    if (!patternsRequired.Contains(contrLine.Pattern)) { patternsRequired += "'" + contrLine.Pattern + "',"; }
                    if (!containerPatterns.Contains(contrLine.Pattern)) { containerPatterns += contrLine.Pattern + "$"; }
                }
            }
        }
        bool tradeRoundHasPattern = false;
        string[] splitRoundName = new string[1];
        string tradeServices = "TRfTRcTGrTClTra";
        WaxRoundNewDetails wxRndDetailsT = new WaxRoundNewDetails();
        for (int r = 1; r < rounds.GetLength(0); r++)
        {
            wxRndDetailsT = WaxRoundNewDetails.getRoundDetails(rounds, r);
            if (wxRndDetailsT.validCollectionsRoundname)
            {
                if (tradeServices.Contains(wxRndDetailsT.service3L))
                {
                    if (wxRndDetailsT.Pattern == null || wxRndDetailsT.Pattern.ToLower() == "default") { wxRndDetailsT.Pattern = ""; }
                    if (wxRndDetailsT.Pattern != "")
                    {
                        tradeRoundHasPattern = true;
                        if (!patternsRequired.Contains(wxRndDetailsT.Pattern)) { patternsRequired += "'" + wxRndDetailsT.Pattern + "',"; }
                        if (!roundPatterns.Contains(wxRndDetailsT.Pattern)) { roundPatterns += wxRndDetailsT.Pattern + "$"; }
                        //break;
                    }
                }
            }
        }
        patternsRequired = patternsRequired.Replace("'Default',", "");
        roundPatterns = roundPatterns.Replace("Default$", "");
        string[,] tradeDataArray = new string[1, 1];
        string[,] tradePatterns = new string[1, 1];//used to hold TradePatterns for crossreferencing later
        string[,] tradeSuspensions = new string[1, 1];
        bool tradePropHasPattern = false;
        bool tradePropHasSuspensions = false;
        TradeInfo tradeInfo = new TradeInfo();
        if (clientDetails.hasTrade && hasTradeContainer)
        {
            //string sqlTrade = "Select NoofCollWks,CollectionFrequency,RecycleFrequency,OrganicFrequency,Pattern,RecyclePattern,OrganicPattern,MultipleCollectionDays,Discount,DiscountType,Band,schedule2,VATRatePercent from TradeData where uprn='" + uprn + "'";
            string sqlTrade = "Select * from TradeData where uprn='" + uprn + "'";
            tradeDataArray = Misc.getDataArrayFromDB(sqlTrade, clientDetails.clientName, connection);
            tradeInfo = TradeInfo.GetTradeParameters(tradeDataArray, 1);
            if (tradeInfo.Pattern == null || tradeInfo.Pattern.ToLower() == "default") { tradeInfo.Pattern = ""; }
            if (tradeInfo.Pattern != "")
            {
                tradePropHasPattern = true;
                if (!patternsRequired.Contains(tradeInfo.Pattern)) { patternsRequired += "'" + tradeInfo.Pattern + "',"; }
            }
            if (tradePropHasPattern || tradeRoundHasPattern || containerHasPattern)
            {
                string sqlPatterns = "Select * from TradePatterns where PatternName in(" + Misc.removeLastChar(patternsRequired) + ");";
                tradePatterns = Misc.getDataArrayFromDB(sqlPatterns, clientDetails.clientName, connection);//only get require trade patterns
            }
            //check for any suspension for this UPRN and store
            string sqlSuspensions = "Select UPRN,suspensionStart_yyyyMMdd,suspensionEnd_yyyyMMdd,suspensionReason from TradeSuspensions where UPRN='" + uprn + "'";
            tradeSuspensions = Misc.getDataArrayFromDB(sqlSuspensions, clientDetails.clientName, connection);
            if (tradeSuspensions.GetLength(0) > 1)
            {
                tradePropHasSuspensions = true;
            }
        }
        roundPatterns = Misc.removeLastChar(roundPatterns);
        containerPatterns = Misc.removeLastChar(containerPatterns);
        #endregion        
        ////**Get Patterns End

        //check for Alternative Summer / Winter rounds - if found - add a dummy alt service container to the containers for use in working out calendar - remove at end        
        #region Alternative Summer Winter Rounds        
        //bool altContainersAdded = false;
        if (clientDetails.SummerWinterAltRounds != "")
        {
            string[] splitSummerWinterAltRounds = clientDetails.SummerWinterAltRounds.Split('$');
            //check for exisiting container - if so add alternative collection container - eg if it has a garden container - add a glass container and copy down garden container parameters
            //Example entry GlsCollectsGARDEN$
            //Example entry GrnCollectsGARDEN$
            //example format <3Letter Alternative serivce>Collects<Bin service>
            string containersToAdd = "";
            for (int i = 0; i < splitSummerWinterAltRounds.Length; i++)
            {
                if (splitSummerWinterAltRounds[i] != "")
                {
                    string[] splitRoundAndService = splitSummerWinterAltRounds[0].Replace("Collects", "$").Split('$');
                    string swAltCollectedByRound3L = splitRoundAndService[0];
                    string swAltWhichBin = splitRoundAndService[1];
                    string binTypeForAltService = Misc.GetBinServiceFullFromService3L(swAltCollectedByRound3L, "XXX");
                    for (int c = 1; c < containers.GetLength(0); c++)
                    {
                        ContainerData containerInfoSWR = ContainerData.GetContainerParameters(containers, c);
                        if (containerInfoSWR.Bin_number.ToLower().Contains(swAltWhichBin.ToLower()))
                        {
                            for (int j = 0; j < containers.GetLength(1); j++)
                            {
                                if (containers[0, j].ToLower() == "bin_number")
                                {
                                    //switch bin service to temp alt service
                                    containersToAdd += containerInfoSWR.Bin_number.ToUpper().Replace(" " + swAltWhichBin.ToUpper() + " ", " " + binTypeForAltService.ToUpper() + " (temp)") + "~"; break;
                                }
                                else
                                {
                                    //copy rest of details for this bin
                                    containersToAdd += containers[c, j] + "~";
                                }
                            }
                            containersToAdd += "¬";
                            //altContainersAdded = true;
                        }
                    }
                }
            }
            string[] tempContainers = containersToAdd.Split('¬');
            //copy to new container array
            string[,] newContainerArray = new string[containers.GetLength(0) + tempContainers.Length - 1, containers.GetLength(1)];
            for (int i = 0; i < containers.GetLength(0); i++)
            {
                for (int j = 0; j < containers.GetLength(1); j++)
                {
                    newContainerArray[i, j] = containers[i, j];
                }
            }
            for (int i = 0; i < tempContainers.Length - 1; i++)
            {
                string[] splitContainersToAdd = tempContainers[i].Split('~');
                for (int j = 0; j < splitContainersToAdd.Length - 1; j++)
                {
                    newContainerArray[containers.GetLength(0) + i, j] = splitContainersToAdd[j];
                }
            }
            //copy back to Containers resized
            containers = new string[newContainerArray.GetLength(0), newContainerArray.GetLength(1)];
            for (int i = 0; i < containers.GetLength(0); i++)
            {
                for (int j = 0; j < containers.GetLength(1); j++)
                {
                    containers[i, j] = newContainerArray[i, j];
                }
            }
            //string endAdd = "Here";//KP edit out 02Aug19
        }
        //alternative Summer Winter Collection End - **Make sure you add NoCollect to non collected date in the AdjustedDatesNewTable
        #endregion


        if (calendarStartDDMMYYYY == "01/01/0001") { }
        string origCalStart = calendarStartDDMMYYYY;
        DateTime startDate = DateTime.ParseExact(calendarStartDDMMYYYY, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        DateTime previousMonth = startDate.AddMonths(-1);//allow for month before, month after to cater for adjusted dates before and adjusted dates before CalandarStartDDMMYYYY
        DateTime endDate = DateTime.ParseExact(calendarEndDDMMYYYY, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        DateTime nextMonth = endDate.AddMonths(1);//allow for month before, month after to cater for adjusted dates before and adjusted dates before CalandarStartDDMMYYYY
        int daysRequired = (nextMonth - previousMonth).Days;

        string servicesForThisUPRN = "";
        string roundsForThisUPRN = "";
        string uniqueRoundDays = "";
        string[] roundNameSplit = new string[1];

        if (rounds.GetLength(0) > 1)
        {
            WaxRoundNewDetails wxRndDetails2 = new WaxRoundNewDetails();
            for (int r = 1; r < rounds.GetLength(0); r++)
            {
                wxRndDetails2 = WaxRoundNewDetails.getRoundDetails(rounds, r);
                roundsForThisUPRN += "'" + wxRndDetails2.RoundName + "',";
                servicesForThisUPRN += "'" + wxRndDetails2.service3L + "',";
                if (!uniqueRoundDays.Contains(wxRndDetails2.Day_DDD)) { uniqueRoundDays += "'" + wxRndDetails2.Day_DDD + "',"; }
            }
            uniqueRoundDays = " where OrigDDD IN (" + uniqueRoundDays.Substring(0, uniqueRoundDays.Length - 1) + ")";
            roundsForThisUPRN += "zzXXzz";
            roundsForThisUPRN = roundsForThisUPRN.Replace(",zzXXzz", "");
            servicesForThisUPRN += "zzXXzz";
            servicesForThisUPRN = servicesForThisUPRN.Replace(",zzXXzz", "");
        }
        //getAdjustedDatesnew from new table for rounds
        //only need to get dates for one month before and one month after        
        //string sqlAdjDates = "Select OrigDateDDsMMsYYYY,OrigDateYYYYMMDD,OrigDDD,Reason,roundName,Service, moveToDDsMMsYYYY,moveToYYYYMMDD from AdjustedDatesNew " + uniqueRoundDays + " and OrigDateYYYYMMDD>='" + previousMonth.ToString("yyyyMMdd") + "' and OrigDateYYYYMMDD<='" + nextMonth.ToString("yyyyMMdd") + "'and  moveToDDsMMsYYYY!='NoCollect' order by OrigDateYYYYMMDD,roundname desc;";//OLD UNUSED TABEL
        string sqlAdjDates = "Select OrigDateDDsMMsYYYY,OrigDateYYYYMMDD,OrigDDD,Reason,roundName,Service, moveToDDsMMsYYYY,moveToYYYYMMDD,moveToDDD from AdjustedDatesNew ";
        sqlAdjDates += uniqueRoundDays + " and OrigDateYYYYMMDD>='" + previousMonth.ToString("yyyyMMdd") + "' and OrigDateYYYYMMDD<='" + nextMonth.ToString("yyyyMMdd") + "' ";
        sqlAdjDates += "and  (service in (" + servicesForThisUPRN + ")or roundname in (" + roundsForThisUPRN + ")) ";
        sqlAdjDates += "order by OrigDateYYYYMMDD,roundname desc;";
        string[,] adjDates = new string[1, 1];
        if (!sqlAdjDates.Contains("and  (service in ()or roundname in ())")) 
        {
            adjDates = Misc.getDataArrayFromDB(sqlAdjDates, councilName, connection);
        }        
        string noCollectsSQL = "Select OrigDateDDsMMsYYYY,Service,roundName from adjustedDatesNew " + uniqueRoundDays + " and OrigDateYYYYMMDD>='" + startDate.AddDays(-1).ToString("yyyyMMdd") + "' and moveToDDsMMsYYYY='NoCollect' ";
        noCollectsSQL += "order by Service, roundName ,OrigDateYYYYMMDD;";
        string[,] noCollectDates = new string[1, 1];
        if (uniqueRoundDays!="")
        {
            Misc.getDataArrayFromDB(noCollectsSQL, councilName, connection);
        }
        //filter out non applicable lines for  dates for this UPRN 
        //-cater for adjustment to service as well as round for dates
        //eg Falkirk 2018/19 Dec UPRN:136017806 - service Ref goes forward, this Ref Round goes back
        //set col 3 to "IgnoreThisLine if there is a round on a day where service is set to the same date.
        //eg Select OrigDateDDsMMsYYYY,OrigDateYYYYMMDD,OrigDDD,Reason,roundName,Service, moveToDDsMMsYYYY,moveToYYYYMMDD from AdjustedDatesNew  where OrigDDD IN ('Mon','Wed') and OrigDateYYYYMMDD>='20191201' and OrigDateYYYYMMDD<='20200131'and  (service in ('Pls','Grn','Gls','Ref')or roundname in ('Sacks Fq1 Cr56 Mon Wkly Pls','Refuse Cr62 Wed Wkly Grn','Refuse Cr62 Wed Wkly Gls','Refuse Cr62 Wed Wkly Ref')) and moveToDDsMMsYYYY!='NoCollect' order by OrigDateYYYYMMDD,roundname desc;
        string thisDate = "--";
        string thisRoundName = "--";
        string compareDate = "--";
        string compareService = "--";
        for (int a = 1; a < adjDates.GetLength(0); a++)
        {
            thisDate = adjDates[a, 0];
            thisRoundName = adjDates[a, 4];
            if (thisRoundName != "")
            {
                for (int a2 = a + 1; a2 < adjDates.GetLength(0); a2++)
                {
                    compareDate = adjDates[a2, 0];
                    compareService = adjDates[a2, 5];
                    if (thisDate != compareDate) { break; }
                    if (compareService != "")
                    {
                        if (thisRoundName.EndsWith(compareService))
                        {
                            adjDates[a2, 3] = "IgnoreThisLine";//set a service adjustment with a overriding round adjustment to IgnoreThisLine
                            break;
                        }
                    }
                }
            }
        }
        //************************************************************************************************************************
        //************************************************************************************************************************
        //Create calendar Array - This will hold collection dates as a boolean for each date in the range for each container
        //First 3 columns are the basic container details - [0]Bin_Number  / [1]ID / [2]Service3L
        //Each Date in the date range + 1 month before and after from col 3 through to number of required dates in rest of colummn
        //This will create an empty date table in the date range with one row for each container for population of collection dates after created
        //create Empty Calendar template with Bins and Dates Start
        string[,] calendarArray = new string[containers.GetLength(0), daysRequired + 3];
        //populate Headings
        calendarArray[0, 0] = "Bin_number";
        calendarArray[0, 1] = "BinID";
        calendarArray[0, 2] = "BinService3L";

        //initialise Calendar with Dates, Dates, Months WeekNos 
        double serviceRotation = Convert.ToDouble(clientDetails.servicesRotation); // common denominator of all service cycles eg if Ref=2 Rec -3 and Grn 4 then 12         
        string dateDDsMMsYYYY = "";
        string lastWeek = "";
        string monthWeek = "";
        int year = 0;
        int month = 0;
        int dayNo = 0;
        int daysInMonth = 0;
        //parmater definition outside of loop start
        DateTime week1Date = new DateTime();
        DateTime calendarDate = new DateTime();
        TimeSpan daysBetween = new TimeSpan();
        double noOfDays;
        double noOfWeeks;
        double evenBit;
        double weekNo;
        //parmater definition outside of loop end
        for (int d = 0; d < daysRequired; d++)
        {
            dateDDsMMsYYYY = previousMonth.AddDays(d).ToString("dd/MM/yyyy");
            week1Date = DateTime.ParseExact(clientDetails.monWeek1Date, "dd/MM/yyyy", null);
            calendarDate = DateTime.ParseExact(dateDDsMMsYYYY, "dd/MM/yyyy", null);
            daysBetween = calendarDate - week1Date;
            noOfDays = Math.Floor(daysBetween.TotalDays);
            noOfWeeks = Math.Floor(noOfDays / 7);
            evenBit = noOfWeeks % serviceRotation;
            weekNo = evenBit + 1;
            //populate WeekOfMonth                         
            year = Convert.ToInt16(dateDDsMMsYYYY.Substring(6, 4));
            month = Convert.ToInt16(dateDDsMMsYYYY.Substring(3, 2));
            dayNo = Convert.ToInt16(dateDDsMMsYYYY.Substring(0, 2));
            daysInMonth = DateTime.DaysInMonth(year, month);
            if (dayNo < 32) { monthWeek = "MW5"; }
            if (dayNo < 29) { monthWeek = "MW4"; }
            if (dayNo < 22) { monthWeek = "MW3"; }
            if (dayNo < 15) { monthWeek = "MW2"; }
            if (dayNo < 8) { monthWeek = "MW1"; }
            if (dayNo > daysInMonth - 7) { lastWeek = "L"; }
            //populate DateFields
            calendarArray[0, d + 3] = dateDDsMMsYYYY + "#";                                //Date dd/MM/yyyy
            calendarArray[0, d + 3] += previousMonth.AddDays(d).ToString("yyyyMMdd") + "#";//Date yyyyMMdd
            calendarArray[0, d + 3] += previousMonth.AddDays(d).ToString("ddd") + "#";
            calendarArray[0, d + 3] += previousMonth.AddDays(d).ToString("MMM") + "#";
            calendarArray[0, d + 3] += "Wk" + weekNo.ToString() + "#";
            calendarArray[0, d + 3] += monthWeek + lastWeek;
            for (int c = 1; c < containers.GetLength(0); c++)
            {
                calendarArray[c, d + 3] = "[0]";
            }
        }

        //------------------------------------------------------------------------------------------------------------------------
        //we now have all the dates required initialised for each container with each collection cell set to 0. 
        //------------------------------------------------------------------------------------------------------------------------        
        for (int s = 1; s < containers.GetLength(0); s++)
        {
            calendarArray[s, 0] = containers[s, 1];//ID (Bin)
            calendarArray[s, 1] = containers[s, 0];//Bin_number
            calendarArray[s, 2] = Misc.GetService3LForBinType(containers[s, 1]);//Service3L for Bin            
        }

        string[] serviceCycles = clientDetails.servicesCycle.Split('#');
        int allServicesRotation = Convert.ToInt32(clientDetails.servicesRotation);
        //**********************************************************************************************************************************
        //**********************************************************************************************************************************
        //** Container process line by line
        //** - step through each container - get the associated rounds and mark (set to 1) if this gets collected or not based on rounds name
        //**      and many many other parameters. Some are trade specific
        //**********************************************************************************************************************************
        //Define parmaters outside loops start
        ContainerData containerInfo = new ContainerData();
        string containerService3L;
        bool containerHasMatchingRoundForService = false;
        string matchRoundName;;
        int thisServiceCycle;;
        string containerService3LAlso;
        int alsoServiceCycle;
        colRoundname alsoRoundName = new colRoundname();
        WaxRoundNewDetails wxRndDetails = new WaxRoundNewDetails();
        string detailsForRound;
        string[] calendarArrayDates = new string[1];
        string calendarArrDDsMMsYYYY = "";//Wk1 Wk2 Wk3.... Wkly
        string calendarArrDDD = "";//Mon Tue Wed Thu ...
        string calendarArrWkNo = "";
        double CalendarWkNo = 0;
        string[] splitRoundDetails = new string[1];
        string roundWeek = "";
        string roundDDD = "";
        string[] roundsForThisService = new string[1];
        bool WeeklyCollection = false;
        int thisRoundWeek = -1;
        bool collectThis = false;
        bool adjustForStartDate = false;
        bool adjustForEndDate = false;        
        int startDateYYYYMMDD = 19730101;
        int endDateYYYYMMDD = 20251231;
        int calendarArrYYYYMMDD;
        int suspensionStart = 19730101;        
        int suspensionEnd = 20501231;
        int calendarArrYYYYMMDD2;
        //Define parmaters outside loops end
        for (int c = 1; c < containers.GetLength(0); c++)
        {            
            containerInfo = ContainerData.GetContainerParameters(containers, c);            
            containerService3L = calendarArray[c, 2];
            //check has matching round
            containerHasMatchingRoundForService = false;
            matchRoundName = "";
            thisServiceCycle = -1;
            containerService3LAlso = "XXX";      //KP Edit 12Jun19
            alsoServiceCycle = -1;//edit KP 12Jun19
            if (containerInfo.AlsoCollectedby != "")       //KP Edit 12Jun19
            {
                alsoRoundName = colRoundname.GetRoundNameParameters(containerInfo.AlsoCollectedby);      //KP Edit 12Jun19
                containerService3LAlso = alsoRoundName.service3L;      //KP Edit 12Jun19
                for (int s = 0; s < serviceCycles.Length; s++)
                {
                    if (serviceCycles[s].StartsWith(containerService3LAlso))
                    {
                        alsoServiceCycle = Convert.ToInt16(serviceCycles[s].Replace(containerService3LAlso + "(", "").Replace(")", ""));//how ofter the also serviceRepeats//edit KP 12Jun19
                    }
                }
            }

            for (int r = 1; r < rounds.GetLength(0); r++)
            {
                wxRndDetails = WaxRoundNewDetails.getRoundDetails(rounds, r);
                if (wxRndDetails.RoundName.EndsWith(containerService3L) || wxRndDetails.RoundName.EndsWith(containerService3LAlso))      //KP Edit 12Jun19
                {
                    containerHasMatchingRoundForService = true;
                    detailsForRound = wxRndDetails.RoundName;
                    if (!matchRoundName.Contains(detailsForRound))
                    {
                        matchRoundName += detailsForRound + "#";
                        for (int s = 0; s < serviceCycles.Length; s++)
                        {
                            if (serviceCycles[s].StartsWith(containerService3L))
                            {
                                thisServiceCycle = Convert.ToInt16(serviceCycles[s].Replace(containerService3L + "(", "").Replace(")", ""));//how often this serviceRepeats
                            }
                        }
                    }
                }
            }
            if (alsoServiceCycle != -1) { thisServiceCycle = alsoServiceCycle; } //edit KP 12Jun19
            if (containerHasMatchingRoundForService)
            {
                //set date based on round name / mon week 1                 
                calendarArrayDates = new string[1];
                calendarArrDDsMMsYYYY = "";//Wk1 Wk2 Wk3.... Wkly
                calendarArrDDD = "";//Mon Tue Wed Thu ...
                calendarArrWkNo = "";
                CalendarWkNo = 0;
                splitRoundDetails = new string[1];
                roundWeek = "";
                roundDDD = "";
                roundsForThisService = new string[1];
                WeeklyCollection = false;
                thisRoundWeek = -1;
                collectThis = false;
                for (int d = 3; d < daysRequired + 3; d++)
                {
                    calendarArrayDates = calendarArray[0, d].Split('#');
                    calendarArrDDsMMsYYYY = calendarArrayDates[0];//Wk1 Wk2 Wk3.... Wkly
                    calendarArrDDD = calendarArrayDates[2];//Mon Tue Wed Thu ...
                    calendarArrWkNo = calendarArrayDates[4];
                    CalendarWkNo = Convert.ToDouble(calendarArrWkNo.Replace("Wk", ""));
                    //if (matchRoundName.Contains("Gls")) { testBreak = "Here"; }
                    //if (calendarArrayDates[0] == "25/10/2018") { testBreak = "Here"; }
                    if (matchRoundName.Contains("AHP") && calendarArrayDates[0] == "10/12/2019") { BreakHere(); }
                    roundsForThisService = matchRoundName.Split('#');
                    for (int r = 0; r < roundsForThisService.Length - 1; r++)//This allows for multiple services per Container - eg 1 refuse container but gets a tue and Thu Collection
                    {
                        splitRoundDetails = roundsForThisService[r].Split(' ');
                        roundWeek = splitRoundDetails[splitRoundDetails.Length - 2].Replace("Wk", "");
                        WeeklyCollection = false;
                        if (roundWeek == "ly") { WeeklyCollection = true; }
                        roundDDD = splitRoundDetails[splitRoundDetails.Length - 3];
                        if (roundDDD == calendarArrDDD) //see if matching Day
                        {
                            if (WeeklyCollection)
                            {
                                calendarArray[c, d] = calendarArray[c, d].Replace("[0]", "[1]");
                                calendarArray[c, d] += " RW";//This container gets a collections based on RoundName containing Wkly
                            }
                            else
                            {
                                if (thisServiceCycle == allServicesRotation)//check if it's the same service cycle for all services
                                {
                                    if (CalendarWkNo == Convert.ToDouble(roundWeek))
                                    {

                                        calendarArray[c, d] = calendarArray[c, d].Replace("[0]", "[1]");
                                        calendarArray[c, d] += " RN";//This container gets a collections based on RoundName
                                    }
                                }
                                else
                                {
                                    //Different service cycle from other services
                                    //**CHECK FOR 6 WEEK ROTATION ON A SERVICE WITH 2 WEEK CYCLE
                                    thisRoundWeek = Convert.ToInt32(roundWeek);
                                    collectThis = false;
                                    //thisRoundWeek         1                     
                                    //thisServiceCycle      2                    1 1 1 1 1 1 1 1
                                    //allServicesRotation   4 Calendar runs from 1 2 3 4 1 2 3 4
                                    //CalendarWkNo          3              
                                    //Collect This           
                                    if (CalendarWkNo == Convert.ToDouble(roundWeek)) { collectThis = true; }
                                    if (!collectThis)//KP Edit 04Nov19 - This handles for example a collection on calendar Wk5 of a 6 week service cycle with the round name being Wk1 (** This would need to show a collecton on Wk1, Wk3 and Wk5)
                                    {
                                        for (int k = Convert.ToInt32(CalendarWkNo); k > 0; k = k - thisServiceCycle)
                                        {
                                            if (k == thisRoundWeek)
                                            {
                                                collectThis = true;
                                                break;
                                            }
                                        }
                                    }
                                    //if (thisServiceCycle                                                          + thisRoundWeek == CalendarWkNo) { collectThis = true; }
                                    //if (thisServiceCycle + thisServiceCycle                                       + thisRoundWeek == CalendarWkNo) { collectThis = true; }
                                    //if (thisServiceCycle + thisServiceCycle + thisServiceCycle                    + thisRoundWeek == CalendarWkNo) { collectThis = true; }
                                    //if (thisServiceCycle + thisServiceCycle + thisServiceCycle + thisServiceCycle + thisRoundWeek == CalendarWkNo) { collectThis = true; }
                                    if (thisServiceCycle == 1) { collectThis = true; }
                                    if (collectThis)
                                    {
                                        calendarArray[c, d] = calendarArray[c, d].Replace("[0]", "[1]");
                                        calendarArray[c, d] += " RN";//This container gets a collections based on RoundName                                         
                                    }
                                }
                            }
                        }
                    }
                }

                //Adjust Collection Dates for start / end date of container
                adjustForStartDate = false;
                adjustForEndDate = false;
                if (containerInfo.Start_date != "") { adjustForStartDate = true; }
                if (containerInfo.End_date != "") { adjustForEndDate = true; }
                startDateYYYYMMDD = 19730101;
                endDateYYYYMMDD = 20251231;
                if (adjustForStartDate)
                {
                    startDateYYYYMMDD = Convert.ToInt32(containerInfo.Start_date.Substring(6, 4) + containerInfo.Start_date.Substring(3, 2) + containerInfo.Start_date.Substring(0, 2));
                }
                if (adjustForEndDate)
                {
                    endDateYYYYMMDD = Convert.ToInt32(containerInfo.End_date.Substring(6, 4) + containerInfo.End_date.Substring(3, 2) + containerInfo.End_date.Substring(0, 2));
                }
                for (int d = 3; d < daysRequired + 3; d++)
                {
                    calendarArrayDates = calendarArray[0, d].Split('#');
                    calendarArrYYYYMMDD = Convert.ToInt32(calendarArrayDates[1]);
                    if (calendarArrYYYYMMDD < startDateYYYYMMDD)
                    {
                        //date before container start date so set to no collection and set indicator as SD
                        calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                        calendarArray[c, d] += " SD";
                    }
                    if (calendarArrYYYYMMDD > endDateYYYYMMDD)
                    {
                        //date after container end date so set to no collection and set indicator as ED
                        calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                        calendarArray[c, d] += " ED";
                    }
                }
                //Adjust for Trade Suspension Dates
                if (tradePropHasSuspensions && containerInfo.tradeContainer)
                {
                    for (int s = 1; s < tradeSuspensions.GetLength(0); s++)
                    {
                        suspensionStart = 19730101;
                        if (tradeSuspensions[s, 1] != "")
                        {
                            suspensionStart = Convert.ToInt32(tradeSuspensions[s, 1]);
                        }
                        suspensionEnd = 20501231;
                        if (tradeSuspensions[s, 2] != "")
                        {
                            suspensionEnd = Convert.ToInt32(tradeSuspensions[s, 2]);
                        }
                        for (int d = 3; d < daysRequired + 3; d++)
                        {
                            calendarArrayDates = calendarArray[0, d].Split('#');
                            calendarArrYYYYMMDD2 = Convert.ToInt32(calendarArrayDates[1]);
                            if (calendarArrYYYYMMDD2 >= suspensionStart && calendarArrYYYYMMDD2 <= suspensionEnd)
                            {
                                //date in between suspension dates so set to 0
                                calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                calendarArray[c, d] += "SS:" + tradeSuspensions[s, 3];//add suspension reason
                            }
                        }
                    }
                }

                //Adjust for Trade Options                
                if (clientDetails.hasTrade)
                {
                    if (hasTradeContainer && containerInfo.tradeContainer)
                    {
                        #region Trade Container Calendar Adjustments
                        //numOfCollectionWeeks is override for billing of the maximum number of billable weeks.                                                                    

                        //** PATTERNS START**
                        #region PATTERNS application                                                
                        bool patternFound = false;
                        string patternString = "";                        
                        if (containerInfo.Pattern == null || containerInfo.Pattern.ToLower() == "default") { containerInfo.Pattern = ""; }
                        if (containerInfo.Pattern != "")
                        {
                            //Container has a pattern so apply container pattern to Calendar
                            patternFound = false;
                            for (int p = 1; p < tradePatterns.GetLength(0); p++)
                            {
                                if (tradePatterns[p, 1] == containerInfo.Pattern)
                                {
                                    patternFound = true;
                                    //get roundinfo for pattern application from first round
                                    for (int r = 1; r < rounds.GetLength(0); r++)
                                    {
                                        WaxRoundNewDetails roundDetails = WaxRoundNewDetails.getRoundDetails(rounds, r);
                                        //if (roundDetails.UPRN == "NoData") 
                                        if (roundDetails.isTradeRound)
                                        {
                                            if (roundDetails.service3L == containerInfo.containerService3L)
                                            {
                                                string patCodeToAdd = p + "#CP#" + roundDetails.Week + "#" + roundDetails.Day_DDD + "#" + roundDetails.Frequency + "#" + containerInfo.Pattern + "$";
                                                if (!patternString.Contains(patCodeToAdd)) //only add if pattern not already added
                                                {
                                                    patternString += patCodeToAdd;
                                                }                                     
                                            }
                                        }
                                    }                                    
                                }
                            }
                            //end container has a pattern
                        }
                        else
                        {
                            //no container pattern, so check Trade Rounds for Patterns
                            if (tradeRoundHasPattern)
                            {
                                //string[]rndPatterns=roundPatterns.Split('$');
                                for (int r = 1; r < rounds.GetLength(0); r++)
                                {
                                    WaxRoundNewDetails roundDetails = WaxRoundNewDetails.getRoundDetails(rounds, r);
                                    if (roundDetails.service3L == containerInfo.containerService3L)
                                    {
                                        if (roundDetails.Pattern == null || roundDetails.Pattern.ToLower() == "default") { roundDetails.Pattern = ""; }
                                        if (roundDetails.Pattern != "")
                                        {
                                            patternFound = false;
                                            for (int p = 1; p < tradePatterns.GetLength(0); p++)
                                            {

                                                if (tradePatterns[p, 1] == roundDetails.Pattern)
                                                {
                                                    patternFound = true;
                                                    string patCodeToAdd = p + "#RP#" + roundDetails.Week + "#" + roundDetails.Day_DDD + "#" + roundDetails.Frequency + "#" + roundDetails.Pattern + "$";
                                                    if (!patternString.Contains(patCodeToAdd)) //only add if pattern not already added
                                                    {
                                                        patternString += patCodeToAdd;
                                                    }
                                                }
                                            }
                                        }
                                        else 
                                        {
                                            //round Pattern to be used, but this round has no pattern. One of the other rounds does, so this needs a default pattern of collects every day                                            
                                            string patCodeToAdd = (tradePatterns.GetLength(0)) + "#RP#" + roundDetails.Week + "#" + roundDetails.Day_DDD + "#" + roundDetails.Frequency + "#ALL$";
                                            if (!patternString.Contains(patCodeToAdd)) //only add if pattern not already added
                                            {
                                                patternString += patCodeToAdd;
                                                //expand TradePatterns to include an All one of All 1s
                                                tradePatterns = Misc.ResizeArray(tradePatterns, 1, 0);
                                                int newRow = tradePatterns.GetLength(0) - 1;
                                                tradePatterns[newRow, 0] = newRow.ToString();
                                                tradePatterns[newRow, 1] = "All";
                                                tradePatterns[newRow, 2] = "";//No Desc
                                                tradePatterns[newRow, 3] = "";//NoYear
                                                for (int j = 4; j < tradePatterns.GetLength(1) -2; j++)
                                                {
                                                    tradePatterns[newRow, j] = "1";
                                                }
                                                tradePatterns[newRow, tradePatterns.GetLength(1) - 2] = "Webaspx";//Edited By
                                                tradePatterns[newRow, tradePatterns.GetLength(1) - 1] = DateTime.Now.ToString("dd/MM/yyyy");//Edited Date
                                            }
                                        }
                                    }
                                }
                                //end - no container pattern but round pattern present for this container
                            }
                            else
                            {
                                //no container or round pattern, so check property (TradeData) pattern
                                if (tradeInfo.Pattern == null || tradeInfo.Pattern.ToLower() == "default") { containerInfo.Pattern = ""; }
                                if (tradeInfo.Pattern != "" && tradeInfo.Pattern != "NotRequestedInSQLSelect")
                                {
                                    //Property has a pattern so apply property pattern to Calendar
                                    patternFound = false;
                                    for (int p = 1; p < tradePatterns.GetLength(0); p++)//find pattern in previously downloaded array
                                    {
                                        if (tradePatterns[p, 1] == tradeInfo.Pattern)
                                        {
                                            patternFound = true;
                                            //patternString += p + "#PP$";                                            
                                            //get roundinfo for pattern application from first round
                                            for (int r = 1; r < rounds.GetLength(0); r++)
                                            {
                                                WaxRoundNewDetails roundDetails = WaxRoundNewDetails.getRoundDetails(rounds, r);
                                                //if (roundDetails.UPRN == "NoData")
                                                if (roundDetails.service3L == containerInfo.containerService3L) { }
                                                patternFound = true;
                                                string patCodeToAdd = p + "#PP#" + roundDetails.Week + "#" + roundDetails.Day_DDD + "#" + roundDetails.Frequency + "#" + tradeInfo.Pattern + "$";
                                                if (!patternString.Contains(patCodeToAdd)) //only add if pattern not already added
                                                {
                                                    patternString += patCodeToAdd;                                                    
                                                }
                                            }                                            
                                        }                                        
                                    }
                                    //end - no container or round pattern so use tradeData pattern (property pattern) for this container
                                }
                            }                                            
                        }
                        if (patternFound)// a pattern (or multiple patterns have been found, so process
                        {
                            string[] patternsFound = Misc.removeLastChar(patternString).Split('$');
                            //"   n  #  RP  #" + roundDetails.Week + "#" + roundDetails.Day_DDD + "#" + roundDetails.Frequency + "#" + patternName"$";
                            //[0]Num # Type #     RndWk               # RndDay                     # rndFrq                       # APR-SEP
                            //[1]Num # Type #     RndWk               # RndDay                     # rndFrq                       # APR-SEP
                            //[2]Num # Type #     RndWk               # RndDay                     # rndFrq                       # OCT-MAR
                            //collate patterns into one binary string                                                        
                            string[,] allPatternsForThisBin = new string[(patternsFound.Length + 1), calendarArray.GetLength(1) - 4];
                            //Row0 - Dates : 01/03/2019#20190301#Fri#Mar#Wk4#MW1#01-Mar
                            //Row0 - Dates : dd/MM/yyyy#yyyyMMdd#ddd#MMM#WxX#MWx#dd-MMM
                            //Row0 - Dates : [0]        [1]      [2] [3] [4] [5] [6]     **Split('#')
                            //Row1 - Pattern1 data
                            //Row2 - Pattern2 data
                            //Row3 - Pattern x data
                            //initialise Full Master Patern
                            int currentYear = Convert.ToInt32(calendarStartDDMMYYYY.Substring(6, 4));
                            string yearsInPeriod = "";
                            string cellYear = "";
                            for (int d = 3; d < (calendarArray.GetLength(1) - 2); d++)
                            {
                                //01/03/2019#20190301#Fri#Mar#Wk4#MW1
                                string dd_MMM = calendarArray[0, d].Split('#')[3] + "_" + calendarArray[0, d].Substring(0, 2);
                                allPatternsForThisBin[0, d - 2] = calendarArray[0, d] + "#" + dd_MMM;
                                cellYear = calendarArray[0, d].Split('#')[0].Substring(6, 4);
                                if (!yearsInPeriod.Contains(cellYear)) { yearsInPeriod += cellYear + "#"; }
                            }
                            //populate grid based on data from round and  Pattern (may have to run over pattern 2 or 3 times to cover fullinvoice period
                            //**consider year                            
                            //double noOfYearsInPeriod = daysRequired / 365.0;
                            //compile a master list of when the pattern is on or off.
                            string typeOfPat = "";
                            string rndWk = "";
                            string rndDay = "";
                            string rndFrq = "";
                            string yearApplicable = "";
                            string patternOnOrOff = "";
                            string[] yearsInRange = Misc.removeLastChar(yearsInPeriod).Split('#');
                            for (int p = 0; p < patternsFound.Length; p++)
                            {
                                string[] splitPatDetail = patternsFound[p].Split('#');
                                int patRow = Convert.ToInt32(splitPatDetail[0]);
                                allPatternsForThisBin[(p + 1), 0] = splitPatDetail[5];//name
                                typeOfPat = splitPatDetail[1]; //CP RP PP (ContainerPattern RoundPattern PropertyPattern)
                                rndWk = splitPatDetail[2];
                                rndDay = splitPatDetail[3];
                                rndFrq = splitPatDetail[4];
                                //populate relevant cells for year
                                //**note - tradePatterns contains 29Feb for leap years
                                string patternYr = "";
                                
                                if (tradePatterns[patRow, 3] != "")//check year is present
                                {
                                    patternYr = tradePatterns[patRow, 3];
                                }                                

                                //populate fullMaster Row from tradePattern Row                                                                            
                                for (int j = 1; j < allPatternsForThisBin.GetLength(1); j++)
                                {
                                    //CalendarArrayDateHeader headerDates = CalendarArrayDateHeader.GetDateHeaderSplits(allPatternsForThisBin[0, j]);
                                    for (int y = 0; y < yearsInRange.Length; y++)
                                    {
                                        if (allPatternsForThisBin[0, j].Contains("/" + yearsInRange[y] + "#"))      //check year
                                        {
                                            for (int patCol = 4; patCol < tradePatterns.GetLength(1); patCol++) //find date in downloaded database pattern
                                            {
                                                if (allPatternsForThisBin[0, j].Contains(tradePatterns[0, patCol])) //Check Date Contains eg Mar_08
                                                {
                                                    //if (tradePatterns[patRow, patCol] == "1") { allPatternsForThisBin[(p + 1), j] = "1#" + typeOfPat + "#PatOn#"  + rndWk + "#" + rndDay + "#" + rndFrq; }
                                                    //if (tradePatterns[patRow, patCol] == "0") { allPatternsForThisBin[(p + 1), j] = "0#" + typeOfPat + "#PatOff#" + rndWk + "#" + rndDay + "#" + rndFrq; }
                                                    if (tradePatterns[patRow, patCol] == "1") { allPatternsForThisBin[(p + 1), j] = typeOfPat + "#PatOn#" + rndWk + "#" + rndDay + "#" + rndFrq + "#YearYes"; }
                                                    if (tradePatterns[patRow, patCol] == "0") { allPatternsForThisBin[(p + 1), j] = typeOfPat + "#PatOff#" + rndWk + "#" + rndDay + "#" + rndFrq + "#YearYes"; }
                                                    //if (tradePatterns[patRow, patCol] == "1")
                                                    //{
                                                    //if (allPatternsForThisBin[0, j].Contains(rndWk))                //Check RoundWeek
                                                    //{
                                                    //if (allPatternsForThisBin[0, j].Contains(rndDay))           //Check RoundDay
                                                    //{
                                                    //if (allPatternsForThisBin[0, j].Contains(rndFrq))       //Check RoundFreq
                                                    //{
                                                    //allPatternsForThisBin[(p + 1), j] = allPatternsForThisBin[(p + 1), j].Replace("0", "1");//This should now be either 1CPPatOn 1CPPatOff

                                                    if (patternYr != "" && !allPatternsForThisBin[0, j].Contains(patternYr))
                                                    {
                                                        allPatternsForThisBin[(p + 1), j] = typeOfPat + "#PatOff#" + rndWk + "#" + rndDay + "#" + rndFrq + "#YearNA"; //reset if set to 1 //This should now be either 0PatOn 0PatOff
                                                                                                                                                                      //allPatternsForThisBin[(p + 1), j] = "0PatOff");//reset if set to 1                                                                    
                                                    }
                                                    break; //check next date
                                                           //}
                                                           //}
                                                           // }
                                                           //}
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // at this point allPatternsForThisBin contains an array. row[0] is the dates in the date range, subsequent rows are populated with each individual pattern found that affects this Bin
                            //eg UPRN 100110308360 Webaspx
                            //[i,0]     [i,1]                                       [i,2]                                       ....    [i,62]
                            //          01/03/2020#20200301#Sun#Mar#Wk2#MW1#Mar_01  02/03/2020#20200302#Mon#Mar#Wk3#MW1#Mar_02  ....    01/05/2020#20200501#Fri#May#Wk3#MW1L#May_01
                            //MAY-JUL   RP#PatOff#Wk1#Tue#                          RP#PatOff#Wk1#Tue#                          ....    RP#PatOn#Wk1#Tue#
                            //NOV-DEC   RP#PatOff#Wk3#Tue#                          RP#PatOff#Wk3#Tue#                          ....    RP#PatOn#Wk3#Tue#

                            string[,] masterPatternForThisBin = new string[2, allPatternsForThisBin.GetLength(1)];
                            string cell = "";
                            //string[] splitDateInfo = new string[1];
                            allPatternsForThisBin[0, 0] = "";
                            //compile the list of patterns into the compiled list - only compiles PatOn from all pattens for this container
                            // at this point full Master pattern contains a header row, plus a row for each pattern for this property - each cell contains the pattern type, day, we
                            for (int j = 1; j < allPatternsForThisBin.GetLength(1); j++) // go through each date
                            {
                                //eg
                                //  01/03/2019#20190301#Fri#Mar#Wk4#MW1#Mar_01
                                //  01/03/2019  [0]
                                //  20190301    [1]
                                //  Fri         [2]
                                //  Mar         [3]
                                //  Wk4         [4]
                                //  MW1         [5]
                                //  Mar_01      [6]
                                CalendarArrayDateHeader splitDateInfo = CalendarArrayDateHeader.GetDateHeaderSplits(allPatternsForThisBin[0, j]);
                                //splitDateInfo = allPatternsForThisBin[0, j].Split('#');
                                masterPatternForThisBin[0, j] = allPatternsForThisBin[0, j];    //populate header with dd/MM/yyyy                                                                
                                //if (splitDateInfo.MMM_dd == "Apr_14" || 
                                //    splitDateInfo.MMM_dd == "May_12" || 
                                //    splitDateInfo.MMM_dd == "May_26" ||
                                //    splitDateInfo.MMM_dd == "Nov_10" ||
                                //    splitDateInfo.MMM_dd == "Nov_24") //test for UPRN 100110308360 on webaspx May and Dec pattern
                                //{
                                //    Misc.BreakHere();
                                //}
                                masterPatternForThisBin[1, j] = "";
                                //masterPatternForThisBin only has 2 rows, the header of dates and the Details of the patterns to be used
                                for (int i = 1; i < allPatternsForThisBin.GetLength(0); i++)//check each pattern occurance for thsi Bin this date and decide whether on or off
                                {
                                    cell = allPatternsForThisBin[i, j];
                                    string[] splitPatDetails2 = cell.Split('#');
                                    //typeOfPat + "#PatOn#"  + rndWk + "#" + rndDay + "#" + rndFrq + "#YearYes";
                                    //RP#PatOff#Wk1#Tue#YearYes
                                    //RP#PatOn#Wk1#Tue#YearNA
                                    typeOfPat       = splitPatDetails2[0];
                                    patternOnOrOff  = splitPatDetails2[1];
                                    rndWk           = splitPatDetails2[2];
                                    rndDay          = splitPatDetails2[3];
                                    rndFrq          = splitPatDetails2[4];
                                    yearApplicable  = splitPatDetails2[5];
                                    if (typeOfPat == "RP") //round pattern, so can be multi pattern
                                    {
                                        //only have to take day and week into consideration if a round pattern, as multi patterns can be applied across 2 or more rounds                                                                                
                                        if (masterPatternForThisBin[1, j] == "")
                                        {
                                            if (patternOnOrOff == "PatOn")
                                            {
                                                if (splitDateInfo.day3L == rndDay)
                                                {
                                                    if (splitDateInfo.weekOfCycleWk == rndWk || rndWk == "Wkly")
                                                    {
                                                        masterPatternForThisBin[1, j] = cell; // if full day, week match and pattern on, populate ll details                                             
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (masterPatternForThisBin[1, j].Contains("PatOff"))//if not already set to On from previous pattern
                                            {
                                               // if (patternOnOrOff == "PatOn")
                                                {
                                                    if (splitDateInfo.day3L == rndDay)
                                                    {
                                                        if (splitDateInfo.weekOfCycleWk == rndWk || rndWk == "Wkly")
                                                        {
                                                            masterPatternForThisBin[1, j] = cell; // if full day, week match and pattern on, populate ll details                                             
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //PP or CP
                                        // property or container patterns will have only 1 pattern
                                        cell = allPatternsForThisBin[i, j];
                                        masterPatternForThisBin[1, j] = cell;
                                    }
                                }
                                //if (splitDateInfo.MMM_dd == "Apr_14" ||
                                //        splitDateInfo.MMM_dd == "May_12" ||
                                //        splitDateInfo.MMM_dd == "May_26" ||
                                //        splitDateInfo.MMM_dd == "Nov_10" ||
                                //        splitDateInfo.MMM_dd == "Nov_24") //test for UPRN 100110308360 on webaspx May and Dec pattern
                                //{
                                //    Misc.BreakHere();
                                //}
                            }                            

                            //Apply pattern
                            //blank all but pattern inclusive from calendar
                            for (int d = 3; d < daysRequired + 3; d++)//step through each day of the period for this container to check for a 0 in the matching pattern
                            {
                                calendarArrayDates = calendarArray[0, d].Split('#');//top row of CalendarNew with all formats of dates appended with #
                                calendarArrDDsMMsYYYY = calendarArrayDates[0];
                                for (int pd = 1; pd < masterPatternForThisBin.GetLength(1) - 2; pd++)//last 2 columns are editedDate and editedBy
                                {
                                    if (calendarArrDDsMMsYYYY == masterPatternForThisBin[0, pd].Split('#')[0])
                                    {
                                        if (masterPatternForThisBin[1, pd]==""|| masterPatternForThisBin[1, pd].Contains("PatOff"))
                                        //if (masterPatternForThisBin[1, pd].Contains( "PatOff"))
                                        {
                                            calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");                                            
                                        }
                                        calendarArray[c, d] += " "+ masterPatternForThisBin[1, pd];//show patern on or off


                                    }
                                }
                            }
                        }

                        #endregion
                        //** PATTERNS END**

                        #region Trade - Collection Days And Frequency                        
                        //if trade collection - set on collectionDays (Container)                    
                        //if (containers[c, conCollectionDaysCol] != "" && containers[c, conCollectionDaysCol] != "0000000")
                        if (containerInfo.CollectionDays != "" && containerInfo.CollectionDays != "0000000")
                        {
                            //blank out any days that aren't in the collections days if this field is populated for this container
                            bool considerCollectionDays = true;
                            //string collectionDays = containers[c, conCollectionDaysCol];
                            if (containerInfo.CollectionDays == "0000000" || containerInfo.CollectionDays == "") { considerCollectionDays = false; }
                            if (considerCollectionDays)
                            {
                                string collectionDaysString = "";
                                if (containerInfo.CollectionDays.Length == 3) //patch for already populated old format trade Colelction days as Mon, Tue Wed not binary week eg 1000100
                                {
                                    collectionDaysString = containerInfo.CollectionDays;
                                }
                                else
                                {
                                    if (containerInfo.CollectionDays.Substring(0, 1) == "1") { collectionDaysString += "Mon"; }
                                    if (containerInfo.CollectionDays.Substring(1, 1) == "1") { collectionDaysString += "Tue"; }
                                    if (containerInfo.CollectionDays.Substring(2, 1) == "1") { collectionDaysString += "Wed"; }
                                    if (containerInfo.CollectionDays.Substring(3, 1) == "1") { collectionDaysString += "Thu"; }
                                    if (containerInfo.CollectionDays.Substring(4, 1) == "1") { collectionDaysString += "Fri"; }
                                    if (containerInfo.CollectionDays.Substring(5, 1) == "1") { collectionDaysString += "Sat"; }
                                    if (containerInfo.CollectionDays.Substring(6, 1) == "1") { collectionDaysString += "Sun"; }
                                }

                                for (int d = 3; d < daysRequired + 3; d++)
                                {
                                    calendarArrayDates = calendarArray[0, d].Split('#');
                                    if (!collectionDaysString.Contains(calendarArrayDates[2])) //check if day matches one of the collection days specified - if not blank out and add reason CD
                                    {
                                        calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                        calendarArray[c, d] += " CD";
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (tradeInfo.MultipleCollectionDays != "" && tradeInfo.MultipleCollectionDays != "NotRequestedInSQLSelect")
                            {
                                bool considerPropCollectionDays = true;
                                if (tradeInfo.MultipleCollectionDays == "0000000" || tradeInfo.MultipleCollectionDays == "") { considerPropCollectionDays = false; }
                                if (considerPropCollectionDays)
                                {
                                    string collectionDaysString = "";
                                    if (tradeInfo.MultipleCollectionDays.Substring(0, 1) == "1") { collectionDaysString += "Mon"; }
                                    if (tradeInfo.MultipleCollectionDays.Substring(1, 1) == "1") { collectionDaysString += "Tue"; }
                                    if (tradeInfo.MultipleCollectionDays.Substring(2, 1) == "1") { collectionDaysString += "Wed"; }
                                    if (tradeInfo.MultipleCollectionDays.Substring(3, 1) == "1") { collectionDaysString += "Thu"; }
                                    if (tradeInfo.MultipleCollectionDays.Substring(4, 1) == "1") { collectionDaysString += "Fri"; }
                                    if (tradeInfo.MultipleCollectionDays.Substring(5, 1) == "1") { collectionDaysString += "Sat"; }
                                    if (tradeInfo.MultipleCollectionDays.Substring(6, 1) == "1") { collectionDaysString += "Sun"; }

                                    for (int d = 3; d < daysRequired + 3; d++)
                                    {
                                        calendarArrayDates = calendarArray[0, d].Split('#');
                                        if (!collectionDaysString.Contains(calendarArrayDates[2])) //check if day matches one of the collection days specified - if not blank out and add reason CD
                                        {
                                            calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                            calendarArray[c, d] += " PD";
                                        }
                                    }
                                }
                            }
                        }
                        //if trade collection - set on frequency (Container) **Must also have CollectionDay(s) present                    
                        //string collectionFrequency = "";
                        //if (containers[c, conFrequencyCol] != "" && containers[c, conCollectionDaysCol] != "")
                        if (containerInfo.Frequency != "" && containerInfo.CollectionDays != "")
                        {
                            //collectionFrequency = containers[c, conFrequencyCol];
                            //#Monthly 1st#Monthly 2nd#Monthly 3rd#Monthly 4th#Monthly Last#Monthly 1st 2nd#Monthly 3rd 4th#Weeks 1 and 3#Weeks 2 and 4#Week1#Week2#Week3#Week4    - possible values of frequency
                            for (int d = 3; d < daysRequired + 3; d++)
                            {
                                calendarArrayDates = calendarArray[0, d].Split('#');
                                bool dontCollect = true;
                                if (containerInfo.Frequency == "Monthly 1st" && calendarArrayDates[5].StartsWith("MW1")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Monthly 2nd" && calendarArrayDates[5].StartsWith("MW2")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Monthly 3rd" && calendarArrayDates[5].StartsWith("MW3")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Monthly 4th" && calendarArrayDates[5].StartsWith("MW4")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Monthly Last" && calendarArrayDates[5].EndsWith("L")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Monthly 1st 2nd" && (calendarArrayDates[5].StartsWith("MW1") || calendarArrayDates[5].StartsWith("MW2"))) { dontCollect = false; }
                                if (containerInfo.Frequency == "Monthly 3rd 4th" && (calendarArrayDates[5].StartsWith("MW3") || calendarArrayDates[5].StartsWith("MW4"))) { dontCollect = false; }
                                if (containerInfo.Frequency == "Weeks 1 and 3" && (calendarArrayDates[4] == "Wk1" || calendarArrayDates[4] == "Wk3")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Weeks 2 and 4" && (calendarArrayDates[4] == "Wk2" || calendarArrayDates[4] == "Wk4")) { dontCollect = false; }
                                if (containerInfo.Frequency == "Week1" && calendarArrayDates[4] == "Wk1") { dontCollect = false; }
                                if (containerInfo.Frequency == "Week2" && calendarArrayDates[4] == "Wk2") { dontCollect = false; }
                                if (containerInfo.Frequency == "Week3" && calendarArrayDates[4] == "Wk3") { dontCollect = false; }
                                if (containerInfo.Frequency == "Week4" && calendarArrayDates[4] == "Wk4") { dontCollect = false; }

                                if (dontCollect)
                                {
                                    //day is a valid day at this point / blank all apart from frequency requirement
                                    //#Monthly 1st#Monthly 2nd#Monthly 3rd#Monthly 4th#Monthly Last         - possible values of frequency
                                    calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                    calendarArray[c, d] += " DF";
                                }
                            }
                        }
                        else
                        {
                            //Handle property Collection Frequency                                                  
                            if (tradeInfo.CollectionFrequency != "" && tradeInfo.MultipleCollectionDays != "" && tradeInfo.MultipleCollectionDays != "NotRequestedInSQLSelect")
                            {
                                //#Monthly 1st#Monthly 2nd#Monthly 3rd#Monthly 4th#Monthly Last         - possible values of frequency                                            
                                for (int d = 3; d < daysRequired + 3; d++)
                                {
                                    calendarArrayDates = calendarArray[0, d].Split('#');
                                    bool dontCollect = true;
                                    if (tradeInfo.CollectionFrequency == "Monthly 1st" && calendarArrayDates[5].StartsWith("MW1")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Monthly 2nd" && calendarArrayDates[5].StartsWith("MW2")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Monthly 3rd" && calendarArrayDates[5].StartsWith("MW3")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Monthly 4th" && calendarArrayDates[5].StartsWith("MW4")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Monthly Last" && calendarArrayDates[5].EndsWith("L")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Monthly 1st 2nd" && (calendarArrayDates[5].StartsWith("MW1") || calendarArrayDates[5].StartsWith("MW2"))) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Monthly 3rd 4th" && (calendarArrayDates[5].StartsWith("MW3") || calendarArrayDates[5].StartsWith("MW4"))) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Weeks 1 and 3" && (calendarArrayDates[4] == "Wk1" || calendarArrayDates[4] == "Wk3")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Weeks 2 and 4" && (calendarArrayDates[4] == "Wk2" || calendarArrayDates[4] == "Wk4")) { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Week1" && calendarArrayDates[4] == "Wk1") { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Week2" && calendarArrayDates[4] == "Wk2") { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Week3" && calendarArrayDates[4] == "Wk3") { dontCollect = false; }
                                    if (tradeInfo.CollectionFrequency == "Week4" && calendarArrayDates[4] == "Wk4") { dontCollect = false; }
                                    if (dontCollect)
                                    {
                                        //day is a valid day at this point / blank all apart from frequency requirement
                                        //#Monthly 1st#Monthly 2nd#Monthly 3rd#Monthly 4th#Monthly Last         - possible values of frequency
                                        calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                        calendarArray[c, d] += " PF";
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Trade - Also Collected By adjustments                        
                        //if trade collection - set on alsoCollected by(Container)
                        //if (containers[c, conAlsoCollectedByCol] != "")
                        if (containerInfo.AlsoCollectedby != "")
                        {
                            //configure for also collected by - if a round is specified, then add this collections to the calendars for this container - add to matchRounName string for adjusting the collection dates
                            //string alsoCollectedByRoundName = containers[c, conAlsoCollectedByCol];
                            //matchRoundName += alsoCollectedByRoundName + "#";// Not here as this will potentially move adjusted dates twice
                            splitRoundDetails = containerInfo.AlsoCollectedby.Split(' ');
                            roundWeek = splitRoundDetails[splitRoundDetails.Length - 2].Replace("Wk", "");
                            WeeklyCollection = false;
                            if (roundWeek == "ly") { WeeklyCollection = true; }
                            roundDDD = splitRoundDetails[splitRoundDetails.Length - 3];
                            int lastDateyyyyMMdd = -1;
                            int firstDateyyyyMMdd = -1;
                            if (tradeInfo.ContractEndDateYYYYMMDD != 0) { lastDateyyyyMMdd = tradeInfo.ContractEndDateYYYYMMDD; }
                            if (tradeInfo.ContractStartDateYYYYMMDD != 0) { firstDateyyyyMMdd = tradeInfo.ContractEndDateYYYYMMDD; }
                            if (containerInfo.StartDateYYYYMMDD != 0) { lastDateyyyyMMdd = containerInfo.StartDateYYYYMMDD; }
                            if (containerInfo.EndDateYYYYMMDD != 0) { firstDateyyyyMMdd = containerInfo.EndDateYYYYMMDD; }

                            for (int d = 3; d < daysRequired + 3; d++)
                            {
                                calendarArrayDates = calendarArray[0, d].Split('#');
                                calendarArrDDsMMsYYYY = calendarArrayDates[0];//Wk1 Wk2 Wk3.... Wkly
                                calendarArrDDD = calendarArrayDates[2];//Mon Tue Wed Thu ...
                                calendarArrWkNo = calendarArrayDates[4];
                                CalendarWkNo = Convert.ToDouble(calendarArrWkNo.Replace("Wk", ""));
                                int calDateYYYYMMDD = Convert.ToInt32(calendarArrayDates[1]);
                                if (roundDDD == calendarArrDDD) //see if matching Day
                                {
                                    if (calDateYYYYMMDD >= firstDateyyyyMMdd && calDateYYYYMMDD <= lastDateyyyyMMdd)
                                    {
                                        if (WeeklyCollection)
                                        {
                                            calendarArray[c, d] = calendarArray[c, d].Replace("[0]", "[1]");
                                            calendarArray[c, d] += " AW";//This container gets a collections based on also collected by RoundName
                                        }
                                        else
                                        {
                                            if (CalendarWkNo == Convert.ToDouble(roundWeek))//**CHECK FOR 6 WEEK ROTATION ON A SERVICE WITH 2 WEEK CYCLE - THIS WILL NOT CATER FOR THIS
                                            {
                                                calendarArray[c, d] = calendarArray[c, d].Replace("[0]", "[1]");
                                                calendarArray[c, d] += " AR";//This container gets a collections based also collected by on RoundName
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region contract Start and End Date Adjustments
                        // Adjust for contract start and contract end dates (THESE OVERRIDE ALL bin start and end dates)
                        //tradeInfo.ContractStartDateYYYYMMDD
                        int tempContractStartDateYYYYMMDD = 19730401;//default to 1st april start date.
                        if (tradeInfo.ContractStartDateYYYYMMDD != 0)
                        {
                            tempContractStartDateYYYYMMDD = tradeInfo.ContractStartDateYYYYMMDD;
                        }
                        int tempContractEndDate = 20990331;
                        if (tradeInfo.ContractEndDateYYYYMMDD != 0)
                        {
                            tempContractEndDate = tradeInfo.ContractEndDateYYYYMMDD;
                        }

                        for (int d = 3; d < daysRequired + 3; d++)
                        {
                            calendarArrayDates = calendarArray[0, d].Split('#');
                            int calDateYYYYMMDD = Convert.ToInt32(calendarArrayDates[1]);
                            //check for before contract start date 
                            if (calDateYYYYMMDD < tempContractStartDateYYYYMMDD)
                            {
                                calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                calendarArray[c, d] += " CS";//this container before contract start
                            }
                            //check for before contract start date 
                            if (calDateYYYYMMDD > tempContractEndDate)
                            {
                                calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                calendarArray[c, d] += " CE";//this container after contract end
                            }

                        }

                        #endregion

                        #endregion
                    }
                }
                //End Adjustments for a Trade Container

                //----------------------------------------------------------
                // Set on adjusted dates for Round or Service
                #region Adjusted Dates for this container
                string[] roundsForThisContainer = matchRoundName.Split('#');
                //containerService3L = Ref, Rec, Grn, Foo, Gls, Pls, Del, Box....
                //             0               1               2       3       4       5               6           7
                //Select OrigDateDDsMMsYYYY,OrigDateYYYYMMDD,OrigDDD,Reason,roundName,Service, moveToDDsMMsYYYY,moveToYYYYMMDD from AdjustedDatesNew order by OrigDateYYYYMMDD                 
                //Check each round for adjustment on service
                string roundService3L;
                string[] splitHeaderDates = new string[1];
                string calDateDDsMMsYYYY;
                string adjOrigDate;
                string adjRoundName;
                string adjServ;
                string adjTo;
                string adjReason;
                bool thisIsTheOriginalCollectionDate;
                bool serviceOrRoundNameMatches;
                for (int r = 0; r < roundsForThisContainer.Length; r++)
                {
                    //Adjust Dates by service                    
                    roundNameSplit = roundsForThisContainer[r].Split(' ');
                    roundService3L = roundNameSplit[roundNameSplit.Length - 1];
                    splitHeaderDates = new string[1];
                    calDateDDsMMsYYYY = "";
                    adjOrigDate = "";
                    adjRoundName = "";
                    adjServ = "";
                    adjTo = "";
                    adjReason = "";
                    thisIsTheOriginalCollectionDate = false;
                    serviceOrRoundNameMatches = false;
                    for (int d = 3; d < daysRequired + 3; d++)//Check one day at a time(d) for adjustments for each container (c)
                    {
                        if (calendarArray[c, d].StartsWith("[1]")) //Gets a Collection
                        {
                            splitHeaderDates = calendarArray[0, d].Split('#');
                            calDateDDsMMsYYYY = splitHeaderDates[0];
                            //if(roundsForThisContainer[r]=="R1206 Fq2 Cr6 Tue Wk1 Rec"){string ctestBreak="yes";}
                            //if (calDateDDsMMsYYYY == "25/12/2018" &&roundService3L=="Rec") { string testBreak3 = "yes"; }
                            for (int a = 1; a < adjDates.GetLength(0); a++) //check through all adjusted dates (a) for any adjustedDates to do with this service or RoundName
                            {
                                adjReason = adjDates[a, 3];
                                if (adjReason != "IgnoreThisLine")
                                {
                                    adjOrigDate  = adjDates[a, 0];
                                    adjRoundName = adjDates[a, 4];
                                    adjServ      = adjDates[a, 5];
                                    adjTo        = adjDates[a, 6];
                                    thisIsTheOriginalCollectionDate = false;
                                    if (calendarArray[c, d].Contains("RN") || calendarArray[c, d].Contains("RW")) { thisIsTheOriginalCollectionDate = true; }
                                    serviceOrRoundNameMatches = false;
                                    //if (adjOrigDate == "27/12/2018") { string breakhere = "yes"; }
                                    if (adjServ != "" && adjServ == containerService3L) { serviceOrRoundNameMatches = true; }
                                    if (adjRoundName != "" && adjRoundName == roundsForThisContainer[r]) { serviceOrRoundNameMatches = true; }
                                    //if(calDateDDsMMsYYYY=="04/01/2019"){}
                                    if (serviceOrRoundNameMatches && adjOrigDate == calDateDDsMMsYYYY && thisIsTheOriginalCollectionDate && adjTo != "")//there is an adjustment for this date(d / a) and service - Only adjust original collection based on RoundName
                                    {
                                        if (adjTo == "LEAVE") 
                                        {
                                            calendarArray[c, d] += " LV";
                                            //don't do anything, this is 
                                        }
                                        else
                                        {
                                            if (adjTo == "NoCollect")//check for no collect dates on existing collections
                                            {
                                                {
                                                    calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");
                                                    calendarArray[c, d] += " NC";
                                                }
                                                break;//check next date
                                            }
                                            else
                                            {
                                                calendarArray[c, d] = calendarArray[c, d].Replace("[1]", "[0]");//set collection to 0 and add the date it now gets collected on (AT)
                                                calendarArray[c, d] += " AT(" + adjTo + ")";//cell should now be [0] RN AT(dd/MM/yyyy) where dd/MM/yyyy is the date it now gets collected on
                                                                                            //for (int n = d + 1; n < daysRequired + 3; n++)//search for adjustedToDate and set to [1] and add the Date it got adjusted From (AF)
                                                for (int n = 1; n < daysRequired + 3; n++)//search for adjustedToDate and set to [1] and add the Date it got adjusted From (AF)
                                                {
                                                    if (calendarArray[0, n].Contains(adjTo) && !calendarArray[c, n].Contains("AF"))//check for matching Adjusted to date where not already adjusted
                                                    {
                                                        calendarArray[c, n] = calendarArray[c, n].Replace("[0]", "[1]");
                                                        adjReason = adjDates[a, 3];
                                                        calendarArray[c, n] += " AF(" + calDateDDsMMsYYYY + "#" + adjReason + ")";
                                                        break;//check next adjuted date
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

            }//end has Container for round
            else
            {
                //ContainerHasNoMatching Round
                for (int d = 3; d < daysRequired + 3; d++)//Check one day at a time(d) for adjustments for each container (c)
                {
                    calendarArray[c, d] += " MR";
                }
            }
        }//next Container
        //************************************************************************************************************************
        //************************************************************************************************************************

        //Falkirk Patch for Staggered Burgundy Bin Roll out Start KP 17Jun19
        if (clientDetails.clientName3L == "FLKOldForPlasticRollOutIfNeedsEnabling make this comparrison FLK")
        {
            for (int r = 1; r < rounds.GetLength(0); r++)
            {
                string applyFromDate = FalkirkChangeRollOutDates(rounds, r);
                if (applyFromDate != "")
                {
                    //map Burgudy (Rec) rounds onto Blue(Pls) up to applyFromDate
                    int intApplyFromDate = Convert.ToInt32(applyFromDate);
                    //get recRows
                    string recRows = "";
                    for (int c = 1; c < calendarArray.GetLength(0); c++)
                    {
                        if (calendarArray[c, 2].Contains("Pls"))
                        {
                            recRows += c + "#";
                        }
                    }
                    if (recRows != "")
                    {
                        string[] recRowsSplit = recRows.Split('#');
                        for (int c = 1; c < calendarArray.GetLength(0); c++)
                        {
                            if (calendarArray[c, 2] == "Rec")
                            {
                                for (int d = 3; d < calendarArray.GetLength(1); d++)
                                {
                                    string[] splitDateInfo = calendarArray[0, d].Split('#');
                                    int calDateyyyyMMdd = Convert.ToInt32(splitDateInfo[1]);
                                    if (calDateyyyyMMdd < intApplyFromDate)
                                    {
                                        for (int x = 0; x < recRowsSplit.Length - 1; x++)
                                        {
                                            int recRowNo = Convert.ToInt32(recRowsSplit[x]);
                                            //map Pls rounds to Rec
                                            if (calendarArray[recRowNo, d].Contains("0"))
                                            {
                                                calendarArray[recRowNo, d] = calendarArray[c, d];
                                                calendarArray[c, d] = "0";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //Falkirk Patch for Staggered Burgundy Bin Roll out End KP 17Jun19
        //Key for Calendar breakdown        
        #region Calendar Breakdown Key
        //Compile All info into Array by service for Calendar render
        string[,] calendarArr = new string[1, 1];
        string[] clientServices = clientDetails.services.Split('#');
        DateTime startDate2 = DateTime.ParseExact(origCalStart, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        DateTime endDate2 = DateTime.ParseExact(calendarEndDDMMYYYY, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //if (!forWebservice)
        {
            int noOfDaysNeeded = (endDate2 - startDate2).Days + 1;
            calendarArr = new string[clientServices.Length + 2, noOfDaysNeeded + 1];
            calendarArr[0, 0] = "";
            calendarArr[calendarArr.GetLength(0) - 1, 0] = "Icon";
            //populate Services
            for (int s = 0; s < clientServices.Length; s++)
            {
                calendarArr[s + 1, 0] = clientServices[s];
            }

            //populate Collected (or not) 1 or 0 for each service + PieIcon
            int noOfServices = clientDetails.services.Split('#').Length;
            for (int d = 0; d < noOfDaysNeeded; d++)
            {
                calendarArr[0, d + 1] = startDate2.AddDays(d).ToString("dd/MM/yyyy");//add date
                string iconBin = "";
                for (int i = 0; i < noOfServices; i++)
                {
                    iconBin += "0";
                }
                //string breakDown = "=== Breakdown ===";
                //string breakDown = "";
                StringBuilder breakDown = new StringBuilder();
                for (int s = 0; s < clientServices.Length; s++)
                {

                    string thisGetsACollection = "";
                    for (int j = 3; j < calendarArray.GetLength(1); j++)
                    {
                        if (calendarArray[0, j].Contains(calendarArr[0, d + 1]))//find matching date in breakDownCalendar
                        {
                            for (int c = 1; c < calendarArray.GetLength(0); c++)//find matching service and total collections
                            {
                                if (clientServices[s] == calendarArray[c, 2])
                                {
                                    if (calendarArray[c, j].Contains("[1]"))
                                    {
                                        thisGetsACollection += "1";
                                    }
                                    string adjustedReason = "";
                                    if (calendarArray[c, j].Contains(" AF"))
                                    {
                                        int indexOpenBrak = calendarArray[c, j].IndexOf("(");
                                        int indexClosBrak = calendarArray[c, j].IndexOf(")");
                                        int lengthOfField = calendarArray[c, j].Length;
                                        adjustedReason = calendarArray[c, j].Substring(indexOpenBrak + 12, lengthOfField - (indexOpenBrak + 13));
                                    }
                                    if (calendarArray[c, j].Contains(" SD")) { breakDown.Append("" + clientServices[s] + " affected by start date of container\n"); }
                                    if (calendarArray[c, j].Contains(" ED")) { breakDown.Append("" + clientServices[s] + " affected by end date of container\n"); }
                                    if (calendarArray[c, j].Contains(" RN")) { breakDown.Append("" + clientServices[s] + " date based on Round Name\n"); }
                                    if (calendarArray[c, j].Contains(" RW")) { breakDown.Append("" + clientServices[s] + " date based on Round Name (Weekly)\n"); }
                                    if (calendarArray[c, j].Contains(" MR")) { breakDown.Append("" + clientServices[s] + " Affected By No Corresponding Round For Service\n"); }
                                    if (calendarArray[c, j].Contains(" AF")) { breakDown.Append("" + clientServices[s] + " this Date has moved from " + calendarArray[c, j].Substring(calendarArray[c, j].IndexOf("AF") + 3, 10) + ":" + adjustedReason + "\n"); }
                                    if (calendarArray[c, j].Contains(" AT")) { breakDown.Append("" + clientServices[s] + " this date has moved To " + calendarArray[c, j].Substring(calendarArray[c, j].IndexOf("AT") + 3, 10) + "\n"); }
                                    if (calendarArray[c, j].Contains(" LV")) { breakDown.Append("" + clientServices[s] + " this date has not been moved for this round contradictory to the rest of service\n"); }
                                    if (calendarArray[c, j].Contains(" NC")) { breakDown.Append("" + clientServices[s] + " collection not showing as No Collection Set (suspended Sevice)\n"); }
                                    if (calendarArray[c, j].Contains(" CD")) { breakDown.Append("" + clientServices[s] + " Affected By Collection Day Of Container\n"); }
                                    if (calendarArray[c, j].Contains(" PD")) { breakDown.Append("" + clientServices[s] + " Affected By Collection Day Of Property\n"); }
                                    if (calendarArray[c, j].Contains(" DF")) { breakDown.Append("" + clientServices[s] + " Affected By Frequency and Days of Container\n"); }
                                    if (calendarArray[c, j].Contains(" RF")) { breakDown.Append("" + clientServices[s] + " Affected By Frequency and Days of Round\n"); }
                                    if (calendarArray[c, j].Contains(" PF")) { breakDown.Append("" + clientServices[s] + " Affected By Frequency and Days of Property\n"); }
                                    if (calendarArray[c, j].Contains(" AR")) { breakDown.Append("" + clientServices[s] + " Affected By Also Collected By Another Round of Container\n"); }
                                    if (calendarArray[c, j].Contains(" AW")) { breakDown.Append("" + clientServices[s] + " Affected By Also Collected By Another Weekly Round of Container\n"); }
                                    if (calendarArray[c, j].Contains(" CP")) { breakDown.Append("" + clientServices[s] + " Affected By Patern Of Container\n"); }
                                    if (calendarArray[c, j].Contains(" RP")) { breakDown.Append("" + clientServices[s] + " Affected By Patern Of Round\n"); }
                                    if (calendarArray[c, j].Contains(" PP")) { breakDown.Append("" + clientServices[s] + " Affected By Pattern Of Property\n"); }
                                }
                            }
                            if (thisGetsACollection.Contains("1"))
                            {
                                calendarArr[s + 1, d + 1] = "1";
                                StringBuilder sb = new StringBuilder();
                                sb.Append(iconBin);
                                sb[noOfServices - s - 1] = '1';
                                iconBin = sb.ToString();
                            }
                            else
                            {
                                calendarArr[s + 1, d + 1] = "0";
                                StringBuilder sb = new StringBuilder();
                                sb.Append(iconBin);
                                sb[noOfServices - s - 1] = '0';
                                iconBin = sb.ToString();
                            }
                            break;
                        }
                    }
                }

                string graphic = "<img src='img/Pie/" + clientServices.Length + "-" + iconBin + ".png' title='" + breakDown.ToString() + "' alt='"+iconBin+"' height='25' width='25'>";
                //string graphic = "<img src='img/Pie/" + clientServices.Length + "-" + iconBin + ".png' title='" + breakDown + "' >";
                if (clientDetails.customCalendarColours)
                {
                    graphic = graphic.Replace("Pie/", "Pie/" + clientDetails.clientName3L + "-");
                }
                calendarArr[calendarArr.GetLength(0) - 1, d + 1] = graphic;
            }
        }
        #endregion


        //Costing per container                
        //int tradeCostCols = 28;
        tradeCosts = new string[1, 40];
        int tradeContainerCount = 0;
        tPricing pricesTable;
        string[,] pricingArray = new string[1, 1];
        if (hasTradeContainer && clientDetails.hasTrade && tradeInfo.UPRN != "NotRequestedInSQLSelect")
        {
            #region Work out Trade Costs to tradeCosts Array
            //This section works out the cost of each container
            //It uses the pricing table to get the band for the container (The parameters used for costing this container), if not present, then the property Band form the Trade Data table is used 
            //Each container is a line item. For pricing and parameters, a container parameter overrides a roundparameter, a round parameter overrides a TradeData Table parameter
            //There are subtle differences in the way some clients charge and work out the costs, eg Ryedale have no disposal cost for recycling.
            string sqlPricing = "select Band,BinType,BinService,HireCost,CollectionCost,DisposalCost,AdminCost,PurchaseCost,Total,perLift,HirePeriod,bandDescription,AdditionalCollectionCost,YearlyContainerSeasonalCost from priceTable";
            pricingArray = Misc.getDataArrayFromDB(sqlPricing, councilName, connection);
            pricesTable = new tPricing(pricingArray);
            for (int s = 1; s < containers.GetLength(0); s++)
            {
                if (containers[s, 1].ToUpper().Contains("TRADE")) { tradeContainerCount++; }
            }
            //tradeCosts = new string[tradeContainerCount + 2, tradeCostCols];
            int ccol = -1;
            ccol++; int tcColID = ccol;     // ID";
            ccol++; int tcColCT = ccol;     // Container#Type";
            ccol++; int tcColCMBW = ccol;   // [CMW]#Container#MaxBillWeeks";
            ccol++; int tcColCD = ccol;     // [CD]#Container#Discount";
            ccol++; int tcColCDT = ccol;    // Container#DiscountType";
            ccol++; int tcColCB = ccol;     // Container#Band";            
            ccol++; int tcColRB = ccol;     // Round#Band";
            ccol++; int tcColPB = ccol;     // Property#Band";
            ccol++; int tcColPMBW = ccol;   // [PMW]#Property#MaxBillWeeks";
            ccol++; int tcColPD = ccol;     // [PD]#Property#Discount";
            ccol++; int tcColPDT = ccol;    // Property#DiscountType";
            ccol++; int tcColPS2 = ccol;    // Property#Schedule2";
            ccol++; int tcColVRP = ccol;    // Property#VATRatePercent";
            ccol++; int tcColPEF = ccol;    // Pricing#EntryFound";
            ccol++; int tcColPAC = ccol;    // [PA]#Pricing#Admin#Cost";
            ccol++; int tcColPPC = ccol;    // [PC]#Pricing#Purchase#Cost";
            ccol++; int tcColPHC = ccol;    // [HC]#Pricing#Hire#Cost";
            ccol++; int tcColPHP = ccol;    // [HP]#Pricing#Hire#Period";             
            ccol++; int tcColPCPL = ccol;   // [CL]#Pricing#CostPer#Lift";
            ccol++; int tcColPDC = ccol;    // [DC]#Pricing#Disposal#Cost";
            ccol++; int tcColcTCC = ccol;   // Container#Total#Collection";
            ccol++; int tcColcTDC = ccol;   // Container#Total#Disposal";
            ccol++; int tcColCOOS = ccol;   // [SC]#Container#One off#seasonal";
            ccol++; int tcColSD = ccol;     // Start#Date";
            ccol++; int tcColED = ccol;     // End#Date";
            ccol++; int tcColAC = ccol;     // [AC]#Actual#Collections";
            ccol++; int tcColWIDR = ccol;   // [W]#Weeks in#Date#Range";
            ccol++; int tcColDIDR = ccol;   // [D]#Days In#Date#Range";
            ccol++; int tcColWO = ccol;     // Working#Out";
            ccol++; int tcColT = ccol;      //"Total";             
            tradeCosts = new string[tradeContainerCount + 2, ccol + 1];
            //****** IMPORTANT DO NOT CHANGE THE FOLLOWING TEXT STRINGS AS THEY'RE USED IN THE Misc.TRADECOSTRETURNS class
            tradeCosts[0, tcColID]   = "ID";
            tradeCosts[0, tcColCT]   = "Container#Type";
            tradeCosts[0, tcColCMBW] = "[CMW]#Container#Max Bill#Weeks";
            tradeCosts[0, tcColCD]   = "[CD]#Container#Discount#per lift";
            tradeCosts[0, tcColCDT]  = "Container#Discount#Type";
            tradeCosts[0, tcColCB]   = "Container#Band";
            tradeCosts[0, tcColRB]   = "Round#Band";
            tradeCosts[0, tcColPB]   = "Prop#Band";
            tradeCosts[0, tcColPMBW] = "[PMW]#Prop#Max Bill#Weeks";
            tradeCosts[0, tcColPD]   = "[PD]#Prop#Discount";
            tradeCosts[0, tcColPDT]  = "Prop#Discount#Type";
            tradeCosts[0, tcColPS2]  = "Prop#Schedule2";
            tradeCosts[0, tcColVRP]  = "[PVRP]#Prop#Vatable";
            tradeCosts[0, tcColPEF]  = "Pricing#Entry Found";
            tradeCosts[0, tcColPAC]  = "[PA]#Pricing#Admin#Cost";
            tradeCosts[0, tcColPPC]  = "[PC]#Pricing#Purchase#Cost";
            tradeCosts[0, tcColPHC]  = "[HC]#Pricing#Hire#Cost";
            tradeCosts[0, tcColPHP]  = "[HP]#Pricing#Hire#Period";
            tradeCosts[0, tcColPCPL] = "[CL]#Pricing#CostPer#Lift";
            tradeCosts[0, tcColPDC]  = "[DC]#Pricing#Disposal#Cost";
            tradeCosts[0, tcColcTCC] = "Container#Total#Collection";
            tradeCosts[0, tcColcTDC] = "Container#Total#Disposal";
            tradeCosts[0, tcColCOOS] = "[SC]#Container#One off#seasonal";
            tradeCosts[0, tcColSD]   = "Start#Date";
            tradeCosts[0, tcColED]   = "End#Date";
            tradeCosts[0, tcColAC]   = "[AC]#Actual#Collections";
            tradeCosts[0, tcColWIDR] = "[W]#Weeks in#Date#Range";
            tradeCosts[0, tcColDIDR] = "[D]#Days In#Date#Range";
            tradeCosts[0, tcColWO]   = "Working#Out";
            tradeCosts[0, tcColT]    = "Total";
            int tcRow = 0;
            string adminCharge = "";
            bool thereIsAContainerStartDateInCalendarRange = false;
            bool multipleBandsOnRoundsWarning = false;
            ContainerData containerInfoT = new ContainerData();
            for (int s = 1; s < containers.GetLength(0); s++)
            {
                containerInfoT = ContainerData.GetContainerParameters(containers, s);                
                if (containerInfoT.tradeContainer)
                {
                    tcRow++;
                    //container Details----------                    
                    tradeCosts[tcRow, tcColID] = containerInfoT.ID.ToString();
                    tradeCosts[tcRow, tcColCT] = containerInfoT.Bin_number;       //Type                                          
                    tradeCosts[tcRow, tcColCMBW] = containerInfoT.NumColWeeks;   //MaxCollWeeks
                    if (containerInfoT.discount == "") { containerInfoT.discount = "0"; }
                    tradeCosts[tcRow, tcColCD] = containerInfoT.discount;
                    if (containerInfoT.discountType == "") { containerInfoT.discountType = "%"; }
                    if (containerInfoT.discountType.ToLower() == "percentage") { containerInfoT.discountType = "%"; }
                    if (containerInfoT.discountType.ToLower() == "amount") { containerInfoT.discountType = "£"; }
                    tradeCosts[tcRow, tcColCDT] = containerInfoT.discountType;    //ContainerDiscountType                                          
                    tradeCosts[tcRow, tcColCB] = containerInfoT.binBand;         //Container Band                    
                    //Round Details------------------------
                    tradeCosts[tcRow, tcColRB] = "-";
                    string rBand = "";
                    for (int r = 1; r < rounds.GetLength(0); r++)
                    {
                        WaxRoundNewDetails wrn = WaxRoundNewDetails.getRoundDetails(rounds, r);
                        if (wrn.roundBand != "")
                        {
                            if (!rBand.Contains(wrn.roundBand + "#"))
                            {
                                rBand += wrn.roundBand + "#";
                            }
                        }
                    }
                    string[] rBandSplit = rBand.Split('#');
                    if (rBandSplit.Length == 2)
                    {
                        tradeCosts[tcRow, tcColRB] = rBandSplit[0];
                    }
                    else
                    {
                        if (rBandSplit.Length > 2)
                        {
                            multipleBandsOnRoundsWarning = true;
                            tradeCosts[tcRow, tcColRB] = rBandSplit[0];
                        }
                    }

                    //property Costing Details---------                     
                    // set defaults if no Trade Data
                    tradeCosts[tcRow, tcColPB]   = "No TData";
                    tradeCosts[tcRow, tcColPMBW] = "No TData";
                    tradeCosts[tcRow, tcColPD]   = "No TData";
                    tradeCosts[tcRow, tcColPDT]  = "No TData";
                    tradeCosts[tcRow, tcColPS2]  = "No TData";
                    if (tradeDataArray.GetLength(0) > 1)
                    {
                        tradeCosts[tcRow, tcColPB]   = tradeInfo.Band;            //property Band                     
                        tradeCosts[tcRow, tcColPMBW] = tradeInfo.NoofCollWks;      //property MaxColWeeks                                                  
                        tradeCosts[tcRow, tcColPD]   = "";        //property Discount - discount applied to full amount so should be on total row
                        tradeCosts[tcRow, tcColPDT]  = "";    //property DiscountType - discount applied to full amount so should be on total row
                        tradeCosts[tcRow, tcColVRP]  = "";//property VatRatePercent applied to full amount so should be on total row
                        tradeCosts[tcRow, tcColPS2]  = tradeInfo.Schedule2;         //property Schedule2
                    }
                    //matching pricing details---------
                    //Pricing Entry Found                     
                    tradeCosts[tcRow, tcColPEF]  = "NotFound";
                    tradeCosts[tcRow, tcColPHC]  = "NotFound";
                    tradeCosts[tcRow, tcColPHP]  = "NotFound";
                    tradeCosts[tcRow, tcColPDC]  = "NotFound";
                    tradeCosts[tcRow, tcColPAC]  = "NotFound";
                    tradeCosts[tcRow, tcColPPC]  = "NotFound";
                    tradeCosts[tcRow, tcColPCPL] = "NotFound";
                    
                    string pricingBinType = containerInfoT.Bin_number.Replace("TRADEREFUSE", "").Replace("TRADERECYCLE", "").Replace("TRADEFOOD", "").Replace("TRADEGARDEN", "").TrimEnd().TrimStart();
                    string bandToFind = "";
                    //check property band
                    if (tradeDataArray.GetLength(0) > 1)
                    {
                        if (tradeInfo.Band != "")
                        {
                            bandToFind = tradeInfo.Band;
                            //tradeCosts[tcRow, tcColPB] = bandToFind;
                            tradeCosts[tcRow, tcColPEF] = bandToFind;
                        }
                        else
                        {
                            tradeCosts[tcRow, tcColPB] = "Non";
                        }
                    }
                    //check bin band - as this superseeds property band
                    if (containerInfoT.binBand != "")
                    {
                        bandToFind = containerInfoT.binBand;
                        //tradeCosts[tcRow, tcColCB] = bandToFind;
                        tradeCosts[tcRow, tcColPEF] = bandToFind;
                    }
                    else
                    {
                        tradeCosts[tcRow, tcColCB] = "Non";
                    }

                    if (bandToFind == "")
                    {
                        tradeCosts[tcRow, tcColPEF] = "No Band Found";
                    }
                    else
                    {
                        //find matching band
                        //bool annualDisposalCost = false;
                        //bool annualCollectionCost = false;
                        bool chargeForDisposalOfRecycling = true;                        
                        if (clientDetails.clientName3L == "RYD")
                        {
                            //annualDisposalCost = true;
                            //annualCollectionCost = true;
                            chargeForDisposalOfRecycling = false;                            
                        }
                        tradeCosts[tcRow, tcColPEF] = bandToFind;
                        bool pricingFound = false;
                        for (int p = 0; p < pricesTable.Length; p++)
                        {
                            tPrice priceRow = pricesTable[p];//get row as an object
                            if (bandToFind == priceRow.Band)
                            {
                                //find matching container for this band
                                if (pricingBinType.StartsWith(priceRow.BinType))
                                {
                                    //Pricing Entry Found
                                    pricingFound = true;
                                    //tradeCosts[tcRow, tcColPAC] = priceRow.AdminCost;
                                    tradeCosts[tcRow, tcColPAC] = "-";
                                    if (!adminCharge.Contains(priceRow.AdminCost)) { adminCharge += priceRow.AdminCost + "#"; }
                                    tradeCosts[tcRow, tcColPPC] = priceRow.PurchaseCost;
                                    tradeCosts[tcRow, tcColPHC] = priceRow.HireCost;
                                    tradeCosts[tcRow, tcColPHP] = priceRow.HirePeriod;
                                    tradeCosts[tcRow, tcColPCPL] = priceRow.perLift;

                                    tradeCosts[tcRow, tcColPDC] = priceRow.DisposalCost;
                                    if (containerInfoT.Bin_number.ToLower().Contains("traderecyc"))
                                    {
                                        if (!chargeForDisposalOfRecycling)
                                        {
                                            tradeCosts[tcRow, tcColPDC] = "0.00"; //ryedale don't charge for traderecycling disposal
                                        }  
                                    }                                    

                                    //tradeCosts[tcRow, tcColPCPL] = priceRow.perLift;
                                    //tradeCosts[tcRow, tcColPDC] = priceRow.DisposalCost;
                                    tradeCosts[tcRow, tcColPEF] += "(" + priceRow.BandDesc + ")";
                                    tradeCosts[tcRow, tcColCOOS] = priceRow.YearlyContainerSeasonalCost;
                                }
                            }
                            if (pricingFound) { break; }
                        }
                    }
                    //Actual Collections
                    tradeCosts[tcRow, tcColAC] = "-1";
                    //count [1] entries in Calendar Array for this binID in the date range selected
                    int actualCollectionsForThisContainer = 0;
                    double collectionWeeks = -1;
                    double collectionDays = -1;
                    //startdate2 and enddate2 are the start and end dates for the period of blling requested

                    endDate2 = endDate2.AddDays(0);
                    int containerStartYYYYMMDD = 19700101;
                    int containerEndYYYYMMDD = 20501231;
                    int patternOffDays = 0;
                    int patternOffWeeks = 0;
                    int suspensionDays = 0;
                    int suspensionWeeks = 0;
                    int suspensionAndPatternDays = 0;
                    int suspensionAndPatternWeeks = 0;
                    int startDateCost;
                    int endDateCost;
                    DateTime startDateCostDT = new DateTime();
                    DateTime endDateCostDT = new DateTime();
                    string[] splitHeaderDates = new string[1];
                    int calDateYYYYMMDD;
                    for (int c = 1; c < calendarArray.GetLength(0); c++)
                    {
                        if (containerInfoT.ID.ToString() == calendarArray[c, 1])
                        {
                            //corresponding container found   -- get billable period for this container
                            startDateCost = Convert.ToInt32(startDate2.ToString("yyyyMMdd"));
                            endDateCost = Convert.ToInt32(endDate2.ToString("yyyyMMdd"));
                            //check get start end date for container...                             
                            if (containerInfoT.Start_date != null && containerInfoT.Start_date != "") { containerStartYYYYMMDD = Convert.ToInt32(containerInfoT.Start_date.Substring(6, 4) + containerInfoT.Start_date.Substring(3, 2) + containerInfoT.Start_date.Substring(0, 2)); }
                            if (containerInfoT.End_date != null && containerInfoT.End_date != "") { containerEndYYYYMMDD = Convert.ToInt32(containerInfoT.End_date.Substring(6, 4) + containerInfoT.End_date.Substring(3, 2) + containerInfoT.End_date.Substring(0, 2)); }
                            if (containerStartYYYYMMDD > startDateCost) { startDateCost = containerStartYYYYMMDD; }
                            if (containerEndYYYYMMDD < endDateCost) { endDateCost = containerEndYYYYMMDD; }
                            startDateCostDT = DateTime.ParseExact(startDateCost.ToString(), "yyyyMMdd", null);
                            endDateCostDT = DateTime.ParseExact(endDateCost.ToString(), "yyyyMMdd", null);
                            collectionDays = (endDateCostDT - startDateCostDT).TotalDays + 1;
                            collectionWeeks = ((endDateCostDT - startDateCostDT).TotalDays) / 7;
                            //Check for start date of this container being after end date of calendar range requested
                            if (collectionDays  < 0) { collectionDays = 0; }
                            if (collectionWeeks < 0 ){ collectionWeeks = 0; }

                            //apply patterns to weeks and days in date range **                                                        
                            for (int j = 3; j < calendarArray.GetLength(1); j++)
                            {
                                splitHeaderDates = calendarArray[0, j].Split('#');
                                calDateYYYYMMDD = Convert.ToInt32(splitHeaderDates[1]);
                                if (calDateYYYYMMDD >= startDateCost && calDateYYYYMMDD <= endDateCost)
                                {
                                    if (calendarArray[c, j].StartsWith("[1]"))
                                    {
                                        actualCollectionsForThisContainer++;
                                    }
                                    if (calendarArray[c, j].Contains("PatOff")) 
                                    { 
                                        patternOffDays++;
                                        if (splitHeaderDates[2] == "Mon") 
                                        {
                                            patternOffWeeks++;
                                        }
                                    }
                                    if (calendarArray[c, j].Contains("SS"))
                                    {
                                        suspensionDays++;
                                        if (splitHeaderDates[2] == "Mon")
                                        {
                                            suspensionWeeks++;
                                        }
                                    }
                                    if (calendarArray[c, j].Contains("SS") || calendarArray[c, j].Contains("PatOff"))
                                    {
                                        suspensionAndPatternDays++;
                                        if (splitHeaderDates[2] == "Mon")
                                        {
                                            suspensionAndPatternWeeks++;
                                        }
                                    }
                                }
                            }
                            tradeCosts[tcRow, tcColSD] = startDateCostDT.ToString("dd/MM/yyyy");//Start Date
                            tradeCosts[tcRow, tcColED] = endDateCostDT.ToString("dd/MM/yyyy"); ;//End Date
                            break;
                        }
                    }
                    tradeCosts[tcRow, tcColAC] = actualCollectionsForThisContainer.ToString();//Number of actual collections
                    //tradeCosts[tcRow, tcColWIDR] = collectionWeeks.ToString("0");//weeks in this date range
                    //tradeCosts[tcRow, tcColDIDR] = collectionDays.ToString("0");//days in this date range
                    tradeCosts[tcRow, tcColWIDR] = (collectionWeeks - suspensionAndPatternWeeks).ToString("0");//weeks in this date range
                    tradeCosts[tcRow, tcColDIDR] = (collectionDays - suspensionAndPatternDays).ToString("0");//days in this date range
                    if (suspensionAndPatternDays != 0)
                    {
                        tradeCosts[tcRow, tcColDIDR] += "(*";
                        tradeCosts[tcRow, tcColWIDR] += "(*";
                        if (suspensionDays != 0) { tradeCosts[tcRow, tcColDIDR] += "S" + suspensionDays; tradeCosts[tcRow, tcColWIDR] += "S" + suspensionWeeks; }
                        if (patternOffDays != 0) { tradeCosts[tcRow, tcColDIDR] += "P" + patternOffDays; tradeCosts[tcRow, tcColWIDR] += "P" + patternOffWeeks; }
                        tradeCosts[tcRow, tcColDIDR] += ")";
                        tradeCosts[tcRow, tcColWIDR] += ")";
                    }
                    if (collectionWeeks < 0) { tradeCosts[tcRow, tcColWIDR] = "Not in Range[" + collectionWeeks.ToString("0.0") + "]"; }
                    if (collectionDays < 1) { tradeCosts[tcRow, tcColDIDR] = "Not in Range[" + collectionDays.ToString("0") + "]"; }

                    //---------------------------------
                    //- Get Values for this container -
                    //---------------------------------
                    double pricingCostPerLift = 0;
                    double pricingDisposalCost = 0;
                    double contMaxBillWeeks = 0;
                    double propMaxBillWeeks = 0;
                    double billableCollections = 0;
                    double oneOffSeasonalCharge = 0;
                    double costForThisContainer = 0;
                    double containerDiscountAmount = 0;
                    double containerPercentageDiscount = 0;
                    double purchaseCost = 0;
                    double hireCost = 0;
                    double proRataHireCost = 0;
                    double containerTotalCollectionCost = 0;
                    double containerTotalDisposalCost = 0;
                    double wksInDateRange = 0;

                    if (tradeCosts[tcRow, tcColPCPL] != "" && tradeCosts[tcRow, tcColPCPL] != "NotFound")
                    {
                        pricingCostPerLift = Convert.ToDouble(tradeCosts[tcRow, tcColPCPL]);
                    }

                    if (tradeCosts[tcRow, tcColPDC] != "" && tradeCosts[tcRow, tcColPDC] != "NotFound")
                    {
                        pricingDisposalCost = Convert.ToDouble(tradeCosts[tcRow, tcColPDC]);
                    }
                    //purchase cost. must have start date present on container, to charge for each
                    // eg trade sacks get charged on every purchase (on start date)                                        
                    if (tradeCosts[tcRow, tcColPPC] != "0")
                    {
                        if (containerInfoT.Start_date != "")
                        {
                            //only add if container has a start date and is in current billing period
                            int startBillPeriod_yyyyMMdd = Convert.ToInt32(Misc.DisplayDateInFormat(tradeCosts[tcRow, tcColSD], "dd/MM/yyyy", "yyyyMMdd"));
                            int endBillPeriod_yyyyMMdd = Convert.ToInt32(Misc.DisplayDateInFormat(tradeCosts[tcRow, tcColED], "dd/MM/yyyy", "yyyyMMdd"));
                            if (containerInfoT.StartDateYYYYMMDD >= startBillPeriod_yyyyMMdd)
                            {
                                int tempContainerEndDate = endBillPeriod_yyyyMMdd;
                                if (containerInfoT.End_date != "")
                                {
                                    tempContainerEndDate = containerInfoT.EndDateYYYYMMDD;
                                }
                                if (containerInfoT.StartDateYYYYMMDD >= startBillPeriod_yyyyMMdd && endBillPeriod_yyyyMMdd <= tempContainerEndDate)
                                {
                                    purchaseCost = Convert.ToDouble(tradeCosts[tcRow, tcColPPC]);
                                }
                            }
                        }
                    }

                    //hire cost - this is the Total cost for the period shown and then charged per week. 
                    //Eg 26 weeks of collection on an annual would be "cost / 52 * 26weeks"
                    //eg 3 weeks on a 18 momnth would be (cost / (52+26) * 3weeks)
                    //eg 3 weeks on a quarterly would be (cost/13) * 3weeks
                    //eg 3 weeks on a montlhly would be (cost*12/52) * 3weeks)
                    //eg 3 weeks on a weekly would be cost * 3
                    //eg 3 weeks on a daily would be cost * 7 * 3weeks
                    //tradeCosts[tcRow, tcColPHC]//hirecost
                    //tradeCosts[tcRow, tcColPHP]//hirecost period
                    //Add hire charge depending on period = 18Month Annually Quarterly Monthly Weekly Daily
                    if (tradeCosts[tcRow, tcColPHC] != "0")
                    {
                        if (tradeCosts[tcRow, tcColPHC] != "NotFound")
                        {//{ tradeCosts[tcRow, tcColPHC] = "0"; }
                            string hirePeriod = tradeCosts[tcRow, tcColPHP];
                            hireCost = Convert.ToDouble(tradeCosts[tcRow, tcColPHC]);
                            proRataHireCost = hireCost;
                            double weeksInDateRange = Convert.ToDouble(tradeCosts[tcRow, tcColWIDR].Split('(')[0]);//colum has (* plus P or S if pattern or Suspension adjustment
                            switch (hirePeriod)
                            {
                                case "Annually":  { proRataHireCost = hireCost / 52         * weeksInDateRange; break; }
                                case "18Month":   { proRataHireCost = hireCost / (52 + 26)  * weeksInDateRange; break; }
                                case "Quarterly": { proRataHireCost = hireCost / 31         * weeksInDateRange; break; }
                                case "Monthly":   { proRataHireCost = hireCost / 12         * weeksInDateRange; break; }
                                case "Weekly":    { proRataHireCost = hireCost              * weeksInDateRange; break; }
                                case "Daily":     { proRataHireCost = hireCost * 7          * weeksInDateRange; break; }
                                default: break;
                            }
                        }
                        if (proRataHireCost != hireCost) { tradeCosts[tcRow, tcColPHC] += "<br/>(*" + proRataHireCost.ToString("0.00") + ")"; }
                    }

                    //check if sched2
                    if (tradeCosts[tcRow, tcColPS2] == "Y") { pricingDisposalCost = 0; }

                    //get container Maximum Billable weeks
                    if (tradeCosts[tcRow, tcColCMBW] == "")
                    {
                        contMaxBillWeeks = 99999;
                    }
                    else
                    {
                        contMaxBillWeeks = Convert.ToDouble(tradeCosts[tcRow, tcColCMBW]);
                    }

                    //get property Maximum Billable weeks
                    if (tradeCosts[tcRow, tcColPMBW] == "" || tradeCosts[tcRow, tcColPMBW] == "NA" || tradeCosts[tcRow, tcColPMBW] == "No TData")
                    {
                        propMaxBillWeeks = 999999;
                    }
                    else
                    {
                        propMaxBillWeeks = Convert.ToDouble(tradeCosts[tcRow, tcColPMBW]);
                    }

                    //populate calculation
                    tradeCosts[tcRow, tcColWO] = "([AC]>[CMW]? OR [AC]>[PWM]?) X ([CL]+[DC]?sched2) - CD + SC";
                    if (proRataHireCost != 0) { tradeCosts[tcRow, tcColWO] += " + HC"; }
                    billableCollections = actualCollectionsForThisContainer;

                    //check for one off seasonal charge for this container                    
                    if (tradeCosts[tcRow, tcColCOOS] != "0")
                    {
                        oneOffSeasonalCharge = Convert.ToDouble(tradeCosts[tcRow, tcColCOOS]);
                        //multiply by number of full 18 months or 12 month periods
                    }

                    //check for max billable collections on container and property - adjust accordingly
                    //if (billableCollections > contMaxBillWeeks) { billableCollections = contMaxBillWeeks; }//old didn't cater for multiple on one contaier collections per week eg mon+thur rounds would give around 104 actual collections per year
                    //if (billableCollections > propMaxBillWeeks) { billableCollections = propMaxBillWeeks; }//old didn't cater for multiple on one container collections per week
                    wksInDateRange = Convert.ToDouble(tradeCosts[tcRow, tcColWIDR].Split('(')[0]);//colum has (* plus P or S if pattern or Suspension adjustment
                    double avCollectionsPerWeek = actualCollectionsForThisContainer / wksInDateRange;
                    if (wksInDateRange > propMaxBillWeeks) { billableCollections = (propMaxBillWeeks * avCollectionsPerWeek); }//check proprty first
                    if (wksInDateRange > contMaxBillWeeks) { billableCollections = (contMaxBillWeeks * avCollectionsPerWeek); }//container overrides proeprty ^^                    
                    containerTotalCollectionCost = billableCollections * pricingCostPerLift;
                    containerTotalDisposalCost   = billableCollections * pricingDisposalCost;
                    costForThisContainer = billableCollections * (pricingCostPerLift + pricingDisposalCost);
                    costForThisContainer += purchaseCost + proRataHireCost;//how do we know whether container is purchased or hired????
                    //Apply Container Discount
                    if (tradeCosts[tcRow, tcColCD] != "")
                    {
                        if (tradeCosts[tcRow, tcColCDT] == "£")
                        {
                            containerDiscountAmount = Convert.ToDouble(tradeCosts[tcRow, tcColCD]);
                            costForThisContainer = costForThisContainer - (containerDiscountAmount * billableCollections);
                            containerTotalCollectionCost = containerTotalCollectionCost - (containerDiscountAmount * billableCollections);
                        }
                        if (tradeCosts[tcRow, tcColCDT] == "%")
                        {
                            containerPercentageDiscount  = Convert.ToDouble(tradeCosts[tcRow, tcColCD]) / 100;
                            costForThisContainer         = costForThisContainer         - (costForThisContainer         * containerPercentageDiscount);//container cost discount
                            containerTotalCollectionCost = containerTotalCollectionCost - (containerTotalCollectionCost * containerPercentageDiscount);//total collection discount
                            containerTotalDisposalCost   = containerTotalDisposalCost   - (containerTotalDisposalCost   * containerPercentageDiscount);//total disposal discount
                        }
                    }

                    costForThisContainer += oneOffSeasonalCharge;
                    containerTotalCollectionCost += oneOffSeasonalCharge;

                    ////Apply Admin Cost if applicable - work out if there is a container in the date rannge selcted
                    int calRangeStartDateYYYYMMDD = Convert.ToInt32(startDate.ToString("yyyyMMdd"));
                    int calRangeEndDateYYYYMMDD = Convert.ToInt32(endDate.ToString("yyyyMMdd"));
                    int conSD = containerStartYYYYMMDD;
                    int conED = containerEndYYYYMMDD;
                    //1 - Start Date of container is in Calendar Range                    
                    if ((conSD >= calRangeStartDateYYYYMMDD || conSD == 19700101) && (conED <= calRangeEndDateYYYYMMDD || conED == 20501231)) { thereIsAContainerStartDateInCalendarRange = true; }
                    if ((calRangeStartDateYYYYMMDD >= conSD || conSD == 19700101) && (calRangeEndDateYYYYMMDD > conED || conED == 20501231)) { thereIsAContainerStartDateInCalendarRange = true; }

                    tradeCosts[tcRow, tcColT] = costForThisContainer.ToString("0.00");
                    tradeCosts[tcRow, tcColcTCC] = containerTotalCollectionCost.ToString("0.00");
                    tradeCosts[tcRow, tcColcTDC] = containerTotalDisposalCost.ToString("0.00");
                }
            }
            //-----------
            //Total Costs
            //-----------
            tradeCosts[tcRow + 1, tcColCT] = "Total";
            double totalCost = 0;
            double propertyDiscountAmount = 0;
            double percentageDiscount = 0;
            double vatRate = 0;
            double adminCost = 0;
            double contTotalDisposal = 0;
            double contTotalCollection = 0;
            if (multipleBandsOnRoundsWarning)
            {
                tradeCosts[tcRow + 1, tcColRB] = "Warning - Multiple Bands on Round - 1st Occurance Used";
            }
            for (int i = 1; i <= tcRow; i++)
            {
                totalCost += Convert.ToDouble(tradeCosts[i, tcColT]); //total cost for the containers
                contTotalDisposal += Convert.ToDouble(tradeCosts[i, tcColcTDC]); //total disposal cost for this UPRN
                contTotalCollection += Convert.ToDouble(tradeCosts[i, tcColcTCC]); //total collection cost for this UPRN
            }
            
            //apply property discount (if amount take of collection cost / if % take of disposal and colleciton total)
            tradeCosts[tcRow + 1, tcColPD] = tradeInfo.Discount.ToString();  //property Discount
            tradeCosts[tcRow + 1, tcColPDT] = tradeInfo.DiscountType;        //property DiscountType
            tradeCosts[tcRow + 1, tcColVRP] = tradeInfo.VATRatePercent;      //property VatRate Percent
            //Property Discount
            if (tradeCosts[tcRow + 1, tcColPD] != "0")
            {
                if (tradeCosts[tcRow + 1, tcColPDT] == "£")
                {
                    propertyDiscountAmount = Convert.ToDouble(tradeCosts[tcRow + 1, tcColPD]);
                    totalCost = totalCost - propertyDiscountAmount;
                    contTotalCollection = contTotalCollection - propertyDiscountAmount;
                }
                if (tradeCosts[tcRow + 1, tcColPDT] == "%")
                {
                    percentageDiscount = Convert.ToDouble(tradeCosts[tcRow + 1, tcColPD]) / 100;
                    totalCost = totalCost - (totalCost * percentageDiscount);
                    contTotalCollection = contTotalCollection - (contTotalCollection * percentageDiscount);
                    contTotalDisposal = contTotalDisposal - (contTotalDisposal * percentageDiscount);
                }
            }
            //calc Vat rate
            if (tradeInfo.VATRatePercent != "" && tradeInfo.VATRatePercent != "NotRequestedInSQLSelect")
            {
                vatRate = Convert.ToDouble(tradeInfo.VATRatePercent);
                totalCost += (totalCost * (vatRate / 100));
            }
            //Admin charge            
            if (tradeInfo.ThirdParty == "Y")
            {
                adminCost = 0;
            }
            else
            {
                string[] splitAdminCharges = adminCharge.Split('#');
                if (splitAdminCharges.Length > 2)
                {
                    //** Should we just use the highest charge????
                    tradeCosts[tcRow + 1, tcColPAC] = adminCharge.Replace("#", "") + "<br/>Error Multiple Admin Charges";
                }
                else
                {
                    //Apply Admin Cost if applicable 
                    int calRangeStartDateYYYYMMDD = Convert.ToInt32(startDate.ToString("yyyyMMdd"));
                    int calRangeEndDateYYYYMMDD = Convert.ToInt32(endDate.ToString("yyyyMMdd"));
                    double daysInRange = 0;
                    double noOfAnniversaryOfContractDates = 0;
                    int tempContractStartDateYYYYMMDD = 19730401;//default to 1st april start date.
                    if (tradeInfo.ContractStartDateYYYYMMDD != 0)
                    {
                        tempContractStartDateYYYYMMDD = tradeInfo.ContractStartDateYYYYMMDD;
                    }
                    int tempContractEndDAte = 20990331;
                    if (tradeInfo.ContractEndDateYYYYMMDD != 0)
                    {
                        tempContractEndDAte = tradeInfo.ContractEndDateYYYYMMDD;
                    }
                    string contractAnnivDateToCheck = tempContractStartDateYYYYMMDD.ToString().Substring(4, 4);
                    if (contractAnnivDateToCheck == "0229") { contractAnnivDateToCheck = "0228"; }//cater for leap year start date

                    bool calendarRangeIncludesAnApril1st = false;
                    double april1Cnt = 0;
                    var dateFormats = new[] { "yyyyMMdd" };
                    DateTime validDate;
                    for (int ad = calRangeStartDateYYYYMMDD; ad < calRangeEndDateYYYYMMDD; ad++)
                    {

                        if (DateTime.TryParseExact(ad.ToString(), dateFormats, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out validDate))
                        {
                            if (ad >= tempContractStartDateYYYYMMDD)
                            {
                                if (ad > tempContractEndDAte)
                                {
                                    break;
                                }
                                if (ad.ToString().Substring(4, 4) == contractAnnivDateToCheck)
                                {
                                    noOfAnniversaryOfContractDates++;
                                }

                                daysInRange++;
                                string year1 = ad.ToString().Substring(0, 4);
                                string year0000 = year1 + "0000";
                                string mmdd = (ad - Convert.ToInt32(year0000)).ToString("0000");
                                if (mmdd == "0401")
                                {
                                    april1Cnt++;
                                }
                            }
                        }
                    }
                    double weeksInRange = daysInRange / 7;
                    double quartersInRange = daysInRange / (364 / 4);
                    double monthsInRange = daysInRange / 30.33333333;
                    if (april1Cnt > 0) { calendarRangeIncludesAnApril1st = true; }
                    //3 - contract start Date is in the Calendar Range                     
                    bool contractStartDateInCalendarRange = true;
                    if (tradeInfo.ContractStartDateYYYYMMDD != 0)
                    {
                        if (tradeInfo.ContractStartDateYYYYMMDD < calRangeStartDateYYYYMMDD || tradeInfo.ContractStartDateYYYYMMDD > calRangeEndDateYYYYMMDD)
                        {
                            contractStartDateInCalendarRange = false;
                        }
                    }
                    if (thereIsAContainerStartDateInCalendarRange)
                    {
                        if (tradeCosts[tcRow + 1, tcColPAC] == "NotFound")
                        {
                            tradeCosts[tcRow + 1, tcColWO] += " **PricingNotFound for this Band and Container Size/Type**";
                        }
                        else
                        {

                            if (tradeInfo.AdminChargeFrequency == "") { tradeInfo.AdminChargeFrequency = "Annually"; }
                            if (splitAdminCharges[0] == "") { splitAdminCharges[0] = "0"; }
                            tradeCosts[tcRow + 1, tcColPAC] = splitAdminCharges[0];
                            double pricingAdminCost = Convert.ToDouble(splitAdminCharges[0]);
                            switch (tradeInfo.AdminChargeFrequency)
                            {
                                //Annually#Quartely#Monthly#Weekly#Daily#OncePerContract#NoAdminFee
                                case "Annually":
                                    {
                                        //admin Fee  (charged on every April 1st)                                    
                                        adminCost = pricingAdminCost * april1Cnt;
                                        break;
                                    }
                                case "ContractAniv"://ContractAniv
                                    {
                                        //admin Fee  (charged on anniversary of contract start)                                    
                                        adminCost = pricingAdminCost * noOfAnniversaryOfContractDates;
                                        break;
                                    }
                                case "Quarterly":
                                    {
                                        //admin Fee((Admin Fee / 4)charged every 3 months from start of contract for the billlng period)
                                        adminCost = pricingAdminCost / 4 * quartersInRange;
                                        break;
                                    }
                                case "Monthly":
                                    {
                                        //admin Fee (Admin Fee /12) charged every month of billing period
                                        adminCost = pricingAdminCost / 12 * monthsInRange;
                                        break;
                                    }
                                case "Weekly":
                                    {
                                        //admin Fee (Admin Fee / 52) charged for every week of the billing period
                                        adminCost = pricingAdminCost / 52 * weeksInRange;
                                        break;
                                    }
                                case "Daily":
                                    {
                                        //admin Fee(admin Fee / [365 or 364] charged every day of billing period)
                                        adminCost = pricingAdminCost / 364 * daysInRange;
                                        break;
                                    }
                                case "OncePerContract":
                                    {
                                        //admin Fee (charged only once on contract start date)
                                        if (contractStartDateInCalendarRange)
                                        {
                                            adminCost = pricingAdminCost;
                                        }

                                        break;
                                    }
                                case "NoAdminFee":
                                    {
                                        //admin Fee No charge
                                        break;
                                    }
                                default:
                                    adminCost = pricingAdminCost;
                                    break;
                            }

                            totalCost += adminCost;
                            tradeCosts[tcRow + 1, tcColWO] += " + PA";
                        }
                    }
                }
            }
            tradeCosts[tcRow + 1, tcColWO] = "(total cost - ([PD]) X [PVRP]+PA{" + tradeInfo.AdminChargeFrequency + ":" + adminCost.ToString("0.00") + "})";
            //if (clientDetails.clientName3L == "RYD") { tradeCosts[tcRow + 1, tcColWO] += "<br/>**Hire cost still to add. KP(Webaspx) 05Aug2020"; }
            tradeCosts[tcRow + 1, tcColT]    = totalCost.ToString("0.00");
            tradeCosts[tcRow + 1, tcColcTDC] = contTotalDisposal.ToString("0.00");
            tradeCosts[tcRow + 1, tcColcTCC] = contTotalCollection.ToString("0.00");
            #endregion
        }


        #region Work out additional Collection Costs to additionalCosts Array
        if (hasTradeContainer && clientDetails.hasTrade && tradeInfo.UPRN != "NotRequestedInSQLSelect")
        {
            //uprn no leading zero
            string uprnNoLeadingZero = "";
            if (uprn.StartsWith("0")) { uprnNoLeadingZero = ("XX" + uprn).Replace("XX0", ""); }
            string sqlAddtional = "Select BinType,BinCount,AdditionalOrLoose,PricingBand,VATChargeable,VATRate,DateRequestedDDsMMsYYYY,Collected,CollectionDateYYYYMMDD, thirdPartyCo, invoiceDateYYYYMMDD, AddedByEmail, ID from AdditionalTradeCollections where UPRN='" + uprn + "' order by CollectionDateYYYYMMDD desc";
            if (uprnNoLeadingZero != "")
            {
                sqlAddtional = "Select BinType,BinCount,AdditionalOrLoose,PricingBand,VATChargeable,VATRate,DateRequestedDDsMMsYYYY,Collected,CollectionDateYYYYMMDD, thirdPartyCo, invoiceDateYYYYMMDD, AddedByEmail, ID from AdditionalTradeCollections where UPRN='" + uprn + "' or uprn='" + uprnNoLeadingZero + "' order by CollectionDateYYYYMMDD desc";
            }
            string[,] tradeAddCollects = Misc.getDataArrayExactFromDB(sqlAddtional, clientDetails.clientName, connection);
            int calRangeStartDateYYYYMMDD2 = Convert.ToInt32(startDate.ToString("yyyyMMdd"));
            int calRangeEndDateYYYYMMDD2 = Convert.ToInt32(endDate.ToString("yyyyMMdd"));
            List<string[]> additionalCollectionsList = new List<string[]>();
            pricesTable = new tPricing(pricingArray);
            double dblAdditionalCollectionsTotalCost = 0;
            //Additional collecitons total Row
            string[] addCollectHeaderRow = new string[12];
            addCollectHeaderRow[0] = "BinType";
            addCollectHeaderRow[1] = "BinCount";
            addCollectHeaderRow[2] = "AdditionalOrLoose";
            addCollectHeaderRow[3] = "PriceBand";
            addCollectHeaderRow[4] = "VATChargeable";
            addCollectHeaderRow[5] = "VATRate";
            addCollectHeaderRow[6] = "Collected";
            addCollectHeaderRow[7] = "collectionDate";
            addCollectHeaderRow[8] = "thirdPartyCo";
            addCollectHeaderRow[9] = "invoiceDate";
            addCollectHeaderRow[10] = "Band Cost";
            addCollectHeaderRow[11] = "Cost";
            additionalCollectionsList.Add(addCollectHeaderRow);

            for (int a = 1; a < tradeAddCollects.GetLength(0); a++)
            {
                //Get Additional Collection costs
                AdditionalCollections addCollectInfo = AdditionalCollections.getAdditionalCollectionInfo(tradeAddCollects, a);
                bool collectionsDateIsInRange = false;
                bool invoiceDateIsInRange = false;
                if (addCollectInfo.collectionDatePresent)
                {
                    int collectDateYYYYMMDD = Convert.ToInt32(addCollectInfo.collectionDateYYYYMMDD);
                    if (collectDateYYYYMMDD >= calRangeStartDateYYYYMMDD2 && collectDateYYYYMMDD <= calRangeEndDateYYYYMMDD2)
                    {
                        collectionsDateIsInRange = true;
                    }
                }
                if (addCollectInfo.invoiceDatePresent)
                {
                    int invDateYYYYMMDD = Convert.ToInt32(addCollectInfo.invoiceDateYYYYMMDD);
                    if (invDateYYYYMMDD >= calRangeStartDateYYYYMMDD2 || invDateYYYYMMDD <= calRangeEndDateYYYYMMDD2)
                    {
                        invoiceDateIsInRange = true;
                    }
                }
                string[] addCollectRow = new string[12];
                //get additional cost of this container from pricing.
                if ((collectionsDateIsInRange || invoiceDateIsInRange) && addCollectInfo.Collected == "Y")
                {
                    bool pricingFound = false;
                    for (int p = 0; p < pricesTable.Length; p++)
                    {
                        tPrice priceRow = pricesTable[p];//get row as an object
                        if (addCollectInfo.PricingBand == priceRow.Band)
                        {
                            //find matching container for this band
                            if (addCollectInfo.BinType.StartsWith(priceRow.BinType))
                            {
                                //Pricing Entry Found
                                pricingFound = true;
                                addCollectRow[0] = addCollectInfo.BinType;
                                addCollectRow[1] = addCollectInfo.BinCount;
                                addCollectRow[2] = addCollectInfo.AdditionalOrLoose;
                                addCollectRow[3] = addCollectInfo.PricingBand + "(" + priceRow.BandDesc + ")";
                                addCollectRow[4] = addCollectInfo.VATChargeable;
                                addCollectRow[5] = addCollectInfo.VATRate;
                                addCollectRow[6] = addCollectInfo.Collected;
                                addCollectRow[7] = Misc.DisplayDateInFormat(addCollectInfo.collectionDateYYYYMMDD, "yyyyMMdd", "ddd dd/MM/yyyy");
                                addCollectRow[8] = addCollectInfo.thirdPartyCo;
                                addCollectRow[9] = Misc.DisplayDateInFormat(addCollectInfo.invoiceDateYYYYMMDD, "yyyyMMdd", "ddd dd/MM/yyyy");
                                addCollectRow[10] = priceRow.AdditionalCollectionCost;
                                double binCountDbl = Convert.ToDouble(addCollectInfo.BinCount);
                                double dblAdditionalCost = 0;
                                if (priceRow.AdditionalCollectionCost != "")
                                {
                                    dblAdditionalCost = Convert.ToDouble(priceRow.AdditionalCollectionCost);
                                }
                                double thisAddCollectionsCost = dblAdditionalCost * binCountDbl;
                                if (addCollectInfo.VATChargeable == "Y")
                                {
                                    if (addCollectInfo.VATRate != "")
                                    {
                                        double vatRate = Convert.ToDouble(addCollectInfo.VATRate);
                                        thisAddCollectionsCost = thisAddCollectionsCost + (vatRate / 100 * thisAddCollectionsCost);
                                    }
                                }
                                addCollectRow[11] = thisAddCollectionsCost.ToString("0.00");
                                dblAdditionalCollectionsTotalCost += thisAddCollectionsCost;
                            }
                        }
                        if (pricingFound) { break; }
                    }
                    additionalCollectionsList.Add(addCollectRow);
                }
            }
            //Additional collecitons total Row
            string[] addCollectTotalRow = new string[12];
            addCollectTotalRow[0] = "Total";
            for (int k = 1; k < 11; k++)
            {
                addCollectTotalRow[k] = "";
            }
            addCollectTotalRow[11] = dblAdditionalCollectionsTotalCost.ToString("0.00");
            additionalCollectionsList.Add(addCollectTotalRow);
            additionalCosts = new string[additionalCollectionsList.Count, 12];
            for (int i = 0; i < additionalCollectionsList.Count; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    additionalCosts[i, j] = additionalCollectionsList[i][j];
                }
            }
        }
        #endregion

        #region Get Address Infomation for invoices
        //LLPG Addresses
        string sqlLLPGAddressInfo = "Select Address,Streetname, Locale,Locality,Town,Postcode,PropType,Assisted from LLPG where UPRN='" + uprn + "'";
        string[,] LLPGAddress = Misc.getDataArrayFromDB(sqlLLPGAddressInfo, clientDetails.clientName, connection);

        //Trade Data Addresses


        #endregion

        #region Depot and Next Collections dates as friendly text        
        string[] friendlyNextCollection = new string[calendarArr.GetLength(0) - 2];
        string roundDepot = "";
        if (returnFriendlyNextCollection) //this is false for calendar look up
        {
            string todayDDsMMsYYYY = DateTime.Now.ToString("dd/MM/yyyy");
            if (calendarStartDDMMYYYY != "") { todayDDsMMsYYYY = calendarStartDDMMYYYY; }
            int todaysIndex = -1;
            for (int i = 0; i < calendarArr.GetLength(1); i++)
            {
                if (calendarArr[0, i] == todayDDsMMsYYYY)
                {
                    todaysIndex = i;
                    break;
                }
            }
            //check for system messages            
            string sqlWMCMessages = "Select * from WMCMessages;";
            string[,] currentWMCMessages = Misc.getDataArrayExactFromDB(sqlWMCMessages, clientDetails.clientName, connection);

            //calArr - row0 is date, last row is cal icon / col 0 is the service
            int servRow = 0;
            string[] servNames = clientDetails.altServiceNames.Split('#');
            string[] servCycles = clientDetails.servicesCycle.Split('#');
            DateTime[] date1 = new DateTime[calendarArr.GetLength(0) - 2];
            DateTime[] date2 = new DateTime[calendarArr.GetLength(0) - 2];
            string localServiceName = "";
            string day = "";
            string serv3L = "";
            string andRepeatEvery = "";
            string cycle = "";
            string wk = "";
            DateTime d1 = new DateTime();
            string reason = "";
            string[] splitReason = new string[1];
            bool removeSecondDate;
            string[] roundNSplit = new string[1];
            for (int s = 1; s < calendarArr.GetLength(0) - 1; s++)
            {
                localServiceName = "";
                day = "";
                serv3L = "";
                andRepeatEvery = "";
                cycle = "";
                wk = "";
                for (int j = 0; j < servNames.Length; j++)
                {
                    if (servNames[j].StartsWith(calendarArr[s, 0]))
                    {
                        localServiceName = "<b>" + servNames[j].Substring(4).Replace(")", "") + "</b>";
                        friendlyNextCollection[servRow] += localServiceName + ": ";
                        break;
                    }
                }
                int datesFound = 0;
                for (int d = todaysIndex; d < calendarArr.GetLength(1); d++)
                {
                    if (calendarArr[s, d].Contains("1"))//collection on this day
                    {
                        if (datesFound == 1) { friendlyNextCollection[servRow] += " then "; }
                        datesFound++;
                        d1 = DateTime.ParseExact(calendarArr[0, d], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        friendlyNextCollection[servRow] += d1.ToString("ddd d MMM");
                        if (datesFound == 1) { date1[servRow] = d1; } else { date2[servRow] = d1; }
                        //for (int cr = 1; cr < calendarArray.GetLength(1); cr++)//find relevant container //**Check this should be GetLength(0)???? KP 02Aug19
                        for (int cr = 1; cr < calendarArray.GetLength(0); cr++)//find relevant container //**Check this should be GetLength(0)???? KP 02Aug19
                        {
                            if (calendarArray[cr, 2].Contains(calendarArr[s, 0]))
                            {
                                for (int c2 = 28; c2 < calendarArray.GetLength(1); c2++)//find Collection date
                                {
                                    if (calendarArray[0, c2].Contains(calendarArr[0, d]))
                                    {
                                        if (calendarArray[cr, c2].Contains("AF"))
                                        {
                                            reason = "";
                                            splitReason = calendarArray[cr, c2].Replace(")", "").Split('#');
                                            if (splitReason.Length > 1) { reason = " for " + splitReason[1]; }
                                            friendlyNextCollection[servRow] += "(Adjusted" + reason + ") ";
                                        }
                                        break;//date Found
                                    }
                                }
                            }
                            break;//serviceFound
                        }


                    }
                    if (datesFound == 2) { break; }
                }
                //work out frequency
                friendlyNextCollection[servRow] += " then every ";
                removeSecondDate = false;
                if (rounds.GetLength(0) > 1)
                {
                    for (int r = 1; r < rounds.GetLength(0); r++)
                    {
                        if (rounds[r, 0].EndsWith(" " + calendarArr[s, 0]))
                        {
                            roundNSplit = rounds[r, 0].Split(' ');
                            day = roundNSplit[roundNSplit.Length - 3];
                            wk = roundNSplit[roundNSplit.Length - 2];
                            serv3L = roundNSplit[roundNSplit.Length - 1];

                            //[WCP-592]  [WCP-556] Start
                            //check for other rounds with same day and service but on different Weeks eg gets Wk1 and Wk2 colleciton (by different crews)                                        
                            //get Cycle for this service
                            string[] serveCySplit = clientDetails.servicesCycle.Split('#');
                            int cycleForThisService = -1;
                            for (int cy = 0; cy < serveCySplit.Length; cy++)
                            {
                                if (serveCySplit[cy] != "")
                                {
                                    if (serveCySplit[cy].Contains(serv3L))
                                    {
                                        string cycleForThisServ = serveCySplit[cy].Replace(serv3L + "(", "").Replace(")", "");
                                        try
                                        {
                                            cycleForThisService = Convert.ToInt32(cycleForThisServ);
                                        }
                                        catch
                                        { }
                                    }
                                }
                            }
                            //pop match Weeks         Rc - RoundCheck                   
                            if (cycleForThisService != -1)
                            {
                                string cycleWeeks = "";
                                string checkCycleWeeks = "";
                                for (int cy = 1; cy <= cycleForThisService; cy++)
                                {
                                    cycleWeeks += "Wk" + cy + " ";
                                }
                                for (int cy = 1; cy <= cycleForThisService; cy++)
                                {
                                    for (int rc = 1; rc < rounds.GetLength(0); rc++)
                                    {
                                        string[] roundNSplitRc = rounds[rc, 0].Split(' ');
                                        string dayRc    = roundNSplitRc[roundNSplitRc.Length - 3];
                                        string wkRc     = roundNSplitRc[roundNSplitRc.Length - 2];
                                        string serv3LRc = roundNSplitRc[roundNSplitRc.Length - 1];
                                        if (!checkCycleWeeks.Contains(wkRc))
                                        {
                                            if (day == dayRc && serv3L == serv3LRc)
                                            {
                                                if (wkRc == ("Wk" + cy.ToString()))
                                                {

                                                    checkCycleWeeks += "Wk" + cy + " ";
                                                }
                                            }
                                        }
                                    }
                                }
                                if (cycleWeeks == checkCycleWeeks) { wk = "Wkly"; }
                            }
                            //[WCP-592] [WCP-556] End

                            if (wk == "Wkly")
                            {
                                friendlyNextCollection[servRow] += "";
                            }
                            else
                            {
                                for (int w = 0; w < servCycles.Length; w++)
                                {
                                    if (servCycles[w].StartsWith(calendarArr[s, 0]))
                                    {
                                        cycle = servCycles[w].Replace(calendarArr[s, 0] + "(", "").Replace(")", "");
                                        TimeSpan daysBetween2 = date2[servRow].Subtract(date1[servRow]);
                                        int daysDifference = daysBetween2.Days;                                                                                                                        
                                        switch (cycle)
                                        {
                                            case "2":  { andRepeatEvery = "alternate "; if (daysDifference == 14) { removeSecondDate = true; }; break; }
                                            case "3":  { andRepeatEvery = "third ";     if (daysDifference == 21) { removeSecondDate = true; }; break; }
                                            case "4":  { andRepeatEvery = "fourth ";    if (daysDifference == 28) { removeSecondDate = true; }; break; }
                                            case "5":  { andRepeatEvery = "fifth ";     if (daysDifference == 35) { removeSecondDate = true; }; break; }
                                            case "6":  { andRepeatEvery = "sixth ";     if (daysDifference == 42) { removeSecondDate = true; }; break; }
                                            case "7":  { andRepeatEvery = "seventh ";   if (daysDifference == 49) { removeSecondDate = true; }; break; }
                                            case "8":  { andRepeatEvery = "eigth ";     if (daysDifference == 56) { removeSecondDate = true; }; break; }
                                            case "9":  { andRepeatEvery = "ninth ";     if (daysDifference == 63) { removeSecondDate = true; }; break; }
                                            case "10": { andRepeatEvery = "tenth ";     if (daysDifference == 70) { removeSecondDate = true; }; break; }
                                            default:   { andRepeatEvery = cycle + " ";  if (daysDifference == (Convert.ToInt16(cycle) * 7)) { removeSecondDate = true; }; break; }
                                        }
                                        break;
                                    }
                                }
                            }
                            //fullDay = Misc.getdayFullFromDay3L(day);
                            //friendlyNextCollection[servRow] += andRepeatEvery + fullDay;
                            friendlyNextCollection[servRow] += andRepeatEvery + day;
                            break;
                        }
                    }
                }//next roundname
                string info1 = localServiceName + ":  then every ";
                //string info2 = localServiceName + ":  then every " + andRepeatEvery + fullDay;
                string info2 = localServiceName + ":  then every " + andRepeatEvery + day;
                if (friendlyNextCollection[servRow] == info1) { friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(info1, localServiceName + ": No " + localServiceName.Replace("<b>", "").Replace("</b>", "") + " waste collection for this address"); }
                if (friendlyNextCollection[servRow] == info2) { friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(info2, localServiceName + ": No live " + localServiceName + " container for this address(" + day + ")"); }
                int adjustedOccurnaces = Misc.CountNeedlesInHayStack("Adjusted", friendlyNextCollection[servRow]);
                if (removeSecondDate && adjustedOccurnaces != 2) { friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" then " + date2[servRow].ToString("ddd d MMM"), ""); }
                string TodayDDDddMMM = DateTime.Now.ToString("ddd d MMM");
                if (friendlyNextCollection[servRow].Contains(TodayDDDddMMM))
                {
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(TodayDDDddMMM, "Today");
                }
                string TomorrowDDDddMMM = DateTime.Now.AddDays(1).ToString("ddd d MMM");
                if (friendlyNextCollection[servRow].Contains(TomorrowDDDddMMM))
                {
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(TomorrowDDDddMMM, "Tomorrow");
                }
                friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace("every 1 ", "every ");
                if (councilName == "RCH" || councilName.ToLower() == "richmondshire")
                {
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Mon", " Monday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Tue", " Tuesday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Wed", " Wednesday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Thu", " Thursday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Fri", " Friday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Sat", " Saturday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" Sun", " Sunday");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace("No live <b>Garden</b> container for this address(" + day + ")", "There is currently no garden waste subscription for this address");
                    friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace("No Garden waste collection for this address", "There is currently no garden waste subscription for this address");
                }

                #region suspendedServiceCheck

                //check for Service Suspension and work out next Collection Date                                
                //Select OrigDateDDsMMsYYYY,Service,roundName where OrigDDD in ('Mon') OrigDateYYYYMMDD>='" + previousMonth.ToString("yyyyMMdd") + "' and moveToDDsMMsYYYY='NoCollect'
                // order by Service, roundName ,OrigDateYYYYMMDD;                                
                int todaysColInCalendarArray = -1;
                string todayYYYYMMDD = DateTime.Now.ToString("yyyyMMdd");
                if (calendarStartDDMMYYYY != "") { todayYYYYMMDD = calendarStartDDMMYYYY.Substring(6, 4) + calendarStartDDMMYYYY.Substring(3, 2) + calendarStartDDMMYYYY.Substring(0, 2); }
                string nextSchedCollectionDateDDsMMsYYYY = "";
                for (int c = 28; c < calendarArray.GetLength(1); c++)//ignore first 4 weeks as this is the month before
                {
                    if (calendarArray[0, c].Contains(todayYYYYMMDD))
                    {
                        todaysColInCalendarArray = c;
                        break;
                    }
                }
                for (int c = todaysColInCalendarArray; c < calendarArray.GetLength(1); c++)//get next scheduled collection
                {
                    if (wk == "Wkly")
                    {
                        if (calendarArray[0, c].Contains(day))
                        {
                            nextSchedCollectionDateDDsMMsYYYY = calendarArray[0, c].Split('#')[0];
                            break;
                        }
                    }
                    else
                    {
                        if (calendarArray[0, c].Contains(wk) && calendarArray[0, c].Contains(day))
                        {
                            nextSchedCollectionDateDDsMMsYYYY = calendarArray[0, c].Split('#')[0];
                            break;
                        }
                    }
                }
                bool serviceSuspension = false;
                string lastNoCollectionDate = "";
                if (cycle == "") { cycle = "-99"; }
                int serviceDaysRepeat = Convert.ToInt16(cycle) * 7;
                string allNoCollectDates = "";
                for (int n = 1; n < noCollectDates.GetLength(0); n++)
                {
                    if (noCollectDates[n, 1] == serv3L)
                    {
                        allNoCollectDates += noCollectDates[n, 0] + "#";
                    }
                }
                if (allNoCollectDates.Contains(nextSchedCollectionDateDDsMMsYYYY))
                {
                    serviceSuspension = true;
                    lastNoCollectionDate = nextSchedCollectionDateDDsMMsYYYY;
                    DateTime nextSchedDT = DateTime.ParseExact(nextSchedCollectionDateDDsMMsYYYY, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    for (int n = 1; n < 99; n++)
                    {
                        string nextCollectDateToCheck = nextSchedDT.AddDays(n * serviceDaysRepeat).ToString("dd/MM/yyyy");
                        if (allNoCollectDates.Contains(nextCollectDateToCheck))
                        {
                            lastNoCollectionDate = nextCollectDateToCheck;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                if (serviceSuspension)
                {
                    friendlyNextCollection[servRow] += " **Service suspended until after " + lastNoCollectionDate + "**";
                    if (councilName == "RCH" || councilName.ToLower() == "richmondshire")
                    {
                        DateTime todayDT = DateTime.Now;
                        string lastYearsSubscriptionYear = todayDT.AddYears(-1).ToString("yyyy");
                        if (todayDT.ToString("yyyy") != lastNoCollectionDate.Substring(6, 4)) { lastYearsSubscriptionYear = todayDT.ToString("yyyy"); }
                        //friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace("There is currently no garden waste subscription for this address **Service suspended until after " + lastNoCollectionDate + "**", lastYearsSubscriptionYear + " garden waste service has now ended and will recommence after " + lastNoCollectionDate + ".");
                        friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace("There is currently no garden waste subscription for this address **Service suspended until after " + lastNoCollectionDate + "**", lastYearsSubscriptionYear + " garden waste service has now ended and subscriptions will re-open in February.");
                    }
                    if (councilName == "SNO" || councilName.ToLower() == "southnorfolk") 
                    {
                        if (friendlyNextCollection[servRow].Contains("**Service suspended until after ") &&
                            friendlyNextCollection[servRow].Contains("/12/202") //show for all suspended in December
                            ) 
                        {
                            friendlyNextCollection[servRow] = friendlyNextCollection[servRow].Replace(" **Service suspended until after " + lastNoCollectionDate + "**", "** No Collections Xmas and New Year Week **");
                        }
                    }
                }
                //End Check for Service Suspension
                #endregion

                #region system messages for this service
                for (int m = 1; m < currentWMCMessages.GetLength(0); m++)
                {
                    WMCMessages messDetails = WMCMessages.GetParameters(currentWMCMessages, m);
                    if (messDetails.isCurrentMessage) 
                    {
                        if (messDetails.MessageService.Contains(calendarArr[s, 0]) ) 
                        {
                            friendlyNextCollection[servRow] += "[" + messDetails.MessageText + "]";
                        }
                    }
                }              
                #endregion

                servRow++;
            }//next service

            if (clientDetails.clientName == "ABC")
            {
                for (int r = 1; r < rounds.GetLength(0); r++)//only gets depot from last round - could get for all by appending to a string and splitting then reporting for each service
                {
                    if (rounds[r, 0].Contains(" AR ")) { roundDepot = "Armagh"; }
                    if (rounds[r, 0].Contains(" BB ")) { roundDepot = "Banbridge"; }
                    if (rounds[r, 0].Contains(" CA ")) { roundDepot = "Craigavon"; }
                }
            }
        }
        #endregion


        CalendarAndCostsData calendarCostsBreakDownAndCostArrays = new CalendarAndCostsData()
        {
            //breakDownByContainerArray  = breakDownByContainer,
            //breakDownByServiceArray    = breakDownByService,
            breakDownByContainerArray = calendarArray,
            breakDownByServiceArray   = calendarArr,
            tradeCostsArray           = tradeCosts,
            additionalCostsArray      = additionalCosts,
            hasTradeContainer         = hasTradeContainer,
            clientServices            = clientServices,
            endDate                   = endDate,
            startDate                 = startDate,
            origCalStart              = origCalStart,
            rounds                    = rounds,
            containers                = containers,
            addressLLPG               = LLPGAddress,
            tradeData                 = tradeDataArray,
            nextCollectionDates       = friendlyNextCollection,
            depot                     = roundDepot,
            otherInfo                 = other_info.ToArray()
        };
        return calendarCostsBreakDownAndCostArrays;
    }
    //==================================================================================================================================================================
    private static void BreakHere() 
    { }
    //==================================================================================================================================================================
    private static string FalkirkChangeRollOutDates(string[,] rounds, int r)
    {        
        int cnt = 0;
        string[,] roundsAndDates = new string[1000, 3];        
        cnt++; roundsAndDates[cnt, 0] = "Burgundy CardPaper Rec Round"; roundsAndDates[cnt, 1] = "Blue PlasticCans Pls Round"; roundsAndDates[cnt, 2] = "yyyyMMdd";
        cnt++; roundsAndDates[cnt, 0] = "R100 Fq2 Cr90 Mon Wk2 Rec";    roundsAndDates[cnt, 1] = "R100 Fq2 Cr90 Mon Wk4 Pls";  roundsAndDates[cnt, 2] = "20190909";
        cnt++; roundsAndDates[cnt, 0] = "R1101 Fq2 Cr1 Mon Wk1 Rec";    roundsAndDates[cnt, 1] = "R1101 Fq2 Cr1 Mon Wk3 Pls";  roundsAndDates[cnt, 2] = "20190708";
        cnt++; roundsAndDates[cnt, 0] = "R1102 Fq2 Cr2 Mon Wk1 Rec";    roundsAndDates[cnt, 1] = "R1102 Fq2 Cr2 Mon Wk3 Pls";  roundsAndDates[cnt, 2] = "20190722";
        cnt++; roundsAndDates[cnt, 0] = "R1103 Fq2 Cr3 Mon Wk1 Rec";    roundsAndDates[cnt, 1] = "R1103 Fq2 Cr3 Mon Wk3 Pls";  roundsAndDates[cnt, 2] = "20190805";
        cnt++; roundsAndDates[cnt, 0] = "R1103 Fq2 Cr3 Mon Wk1 Rec";    roundsAndDates[cnt, 1] = "R1103 Fq2 Cr3 Mon Wk3 Pls";  roundsAndDates[cnt, 2] = "20190805";
        cnt++; roundsAndDates[cnt, 0] = "R1104 Fq2 Cr4 Mon Wk1 Rec";    roundsAndDates[cnt, 1] = "R1104 Fq2 Cr4 Mon Wk3 Pls";  roundsAndDates[cnt, 2] = "20190902";
        cnt++; roundsAndDates[cnt, 0] = "R1105 Fq2 Cr5 Mon Wk1 Rec";    roundsAndDates[cnt, 1] = "R1105 Fq2 Cr5 Mon Wk3 Pls";  roundsAndDates[cnt, 2] = "20190902";
        cnt++; roundsAndDates[cnt, 0] = "R1159 Fq2 Cr59 Mon Wk1 Rec";   roundsAndDates[cnt, 1] = "R1159 Fq2 Cr59 Mon Wk3 Pls"; roundsAndDates[cnt, 2] = "20190930";
        cnt++; roundsAndDates[cnt, 0] = "R1206 Fq2 Cr6 Tue Wk1 Rec";    roundsAndDates[cnt, 1] = "R1206 Fq2 Cr6 Tue Wk3 Pls";  roundsAndDates[cnt, 2] = "20190709";
        cnt++; roundsAndDates[cnt, 0] = "R1207 Fq2 Cr7 Tue Wk1 Rec";    roundsAndDates[cnt, 1] = "R1207 Fq2 Cr7 Tue Wk3 Pls";  roundsAndDates[cnt, 2] = "20190806";
        cnt++; roundsAndDates[cnt, 0] = "R1208 Fq2 Cr8 Tue Wk1 Rec";    roundsAndDates[cnt, 1] = "R1208 Fq2 Cr8 Tue Wk3 Pls";  roundsAndDates[cnt, 2] = "20190806";
        cnt++; roundsAndDates[cnt, 0] = "R1209 Fq2 Cr9 Tue Wk1 Rec";    roundsAndDates[cnt, 1] = "R1209 Fq2 Cr9 Tue Wk3 Pls";  roundsAndDates[cnt, 2] = "20190903";
        cnt++; roundsAndDates[cnt, 0] = "R1210 Fq2 Cr10 Tue Wk1 Rec";   roundsAndDates[cnt, 1] = "R1210 Fq2 Cr10 Tue Wk3 Pls"; roundsAndDates[cnt, 2] = "20190903";
        cnt++; roundsAndDates[cnt, 0] = "R1311 Fq2 Cr11 Wed Wk1 Rec";   roundsAndDates[cnt, 1] = "R1311 Fq2 Cr11 Wed Wk3 Pls"; roundsAndDates[cnt, 2] = "20190710";
        cnt++; roundsAndDates[cnt, 0] = "R1312 Fq2 Cr12 Wed Wk1 Rec";   roundsAndDates[cnt, 1] = "R1312 Fq2 Cr12 Wed Wk3 Pls"; roundsAndDates[cnt, 2] = "20190807";
        cnt++; roundsAndDates[cnt, 0] = "R1313 Fq2 Cr13 Wed Wk1 Rec";   roundsAndDates[cnt, 1] = "R1313 Fq2 Cr13 Wed Wk3 Pls"; roundsAndDates[cnt, 2] = "20190807";
        cnt++; roundsAndDates[cnt, 0] = "R1315 Fq2 Cr15 Wed Wk1 Rec";   roundsAndDates[cnt, 1] = "R1315 Fq2 Cr15 Wed Wk3 Pls"; roundsAndDates[cnt, 2] = "20190904";
        cnt++; roundsAndDates[cnt, 0] = "R1316 Fq2 Cr16 Wed Wk1 Rec";   roundsAndDates[cnt, 1] = "R1316 Fq2 Cr16 Wed Wk3 Pls"; roundsAndDates[cnt, 2] = "20190904";
        cnt++; roundsAndDates[cnt, 0] = "R1360 Fq2 Cr60 Wed Wk1 Rec";   roundsAndDates[cnt, 1] = "R1360 Fq2 Cr60 Wed Wk3 Pls"; roundsAndDates[cnt, 2] = "20191002";
        cnt++; roundsAndDates[cnt, 0] = "R1417 Fq2 Cr17 Thu Wk1 Rec";   roundsAndDates[cnt, 1] = "R1417 Fq2 Cr17 Thu Wk3 Pls"; roundsAndDates[cnt, 2] = "20190711";
        cnt++; roundsAndDates[cnt, 0] = "R1418 Fq2 Cr18 Thu Wk1 Rec";   roundsAndDates[cnt, 1] = "R1418 Fq2 Cr18 Thu Wk3 Pls"; roundsAndDates[cnt, 2] = "20190808";
        cnt++; roundsAndDates[cnt, 0] = "R1419 Fq2 Cr19 Thu Wk1 Rec";   roundsAndDates[cnt, 1] = "R1419 Fq2 Cr19 Thu Wk3 Pls"; roundsAndDates[cnt, 2] = "20190808";
        cnt++; roundsAndDates[cnt, 0] = "R1420 Fq2 Cr20 Thu Wk1 Rec";   roundsAndDates[cnt, 1] = "R1420 Fq2 Cr20 Thu Wk3 Pls"; roundsAndDates[cnt, 2] = "20190905";
        cnt++; roundsAndDates[cnt, 0] = "R1421 Fq2 Cr21 Thu Wk1 Rec";   roundsAndDates[cnt, 1] = "R1421 Fq2 Cr21 Thu Wk3 Pls"; roundsAndDates[cnt, 2] = "20190905";
        cnt++; roundsAndDates[cnt, 0] = "R1458 Fq2 Cr58 Thu Wk1 Rec";   roundsAndDates[cnt, 1] = "R1458 Fq2 Cr58 Thu Wk3 Pls"; roundsAndDates[cnt, 2] = "20191003";
        cnt++; roundsAndDates[cnt, 0] = "R1522 Fq2 Cr22 Fri Wk1 Rec";   roundsAndDates[cnt, 1] = "R1522 Fq2 Cr22 Fri Wk3 Pls"; roundsAndDates[cnt, 2] = "20190712";
        cnt++; roundsAndDates[cnt, 0] = "R1523 Fq2 Cr23 Fri Wk1 Rec";   roundsAndDates[cnt, 1] = "R1523 Fq2 Cr23 Fri Wk3 Pls"; roundsAndDates[cnt, 2] = "20190809";
        cnt++; roundsAndDates[cnt, 0] = "R1524 Fq2 Cr24 Fri Wk1 Rec";   roundsAndDates[cnt, 1] = "R1524 Fq2 Cr24 Fri Wk3 Pls"; roundsAndDates[cnt, 2] = "20190809";
        cnt++; roundsAndDates[cnt, 0] = "R1525 Fq2 Cr25 Fri Wk1 Rec";   roundsAndDates[cnt, 1] = "R1525 Fq2 Cr25 Fri Wk3 Pls"; roundsAndDates[cnt, 2] = "20190906";
        cnt++; roundsAndDates[cnt, 0] = "R1526 Fq2 Cr26 Fri Wk1 Rec";   roundsAndDates[cnt, 1] = "R1526 Fq2 Cr26 Fri Wk3 Pls"; roundsAndDates[cnt, 2] = "20190906";
        cnt++; roundsAndDates[cnt, 0] = "R1557 Fq2 Cr57 Fri Wk1 Rec";   roundsAndDates[cnt, 1] = "R1557 Fq2 Cr57 Fri Wk3 Pls"; roundsAndDates[cnt, 2] = "20191004";
        cnt++; roundsAndDates[cnt, 0] = "R200 Fq2 Cr92 Tue Wk2 Rec";    roundsAndDates[cnt, 1] = "R200 Fq2 Cr92 Tue Wk4 Pls";  roundsAndDates[cnt, 2] = "20190910";
        cnt++; roundsAndDates[cnt, 0] = "R2127 Fq2 Cr27 Mon Wk2 Rec";   roundsAndDates[cnt, 1] = "R2127 Fq2 Cr27 Mon Wk4 Pls"; roundsAndDates[cnt, 2] = "20190923";
        cnt++; roundsAndDates[cnt, 0] = "R2128 Fq2 Cr28 Mon Wk2 Rec";   roundsAndDates[cnt, 1] = "R2128 Fq2 Cr28 Mon Wk4 Pls"; roundsAndDates[cnt, 2] = "20190729";
        cnt++; roundsAndDates[cnt, 0] = "R2129 Fq2 Cr29 Mon Wk2 Rec";   roundsAndDates[cnt, 1] = "R2129 Fq2 Cr29 Mon Wk4 Pls"; roundsAndDates[cnt, 2] = "20190812";
        cnt++; roundsAndDates[cnt, 0] = "R2130 Fq2 Cr30 Mon Wk2 Rec";   roundsAndDates[cnt, 1] = "R2130 Fq2 Cr30 Mon Wk4 Pls"; roundsAndDates[cnt, 2] = "20190826";
        cnt++; roundsAndDates[cnt, 0] = "R2131 Fq2 Cr31 Mon Wk2 Rec";   roundsAndDates[cnt, 1] = "R2131 Fq2 Cr31 Mon Wk4 Pls"; roundsAndDates[cnt, 2] = "20190909";
        cnt++; roundsAndDates[cnt, 0] = "R2137 Fq2 Cr37 Mon Wk2 Rec";   roundsAndDates[cnt, 1] = "R2137 Fq2 Cr37 Mon Wk4 Pls"; roundsAndDates[cnt, 2] = "20190715";
        cnt++; roundsAndDates[cnt, 0] = "R2232 Fq2 Cr32 Tue Wk2 Rec";   roundsAndDates[cnt, 1] = "R2232 Fq2 Cr32 Tue Wk4 Pls"; roundsAndDates[cnt, 2] = "20190716";
        cnt++; roundsAndDates[cnt, 0] = "R2233 Fq2 Cr33 Tue Wk2 Rec";   roundsAndDates[cnt, 1] = "R2233 Fq2 Cr33 Tue Wk4 Pls"; roundsAndDates[cnt, 2] = "20190730";
        cnt++; roundsAndDates[cnt, 0] = "R2234 Fq2 Cr34 Tue Wk2 Rec";   roundsAndDates[cnt, 1] = "R2234 Fq2 Cr34 Tue Wk4 Pls"; roundsAndDates[cnt, 2] = "20190813";
        cnt++; roundsAndDates[cnt, 0] = "R2235 Fq2 Cr35 Tue Wk2 Rec";   roundsAndDates[cnt, 1] = "R2235 Fq2 Cr35 Tue Wk4 Pls"; roundsAndDates[cnt, 2] = "20190827";
        cnt++; roundsAndDates[cnt, 0] = "R2236 Fq2 Cr36 Tue Wk2 Rec";   roundsAndDates[cnt, 1] = "R2236 Fq2 Cr36 Tue Wk4 Pls"; roundsAndDates[cnt, 2] = "20190910";
        cnt++; roundsAndDates[cnt, 0] = "R2254 Fq2 Cr54 Tue Wk2 Rec";   roundsAndDates[cnt, 1] = "R2254 Fq2 Cr54 Tue Wk4 Pls"; roundsAndDates[cnt, 2] = "20190924";
        cnt++; roundsAndDates[cnt, 0] = "R2338 Fq2 Cr38 Wed Wk2 Rec";   roundsAndDates[cnt, 1] = "R2338 Fq2 Cr38 Wed Wk4 Pls"; roundsAndDates[cnt, 2] = "20190731";
        cnt++; roundsAndDates[cnt, 0] = "R2339 Fq2 Cr39 Wed Wk2 Rec";   roundsAndDates[cnt, 1] = "R2339 Fq2 Cr39 Wed Wk4 Pls"; roundsAndDates[cnt, 2] = "20190814";
        cnt++; roundsAndDates[cnt, 0] = "R2340 Fq2 Cr40 Wed Wk2 Rec";   roundsAndDates[cnt, 1] = "R2340 Fq2 Cr40 Wed Wk4 Pls"; roundsAndDates[cnt, 2] = "20190828";
        cnt++; roundsAndDates[cnt, 0] = "R2341 Fq2 Cr41 Wed Wk2 Rec";   roundsAndDates[cnt, 1] = "R2341 Fq2 Cr41 Wed Wk4 Pls"; roundsAndDates[cnt, 2] = "20190911";
        cnt++; roundsAndDates[cnt, 0] = "R2342 Fq2 Cr42 Wed Wk2 Rec";   roundsAndDates[cnt, 1] = "R2342 Fq2 Cr42 Wed Wk4 Pls"; roundsAndDates[cnt, 2] = "20190925";
        cnt++; roundsAndDates[cnt, 0] = "R2344 Fq2 Cr44 Wed Wk2 Rec";   roundsAndDates[cnt, 1] = "R2344 Fq2 Cr44 Wed Wk4 Pls"; roundsAndDates[cnt, 2] = "20190717";
        cnt++; roundsAndDates[cnt, 0] = "R2443 Fq2 Cr43 Thu Wk2 Rec";   roundsAndDates[cnt, 1] = "R2443 Fq2 Cr43 Thu Wk4 Pls"; roundsAndDates[cnt, 2] = "20190718";
        cnt++; roundsAndDates[cnt, 0] = "R2445 Fq2 Cr45 Thu Wk2 Rec";   roundsAndDates[cnt, 1] = "R2445 Fq2 Cr45 Thu Wk4 Pls"; roundsAndDates[cnt, 2] = "20190815";
        cnt++; roundsAndDates[cnt, 0] = "R2446 Fq2 Cr46 Thu Wk2 Rec";   roundsAndDates[cnt, 1] = "R2446 Fq2 Cr46 Thu Wk4 Pls"; roundsAndDates[cnt, 2] = "20190829";
        cnt++; roundsAndDates[cnt, 0] = "R2451 Fq2 Cr61 Thu Wk2 Rec";   roundsAndDates[cnt, 1] = "R2451 Fq2 Cr61 Thu Wk4 Pls"; roundsAndDates[cnt, 2] = "20190912";
        cnt++; roundsAndDates[cnt, 0] = "R2452 Fq2 Cr52 Thu Wk2 Rec";   roundsAndDates[cnt, 1] = "R2452 Fq2 Cr52 Thu Wk4 Pls"; roundsAndDates[cnt, 2] = "20190801";
        cnt++; roundsAndDates[cnt, 0] = "R2456 Fq2 Cr56 Thu Wk2 Rec";   roundsAndDates[cnt, 1] = "R2456 Fq2 Cr56 Thu Wk4 Pls"; roundsAndDates[cnt, 2] = "20190926";
        cnt++; roundsAndDates[cnt, 0] = "R2547 Fq2 Cr47 Fri Wk2 Rec";   roundsAndDates[cnt, 1] = "R2547 Fq2 Cr47 Fri Wk4 Pls"; roundsAndDates[cnt, 2] = "20190805";
        cnt++; roundsAndDates[cnt, 0] = "R2548 Fq2 Cr48 Fri Wk2 Rec";   roundsAndDates[cnt, 1] = "R2548 Fq2 Cr48 Fri Wk4 Pls"; roundsAndDates[cnt, 2] = "20190802";
        cnt++; roundsAndDates[cnt, 0] = "R2549 Fq2 Cr49 Fri Wk2 Rec";   roundsAndDates[cnt, 1] = "R2549 Fq2 Cr49 Fri Wk4 Pls"; roundsAndDates[cnt, 2] = "20190816";
        cnt++; roundsAndDates[cnt, 0] = "R2550 Fq2 Cr50 Fri Wk2 Rec";   roundsAndDates[cnt, 1] = "R2550 Fq2 Cr50 Fri Wk4 Pls"; roundsAndDates[cnt, 2] = "20190830";
        cnt++; roundsAndDates[cnt, 0] = "R2553 Fq2 Cr53 Fri Wk2 Rec";   roundsAndDates[cnt, 1] = "R2553 Fq2 Cr53 Fri Wk4 Pls"; roundsAndDates[cnt, 2] = "20190913";
        cnt++; roundsAndDates[cnt, 0] = "R2555 Fq2 Cr55 Fri Wk2 Rec";   roundsAndDates[cnt, 1] = "R2555 Fq2 Cr55 Fri Wk4 Pls"; roundsAndDates[cnt, 2] = "20190927";
        cnt++; roundsAndDates[cnt, 0] = "R300 Fq2 Cr93 Wed Wk2 Rec";    roundsAndDates[cnt, 1] = "R300 Fq2 Cr93 Wed Wk4 Pls";  roundsAndDates[cnt, 2] = "20190917";
        cnt++; roundsAndDates[cnt, 0] = "R400 Fq2 Cr94 Thu Wk2 Rec";    roundsAndDates[cnt, 1] = "R400 Fq2 Cr94 Thu Wk4 Pls";  roundsAndDates[cnt, 2] = "20190912";
        cnt++; roundsAndDates[cnt, 0] = "R500 Fq2 Cr95 Fri Wk2 Rec";    roundsAndDates[cnt, 1] = "R500 Fq2 Cr95 Fri Wk4 Pls";  roundsAndDates[cnt, 2] = "20190913";
        cnt++; roundsAndDates[cnt, 0] = "R700 Fq2 Cr97 Wed Wk2 Rec";    roundsAndDates[cnt, 1] = "R700 Fq2 Cr97 Wed Wk4 Pls";  roundsAndDates[cnt, 2] = "20190925";
        //Ref(4)#Pls(4)#Foo(1)#Box(2)#Grn(2)#Gls(4)#Rec(4)        
        //string roundnameDollarDayDollarColClientServiceCycles = "";//KP edit out 02Aug19
        string yyyyMMddToday = DateTime.Now.ToString("yyyyMMdd");
        int todayyyyyMMdd = Convert.ToInt32(yyyyMMddToday);    
        WaxRoundNewDetails roundDetails = WaxRoundNewDetails.getRoundDetails(rounds, r);                
        string upToDate = "";
        for (int i = 1; i <= cnt; i++)
        {
            if (roundDetails.RoundName == roundsAndDates[i, 1])
            {
                int startDate = Convert.ToInt32(roundsAndDates[i, 2]);
                if (todayyyyyMMdd < startDate) 
                {                        
                    upToDate = roundsAndDates[i, 2];
                    break;
                }
            }
        }
        return upToDate;
    }
    //=================================================================================================================================================================================    
    public static bool testReturnBool()
    {
        //only required for incab clients thathave collections
        bool clientHasCollections = false;        
        return clientHasCollections;
    }
    //=================================================================================================================================================================================
    public static string testRturnString()
    {
        string username = "zzzz";        
        return username;
    }
    //=================================================================================================================================================================================   
    public static string[,] testReturnArray()
    {        
        string[,] exactSizeArray = new string[1, 1];        
        return exactSizeArray;
    }
    //=================================================================================================================================================================================        
    //public static StringBuilder createDocument(string[] uprns, int uprnIndex, string docType, TradeJobsRequests tJobReq, TradeInfo tInfo, CalendarAndCostsData calendarInfo, colClient clientDetails)
    //{
        
    //}
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================        
    //=================================================================================================================================================================================    
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================
    //=================================================================================================================================================================================

   
}
    

    
