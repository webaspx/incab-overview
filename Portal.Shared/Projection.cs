﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portal.Shared
{
    public static class Projection
    {
        public enum GridRef
        {
            UK,
            OSNI,
            OSNI2,
        }

        public static double[] OSToLatLon(double e, double n, GridRef gridRef)
        {
            return ConvertOStoLatLon(e, n, gridRef);
        }

        public static double[] LatLonToEN(double lat, double lon)
        {
            var converter = new OSConversion();
            return converter.latlongtoen(lat, lon);
        }

        #region OS To Lat Lon

        private static double[] ConvertOStoLatLon(double ee11, double nn11, GridRef gridRef)
        {
            //double NewLat = 0;
            //double NewLon = 0;
            double[] retLatLon = new double[2];
            //int not_osgb = 1;
            //int iusenavteq = 1;
            /*if (iusenavteq == 1 | not_osgb == 1)
            {
                if (GridRef != "OSNI")
                {
                    if (GridRef != "OSNI2")
                    {
                        NewLon = ee11;
                        NewLat = nn11;
                        retLatLon[0] = NewLat;
                        retLatLon[1] = NewLon;
                        return retLatLon;
                    }
                }
            }*/

            double a, b, Fo, phio, lambdao, Eo, No, degtorad, ee, nn;
            a = 6377563.396;
            b = 6356256.91;
            Fo = 0.9996012717;
            phio = 49;
            lambdao = -2;
            Eo = 400000;
            No = -100000;
            if (gridRef == GridRef.OSNI)
            {
                a = 6377340.189;
                b = 6356034.447;
                Fo = 1.000035;
                phio = 53.50;
                lambdao = -8;
                Eo = 200000;
                No = 250000;
            }
            else if (gridRef == GridRef.OSNI2)
            {
                a = 6377340.189;
                b = 6356034.447;
                Fo = 1.000035;
                phio = 53.50;
                lambdao = -8;
                Eo = 200000;
                No = 250000;
            }
            /*else if (gridRef == GridRef.OSNI1)
            {
                a = 6377340.189;
                b = 6356034.447;
                Fo = 1.000035;
                phio = 53.50;
                lambdao = -8;
                Eo = 200000;
                No = 250000;
            }*/

            degtorad = Math.Atan(1) * 4 / 180;
            phio = phio * degtorad;
            lambdao = lambdao * degtorad;
            //' easting
            double n, v;
            ee = ee11;
            //'northing
            nn = nn11;
            ee = Convert.ToDouble(ee);
            nn = Convert.ToDouble(nn);
        line15:
            int icorrect = 1;
            if (icorrect == 1)
            {
                if (ee > 1000000)
                {
                    ee = ee / 10;
                    goto line15;
                }
                if (nn > 1000000)
                {
                    nn = nn / 10;
                    goto line15;
                }
            }
            double e2, phip, phi, m, Diff, tol, rho, eta2, VII, VIII, ix, X, XI, XII, XIIA, lambda;
            e2 = (a * a - b * b) / (a * a);
            phip = (nn - No) / (a * Fo) + phio;
            n = (a - b) / (a + b);
        line10:
            phi = phip;
            m = 0;
            m = m + b * Fo * ((1 + n + 5 * n * n / 4 + 5 * n * n * n / 4) * (phi - phio));
            m = m - b * Fo * ((3 * n + 3 * n * n + 21 * n * n * n / 8) * Math.Sin(phi - phio) * Math.Cos(phi + phio));
            m = m + b * Fo * ((15 * n * n / 8 + 15 * n * n * n / 8) * Math.Sin(2 * (phi - phio)) * Math.Cos(2 * (phi + phio)));
            m = m - b * Fo * (35 * n * n * n / 24 * Math.Sin(3 * (phi - phio)) * Math.Cos(3 * (phi + phio)));
            Diff = nn - No - m;
            tol = 0.00001;
            if (gridRef == GridRef.OSNI2)
            {
                tol = 0.000001;
            }
            if (Math.Abs(Diff) > tol)
            {
                phip = (nn - No - m) / (a * Fo) + phip;
                goto line10;
            }
            rho = a * Fo * (1 - e2) * Math.Pow((1 - e2 * Math.Sin(phi) * Math.Sin(phi)), (-1.5));
            v = a * Fo * Math.Pow((1 - e2 * Math.Sin(phi) * Math.Sin(phi)), (-0.5));
            eta2 = v / rho - 1;
            VII = Math.Tan(phip) / (2 * rho * v);
            VIII = Math.Tan(phip) / (24 * rho * v * v * v) * (5 + 3 * Math.Tan(phip) * Math.Tan(phip) + eta2 - 9 * Math.Tan(phip) * Math.Tan(phip) * eta2);
            ix = Math.Tan(phip) / (720 * rho * v * v * v * v * v) * (61 + 90 * Math.Tan(phip) * Math.Tan(phip) + 45 * Math.Tan(phip) * Math.Tan(phip) * Math.Tan(phip) * Math.Tan(phip));
            X = 1 / v / Math.Cos(phip);
            XI = (1 / (Math.Cos(phip) * 6 * v * v * v)) * (v / rho + 2 * Math.Tan(phip) * Math.Tan(phip));
            XII = (1 / (Math.Cos(phip) * 120 * v * v * v * v * v)) * (5 + 28 * Math.Tan(phip) * Math.Tan(phip) + 24 * Math.Tan(phip) * Math.Tan(phip) * Math.Tan(phip) * Math.Tan(phip));
            XIIA = (1 / (Math.Cos(phip) * 5040 * Math.Pow(v, 7))) * (61 + 662 * Math.Pow((Math.Tan(phip)), 2) + 1320 * Math.Pow((Math.Tan(phip)), 4) + 720 * Math.Pow((Math.Tan(phip)), 6));
            phi = phip - VII * (ee - Eo) * (ee - Eo) + VIII * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) - ix * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo);
            lambda = lambdao + X * (ee - Eo) - XI * (ee - Eo) * (ee - Eo) * (ee - Eo) + XII * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) - XIIA * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo) * (ee - Eo);
            phi = phi / degtorad;
            lambda = lambda / degtorad;

            retLatLon[0] = phi;
            retLatLon[1] = lambda;

            int ino = 0;
            if (gridRef == GridRef.OSNI)
            {
                ino = 1;
            }
            /*else if (gridRef == GridRef.OSNI1)
            {
                ino = 1;
            }*/
            if (ino == 0)
            {

                double phi1, lamda1;
                phi1 = 0;
                lamda1 = 0;
                retLatLon = GetNewLatLon(phi, lambda, phi1, lamda1, gridRef);

            }
            return retLatLon;
        }

        private static double[] GetNewLatLon(double phi, double lamda, double phi1, double lamda1, GridRef gridRef)
        {
            double NGa, Ngb, WGS84a, WGS84b, Pi;
            double NGphiOrigin, NGlamdaOrigin;

            NGa = 6377563.396;
            Ngb = 6356256.91;
            if (gridRef == GridRef.OSNI)
            {
                NGa = 6377340.189;
                Ngb = 6356034.447;
            }
            else if (gridRef == GridRef.OSNI2)
            {
                NGa = 6377340.189;
                Ngb = 6356034.447;
            }

            WGS84a = 6378137;
            WGS84b = 6356752.3141;
            Pi = Math.Atan(1) * 4;

            phi = phi * Pi / 180;
            lamda = lamda * Pi / 180;
            NGphiOrigin = (49 * Pi / 180);
            NGlamdaOrigin = (-2 * Pi / 180);
            if (gridRef == GridRef.OSNI)
            {
                NGphiOrigin = (53.5 * Pi / 180);
                NGlamdaOrigin = (-8 * Pi / 180);
            }
            else if (gridRef == GridRef.OSNI2)
            {
                NGphiOrigin = (53.5 * Pi / 180);
                NGlamdaOrigin = (-8 * Pi / 180);
            }
            double scalechange, rx, ry, rz, r0c0, r0c1, r0c2, r1c0, r1c1, r1c2, r2c0, r2c1, r2c2;

            scalechange = (20.4894 / 1000000);
            rz = (-0.8421);
            ry = (-0.247);
            rx = (-0.1502);

            if (gridRef == GridRef.OSNI2)
            {
                scalechange = -0.00000815;
                rx = -1.042;
                ry = -0.214;
                rz = -0.631;
            }


            r0c0 = (1 + scalechange);
            r0c1 = (-1 * rz * 0.000004848136811);
            r0c2 = (ry * 0.000004848136811);
            r1c0 = (rz * 0.000004848136811);
            r1c1 = r0c0;
            r1c2 = (-1 * rx * 0.000004848136811);

            r2c0 = (-1 * ry * 0.000004848136811);
            r2c1 = (rx * 0.000004848136811);
            r2c2 = r0c0;

            double hh, m_eSquared;
            double inCoOrdX, inCoOrdY, inCoOrdZ, v, nv, mnv, tx, ty, tz, s1, prod1, prod2, prod3, v1;
            //' osgb36 to wgs84
            //'// Reverse Helmert Transformation
            //'   // lat long height
            //'
            //'  // This lat long to 3-d uses OSGB m_eSquared, NGa
            hh = 24.7;
            if (gridRef == GridRef.OSNI2)
            {
                hh = 0.0;
            }


            m_eSquared = (NGa * NGa - Ngb * Ngb) / (NGa * NGa);
            v = NGa / Math.Pow((1 - m_eSquared * ((Math.Sin(phi) * Math.Sin(phi)))), 0.5);
            inCoOrdX = (v + hh) * Math.Cos(phi) * Math.Cos(lamda);
            inCoOrdY = (v + hh) * Math.Cos(phi) * Math.Sin(lamda);
            inCoOrdZ = ((1 - m_eSquared) * v + hh) * Math.Sin(phi);

            //' // All parameter signs reversed.
            nv = 0.000004848136811;

            nv = Pi / 3600 / 180;
            mnv = -nv;
            tx = 446.448;
            ty = -125.157;
            tz = 542.06;
            if (gridRef == GridRef.OSNI2)
            {
                tx = 482.530;
                ty = -130.596;
                tz = 564.557;
                scalechange = -0.00000815;

            }
            s1 = 1 - scalechange;
            prod1 = tx + s1 * inCoOrdX + (rz * nv) * inCoOrdY + (ry * mnv) * inCoOrdZ;

            prod2 = ty + (rz * mnv) * inCoOrdX + (1 - scalechange) * inCoOrdY + (rx * nv) * inCoOrdZ;

            prod3 = tz + (ry * nv) * inCoOrdX + (rx * mnv) * inCoOrdY + (1 - scalechange) * inCoOrdZ;

            double s, m_WGS84eSquared, precision, p, lat1;
            s = -20.4894;
            rz = (0.8421);
            ry = (0.247);
            rx = (0.1502);
            if (gridRef == GridRef.OSNI2)
            {
                s = 8.15;
                rx = -1.042;
                ry = -0.214;
                rz = -0.631;

            }
            s1 = 1 + s / 1000000;
            rx = rx * nv;
            ry = ry * nv;
            rz = rz * nv;

            prod1 = tx + s1 * inCoOrdX - rz * inCoOrdY + ry * inCoOrdZ;
            prod2 = ty + rz * inCoOrdX + s1 * inCoOrdY - rx * inCoOrdZ;
            prod3 = tz - ry * inCoOrdX + rx * inCoOrdY + s1 * inCoOrdZ;
            double lat2, v2, lambda;

            //'    // This 3-d to lat long uses WGS84 m_eSquaredIN, NGaIN
            m_WGS84eSquared = (WGS84a * WGS84a - WGS84b * WGS84b) / (WGS84a * WGS84a);
            precision = 4 / WGS84a;
            p = Math.Sqrt(prod1 * prod1 + prod2 * prod2);

            lat1 = Math.Atan(prod3 / (p * (1 - m_WGS84eSquared)));

            v1 = WGS84a / Math.Pow((1 - m_WGS84eSquared * Math.Pow((Math.Sin(lat1)), 2)), 0.5);
            lat2 = Math.Atan((prod3 + m_WGS84eSquared * v1 * Math.Sin(lat1)) / p);
            phi = lat2;

            v2 = WGS84a / Math.Pow((1 - m_WGS84eSquared * (Math.Sin(lat2)) * (Math.Sin(lat2))), 0.5);
            lat2 = Math.Atan((prod3 + m_WGS84eSquared * v2 * Math.Sin(lat2)) / p);
            phi = lat2;

            v2 = WGS84a / Math.Pow((1 - m_WGS84eSquared * (Math.Sin(lat2)) * (Math.Sin(lat2))), 0.5);
            lat2 = Math.Atan((prod3 + m_WGS84eSquared * v2 * Math.Sin(lat2)) / p);
            phi = lat2;

            //'v2 = WGS84a / (1# - m_WGS84eSquared * (Sin(phi)) ^ 2) ^ 0.5 //' // we iterate once more.
            //'phi = Atn((prod3 + m_WGS84eSquared * v2 * Sin(phi)) / p)

            lambda = Math.Atan(prod2 / prod1);

            phi = phi * 180 / Pi;
            lambda = lambda * 180 / Pi;
            lamda1 = lambda;
            phi1 = phi;
            double[] retLatLon = new double[2];
            retLatLon[0] = phi1;
            retLatLon[1] = lamda1;
            return retLatLon;
        }

        #endregion

        #region Lat Lon To EN

        class OSConversion
        {
            int starting;
            double oseasting0;
            double osnorthing0;
            int iusenavteq = 0;
            int not_osgb = 0;
            double NewEasting;
            double NewNorthing;
            string GridRef;
            double NewLat;
            double NewLon;
            double rad;
            double eccsq, ecc2sq, PA, PB, PC;

            //'WGS ellipsoid constants
            double molod_WGS84_a = 6378137;
            double molod_WGS84_f;
            double TMscale = 0.9996012717;
            double majoraxis = 6377563.396;
            double minoraxis = 6356256.91;

            //'Molodensky constants
            double molod_da = 573.604;
            double molod_df = 0.000011960023;
            double molod_dx = 375;
            double molod_dy = -111;
            double molod_dz = 431;

            public double[] latlongtoen(double llat3, double llon3)
            {
                if (iusenavteq == 1 | not_osgb == 1)
                {
                    if (GridRef != "OSNI")
                    {
                        if (GridRef != "OSNI2")
                        {
                            NewEasting = llon3;
                            NewNorthing = llat3;
                            return new double[] { NewEasting, NewNorthing };
                        }
                    }
                }
                double i, ii, iii, iiia;
                double Pi, phi, lam, e0, n0, afo, bfo, phi0;
                double lam0, e_squared, n, nu, rho, eta2, p, m, iv;
                double v, vi;
                //double a, b, f0;
                //' convert 84 datum to 36 datum lat/lon
                double llat, llon;
                llat = llat3;
                llon = llon3;
                if (GridRef != "OSNI")
                {
                    if (GridRef != "OSNI2")
                    {
                        osref(llat3, llon3);
                        llat = NewLat;
                        llon = NewLon;
                    }
                }
                /*
                    //dim easting As Double, northing As Double
                    'test
                    'llat = 52.65757030139
                    'llon = 1.717921581
                    'Easting 651409.903  m
                    'Northing    313177.270  m
                */
                Pi = Math.Atan(1) * 4;
                phi = Pi * llat / 180;
                lam = Pi * llon / 180;
                //a = 6377563.396;
                //b = 6356256.91;
                //'Central Meridan Scale
                //f0 = 0.9996012717;
                //'True origin Easting,
                e0 = 400000;
                //'True origin Northing,
                n0 = -100000;
                afo = 6375020.48098897;
                bfo = 6353722.49048791;
                phi0 = 49.0 * Math.PI / 180.0;
                lam0 = -2.0 * Math.PI / 180.0;
                if (GridRef == "OSNI")
                {
                    e0 = 200000;
                    n0 = 250000;
                    afo = 6377340.189;
                    bfo = 6356034.447;
                    phi0 = 53.5 * Math.PI / 180.0;
                    lam0 = -8.0 * Math.PI / 180.0;
                }
                double afo1 = afo;
                double af = afo;
                if (GridRef == "OSNI2")
                {
                    e0 = 200000;
                    n0 = 250000;
                    afo = 6377340.189;
                    bfo = 6356034.447;
                    phi0 = 53.5 * Math.PI / 180.0;
                    lam0 = -8.0 * Math.PI / 180.0;

                    double a = 6378137.0;
                    double b = 6356752.313;
                    double Fo = 1.000035;
                    //phio = 53.50;
                    //lambdao = -8;
                    //Eo = 200000;
                    //No = 250000;
                    afo = a * Fo;
                    bfo = b * Fo;
                    af = a;
                    double H = 0;
                    Pi = 3.14159265358979;
                    double RadPHI = phi;
                    double RadLAM = lam;

                    //# Compute eccentricity squared and nu
                    double e2 = (Math.Pow(a, 2) - Math.Pow(b, 2)) / Math.Pow(a, 2);
                    double V = a / (Math.Sqrt(1 - (e2 * (Math.Pow(Math.Sin(RadPHI), 2)))));

                    //# Compute X
                    double X1 = (V + H) * (Math.Cos(RadPHI)) * (Math.Cos(RadLAM));
                    double Y1 = (V + H) * (Math.Cos(RadPHI)) * (Math.Sin(RadLAM));
                    double Z1 = ((V * (1 - e2)) + H) * (Math.Sin(RadPHI));

                    /*
                     * 
                     * 
            double x2 = Helmert_X(x1,y1,z1,-482.53 ,+0.214,+0.631,-8.15);
                  double y2 = Helmert_Y(x1,y1,z1,+130.596,+1.042,+0.631,-8.15);
                  double z2 = Helmert_Z(x1,y1,z1,-564.557,+1.042,+0.214,-8.15);

                     */
                    //#Compute transformed X coord
                    double DX = -482.53;
                    double DY = 130.596;
                    double DZ = -564.557;
                    double X_Rot = 1.042;
                    double Y_Rot = 0.214;
                    double Z_Rot = 0.631;
                    double s = -8.15;
                    double sfactor = s * 0.000001;
                    double RadX_Rot = (X_Rot / 3600) * (Pi / 180);
                    double RadY_Rot = (Y_Rot / 3600) * (Pi / 180);
                    double RadZ_Rot = (Z_Rot / 3600) * (Pi / 180);
                    double x2 = (X1 + (X1 * sfactor) - (Y1 * RadZ_Rot) + (Z1 * RadY_Rot) + DX);
                    double y2 = (X1 * RadZ_Rot) + Y1 + (Y1 * sfactor) - (Z1 * RadX_Rot) + DY;
                    double z2 = (-1.0 * X1 * RadY_Rot) + (Y1 * RadX_Rot) + Z1 + (Z1 * sfactor) + DZ;
                    double lllat = XYZ_to_Lat(x2, y2, z2, 6377340.189, 6356034.447);
                    double lllon = XYZ_to_Long(x2, y2);

                    NewEasting = Lat_Long_to_East(lllat, lllon, 6377340.189, 6356034.447, 200000, 1.000035, 53.50000, -8.00000);
                    NewNorthing = Lat_Long_to_North(lllat, lllon, 6377340.189, 6356034.447, 200000, 250000, 1.000035, 53.50000, -8.00000);

                    return new double[] { NewEasting, NewNorthing };
                }

                e_squared = (afo * afo - bfo * bfo) / (afo * afo);
                n = (afo - bfo) / (afo + bfo);
                nu = af / (Math.Pow(1 - (e_squared * (Math.Sin(phi)) * (Math.Sin(phi))), 0.5));
                rho = (nu * (1 - e_squared)) / (1 - (e_squared * (Math.Sin(phi)) * (Math.Sin(phi))));
                eta2 = (nu / rho) - 1;
                p = lam - lam0;
                m = Marc(bfo, n, phi0, phi);
                i = m + n0;
                ii = (nu / 2) * (Math.Sin(phi)) * (Math.Cos(phi));
                iii = ((nu / 24) * (Math.Sin(phi)) * (Math.Pow(Math.Cos(phi), 3))) * (5 - Math.Pow((Math.Tan(phi)), 2) + (9 * eta2));
                iiia = ((nu / 720) * (Math.Sin(phi)) * Math.Pow((Math.Cos(phi)), 5)) * (61 - (58 * Math.Pow((Math.Tan(phi)), 2)) + Math.Pow((Math.Tan(phi)), 4));
                iv = nu * (Math.Cos(phi));
                v = (nu / 6) * Math.Pow((Math.Cos(phi)), 3) * ((nu / rho) - Math.Pow((Math.Tan(phi)), 2));
                vi = (nu / 120) * Math.Pow((Math.Cos(phi)), 5) * (5 - (18 * Math.Pow((Math.Tan(phi)), 2)) + Math.Pow((Math.Tan(phi)), 4) + (14 * eta2) - (58 * Math.Pow((Math.Tan(phi)), 2) * eta2));
                double easting, northing;
                easting = e0 + (p * iv) + ((p * p * p) * v) + ((p * p * p * p * p) * vi);
                northing = i + ((p * p) * ii) + ((p * p * p * p) * iii) + ((p * p * p * p * p * p) * iiia);

                return new double[] { easting, northing };
            }

            private void osref(double llat, double llon)
            {
                setup_os_param();
                Molodensky2(llat, llon);
                llat = NewLat;
                llon = NewLon;
                LLtoCOORDS(llat, llon);
                //    OSeasting = NewEasting;
                //    OSnorthting = NewNorthing;
            }

            private void setup_os_param()
            {
                //'run first time any published routine is called.
                //'set up for 1st two letters of grid ref
                if (starting == 0)
                {
                    //'define origin
                    double llat0, llon0, majsq, minsq, ecc4, ecc6;
                    llon0 = -2;
                    llat0 = 49;
                    if (GridRef == "OSNI")
                    {
                        llon0 = -8;
                        llat0 = 53.5;
                    }
                    if (GridRef == "OSNI2")
                    {
                        llon0 = -8;
                        llat0 = 53.5;
                    }
                    //'rad = Atn(1) / 45
                    rad = 3.14159265358979 / 180;
                    majsq = majoraxis * majoraxis;
                    minsq = minoraxis * minoraxis;
                    eccsq = (majsq - minsq) / majsq;//  'eccentricity ^ 2
                    ecc4 = eccsq * eccsq;
                    ecc6 = eccsq * ecc4;
                    ecc2sq = (majsq - minsq) / minsq; //'second eccentricity ^2
                    //'meridinal distance parameters
                    PA = 1 + 0.75 * eccsq + 0.703125 * ecc4 + 0.068359375 * ecc6;
                    PB = 0.75 * eccsq + 0.9375 * ecc4 + 1.025390625 * ecc6;
                    PC = 0.234375 * ecc4 + 0.41015625 * ecc6;
                    LLtoCOORDS(llat0, llon0);
                    //OSeasting = NewEasting;
                    //OSnorthting = NewNorthing;
                    oseasting0 = -400000;
                    osnorthing0 = osnorthing0 + 100000;
                    molod_WGS84_f = 1 / 298.257223563;
                    starting = 1;
                }
            }
            private void Molodensky2(double llat, double llon)
            {
                double to84, da, df, dx, dy, dz, my_a, my_f;
                to84 = -1;
                //'converts longitudearray!(index), latitudearray!(index) between OS1936 and WGS84
                //'NB to84% either +1 or -1
                da = molod_da * to84;
                df = molod_df * to84;
                dx = molod_dx * to84;
                dy = molod_dy * to84;
                dz = molod_dz * to84;
                double my_es, my_lat, my_lon, slat, slon, clat, clon, Rn, Rm;
                my_a = molod_WGS84_a - da;
                my_f = molod_WGS84_f - df;
                my_es = 2 * my_f - my_f * my_f;
                my_lat = llat * rad;
                my_lon = llon * rad;

                slat = Math.Sin(my_lat);
                slon = Math.Sin(my_lon);
                clat = Math.Cos(my_lat);
                clon = Math.Cos(my_lon);

                Rn = my_a / Math.Sqrt(1 - (my_es * slat * slat));
                Rm = my_a * (1 - my_es) / (Math.Pow(1 - my_es * slat * slat, 1.5));
                double d1, d2, d3, dlat, dlon;
                d1 = ((-dx * slat * clon - dy * slat * slon) + dz * clat);
                d2 = da * (Rn * my_es * slat * clat) / my_a;
                d3 = df * (Rm / (1 - my_f) + Rn * (1 - my_f)) * slat * clat;

                dlat = (d1 + d2 + d3) / Rm;
                llat = (my_lat + dlat) / rad;
                dlon = (-dx * slon + dy * clon) / (Rn * clat);
                llon = (my_lon + dlon) / rad;
                //return values
                NewLat = llat;
                NewLon = llon;
            }

            private void LLtoCOORDS(double llat, double llon)
            {
                double llat0, llon0, longi, lati, t, tsq, l, Lsq, etasq, nu;
                double OSeasting, OSnorthing;
                int IFLAG;
                llon0 = -2;
                llat0 = 49;
                if (GridRef == "OSNI")
                {
                    llon0 = -8;
                    llat0 = 53.5;
                }
                if (GridRef == "OSNI2")
                {
                    llon0 = -8;
                    llat0 = 53.5;
                }
                longi = rad * (llon - llon0);
                lati = rad * llat;
                t = Math.Tan(lati);
                tsq = t * t;
                l = longi * Math.Cos(lati);
                Lsq = l * l;
                etasq = Math.Cos(lati);
                etasq = etasq * etasq * ecc2sq;
                nu = Math.Sin(lati);
                nu = Math.Sqrt(1 - eccsq * nu * nu);
                nu = majoraxis / nu;
                //'compute easting
                double A1, A3, A5, A7, a2, A4, A6;
                A7 = (61 - tsq * (479 - 179 * tsq + tsq * tsq)) / 5040;
                A5 = (5 - tsq * (18 - tsq) + etasq * (14 - 58 * tsq)) / 120;
                A3 = (1 - tsq + etasq) / 6;
                A1 = TMscale * nu;
                OSeasting = A1 * l * (1 + Lsq * (A3 + Lsq * (A5 + A7 * Lsq)));
                //'OSeasting&(index) = OSeasting&(index)
                //'now do northings
                double m;
                m = majoraxis * (1 - eccsq) * (PA * lati - 0.5 * PB * Math.Sin(2 * lati) + 0.25 * PC * Math.Sin(4 * lati));
                a2 = 0.5 * A1 * t;
                A4 = (5 - tsq + etasq * (9 + 4 * etasq)) / 12;
                A6 = (61 - tsq * (58 - tsq) + etasq * (270 - 330 * tsq)) / 360;
                OSnorthing = TMscale * m + a2 * Lsq * (1 + Lsq * (A4 + A6 * Lsq));
                IFLAG = 0;
                if (llat != llat0)
                {
                    if (llon != llon0)
                    {
                        OSnorthing = OSnorthing - osnorthing0;
                        OSeasting = OSeasting - oseasting0;
                        IFLAG = 1;
                    }
                }
                if (IFLAG == 0)
                {
                    osnorthing0 = OSnorthing;
                    oseasting0 = OSeasting;
                }
                NewEasting = OSeasting;
                NewNorthing = OSnorthing;
            }
            private double Lat_Long_to_East(double PHI, double LAM, double a, double b, double e0, double f0, double PHI0, double LAM0)
            {
                /*
                 #Project Latitude and longitude to Transverse Mercator eastings.
                #Input: - _
                #    Latitude (PHI) and Longitude (LAM) in decimal degrees; _
                #    ellipsoid axis dimensions (a & b) in meters; _
                #    eastings of false origin (e0) in meters; _
                #    central meridian scale factor (f0); _
                # latitude (PHI0) and longitude (LAM0) of false origin in decimal degrees.

                # Convert angle measures to radians
                */
                double Pi = 3.14159265358979;
                double RadPHI = PHI * (Pi / 180);
                double RadLAM = LAM * (Pi / 180);
                double RadPHI0 = PHI0 * (Pi / 180);
                double RadLAM0 = LAM0 * (Pi / 180);

                double af0 = a * f0;
                double bf0 = b * f0;
                double e2 = (pow(af0, 2) - pow(bf0, 2)) / pow(af0, 2);
                double n = (af0 - bf0) / (af0 + bf0);
                double nu = af0 / (sqrt(1 - (e2 * pow(sin(RadPHI), 2))));
                double rho = (nu * (1 - e2)) / (1 - (e2 * pow(sin(RadPHI), 2)));
                double eta2 = (nu / rho) - 1;
                double p = RadLAM - RadLAM0;

                double IV = nu * (cos(RadPHI));
                double V = (nu / 6) * (pow(cos(RadPHI), 3)) * ((nu / rho) - (pow(tan(RadPHI), 2)));
                double VI = (nu / 120) * (pow(cos(RadPHI), 5)) * (5 - (18 * (pow(tan(RadPHI), 2))) + (pow(tan(RadPHI), 4)) + (14 * eta2) - (58 * (pow(tan(RadPHI), 2)) * eta2));

                return e0 + (p * IV) + (pow(p, 3) * V) + (pow(p, 5) * VI);
            }
            private double cos(double val)
            {
                return Math.Cos(val);
            }
            private double sin(double val)
            {
                return Math.Sin(val);
            }
            private double abs(double val)
            {
                return Math.Abs(val);
            }
            private double sqrt(double val)
            {
                return Math.Sqrt(val);
            }
            private double tan(double val)
            {
                return Math.Tan(val);
            }
            private double pow(double val1, double val2)
            {
                return Math.Pow(val1, val2);
            }
            private double atan2(double val1, double val2)
            {
                return Math.Atan2(val1, val2);
            }


            private double Lat_Long_to_North(double PHI, double LAM, double a, double b, double e0, double n0, double f0, double PHI0, double LAM0)
            {
                /*
                # Project Latitude and longitude to Transverse Mercator northings
                # Input: - _
                # Latitude (PHI) and Longitude (LAM) in decimal degrees; _
                # ellipsoid axis dimensions (a & b) in meters; _
                # eastings (e0) and northings (n0) of false origin in meters; _
                # central meridian scale factor (f0); _
                # latitude (PHI0) and longitude (LAM0) of false origin in decimal degrees.

                # REQUIRES THE "Marc" FUNCTION

                # Convert angle measures to radians
                */
                double Pi = 3.14159265358979;
                double RadPHI = PHI * (Pi / 180);
                double RadLAM = LAM * (Pi / 180);
                double RadPHI0 = PHI0 * (Pi / 180);
                double RadLAM0 = LAM0 * (Pi / 180);

                double af0 = a * f0;
                double bf0 = b * f0;
                double e2 = (pow(af0, 2) - pow(bf0, 2)) / pow(af0, 2);
                double n = (af0 - bf0) / (af0 + bf0);
                double nu = af0 / (sqrt(1 - (e2 * pow(sin(RadPHI), 2))));
                double rho = (nu * (1 - e2)) / (1 - (e2 * pow(sin(RadPHI), 2)));
                double eta2 = (nu / rho) - 1;
                double p = RadLAM - RadLAM0;
                double M = Marc(bf0, n, RadPHI0, RadPHI);

                double I = M + n0;
                double II = (nu / 2) * (sin(RadPHI)) * (cos(RadPHI));
                double III = ((nu / 24) * (sin(RadPHI)) * (pow(cos(RadPHI), 3))) * (5 - (pow(tan(RadPHI), 2)) + (9 * eta2));
                double IIIA = ((nu / 720) * (sin(RadPHI)) * (pow(cos(RadPHI), 5))) * (61 - (58 * (pow(tan(RadPHI), 2))) + (pow(tan(RadPHI), 4)));

                return I + (pow(p, 2) * II) + (pow(p, 4) * III) + (pow(p, 6) * IIIA);
            }

            private double Marc(double bf0, double n, double phi0, double phi)
            {
                //'Compute meridional arc.
                //'Input: - _
                // ellipsoid semi major axis multiplied by central meridian scale factor (bf0) in meters; _//
                // n (computed from a, b and f0); _
                // lat of false origin (PHI0) and initial or final latitude of point (PHI) IN RADIANS.
                /*
                'THIS FUNCTION IS CALLED BY THE - _
                 "Lat_Long_to_North" and "InitialLat" FUNCTIONS
                'THIS FUNCTION IS ALSO USED ON IT'S OWN IN THE "Projection and Transformation Calculations.xls" SPREADSHEET
                 */
                //    line1:
                double Marc1, Marc2, Marc3, Marc4;
                Marc1 = ((1.0 + n + ((5.0 / 4.0) * (Math.Pow(n, 2))) + ((5.0 / 4.0) * (Math.Pow(n, 3)))) * (phi - phi0));
                Marc2 = -(((3 * n) + (3 * (Math.Pow(n, 2))) + ((21.0 / 8.0) * (Math.Pow(n, 3)))) * (Math.Sin(phi - phi0)) * (Math.Cos(phi + phi0)));
                Marc3 = +((((15.0 / 8.0) * (Math.Pow(n, 2))) + ((15.0 / 8.0) * (Math.Pow(n, 3)))) * (Math.Sin(2 * (phi - phi0))) * (Math.Cos(2 * (phi + phi0))));
                Marc4 = -(((35.0 / 24.0) * (n * n * n)) * (Math.Sin(3 * (phi - phi0))) * (Math.Cos(3 * (phi + phi0))));
                Marc1 = bf0 * (Marc1 + Marc2 + Marc3 + Marc4);

                return Marc1;
            }

            private double XYZ_to_Lat(double X, double Y, double Z, double a, double b)
            {
                /*
                # Convert XYZ to Latitude (PHI) in Dec Degrees.
                # Input: - _
                # XYZ cartesian coords (X,Y,Z) and ellipsoid axis dimensions (a & b), all in meters.

                # THIS FUNCTION REQUIRES THE "Iterate_XYZ_to_Lat" FUNCTION
                # THIS FUNCTION IS CALLED BY THE "XYZ_to_H" FUNCTION
                */
                double RootXYSqr = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
                double e2 = (Math.Pow(a, 2) - Math.Pow(b, 2)) / Math.Pow(a, 2);
                double PHI1 = Math.Atan2(Z, (RootXYSqr * (1 - e2)));

                double PHI = Iterate_XYZ_to_Lat(a, e2, PHI1, Z, RootXYSqr);

                double Pi = 3.14159265358979;

                return PHI * (180 / Pi);
            }
            private double XYZ_to_Long(double X, double Y)
            {
                /*
                # Convert XYZ to Longitude (LAM) in Dec Degrees.
                # Input: - _
                # X and Y cartesian coords in meters.
                */
                double Pi = 3.14159265358979;
                return Math.Atan2(Y, X) * (180 / Pi);
            }

            private double Iterate_XYZ_to_Lat(double a, double e2, double PHI1, double Z, double RootXYSqr)
            {
                /*
                # Iteratively computes Latitude (PHI).
                # Input: - _
                #    ellipsoid semi major axis (a) in meters; _
                #    eta squared (e2); _
                #    estimated value for latitude (PHI1) in radians; _
                #    cartesian Z coordinate (Z) in meters; _
                # RootXYSqr computed from X & Y in meters.

                # THIS FUNCTION IS CALLED BY THE "XYZ_to_PHI" FUNCTION
                # THIS FUNCTION IS ALSO USED ON IT'S OWN IN THE _
                # "Projection and Transformation Calculations.xls" SPREADSHEET
                */

                double V = a / (Math.Sqrt(1 - (e2 * Math.Pow(Math.Sin(PHI1), 2))));
                double PHI2 = Math.Atan2((Z + (e2 * V * (Math.Sin(PHI1)))), RootXYSqr);

                while (Math.Abs(PHI1 - PHI2) > 0.000000001)
                {
                    PHI1 = PHI2;
                    V = a / (Math.Sqrt(1 - (e2 * Math.Pow(Math.Sin(PHI1), 2))));
                    PHI2 = Math.Atan2((Z + (e2 * V * (Math.Sin(PHI1)))), RootXYSqr);
                }

                return PHI2;
            }
        }

        #endregion
    }
}
