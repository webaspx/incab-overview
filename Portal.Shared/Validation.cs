﻿#define Azure             //comment out all 3 for Azure
//#define VeoliaLocal       //uncomment for Veolia PreProd
//#define VeoliaLive        //uncomment for Veolia Prod
//#define HaltonLive        //uncomment for Halton Live
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
//using System.Collections.Generic;
using System.Collections;
//using System.Linq;


/// <summary>
/// Summary description for Validation
/// </summary>
public class Validation
{
#if VeoliaLocal
    static string Dpassword = "WMCollections";
#endif

#if VeoliaLive
    static string Dpassword = "WMCollections";
#endif

#if Azure
    static string Dpassword = "Web08#580";
#endif

#if HaltonLive
    static string Dpassword = "WMC011ect_U53R";
#endif
    public static string councilname = "";
    static string userName = "";
    static string dataSource = "";
    static string sampleDatabaseName = "";
    static string[,] datalist = new string[1001, 400];
    //static int idatarow = 0;//KP edit out 02Aug19
    //static int idatacol = 0;//KP edit out 02Aug19

    public static Boolean validatedomain(string emailaddress)
    {
        ClientCodeInfo clnt = ClientCodeInfo.GetParameters("", "", "", "", "", emailaddress.ToLower());
        if (clnt.ClientName != "") 
        {
            return true;
        }
        return false;
        #region oldCode KP replaced with ClientCodeInfo to cut down on new clent set up time 26Jul2021
        /*

        emailaddress = emailaddress.ToLower();
        if (emailaddress.IndexOf("armagh.gov.uk")                   >= 0) { return true; }
        if (emailaddress.IndexOf("banbridge.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("craigavon.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("armaghbanbridgecraigavon.gov.uk") >= 0) { return true; }
        if (emailaddress.IndexOf("ardsandnorthdown.gov.uk")         >= 0) { return true; }
        if (emailaddress.IndexOf("abc.gov.uk")                      >= 0) { return true; }
        if (emailaddress.IndexOf("blaenaugwent.gov.uk")             >= 0) { return true; }
        if (emailaddress.IndexOf("blaenau-gwent.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("bournemouth.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("bromsgrove.gov.uk")               >= 0) { return true; }
        //if (emailaddress.IndexOf("cambridge.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("carlisle.gov.uk")                 >= 0) { return true; }
        if (emailaddress.IndexOf("carmarthenshire.gov.uk")          >= 0) { return true; }
        if (emailaddress.IndexOf("causewaycoast.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("conwy.gov.uk")                    >= 0) { return true; }
        if (emailaddress.IndexOf("copeland.gov.uk")                 >= 0) { return true; }
        if (emailaddress.IndexOf("copeland.com")                    >= 0) { return true; }
        if (emailaddress.IndexOf("dorsetcc.gov.uk")                 >= 0) { return true; }
        if (emailaddress.IndexOf("dorsetwastepartnership.gov.uk")   >= 0) { return true; }
        if (emailaddress.IndexOf("dumfriesandgalloway.gov.uk")      >= 0) { return true; }
        if (emailaddress.IndexOf("eastayrshire.gov.uk")             >= 0) { return true; }
        if (emailaddress.IndexOf("east-ayrshire.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("eastlothian.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("eastrenfrewshire.gov.uk")         >= 0) { return true; }
        if (emailaddress.IndexOf("edinburgh.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("falkirk.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("falkstreets.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("testfalk.gov.uk")                 >= 0) { return true; }
        if (emailaddress.IndexOf("glamorgan.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("gwynedd.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("halton.gov.uk")                   >= 0) { return true; }
        //if (emailaddress.IndexOf("haltonlocal.gov.uk")            >= 0) { return true; }//halton local edit 24Dec14
        if (emailaddress.IndexOf("hambleton.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("lagan@dorset.com")                >= 0) { return true; }
        //if (emailaddress.IndexOf("liverpool.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("middlesbrough.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("midulster.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("monmouthshire.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("monmouthshire.com")               >= 0) { return true; }
        if (emailaddress.IndexOf("northayrshire.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("oadbyandwigston.gov.uk")          >= 0) { return true; }
        if (emailaddress.IndexOf("oadby-wigston.gov.uk")            >= 0) { return true; }        
        if (emailaddress.IndexOf("pembrokeshire.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("replicarhon.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("rhondda.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("rctcbc.gov.uk")                   >= 0) { return true; }
        if (emailaddress.IndexOf("richmondshire.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("richmondshire.com")               >= 0) { return true; }
        if (emailaddress.IndexOf("rugby.gov.uk")                    >= 0) { return true; }
        if (emailaddress.IndexOf("rutland.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("ryedale.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("rwtestdemo.gov.uk")               >= 0) { return true; }
        if (emailaddress.IndexOf("testrye.gov.uk")                  >= 0) { return true; }
        //if (emailaddress.IndexOf("scambs.gov.uk")                   >= 0) { return true; }
        if (emailaddress.IndexOf("scarborough.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("scotborders.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("scottishborders.gov.uk")          >= 0) { return true; }
        if (emailaddress.IndexOf("sharedcam.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("southayrshire.gov.uk")            >= 0) { return true; }
        if (emailaddress.IndexOf("southlanarkshire.gov.uk")         >= 0) { return true; }
        if (emailaddress.IndexOf("southnorfolk.gov.uk")             >= 0) { return true; }
        if (emailaddress.IndexOf("southstaffordshire.gov.uk")       >= 0) { return true; }
        if (emailaddress.IndexOf("s-norfolk.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("stirling.gov.uk")                 >= 0) { return true; }
        if (emailaddress.IndexOf("stirling.com")                    >= 0) { return true; }
        if (emailaddress.IndexOf("swansea.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("swindon.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("swindon.com")                     >= 0) { return true; }
        if (emailaddress.IndexOf("swntest.gov.uk")                  >= 0) { return true; }
        //if (emailaddress.IndexOf("testing.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("torridge.gov.uk")                 >= 0) { return true; }
        if (emailaddress.IndexOf("testtor.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("urbaserbournemouth.gov.uk")       >= 0) { return true; }
        if (emailaddress.IndexOf("veolia.com")                      >= 0) { return true; }
        if (emailaddress.IndexOf("virginiabeach.com")               >= 0) { return true; }
        if (emailaddress.IndexOf("webaspx.com")                     >= 0) { return true; }
        if (emailaddress.IndexOf("webaspx.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("westlancs.gov.uk")                >= 0) { return true; }
        if (emailaddress.IndexOf("westlothian.gov.uk")              >= 0) { return true; }
        if (emailaddress.IndexOf("wrexham.gov.uk")                  >= 0) { return true; }
        if (emailaddress.IndexOf("york.gov.uk")                     >= 0) { return true; }
        //incab customers        
        return false;
        */
        #endregion
    }
    public static void getsource(string council, out string dataSource1, out string userName1)
    {
        //New Get URL info KP 25Jun15 Start
        //eg http://localhost:60527/WebSite1test/Default2.aspx?QueryString1=1&QueryString2=2
        //string host         = HttpContext.Current.Request.Url.Host;             //  localhost
        //string authority    = HttpContext.Current.Request.Url.Authority;        //  localhost:60527
        //string port         = HttpContext.Current.Request.Url.Port.ToString();  //  60527
        //string path         = HttpContext.Current.Request.Url.AbsolutePath;     //  /WebSite1test/Default2.aspx
        //string applicationPath = HttpContext.Current.Request.ApplicationPath;   //  /Website1test
        //string url          = HttpContext.Current.Request.Url.AbsoluteUri.ToString();      //  http://localhost:60527/WebSite1test/Default2.aspx?QueryString1=1&QueryString1=2        
        //string pathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;     //  /WebSite1test/Default2.aspx?QueryString1=1&QueryString2=2
        //string query        = HttpContext.Current.Request.Url.Query;            //  ?QueryString1=1&QueryString2=2
        //string fragment     = HttpContext.Current.Request.Url.Fragment;         //  Everything that follows a # in a URL - **Please note , this part is for the browser only and never sent to the server**                        
        //New Get URL info KP 25Jun15 End
        
        dataSource1 = "";
        userName1 = "";
        string server = "";

        ClientCodeInfo clnt = ClientCodeInfo.GetParameters("", council, "", "", "", "");
        if (clnt.ClientName != "")
        {
            councilname = clnt.ClientName;
            userName1 = clnt.ServerUserName;
            dataSource1 = clnt.Datasource;
            server = clnt.Server;            
        }
        return;
        #region oldCode KP replaced with ClientCodeInfo to cut down on new clent set up time 26Jul2021
        /*

        if (council == "ABC")                   { server = "yn19k5t3s4"; councilname = "ABC"; }//ms
        if (council == "ArdsAndNorthDown")      { server = "an20170606"; councilname = "ArdsAndNorthDown"; }//ms
        if (council == "BlaenauGwent")          { server = "bg20200528"; councilname = "BlaenauGwent"; }//ms
        if (council == "Bournemouth")           { server = "bm20160121"; councilname = "Bournemouth"; }//ms
        if (council == "Bromsgrove")            { server = "al41tc1vv6"; councilname = "Bromsgrove"; }
      //if (council == "Cambridge")             { server = "u21rfju18c"; councilname = "Cambridge"; }
        if (council == "Carlise")               { server = "cl20170906"; councilname = "Carlisle"; }//ms
      //if (council == "Conwy")                 { server = "psxm02t85i"; councilname = "Conwy"; }
        if (council == "Carlisle")              { server = "cl20170906"; councilname = "Carlisle"; }//ms
        if (council == "Carmarthenshire")       { server = "cm20160712"; councilname = "Carmarthenshire"; }//ms
        if (council == "CausewayCoast")         { server = "cc20200205"; councilname = "CausewayCoast"; }//ms
        if (council == "Copeland")              { server = "cp20160121"; councilname = "Copeland"; }//ms
        if (council == "DumfriesAndGalloway")   { server = "dg20210715"; councilname = "DumfriesAndGalloway"; }
        if (council == "DWP")                   { server = "psxm02t85i"; councilname = "DWP"; }     //New Line Sat 28June14 15:25
      //if (council == "Edinburgh")             { server = "eb20160307"; councilname = "Edinburgh"; }
        if (council == "EastAyrshire")          { server = "ea20200205"; councilname = "EastAyrshire"; }
        if (council == "EastLothian")           { server = "el20181101"; councilname = "EastLothian"; }
        if (council == "EastRenfrewshire")      { server = "er20200205"; councilname = "EastRenfrewshire"; }
        if (council == "Falkirk")               { userName1 = "mike.devine@webaspx.com@al41tc1vv6"; dataSource1 = "tcp:al41tc1vv6.database.windows.net"; councilname = "Falkirk"; return; }
        if (council == "FalkStreets")           { userName1 = "mike.devine@webaspx.com@fs03032020"; dataSource1 = "tcp:fs03032020.database.windows.net"; councilname = "FalkStreets"; return; }
        if (council == "TestFalk")              { userName1 = "mike.devine@webaspx.com@al41tc1vv6"; dataSource1 = "tcp:al41tc1vv6.database.windows.net"; councilname = "TestFalk"; return; }
        if (council == "Glamorgan")             { server = "gl20170906"; councilname = "Glamorgan"; }//ms
        //if (council == "Gwynedd")               { server = "nqlvv7huh8"; councilname = "Gwynedd"; }        
        if (council == "Halton")                { server = "gyqaecvtl4"; councilname = "Halton"; }//ms
        //if (council == "HaltonLocal")           { server = @"WMOPS\MSSQLSERVER2012"; councilname = "HaltonLocal"; }//halton local edit 24Dec14
        if (council == "Hambleton")             { server = "ha20200130"; councilname = "Hambleton"; }//ms
        if (council == "Liverpool")             { server = "lv20190399"; councilname = "Liverpool"; }//ms
        if (council == "Middlesbrough")         { server = "mb20200810"; councilname = "Middlesbrough"; }//ms
        if (council == "MidUlster")             { server = "mu20200528"; councilname = "MidUlster"; }//ms
        if (council == "Monmouthshire")         { server = "mm20160712"; councilname = "Monmouthshire"; }//ms
        if (council == "NorthAyrshire")         { server = "na20171220"; councilname = "NorthAyrshire"; }//ms
        if (council == "OadbyAndWigston")       { server = "ow20180904"; councilname = "OadbyAndWigston"; }//ms
      //if (council == "Pembbrokeshire")        { server = "pm20190327"; councilname = "Pembbrokeshire"; }//ms
        if (council == "Pembrokeshire")         { server = "pm20190327"; councilname = "Pembrokeshire"; }//ms
        if (council == "ReplicaRhon")           { server = "rhreplica20200904";councilname = "ReplicaRhon"; }
        if (council == "Rhondda")               { server = "rh20171214"; councilname = "Rhondda"; }//ms
        if (council == "Richmondshire")         { server = "rc20160712"; councilname = "Richmondshire"; }//ms
        if (council == "Rugby")                 { server = "rg20170316"; councilname = "Rugby"; }//ms
        if (council == "Rutland")               { server = "rl20160121"; councilname = "Rutland"; }//ms
        if (council == "RWTestDemo")            { server = "rw20190905"; councilname = "RWTestDemo"; }//MD
        if (council == "Ryedale")               { server = "ry20160524"; councilname = "Ryedale"; }//ms
        if (council == "TestRye")               { server = "ry20160524"; councilname = "TestRye"; }//ms
        if (council == "Scarborough")           { server = "sb20170320"; councilname = "Scarborough"; }//ms
        if (council == "ScottishBorders")       { server = "ens2meugcp"; councilname = "ScottishBorders"; }//ms
        if (council == "SharedCam")             { server = "nqlvv7huh8"; councilname = "SharedCam"; }//ms
        if (council == "SouthAyrshire")         { server = "sa20171214"; councilname = "SouthAyrshire"; }//ms
      //if (council == "SouthCambs")            { server = "u21rfju18c"; councilname = "SouthCambs"; }
        if (council == "SouthLanarkshire")      { server = "sl20171214"; councilname = "SouthLanarkshire"; }//ms
        if (council == "SouthNorfolk")          { server = "sn20190906"; councilname = "SouthNorfolk"; }//ms
        if (council == "SouthStaffordshire")    { server = "ss2021";     councilname = "SouthStaffordshire"; }
        if (council == "Stirling")              { server = "st20160712"; councilname = "Stirling"; }//ms
        if (council == "Swansea")               { server = "sw20170320"; councilname = "Swansea"; }//ms
        if (council == "Swindon")               { server = "sw20160712"; councilname = "Swindon"; }//ms
        if (council == "SwnTest")               { server = "sw20160712"; councilname = "SwnTest"; }//ms    
        if (council == "Torridge")              { server = "tr20180206"; councilname = "Torridge"; }//ms        
        if (council == "TestTor")               { server = "tr20180206"; councilname = "TestTor"; }//ms        
        if (council == "TestDWP")               { server = "h2pkj6jgdx"; councilname = "TestDWP"; }
        if (council == "UrbaserBournemouth")    { server = "ub20161121"; councilname = "UrbaserBournemouth"; }//ms
        if (council == "Veolia")                { server = "h2pkj6jgdx"; councilname = "Veolia"; }//azure Veolia                
      //if (council == "VirginiaBeach")         { server = "vvvvvvvvvv"; councilname = "VirginiaBeach"; }
        if (council == "Webaspx")               { server = "h2pkj6jgdx"; councilname = "Webaspx"; }//added councilname=Webaspx 27Nov14 KP
        if (council == "WestLancs")             { server = "wl20160121"; councilname = "WestLancs"; }//ms
      //if (council == "WestLothian")           { server = "al41tc1vv6"; councilname = "WestLothian"; }
        if (council == "Wrexham")               { server = "yx3plf20le"; councilname = "Wrexham"; }//ms
        if (council == "York")                  { server = "yk20190821"; councilname = "York"; } //ms
        if (council == "LiveDWP")               { server = "psxm02t85i"; councilname = "DWP"; }                
        //if (council.EndsWith("Local") == true)          //halton local edit 24Dec14
        //{                                               //halton local edit 24Dec14
        //    userName1 = "sa";                           //halton local edit 24Dec14
        //    dataSource1 = @"WMOPS\MSSQLSERVER2012";     //halton local edit 24Dec14
        //}                                               //halton local edit 24Dec14
        //else                                            //halton local edit 24Dec14
        {                                               //halton local edit 24Dec14
            if (server != "")
            {
                userName1 = "mike.devine@webaspx.com@" + server;
                dataSource1 = "tcp:" + server + ".database.windows.net";
            }
        }                                               //halton local edit 24Dec14
#if VeoliaLocal
                if (council == "Veolia") { server = "DCTRPSQL27"; councilname = "Veolia"; }//pre prod        
                if (council == "Veolia") { server = "DCTRPSQL27"; dataSource1 = server; councilname = "Veolia"; userName1 = "WMCollections"; }
#endif
#if VeoliaLive
                if (council == "Veolia") { server = "DCTRSQL27"; councilname = "Veolia"; }//live        
                if (council == "Veolia") { server = "DCTRSQL27"; dataSource1 = server; councilname = "Veolia"; userName1 = "WMCollections"; }
#endif
        //#if HaltonLive
        //        if (council == "Halton") { server = "SVMSQL2012A"; councilname = "Halton"; dataSource1 = server; userName1 = "WMCollections"; }
        //#endif        
        return;
        */
        #endregion
    }
    //======================================================================================================================================================
    public static void getdomain(string emailaddress, out string userName1, out string dataSource1)
    {
        userName1 = "";
        dataSource1 = "";
        emailaddress = emailaddress.Trim().ToLower();
        ClientCodeInfo clnt = ClientCodeInfo.GetParameters("", "", "", "", "", emailaddress);
        if (clnt.ClientName != "") 
        {
            //userName1 = clnt.ServerUserName;
            //dataSource1 = clnt.Datasource;
            getsource(clnt.ClientName, out dataSource1, out userName1);
            return;
        }
        
        #region oldCode KP replaced with ClientCodeInfo to cut down on new clent set up time 26Jul2021
        /*

        if (emailaddress.IndexOf("abc.gov.uk")                      >= 0) { getsource("ABC", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("ardsandnorthdown.gov.uk")         >= 0) { getsource("ArdsAndNorthDown", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("armaghbanbridgecraigavon.gov.uk") >= 0) { getsource("ABC", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("armagh.gov.uk")                   >= 0) { getsource("ABC", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("banbridge.gov.uk")                >= 0) { getsource("ABC", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("urbaserbournemouth.gov.uk")       >= 0) { getsource("UrbaserBournemouth", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("blaenaugwent.gov.uk")             >= 0) { getsource("BlaenauGwent", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("blaenau-gwent.gov.uk")            >= 0) { getsource("BlaenauGwent", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("bournemouth.gov.uk")              >= 0) { getsource("Bournemouth", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("bromsgrove.gov.uk")               >= 0) { getsource("Bromsgrove", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("carmarthenshire.gov.uk")          >= 0) { getsource("Carmarthenshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("craigavon.gov.uk")                >= 0) { getsource("ABC", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("cambridge.gov.uk")                >= 0) { getsource("Cambridge", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("carlisle.gov.uk")                 >= 0) { getsource("Carlisle", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("causewaycoast.gov.uk")            >= 0) { getsource("CausewayCoast", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("conwy.gov.uk")                    >= 0) { getsource("Conwy", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("copeland.gov.uk")                 >= 0) { getsource("Copeland", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("dorsetcc.gov.uk")                 >= 0) { getsource("DWP", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("dorsetwastepartnership.gov.uk")   >= 0) { getsource("DWP", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("dumfriesandgalloway.gov.uk")      >= 0) { getsource("DumfriesAndGalloway", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("eastayrshire.gov.uk")             >= 0) { getsource("EastAyrshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("east-ayrshire.gov.uk")            >= 0) { getsource("EastAyrshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("eastlothian.gov.uk")              >= 0) { getsource("EastLothian", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("eastrenfrewshire.gov.uk")         >= 0) { getsource("EastRenfrewshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("edinburgh.gov.uk")                >= 0) { getsource("Edinburgh", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("falkirk.gov.uk")                  >= 0) { getsource("Falkirk", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("falkstreets.gov.uk")              >= 0) { getsource("FalkStreets", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("testfalk.gov.uk")                 >= 0) { getsource("TestFalk", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("glamorgan.gov.uk")                >= 0) { getsource("Glamorgan", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("gwynedd.gov.uk")                  >= 0) { getsource("Gwynedd", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("halton.gov.uk")                   >= 0) { getsource("Halton", out dataSource1, out userName1); return; }
        //if (emailaddress.IndexOf("haltonlocal.gov.uk")              >= 0) { getsource("HaltonLocal"     , out dataSource1, out userName1);  return;}
        if (emailaddress.IndexOf("hambleton.gov.uk")                >= 0) { getsource("Hambleton", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("lagan@dorset.com")                >= 0) { getsource("DWP", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("lagan.user@dorsetcc.gov.uk")      >= 0) { getsource("DWP", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("liverpool.gov.uk")                >= 0) { getsource("Liverpool", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("middlesbrough.gov.uk")            >= 0) { getsource("Middlesbrough", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("midulster.gov.uk")                >= 0) { getsource("MidUlster", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("monmouthshire.gov.uk")            >= 0) { getsource("Monmouthshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("northayrshire.gov.uk")            >= 0) { getsource("NorthAyrshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("oadbyandwigston.gov.uk")          >= 0) { getsource("OadbyAndWigston", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("oadby-wigston.gov.uk")            >= 0) { getsource("OadbyAndWigston", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("pembbrokeshire.gov.uk")           >= 0) { getsource("Pembbrokeshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("pembrokeshire.gov.uk")            >= 0) { getsource("Pembrokeshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("replicarhon.gov.uk")              >= 0) { getsource("ReplicaRhon", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("rhondda.gov.uk")                  >= 0) { getsource("Rhondda", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("rctcbc.gov.uk")                   >= 0) { getsource("Rhondda", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("richmondshire.gov.uk")            >= 0) { getsource("Richmondshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("rugby.gov.uk")                    >= 0) { getsource("Rugby", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("rutland.gov.uk")                  >= 0) { getsource("Rutland", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("ryedale.gov.uk")                  >= 0) { getsource("Ryedale", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("testrye.gov.uk")                  >= 0) { getsource("TestRye", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("rwtestdemo.gov.uk")               >= 0) { getsource("RWTestDemo", out dataSource1, out userName1); return; }
        //if (emailaddress.IndexOf("scambs.gov.uk")                   >= 0) { getsource("SouthCambs", out dataSource1, out userName1);  return;}
        if (emailaddress.IndexOf("scarborough.gov.uk")              >= 0) { getsource("Scarborough", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("scottishborders.gov.uk")          >= 0) { getsource("ScottishBorders", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("scotborders.gov.uk")              >= 0) { getsource("ScottishBorders", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("sharedcam.gov.uk")                >= 0) { getsource("SharedCam", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("southlanarkshire.gov.uk")         >= 0) { getsource("SouthLanarkshire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("s-norfolk.gov.uk")                >= 0) { getsource("SouthNorfolk", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("southnorfolk.gov.uk")             >= 0) { getsource("SouthNorfolk", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("southstaffordshire.gov.uk")       >= 0) { getsource("SouthStaffordshuire", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("stirling.gov.uk")                 >= 0) { getsource("Stirling", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("swansea.gov.uk")                  >= 0) { getsource("Swansea", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("swindon.gov.uk")                  >= 0) { getsource("Swindon", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("swntest.gov.uk")                  >= 0) { getsource("SwnTest", out dataSource1, out userName1); return; }
        //if (emailaddress.IndexOf("testing.gov.uk")                  >= 0) { getsource("Testing", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("torridge.gov.uk")                 >= 0) { getsource("Torridge", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("testtor.gov.uk")                  >= 0) { getsource("TestTor", out dataSource1, out userName1); return; }
        
        if (emailaddress.IndexOf("veolia.com")                      >= 0) { getsource("Veolia", out dataSource1, out userName1); return; }
        //if (emailaddress.IndexOf("virginiabeach.com")               >= 0) { getsource("VirginiaBeach", out dataSource1, out userName1);  return;}
        if (emailaddress.IndexOf("webaspx.com")                     >= 0) { getsource("Webaspx", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("webaspx.gov.uk")                  >= 0) { getsource("Webaspx", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("westlancs.gov.uk")                >= 0) { getsource("WestLancs", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("westLothian.gov.uk")              >= 0) { getsource("WestLothian", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("wrexham.gov.uk")                  >= 0) { getsource("Wrexham", out dataSource1, out userName1); return; }
        if (emailaddress.IndexOf("york.gov.uk")                     >= 0) { getsource("York", out dataSource1, out userName1); return; }

        //specials for testing different website versions
        if (emailaddress.IndexOf("live.webaspx@dorsetcc.gov.uk")  >= 0) { getsource("LiveDWP", out dataSource1, out userName1); councilname = "DWP"; return; }
        if (emailaddress.IndexOf("test.webaspx@dorsetcc.gov.uk")  >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("test1.webaspx@dorsetcc.gov.uk") >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("test2.webaspx@dorsetcc.gov.uk") >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("test3.webaspx@dorsetcc.gov.uk") >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("test4.webaspx@dorsetcc.gov.uk") >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("test5.webaspx@dorsetcc.gov.uk") >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("tm.webaspx@dorsetcc.gov.uk") >= 0) { getsource("TestDWP", out dataSource1, out userName1); councilname = "TestDWP"; return; }
        if (emailaddress.IndexOf("usertest.webaspx@dorsetcc.gov.uk") >= 0) { getsource("UserDWP", out dataSource1, out userName1); councilname = "DWP"; return; }                
        */
        #endregion
    }
    #region oldCode no references, not used, does not cater for Digital depot - KP 26Jul2021
    /*
    public static bool checkuser(string username, string password)
    {
        SqlConnectionStringBuilder connString2Builder;
        connString2Builder = new SqlConnectionStringBuilder();
        connString2Builder.DataSource = dataSource;
        connString2Builder.InitialCatalog = sampleDatabaseName;
        connString2Builder.Encrypt = true;
        connString2Builder.TrustServerCertificate = false;
        connString2Builder.TrustServerCertificate = true;
        connString2Builder.UserID = userName;
        connString2Builder.Password = Dpassword;
        string query = "";

        query = "Select Password from Users WHERE Email='" + username + "'";
        int igot = 0;
        if (query != "")
        {
            using (SqlConnection conn = new SqlConnection(connString2Builder.ToString()))
            {
                using (SqlCommand command = conn.CreateCommand())
                {
                    try
                    {
                        conn.Open();
                        string sql1 = query;
                        command.CommandText = sql1;
                        if (sql1.ToUpper().StartsWith("SELECT ") == true)
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                // Loop over the results
                                while (reader.Read())
                                {
                                    if (reader[0].ToString().ToUpper() == password.Trim().ToUpper())
                                    {
                                        igot = 1;
                                    }
                                }
                                //finished://KP edit out 02Aug19
                                //  igot = igot;//KP edit out 02Aug19
                            }
                        }
                    }
                    catch
                    {
                    }
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        if (igot == 1)
        {
            return true;
        }
        return false;

    }
    */
    #endregion

}