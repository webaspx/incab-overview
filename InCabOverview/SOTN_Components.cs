﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Device.Location;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;

namespace InCabOverview
{
    public enum Status
    {
        Productive, Unproductive, Depot, Tip
    }

    public enum MessageType
    {
        //GPS, Issue, NoGPSFix, CSY, CPY, CPN, CSN, DriverCheck, LogIn, Tip, UI, Message, Other
        CSY, CSN, CPY, CPN,
        DriverCheck, DriverCheckEOD,
        GPS,
        INLogin, INLogout,
        Issue, Unissue,
        Message,
        Other,
        Panic,
        Tip
    }

    public enum IdentifierType
    {
        RegistrationRound, Registration, Round, None,
    }

    public struct IssueType
    {
        public String IssueCode;
        public String IssueText;
    }

    public class DateRange
    {
        public DateTime Start, End;
    }

    public class Issue
    {
        //public String House     { get; set; }
        //public String Street    { get; set; }
        //public String UPRN      { get; set; }
        //public String Round     { get; set; }
        //public DateTime Sent    { get; set; }
        public String User      { get; set; }

        public int ID;

        public DateTime Time;

        public String DriverID;

        public GeoCoordinate RoundedCoordinate;

        public GeoCoordinate AccurateCoordinate;

        public String Registration;

        public Round Round;

        public String UPRN;

        public MessageType MessageType;

        public String IssueType;

        public String IssueText;

        public String IssueCode;

        public String House = "";

        public String Street = "";

        public String Town = "";

        public String PostCode;

        public Int32 Hash;

        public Issue(DataRow Row, String PostCode = null)
        {
            try
            {
                this.ID = Convert.ToInt32(Row["OriginalID"].ToString());

                this.Time = DateTime.Parse(Row["DateTimeYYYYMMDDhhmmss"].ToString());

                this.DriverID = Row["DriverID"].ToString();

                Enum.TryParse((string)Row["MessageType"], out this.MessageType);

                this.Registration = Row["Registration"].ToString();

                //if (this.MessageType == MessageType.Unissue)
                if (!string.IsNullOrWhiteSpace(Row["Lat"].ToString()) && !string.IsNullOrWhiteSpace(Row["Lon"].ToString()))
                {
                    Double Latitude = Convert.ToDouble(Row["Lat"].ToString());

                    Double Longitude = Convert.ToDouble(Row["Lon"].ToString());

                    this.RoundedCoordinate = SOTN_Extensions.RoundCoordinate(Latitude, Longitude);// SOTN_.RoundCoordinate(Latitude, Longitude);

                    this.AccurateCoordinate = new GeoCoordinate(Convert.ToDouble(Row["Lat"].ToString()), Convert.ToDouble(Row["Lon"].ToString()));
                }
                else
                {
                    this.RoundedCoordinate = new GeoCoordinate(0, 0);                    
                }

                this.Round = new Round(Row["RoundDay"].ToString(), Row["RoundWeek"].ToString(), Row["RoundService"].ToString(), Row["RoundCrew"].ToString());




                this.UPRN = Row["UPRN"].ToString();

                this.IssueType = Row["IssueType"].ToString();

                this.IssueCode = Row["IssueCode"].ToString();

                this.IssueText = Row["Issue"].ToString();

                if (this.MessageType == MessageType.Unissue && this.IssueText.Contains(IssueCode))
                {
                    if (!string.IsNullOrWhiteSpace(IssueCode))
                    {
                        this.IssueText = this.IssueText.Replace(this.IssueCode).Replace(" :");
                    }
                }

                this.House = Row["HouseName"].ToString().TitleCase();

                this.Street = Row["StreetName"].ToString().TitleCase();

                this.PostCode = PostCode ?? "";

                this.User = Row["DriverID"].ToString();

                if (this.UPRN == "NO UPRN")
                {
                    if (this.Street.Contains(","))
                    {
                        String NewTown = this.Street.Split(',').Last();

                        NewTown = NewTown.Substring(1, NewTown.Length - 1);

                        this.Town = NewTown;

                        this.Street = this.Street.Replace(String.Format(", {0}", NewTown));
                    }
                }
            }
            catch(Exception ex)
            {
                ex.Message.ToString();
            }
        }


        public Issue() {}
        public override Int32 GetHashCode()
        {
            int hash = this.Time.GetHashCode() ^
                       this.DriverID.GetHashCode() ^
                       this.RoundedCoordinate.GetHashCode() ^
                       this.Registration.GetHashCode() ^
                       this.Round.GetPrivateHashCode() ^
                       this.UPRN.GetHashCode() ^
                       this.IssueType.GetHashCode() ^
                       this.IssueText.GetHashCode() ^
                       this.IssueCode.GetHashCode() ^
                       this.House.GetHashCode() ^
                       this.Street.GetHashCode() ^
                       this.PostCode.GetHashCode();

            this.Hash = hash;

            return hash;
        }
    }

    public class LogIn
    {
        public readonly TimeSpan StartTime = new TimeSpan(0, 0, 1);
        public readonly TimeSpan EndTime = new TimeSpan(23, 59, 59);

        public readonly DateTime Time;

        public readonly Round Round;

        public readonly string DriverID;

        public readonly string Registration;

        public List<DateRange> DateRanges = new List<DateRange>();

        //public DateTime EndTime;

        // Old
        public LogIn(DataRow row, string[] data)
        {
            Round = new Round(data[0].Split(':'));
            Registration = data[0].Split(':')[0].Replace("$IN$");
            Time = DateTime.ParseExact(data[1].ToString(), "ddMMyyyy", CultureInfo.InvariantCulture) + TimeSpan.FromHours(Convert.ToDouble(data[2])).StripMS();
            DriverID = data[3];
        }

        public LogIn(DataRow row, DataRow[] allLoginsForVehicle)
        {
            var roundDay = row["RoundDay"].ToString();
            var roundWeek = row["RoundWeek"].ToString();
            var roundService = row["RoundService"].ToString();
            var roundCrew = row["RoundCrew"].ToString();

            Round = new Round(roundDay, roundWeek, roundService, roundCrew);
            Registration = row["Registration"].ToString();
            Time = DateTime.Parse(row["DateTimeYYYYMMDDhhmmss"].ToString());
            DriverID = row["User1"].ToString();

            StartTime = Time.TimeOfDay;

            if (allLoginsForVehicle.Length == 0)
            {

            }
            else
            {
                var firstPreviousLogin = allLoginsForVehicle.LastOrDefault(x => DateTime.Parse(x["DateTimeYYYYMMDDhhmmss"].ToString()) < Time);
                var firstFutureLogin = allLoginsForVehicle.FirstOrDefault(x => DateTime.Parse(x["DateTimeYYYYMMDDhhmmss"].ToString()) > Time);

                //if (firstPreviousLogin != null)
                //{
                //    StartTime = DateTime.Parse(firstPreviousLogin["DateTimeYYYYMMDDhhmmss"].ToString()).TimeOfDay;
                //}


                if (firstFutureLogin != null)
                {
                    EndTime = DateTime.Parse(firstFutureLogin["DateTimeYYYYMMDDhhmmss"].ToString()).TimeOfDay;
                }
            }
            //else if ()
            //{
            //
            //}

        }


        public override bool Equals(object obj)
        {
            if (!(obj is LogIn t))
            {
                return false;
            }

            var equals = (Time == t.Time && Round == t.Round && DriverID == t.DriverID && Registration == t.Registration);

            return equals;
        }

        public override string ToString()
        {
            return $"{Registration},{DriverID.Replace("York", "").Replace("Base", "")},{Round.Crew} {Round.Day} {Round.Week} {Round.Service},{Time.TimeOfDay}";
        }
    }

    public class Facility
    {
        public String FacilityType { get; set; }

        public String FacilityName { get; set; }

        public GeoCoordinate FacilityCoordinate { get; set; }

        public Facility(String FacilityType, String FacilityName, GeoCoordinate FacilityCoordinate)
        {
            this.FacilityType = FacilityType;
            this.FacilityName = FacilityName;
            this.FacilityCoordinate = FacilityCoordinate;
        }
    }



    public class Round
    {
        public readonly string Week;

        public string Service;

        public string Day;

        public string Crew;

        public int Hash;

        public Round() { }

        public Round(string Day, string Week, string Service, string Crew = "")
        {
            this.Day = FormatWeekday(Day);

            this.Week = $"Wk {Week}";

            this.Service = FormatService(Service);

            this.Crew = "Cr" + Crew;

            this.Hash = GetPrivateHashCode(Crew != "");
        }

        private string FormatService(string service)
        {
            switch (service)
            {
                case "Ref":
                    return "Refuse";
                case "Rec":
                    return "Recycling";
                case "Org":
                    return "Organic";
                case "Gls":
                case "Gla":
                    return "Glass";
                case "Foo":
                    return "Food";
                case "Grn":
                    return "Garden";
                case "Trd":
                case "Tra":
                    return "Trade";
                case "TRf":
                    return "Trade Refuse";
                case "Pls":
                case "Pla":
                    return "Plastic";
                case "Box":
                    return "Box";
                case "Sma":
                    return "Small Mech";
                case "Lar":
                    return "Large Mech";
                case "Bul":
                    return "Bulky";
                default:
                    return service;
            }
        }
        private string FormatWeekday(string day)
        {
            if (day.Length > 3)
            {
                return day;
            }

            switch (day)
            {
                case "Mon":
                    return "Monday";
                case "Tue":
                    return "Tuesday";
                case "Wed":
                    return "Wednesday";
                case "Thu":
                    return "Thursday";
                case "Fri":
                    return "Friday";
                case "Sat":
                    return "Saturday";
                case "Sun":
                    return "Sunday";
                default:
                    return "N/A";
            }
        }

        public Round(String[] Data)
        {
            this.Day = Data[3];

            this.Service = Data[2];

            this.Crew = "Cr" + Data[5];

            this.Week = String.Format("Wk {0}", Data[4].Replace("Week"));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Round t))
            {
                return false;
            }

            var equals = (Week == t.Week && Service == t.Service && Day == t.Day && Crew == t.Crew);

            return equals;
        }


        public override string ToString()
        {
            return $"{Day} {Week} {Service} {Crew}";
        }

        public override int GetHashCode()
        {
            return GetPrivateHashCode(true);
        }


        public int GetPrivateHashCode(bool includeCrew = false)
        {
            if (includeCrew)
            {
                return this.Day.GetHashCode() ^
                       this.Week.GetHashCode() ^
                       this.Crew.GetHashCode() ^
                       this.Service.GetHashCode();
            }
            else
            {
                return this.Day.GetHashCode() ^
                       this.Week.GetHashCode() ^
                       this.Service.GetHashCode();
            }
        }
    }

    /*
    public class Round
    {
        public Int32 Week;

        public String Service;

        public String Day;

        public String Crew;

        public Round()
        {

        }

        public Round(String[] Data)
        {
            this.Day = Data[3];

            this.Service = Data[2];

            this.Crew = Data[5];

            this.Week = Convert.ToInt32(Data[4].Replace("Week"));
        }

        public String ToString(Boolean includeCrew = false)
        {
            if (includeCrew)
            {
                return String.Format("{0} {1} {2} {3}", this.Day, this.Week, this.Crew, this.Service);
            }
            else
            {
                return String.Format("{0} {1} {2}", this.Day, this.Week, this.Service);
            }
        }
    }*/

    public class Message
    {
        public int ID;

        //public String Data;

        public string Registation;

        public DateTime Time;

        //public IssueType Issue;
        public Round Round;

        public Coordinate Coordinate;

        public MessageType MessageType;

        public IdentifierType Identifier;

        public string cStreetID, cPropertyID, UPRN, USRN;


        public Issue Issue;
        public DriverCheck DriverCheck;
        public TipTicket TipTicket = null;
        public Message() {}

        public Message(DataRow Row/*, Dictionary<String, List<IssueType>> Issues*/)
        {
            this.ID = Convert.ToInt32(Row["OriginalID"]);
            this.Time = DateTime.Parse(Row["DateTimeYYYYMMDDhhmmss"].ToString());

            

            if (double.TryParse(Row["Lat"].ToString(), out var latitude) &&
                double.TryParse(Row["Lon"].ToString(), out var longitude))
            {
                this.Coordinate = new Coordinate(longitude, latitude);
            }
            else
            {
                this.Coordinate = new Coordinate(0, 0);
            }

            _ = Enum.TryParse((string)Row["MessageType"], out this.MessageType);

            string registration = (string)Row["Registration"],
                   roundDay = (string)Row["roundCrew"],
                   roundWeek = (string)Row["roundWeek"],
                   roundService = (string)Row["roundService"],
                   roundCrew = (string)Row["roundCrew"];

            this.Registation = registration;

            bool hasRoundInformation = !string.IsNullOrWhiteSpace(roundDay) &&
                                       !string.IsNullOrWhiteSpace(roundWeek) &&
                                       !string.IsNullOrWhiteSpace(roundService) &&
                                       !string.IsNullOrWhiteSpace(roundCrew);


            if (!string.IsNullOrWhiteSpace(registration) && hasRoundInformation)
            {
                this.Identifier = IdentifierType.RegistrationRound;
            }
            else if (!string.IsNullOrWhiteSpace(registration))
            {
                this.Identifier = IdentifierType.Registration;
            }
            else if (hasRoundInformation)
            {
                this.Identifier = IdentifierType.Round;
            }
            else
            {
                this.Identifier = IdentifierType.None;
            }

            if (this.Identifier == IdentifierType.RegistrationRound || this.Identifier == IdentifierType.Round)
            {
                this.Round = new Round(roundDay, roundWeek, roundService, roundCrew);
            }



            if (this.MessageType == MessageType.Tip)
            {
                this.TipTicket = new TipTicket()
                {
                    Gross = Convert.ToInt32(Row["TipGross"]),
                    Tare = Convert.ToInt32(Row["TipTare"]),
                    Unit = (string)Row["TipUnits"],
                    Location = (string)Row["TipName"]
                };
            }
            else if (this.MessageType == MessageType.Issue)
            {
                this.Issue = new Issue(Row);
            }
            else if (this.MessageType == MessageType.DriverCheck)
            {
                this.DriverCheck = new DriverCheck(Row);
            }


            this.cStreetID = (string)Row["CStreetID"];
            this.cPropertyID = (string)Row["CPropertyID"];
            this.USRN = (string)Row["USRN"];
            this.UPRN = (string)Row["UPRN"];


            //String[] Data = Row.ItemArray.StringArray();
            //
            //Boolean CoordinateValid = false;
            //
            //if (Data[1].Contains("GPS:"))
            //{
            //    MessageType = MessageType.GPS;
            //
            //    Identifier = IdentifierType.Registration;
            //
            //    CoordinateValid = true;
            //}
            //else if (Data[1].Contains("NoGPSFix"))
            //{
            //    MessageType = MessageType.NoGPSFix;
            //
            //    Identifier = IdentifierType.RegistrationRound;
            //
            //    CoordinateValid = true;
            //}
            //else if (Data[1].Contains("Driver Check"))
            //{
            //    MessageType = MessageType.DriverCheck;
            //
            //    CoordinateValid = true;
            //}
            //else if (Data[1].Contains("$IN$"))
            //{
            //    MessageType = MessageType.LogIn;
            //
            //    Identifier = IdentifierType.Round;
            //}
            //else if (Data[1].Contains("$CSY$"))
            //{
            //    MessageType = MessageType.CSY;
            //
            //    Identifier = IdentifierType.Round;
            //}
            //else if (Data[1].Contains("$CSN$"))
            //{
            //    MessageType = MessageType.CSN;
            //
            //    Identifier = IdentifierType.Round;
            //}
            //else if (Data[1].Contains("$CPY$"))
            //{
            //    MessageType = MessageType.CPY;
            //
            //    Identifier = IdentifierType.Round;
            //}
            //else if (Data[1].Contains("$CPN$"))
            //{
            //    MessageType = MessageType.CPN;
            //
            //    Identifier = IdentifierType.Round;
            //}
            //else if (Data[1].Contains("$TT$"))
            //{
            //    MessageType = MessageType.TT;
            //}
            //else if (Data[1].Contains("$UI$"))
            //{
            //    MessageType = MessageType.UI;
            //}
            //else
            //{
            //    IssueType[] IssuesDetected = Issues.Values.SelectMany(x => x.Where(y => Data[1].Contains(y.IssueCode) || Data[1].Contains(y.IssueText))).ToArray();
            //
            //
            //    if (usingMessagesSplit)
            //    {
            //        if (Data[1] == "Issue")
            //        {
            //            this.Issue = new IssueType()
            //            {
            //                IssueCode = Row["IssueCode"].ToString(),
            //                IssueText = Row["Issue"].ToString()
            //            };
            //
            //            Identifier = IdentifierType.Round;
            //
            //            MessageType = MessageType.Issue;
            //        }
            //    }
            //
            //    if (IssuesDetected.Length > 0)
            //    {
            //        this.Issue = IssuesDetected[0];
            //
            //        Identifier = IdentifierType.Round;
            //
            //        MessageType = MessageType.Issue;
            //    }
            //    else
            //    {
            //        MessageType = (Data[1].Contains("$")) ? MessageType.Other : MessageType.Message;
            //    }
            //}

            //DateTime TimeSent;
            //
            //if (usingMessagesSplit)
            //{
            //    TimeSent = DateTime.Parse(Data[2]);
            //}
            //else if ((Data[2].Length > 8))
            //{
            //    TimeSent = DateTime.ParseExact(Data[2].RemoveLast(Data[2].Length - 8), "ddMMyyyy", CultureInfo.CurrentCulture) + TimeSpan.FromHours(Convert.ToDouble(Data[3]));
            //}
            //else
            //{
            //    TimeSent = DateTime.ParseExact(Data[2], "ddMMyyyy", CultureInfo.CurrentCulture) + TimeSpan.FromHours(Convert.ToDouble(Data[3]));
            //}
            //
            //this.ID = ((usingMessagesSplit) ? Convert.ToInt32(Row["OriginalID"].ToString()) : Convert.ToInt32(Row[0].ToString()));
            //
            //this.Sent = TimeSent;
            //
            //this.Data = Row.ItemArray[1].ToString();
            //
            //this.Coordinate = (CoordinateValid) ? Data.Coordinate2() : null;
            //
            //if (CoordinateValid)
            //{
            //    if (this.Coordinate.Latitude < 50 && this.Coordinate.Latitude != 0 && this.Coordinate.Longitude != 0)
            //    {
            //
            //    }
            //}
        }

    }

    public class Property
    {
        public string PropertyID;

        public string StreetID;

        public GeoCoordinate Coordinate;

        public DateTime TimeCollected;

        public Property()
        {

        }
    }

    public partial class Coordinate : GeoCoordinate
    {
        public Coordinate() {}

        public DateTime Time { get; set; }
        public Coordinate(Double Longitude, Double Latitude, Double Time)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Time = DateTime.Today.Date + TimeSpan.FromHours(Time).StripMS();
        }
        public Coordinate(Double Longitude, Double Latitude, DateTime Time, Double Speed)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Time = Time;
            this.Speed = Speed;
        }

        public Coordinate(Double Longitude, Double Latitude, DateTime Time)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Time = Time;
        }
        public Coordinate(Double Longitude, Double Latitude)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
        }
    }

    #region WIP
    /*
    public partial class # : GeoCoordinate
    {
        public DateTime Time { get; set; }

        public Coordinate(Double Latitude, Double Longitude, Double Time)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Time = DateTime.Today.Date + TimeSpan.FromHours(Time).StripMS();
        }
        public Coordinate(Double Latitude, Double Longitude, DateTime Time, Double Speed)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Time = Time;
            this.Speed = Speed;
        }

        public Coordinate(Double Latitude, Double Longitude, DateTime Time)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
            this.Time = Time;
        }
        public Coordinate(Double Latitude, Double Longitude)
        {
            this.Longitude = Longitude;
            this.Latitude = Latitude;
        }
    }
    */
    #endregion

    public class ReceptionVisit
    {
        public Int32 Yield;

        public String ReceptionName;

        public DateTime Enter, Leave;

        public TimeSpan VisitLength;

        public Boolean Ticket;

        public ReceptionVisit(String ReceptionName, DateTime Enter, DateTime Leave, Int32 Yield = 0, Boolean Ticket = false)
        {
            this.Yield = Yield;
            this.Enter = Enter;
            this.Leave = Leave;
            this.ReceptionName = ReceptionName;
            this.Ticket = Ticket;
            VisitLength = Leave - Enter;
        }
    }

    public class LatLonConversions          //Not 100% accurate, +- 15m tolerance
    {
        const Double a = 6377563.396, b = 6356256.91, e2 = (a - b) / a, n0 = -100000, e0 = 400000, f0 = 0.999601272, phi0 = 0.855211333, lambda0 = -0.034906585, n = (a - b) / (a + b);

        public static GeoCoordinate ConvertOSToLatLon(Double E, Double N) //
        {
            Double phi = (N - n0) / (a * f0) + phi0;
            Double M = b * f0 * ((1 + n + 5 / 4 * n * n + 5 / 4 * n * n * n) * (phi - phi0) - (3 * n + 3 * n * n + 21 / 8 * n * n * n) * Math.Sin(phi - phi0) * Math.Cos(phi + phi0) +
                        (15 / 8 * n * n + 15 / 8 * n * n * n) * Math.Sin(2 * (phi - phi0)) * Math.Cos(2 * (phi + phi0)) - 35 / 24 * n * n * n * Math.Sin(3 * (phi - phi0)) * Math.Cos(3 * (phi + phi0)));

            while (N - n0 - M >= 0.01)
            {
                phi = (N - n0 - M) / (a * f0) + phi;
                M = b * f0 * ((1 + n + 5 / 4 * n * n + 5 / 4 * n * n * n) * (phi - phi0) - (3 * n + 3 * n * n + 21 / 8 * n * n * n) * Math.Sin(phi - phi0) * Math.Cos(phi + phi0) + (15 / 8 * n * n + 15 / 8 * n * n * n) * Math.Sin(2 * (phi - phi0)) * Math.Cos(2 * (phi + phi0)) - 35 / 24 * n * n * n * Math.Sin(3 * (phi - phi0)) * Math.Cos(3 * (phi + phi0)));
            }
            Double v = a * f0 * Math.Pow(1 - e2 * Math.Sin(phi) * Math.Sin(phi), -0.5),
                   p = a * f0 * Math.Pow(1 - e2 * Math.Sin(phi) * Math.Sin(phi), -1.5) * (1 - e2),
                   n2 = v / p - 1,
                   vii = Math.Tan(phi) / (2 * p * v),
                   viii = Math.Tan(phi) / (24 * p * v * v * v) * (5 + 3 * Math.Tan(phi) * Math.Tan(phi) + n2 - 9 * Math.Tan(phi) * Math.Tan(phi) * n2),
                   ix = Math.Tan(phi) / (720 * p * Math.Pow(v, 5)) * (61 + 90 * Math.Tan(phi) * Math.Tan(phi) + 45 * Math.Pow(Math.Tan(phi), 4)),
                   x = (1 / Math.Cos(phi)) / v,
                   xi = (1 / Math.Cos(phi)) / (6 * Math.Pow(v, 3)) * (v / p + 2 * Math.Pow(Math.Tan(phi), 2)),
                   xii = (1 / Math.Cos(phi)) / (120 * Math.Pow(v, 5)) * (5 + 28 * Math.Pow(Math.Tan(phi), 2) + 24 * Math.Pow(Math.Tan(phi), 4)),
                   xiia = (1 / Math.Cos(phi)) / (5040 * Math.Pow(v, 7)) * (61 + 662 * Math.Pow(Math.Tan(phi), 2) + 1320 * Math.Pow(Math.Tan(phi), 4) + 720 * Math.Pow(Math.Tan(phi), 6)),
                   e = (E - e0),
                   lon = (phi - vii * e * e + viii * e * e * e * e - ix * e * e * e * e * e * e) * 180 / Math.PI,
                   lat = (lambda0 + x * e - xi * e * e * e + xii * e * e * e * e * e - xiia * e * e * e * e * e * e * e) * 180 / Math.PI;
            return new GeoCoordinate(lon, lat);
        }
    }

    public class HouseInformation
    {
        public String PostCode;

        public String Town;

        public HouseInformation(String PostCode, String Town)
        {
            this.PostCode = PostCode;

            this.Town = Town;
        }
    }

    public class SOTN_Error
    {
        public String Client;

        public Exception Exception;

        public String Message;

        public DateTime Time;

        public String DateRequested;

        public Int32 LineNumber;

        public SOTN_Error(String Client, String Message, Exception Exception, DateTime Time, String MD)
        {
            this.Client = Client;

            this.Exception = Exception;

            this.Message = Message;

            this.Time = Time;

            this.DateRequested = MD;

            this.LineNumber = new StackTrace().GetFrame(0).GetFileLineNumber();
        }
    }

    #region MESSAGES FORMAT
    /* 
     * - Date
     * - Time
     * - User
     * 
     * - GPS:
     *      "GPS:Client:Service:Reg:Coordinates
     *       
     * - Issue:
     *      "House Issue Street:UPRN:Day Week# Service Crew:Coordinate"
     *      - Contamination
     *          "House Contamination IssueCode Street:UPRN:Day Week# Service Crew:Coordinate"
     *          
     *  - No GPS Fix
     *      "$NoGPSFix$Reg:DriverID:Day Week# Service Crew:Coordinate"
     * 
     *  - CSY
     *      "$CSY$StreetID:Service:Day:Week #:Crew"
     *      
     *  - Driver Check
     *      "?Answer$?$Driver Check Coordinate"
     *      
     *  - Log In
     *      "$In$Reg:DriverID:Service:Day:Week#:?:IC Version:LastUpdated"
     */
    #endregion
}