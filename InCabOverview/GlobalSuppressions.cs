using System.Diagnostics.CodeAnalysis;

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "ex", Scope = "member", Target = "~M:InCabOverview.Database.Read(System.String,System.String,System.Data.SqlClient.SqlParameter[])~System.Data.DataTable")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "FinalTipComplete", Scope = "member", Target = "~M:InCabOverview.SOTN_Unit.FuncTimeToComplete(System.Collections.Generic.List{incabOverview.Message},System.Collections.Generic.List{incabOverview.Facility},incabOverview.LogIn,incabOverview.Property)~System.DateTime")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "PaddingLength", Scope = "member", Target = "~M:InCabOverview.StateOfTheNation.GetAllLoggedInUnits(System.String,System.String)~System.Collections.Generic.List{incabOverview.LogIn}")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "portalStyle", Scope = "member", Target = "~M:InCabOverview.StateOfTheNation.SetPortalStyle~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "Temp", Scope = "member", Target = "~M:InCabOverview.StateOfTheNation.GetAllLoggedInUnits(System.String,System.String)~System.Collections.Generic.List{incabOverview.LogIn}")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "~F:InCabOverview.SOTN_Unit.UnproductiveTime")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "SignIn", Scope = "member", Target = "~M:InCabOverview.SOTN_Unit.FuncTimeToComplete(System.Collections.Generic.List{incabOverview.Message},System.Collections.Generic.List{incabOverview.Facility},incabOverview.LogIn,incabOverview.Property)~System.DateTime")]
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

