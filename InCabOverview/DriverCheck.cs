﻿using System;
using System.Data;

namespace InCabOverview
{
    public class DriverCheck
    {
        public int ChecksPassed;

        public int ChecksFailed;

        public string CheckData;

        public DriverCheck() {}

        public DriverCheck(DataRow Row)
        {
            string answers = (string)Row["DCAnswers"];

            ChecksPassed = answers.Split(new[] { "Y" }, StringSplitOptions.None).Length - 1;

            ChecksFailed = answers.Split(new[] { "N" }, StringSplitOptions.None).Length - 1;

            CheckData = answers;
        }
    }
}