﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StateOfTheNation.aspx.cs" Inherits="InCabOverview.StateOfTheNation" Debug="true" Async="true" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head" runat="server">
    <title>InCab SOTN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"  media="screen" />
    <link rel="stylesheet" type="text/css" href="css/materialicons.css" />
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/inCabStyleSheet.css" />
    <link rel="stylesheet" type="text/css" href="css/ionicons.min.css" />
    <link rel="icon" type="image/icon" href="Resources/favicon.ico" />
    <link rel="shortcut icon" type="image/icon" href="Resources/favicon.ico" />
    <script type="text/javascript" src="/Scripts/jquery/jquery-latest.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="/Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/Scripts/Chart.min.js"></script>
    <script type="text/javascript" src="/Scripts/charts.js"></script>
    <script type="text/javascript" src="/Scripts/ajax.js"></script>
    <script type="text/javascript" src="/Scripts/SOTN_Charts.js"></script>
    <script type="text/javascript" src="/Scripts/SOTN_Data.js"></script>   
    <script type="text/javascript">
        var client = '<%= this.Client %>';
        var MD = '<%= this.MD %>';
        var Time = new Date();
        var CurrentIndex = -1, Errors = 0, TotalTipTicketTimes, FontSize, ScrollPosition = 0;
        var repData = new Array();
        var ErrorCount = 0;
        var firstTime = true;

        // Dynamic street style loading
        var portalStyle = '<%= this.PortalStyle %>';
        //portalStyle = "Streets"; // TEMPCODE

        $(function () {
            GetData();

            $('#body').on("mousewheel", function () {
                ScrollPosition = ($(document).scrollTop());
            });

            if (window.document.documentMode) {
                $('#MGPanelBar .Tile-Icon').css('margin-bottom', '-15px');
            }
        });

        /* AJAX */

        function GetData() {
            if (client == "Unauthenticated") {
                CriticalError('<strong>Warning!</strong> Unauthenicated User.');
                return;
            }

            if (MD == "Invalid Date Selected") {
                CriticalError('<strong>Warning!</strong> Invalid Date Selected.');
                return;
            }

            if (!client && client.length < 1) {
                CriticalError('<strong>Warning!</strong> No client selected.');
                return;
            }
            
            CurrentIndex = 0;
            Errors = 0;
            $('#Loader').fadeIn(500);
            $('#SOTN_Table_Data').fadeOut(500);
            $('#SOTN_Banner').fadeOut(500);
            $('#SOTN_Chart_Data').fadeOut(500);
            $('#CriticalWarning').fadeOut(500);
            $('table > tbody').empty();
            $('#LoaderMessage').text('Getting Logins...');
            ajax('StateOfTheNation.aspx', 'FuncInCabData', { "Client": client, "CurrentIndex": CurrentIndex, "MD": MD, "PortalStyle": portalStyle }, success, fail, 90000);
        }

        var success = function (e) {
            $('#LoaderMessage').html("Loading...<br>" + (CurrentIndex + 1) + " of " + e.totalVehicles).fadeIn(500);

            if (e.success != true) {
                CriticalError('<strong>Error: </strong> ' + e.data + ((e.alertDeveloper) ? '.<br>A developer has been notified of the issue.' : ''));
                $('#Loader').fadeOut(500);
                return;
            }

            if (e.data != null) {
                if (e.tipvisittimes != null) {
                    TotalTipTicketTimes = e.tipvisittimes;
                }

                if (e.data == null) {
                    Errors++;
                } else {
                    repData[CurrentIndex - Errors] = e.data;
                }

                CurrentIndex++;

                ajax('StateOfTheNation.aspx', 'FuncInCabData', { "Client": client, "CurrentIndex": CurrentIndex, "MD": MD, "PortalStyle": portalStyle }, success, fail, 60000);

                return;
            } else {
                if (repData.length == 0) {
                    CriticalError('<strong>Warning!</strong> No Login Messages Found.');
                    $('#Loader').fadeOut(500);
                } else {

                    var tbody = $('#SOTN_Table > tbody');
                    $('#Loader').fadeOut(500);
                    $('#SOTN_Chart_Data').fadeIn(500);
                    $('#SOTN_Banner').fadeIn(500);

                    $('#TVPanelTable > tbody').empty();
                    $('#VehiclesOut').empty();
                    $("ul").empty();
                    tbody.empty();

                    for (var i = 0; i < CurrentIndex - Errors; i++) {
                        var item = repData[i],
                            row = document.createElement('tr'),
                            ID = item.DriverID + "-" + item.Round.Crew + item.Round.Day + item.Round.Week + item.Round.Service;
                        row.id = ID;

                        var DriverID = FuncAddNewCell(undefined, item.DriverID.replace(/\D/g, ''));
                        var Registration = FuncAddNewCell(undefined, item.Registration);
                        var StartTime = FuncAddNewCell(undefined, item.StartTime.substring(0, 5));
                        var EndTime = FuncAddNewCell(undefined, item.EndTime == "23:59:59" ? "-" : item.EndTime.substring(0, 5));
                        var Round = FuncAddNewCell(undefined, item.Round.Crew + " " + item.Round.Day + " " + item.Round.Week + " " + item.Round.Service);
                        var BinsCollected = FuncAddNewCell(ID + "-BinsCollected", item.BinsCollected);
                        var BinsUncollected = FuncAddNewCell(ID + "-BinsOnRound", item.BinsOnRound);
                        var StreetsCollected = FuncAddNewCell(ID + "-StreetsCollected", item.StreetsCollected);
                        var StreetsUncollected = FuncAddNewCell(undefined, item.StreetsUncollected);
                        var PickRate = FuncAddNewCell(ID + "-PickRate", item.PickRate);
                        var IssuesRaised = FuncAddNewCell(undefined, item.IssuesRaised);
                        var BinNotOut = FuncAddNewCell(undefined, item.BinNotOut);
                        var TimeWorked = FuncAddNewCell(undefined, item.HoursWorked.substring(0, 8));
                        var MilesTravelled = FuncAddNewCell(ID + "-MilesTravelled", item.MilesTravelled);
                        var Speed = FuncAddNewCell(ID + "-Speed", item.Speed);
                        var DriverChecks = FuncAddNewCell(undefined, item.DriverChecks[0] + " | " + item.DriverChecks[1]);
                        var TimeToComplete = FuncAddNewCell(ID + "-TimeToComplete", item.TimeToComplete.slice(11, 19));//((isToday(item.TimeToComplete)) ? "" : item.TimeToComplete.slice(8, 10) + "/" + item.TimeToComplete.slice(5, 7) + "/" + item.TimeToComplete.slice(0, 4) + " ") + item.TimeToComplete.slice(11, 19));
                        var TipVisits = FuncAddNewCell(undefined, (item.TipVisits == null) ? "0?" : item.TipVisits.length);
                        var TotalYield = FuncAddNewCell(undefined, item.TotalYield);
                        var Completion = FuncAddNewCell(ID + "-Completion", item.Completion + "%");

                        if (portalStyle == "Streets") {
                            $(row).append(DriverID,
                                Registration,
                                StartTime,
                                EndTime,
                                Round,
                                FuncAddNewCell(ID + "-SectionsCollected", item.BinsCollected),
                                FuncAddNewCell(ID + "-SectionsUncollected", item.BinsUncollected),
                                StreetsCollected,
                                StreetsUncollected,
                                FuncAddNewCell(undefined, item.NonGradingIssues),
                                TimeWorked,
                                MilesTravelled,
                                Speed,
                                DriverChecks,
                                TimeToComplete,
                                TipVisits,
                                TotalYield,
                                Completion);

                            $('#SOTN_Table thead tr th:contains("Bins Collected")').text("Sections Complete");
                            $('#SOTN_Table thead tr th:contains("Bins On Round")').text("Sections to Complete");
                            $('#SOTN_Table thead tr th:contains("Streets Collected")').text("Streets Complete");
                            $('#SOTN_Table thead tr th:contains("Streets On Round")').text("Streets to Complete");
                            $('#SOTN_Table thead tr th:contains("Pick Rate")').remove();
                            $('#SOTN_Table thead tr th:contains("Bin Not Out")').remove();
                            $('#SOTN_Table thead tr th:contains("Average Speed (Mph)")').css("visibility", "visible");
                        }
                        else {
                            $(row).append(DriverID,
                                Registration,
                                StartTime,
                                EndTime,
                                Round,
                                BinsCollected,
                                BinsUncollected,
                                StreetsCollected,
                                StreetsUncollected,
                                PickRate,
                                IssuesRaised,
                                BinNotOut,
                                TimeWorked,
                                MilesTravelled,
                                //Speed,
                                DriverChecks,
                                TimeToComplete,
                                TipVisits,
                                TotalYield,
                                Completion);

                            $('#SOTN_Table thead tr th:contains("Average Speed (Mph)")').remove();
                        }
                        $(tbody).append(row);

                        if (parseInt(BinsUncollected.textContent) < 1 && parseInt(StreetsUncollected.textContent) < 1) {
                            ErrorCount++;

                            $('#MainAlert').show();

                            $(row).append(FuncAddTableAlert('SOTN_Table', 2, i, "Driver has logged into a non-existent round."));

                            FuncAddListAlert("No Round Information Found", item.DriverID.replace(/\D/g, '') + ' (' + item.Round.Crew + " " + item.Round.Day + " " + item.Round.Week + " " + item.Round.Service + ')');
                        }

                        if (item.Completion == "100" && item.TimeToComplete.indexOf("00:00:00")) {
                            $(row).addClass("Table-Complete");
                        }
                    }

                    //if (ErrorCount > 0) {
                    //    $('#SOTN_Table tbody tr td')
                    //}


                    AddRoundCompletionChart(repData);

                    RoundCompletionChart.update();

                    AddBinsCollectedChart(repData);

                    AddClockData(repData, 1);

                    AddValueToPanels(repData);

                    AddIssueTypeChart(repData);

                    ServiceData(repData);

                    // Add call to check the driver logins
                    CheckForMulitpleDriverLogins();

                    // TODO 6: This is broken.

                    if (MD == new Date().toLocaleDateString().replace(/\//g, "")) {
                        ComparePreviousPass(repData, client, MD);

                        SetLocalStorage(repData, client, MD);
                    }

                    var RoundCanvas = $('#RoundCompletionChart').children()[0];

                    RoundCanvas.id = "RoundCompletionChartCanvas";

                    $(RoundCanvas).addClass("ScalingChart");

                    if (!$('#RoundCompletionChart').attr('data-toggle')) {
                        $('#RoundCompletionChart').attr("data-toggle", "tooltip").attr("data-placement", "top").attr("title", "Round Completion").tooltip();//.attr("id", "RCID");//{ track: true }).attr("id", "TT");
                    }

                    $('#BinsCollectedChart').children()[1].id = "BinsCollectedCanvas";

                    $("#TimeColumn").empty().append(new Date().toString().slice(15, 24));

                    $("html, body").scrollTop(ScrollPosition);
                }

                if (ErrorCount > 0) {
                    FuncAddTableAlert('SOTN_Table', 2, i, "");
                }

                FuncAddTipVisitToolTips();
            }
        }

        /* Error Handling */

        var fail = function (e) {
            CriticalError('<strong>Error:</strong> ' + e.status + '</br>' + e.statusText);
            $('#Loader').hide();
        }

        function CriticalError(Message) {
            $('#CriticalText').html(Message);

            $('#CriticalWarning').fadeIn(500);
        }

        /* UI Changes */

        var BNOPanelActive = true, TVPanelActive = true, DCPanelActive = true, MGPanelActive = true, CTPanelActive = true, NGPanelActive = true
        oldX = window.screenX, oldY = window.screenY;


        $(document).ready(function () {
            // Portal Style Updates
            portalStyleUpdates();

            $('[data-toggle="tooltip"]').tooltip({ 'placement': 'top' });

            $('#DCPanel').on('click', function (e) {
                DCPanelActive = PanelTile('#DCPanel', DCPanelActive);
            });

            $('#TVPanel').on('click', function () {
                TVPanelActive = PanelTile('#TVPanel', TVPanelActive);
            });

            $('#NGPanel').on('click', function () {
                NGPanelActive = PanelTile('#NGPanel', NGPanelActive);
            });

            $('#BNOPanel').on('click', function () {
                BNOPanelActive = PanelTile('#BNOPanel', BNOPanelActive);
            });

            $('#MGPanel').on('click', function () {
                MGPanelActive = PanelTile('#MGPanel', MGPanelActive);
            });

            $('#CTPanel').on('click', function () {
                CTPanelActive = PanelTile('#CTPanel', CTPanelActive);
            });

            $('#Alerts-Tile').on('click', function () {
                setTimeout(function () { RemoveAlert(); }, 10);
            });

            $('#PanelMain').on('click', function (e) {
                if (e.target.id == this.id) {
                    ClosePanel();
                }
            });

        });

        $(window).resize(function () {
            var Font = parseFloat($('#BinsCollectedChart').width() / 5);

            if (Font > 0) {
                FontSize = Font;
            }

            if (FontSize > 80) {
                FontSize = 80;
            }

            $('#BinsCollectedIcon').css("font-size", FontSize + "px");
            $('#StreetsCollectedIcon').css("font-size", FontSize + "px");
            $('#IssueOccuranceChartIcon').css("font-size", FontSize + "px");

            var X = ($('#BinsCollectedCanvas').outerWidth() / 2) - ($('#BinsCollectedIcon').outerWidth() / 2);// + $('#BinsCollectedChart').position().left;
            var Y = ($('#BinsCollectedCanvas').outerHeight() / 2) - ($('#BinsCollectedIcon').outerHeight() / 2);// - -$('#BinsCollectedChart').position().top;

            $('#BinsCollectedIcon').css("right", X + "px");
            $('#BinsCollectedIcon').css("bottom", Y + "px");
            $('#BinsCollectedIcon').css("max-width", "60px");

            $('#StreetsCollectedIcon').css("right", X + "px");
            $('#StreetsCollectedIcon').css("bottom", Y + "px");
            $('#StreetsCollectedIcon').css("max-width", "60px");

            $('#IssueOccuranceChartIcon').css("right", X + "px");
            $('#IssueOccuranceChartIcon').css("bottom", Y + "px");
            $('#IssueOccuranceChartIcon').css("max-width", "60px");

            if ($(window).width() > 800) {
                $(".Tile").fadeIn();
            }
        });

        function FuncShowTableData() {
            if ($('#SOTN_TableContainer').css('display') == 'none') {
                $('#SOTN_Chart_Data').fadeOut(250);
                $('#SOTN_TableContainer').delay(250).fadeIn(250);
                $('#SOTN_Table_Data').delay(250).fadeIn(250);
            }
        }

        function FuncShowChartData() {
            if ($('#SOTN_Chart_Data').css('display') == 'none') {
                $('#SOTN_TableContainer').fadeOut(250);
                $('#SOTN_Chart_Data').delay(250).fadeIn(250);
                $('#SOTN_Chart_Data').trigger('resize');
            }
        }

        function RemoveAlert() {
            var Alerts = document.getElementById("Alerts-List").getElementsByTagName("li");

            var Counter = 0;

            $(Alerts).each(function () {
                if ($(this).height() < 1) {
                    Counter++;
                }
            });

            if (Counter == Alerts.length) {
                $('#Alerts-None').fadeIn();
                $('#Alerts-Some').fadeOut();
            }
        }

        function PanelTile(ID, CurrentActive) {
            if ($(ID + 'Value').text() == "0") {
                return;
            }

            //var HideOthers = ($(window).width() < 1100 && !$(ID).css("flex"));

            ClosePanel(ID);

            //if (HideOthers) {s
            //    //$(".Tile").not(ID).fadeOut();//.each(function () { $(this).fadeOut() });
            //}

            var Width = 590; //$('#PanelMain').width() / 3.1;

            if (CurrentActive) {
                $(ID).width(Width);
                $(ID).height(Width);
                $(ID).css("flex", "100%");
                $(ID).addClass('Tile-NoHover');
            }
            else {
                $(ID).css("flex", "");
                $(ID).removeClass('Tile-NoHover');
            }

            $(ID + 'Content').animate({ height: 'toggle' }, 350, function () {
                //CurrentActive: Shrinking = false, Expanding = true
                if ($(ID + 'Table').outerHeight() > (($(ID + 'Content').outerHeight() / 100) * 75)) {
                    $(ID + 'Bar').fadeToggle(100);
                    $(ID + 'Content').height(525);
                    $('#TipVisitIcon').fadeIn();
                }

                if (!CurrentActive) {
                    $(ID + 'Bar').fadeIn(100);
                    $(ID).width(180);
                    $(ID).height(180);
                    $('#TipVisitIcon').fadeOut();
                    $(".Tile").not(ID).fadeIn()
                }
            });

            return !CurrentActive;
        }

        function ClosePanel(Panel) {
            if (Panel != '#BNOPanel')
                if (!BNOPanelActive)
                    $('#BNOPanel').click();
            if (Panel != '#TVPanel')
                if (!TVPanelActive)
                    $('#TVPanel').click();
            if (Panel != '#DCPanel')
                if (!DCPanelActive)
                    $('#DCPanel').click();
            if (Panel != '#MGPanel')
                if (!MGPanelActive)
                    $('#MGPanel').click();
            if (Panel != '#CTPanel')
                if (!CTPanelActive)
                    $('#CTPanel').click();
        }

        function FuncAddTipVisitToolTips() {
            $('.TipVisit').each(function () {
                $(this).attr("data-toggle", "tooltip")
                    .attr("data-placement", "bottom")
                    .attr("title", "Unconfirmed").tooltip();
            });
        }

        /* Dynamic Portal Style Updates */
        function portalStyleUpdates() {
            if (portalStyle == "Streets") {
                // No Action Required from Grading Screen (J1)
                //binNotOutTileUpdate();
                // Parked Cars, Road Works, Blocked Gully (J2)
                //contaminationTileUpdate();
                // change to icon, "fa fa-exclamation Chart-Icon" (J3)
                binChartIconUpdate();

                $('#NGPanel').show();


                $('#BinsCollectedIcon').css('left', '47%');
                $('#BNOPanel').remove();
                $('#CTPanel').remove();
                $('#BinsCollectedIcon').attr('title', 'Total Sections Complete');
                $('#StreetsCollectedIcon').attr('title', 'Total Streets Complete');
            }
            else {
                $('#NGPanel').remove();

            }
        }

        function binNotOutTileUpdate() {
            var element = document.getElementById("binNotOutTile");
            // set inner html
            element.innerHTML = "No Action Required from Grading Screen";
            //set class style
            element.classList.add("streets-tile");
            // still need to add this class to css
        }

        //function nonGradingIssuesTileUpdate() {
        //    var element = document.getElementById("nonGradingTile");
        //    // set inner html
        //    element.innerHTML = "No Action Required from Grading Screen";
        //    //set class style
        //    element.classList.add("streets-tile");
        //    // still need to add this class to css
        //}

        function contaminationTileUpdate() {
            var element = document.getElementById("contaminationTile");
            // set inner html
            element.innerHTML = "Parked Cars, Road Works, Blocked Gully";
            //set class style
            element.classList.add("streets-tile");
            // still need to add this class to css
        }

        function binChartIconUpdate() {
            var element = document.getElementById("BinsCollectedIcon");
            element.classList.remove("fa-trash");
            element.classList.add("fa-exclamation");
        }

        function CheckForMulitpleDriverLogins() {
            var success = function (response) {
                var driverOverlapErrors = response.driverOverlapErrors;
                driverOverlapErrors.forEach(function (item) {
                    FuncAddListAlert("One Driver has logged into two different vehicles", item);
                });
            };

            var fail = function (response) {
                console.log("Failed to check for drivers logged into multiple vehicles");
            };

            ajax('StateOfTheNation.aspx', 'CheckLoggedInDrivers', { "clientName": client, "date_ddMMyyyy": MD }, success, fail, 60000);

        }

        function ReplaceIssueCodesWithText(data) {
            var convertedDataArray = [];

            data.forEach(function (item) {
                convertedDataArray.push([String(item.key), String(item.value)]);
            });

            var success = function (response) {
                var convertedData = response.convertedData;
                var keyValuePairs = [];
                convertedData.forEach(function (item) {
                    keyValuePairs.push({ key: item[0], value: parseInt(item[1]) });
                });

                CreateIssueTypeChart(keyValuePairs);
            };

            var fail = function (response) {
                // inputed data will be returned
                CreateIssueTypeChart(data);
            };

            var convertedDataArray = [];

            data.forEach(function (item) {
                convertedDataArray.push([String(item.key), String(item.value)]);
            });

            ajax('StateOfTheNation.aspx', 'ReplaceIssueCodesWithIssueText', { "clientName": client, "data": convertedDataArray }, success, fail, 60000);
        }

    </script>
</head>

<body id="body">
    <div id="Loader" class="LoaderBody">
        <div class="LogoSquare">
            <div class="LogoOutLine"></div>
        </div>
        <div class="LogoCircle">
        </div>
        <div class="LoaderText" id="Div2">
            <div id="LoaderMessage"></div>
        </div>
    </div>

    <div id="CriticalWarning" class="alert alert-danger" style="margin: auto; width: 98%; margin-top: 10px; display: none;">
        <span id="CriticalText">
            <strong>Warning!</strong> No client selected.
        </span>
    </div>

    <div id="SOTN_Banner" style="width: 100%; height: 42px; background-color: #1e3663; margin-top: 0px; text-align: center; color: white; display: none;">
        <strong style="font-size: 30px; float: left; margin-left: 26px;">InCab Overview</strong>
        <!--<button class="Iconbutton" onclick="openNav()" data-toggle="tooltip" title="Settings">
            <i class="fa fa-sliders" style="font-size: 30px;"></i>
        </button>
        <button type="button" disabled="" class="Iconbutton" onclick="EmailData();" data-toggle="tooltip" title="Email CSV">
            <i class="fa fa-envelope-o" aria-hidden="true" style="font-size: 30px; color: #a3a3a3"></i>
        </button>-->
        <button class="Iconbutton" onclick="ExportData('SOTN_Table');" data-toggle="tooltip" title="Download CSV">
            <i class="fa fa-download" aria-hidden="true" style="font-size: 30px; color: White"></i>
        </button>
        <!--<button type="button" disabled="" class="Iconbutton" data-toggle="tooltip" title="Historic Data">
            <i class="material-icons" style="font-size: 32px; color: #a3a3a3">history</i>
        </button>-->
        <button class="Iconbutton" onclick="FuncShowTableData();" data-toggle="tooltip" title="Table">
            <i class="fa fa-table" style="font-size: 30px; color: White"></i>
        </button>
        <button class="Iconbutton" onclick="FuncShowChartData();" data-toggle="tooltip" title="Charts">
            <i class="fa fa-bar-chart" style="font-size: 30px; color: white"></i>
        </button>
        

        <!--
            <div class="dropdown-container">
            <div class="dropdown-button noselect">
                <div class="dropdown-label">States</div>
                <div class="dropdown-quantity">(<span class="quantity">Any</span>)</div>
                <i class="fa fa-filter"></i>
            </div>
            <div class="dropdown-list" style="display: none;">
                <input type="search" placeholder="Search states" class="dropdown-search"/>
                <ul></ul>
            </div>
        </div>
            </button>
        <button onclick="RoundCompletionBarClick('Copeland0024Base-MTue1Rec');"> 
            Test
        </button>
        -->
    </div>

    <div id="SOTN_TableContainer" class="container" style="display: flex; display: none; background-color: #efefef">
        <div class="Tile-Full" style="flex: 100%; height: auto; width: auto">
        <div id="SOTN_Table_Data" class="containerBody" style="margin: 10px;">
            <table id="SOTN_Table" class="webaspx-table">
                <thead>
                    <!--<tr>
                        <th id="TimeColumn" class="Single-Column"></th>
                        <th colspan="1" style="background-color: inherit;"></th>
                        <th colspan="6" style="-webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;">Collection Data</th>
                        <th colspan="2">Issues</th>
                        <th colspan="2">Crew Data</th>
                    </tr>-->
                    <tr>
                        <th style="-webkit-border-top-left-radius: 0px; border-top-left-radius: 0px;">Driver ID</th>
                        <th>Registration</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Round<%--<br />(Cr D Wk Srv)--%></th>
                        <th>Bins Collected<i class="fa fa-exclamation-circle" data-toggle="tooltip" title="Estimated"></i></th>
                        <th>Bins On Round</th>
                        <th>Streets Collected</th>
                        <th>Streets to Collect</th>
                        <th>Pick Rate<i class="fa fa-exclamation-circle" data-toggle="tooltip" title="Estimated"></i></th>
                        <th>Issues Raised</th>
                        <th>Bin Not Out</th>
                        <th>Time Worked</th>
                        <th>Miles Travelled</th>
                        <th style="visibility: hidden">Average Speed (Mph)</th>
                        <th>Driver Checks<br />(Pass | Fail)</th>
                        <th>Time To Complete <i class="fa fa-exclamation-circle" data-toggle="tooltip" title="Estimated"></i></th>
                        <th>Tip Visits</th>
                        <th>Yield (KG)</th>
                        <th>Round Completion</th>
                        <th id="MainAlert" class="Single-Column" style="display: none; border-radius: 0px;">Alerts</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
            </div>
    </div>

    <div style="background-color: #efefef">
        <div id="SOTN_Chart_Data" class="container" style="display: none;">
            <div class="row">
                <div id="PanelMain" class="PanelMain">
                    <!--<div style="overflow:hidden;">-->
                        <div id="DCPanel" class="Tile">
                            <div class="Tile-Title"> Driver Checks </div>
                            <div id="DCPanelContent" class="Tile-Content">
                                <table id="DCPanelTable" class="webaspx-table" style="table-layout:fixed; overflow:scroll;">
                                    <thead class="Tile-Table-Header">
                                        <tr>
                                            <td>Vehicle</td>
                                            <td>Round</td>
                                            <td>Passed</td>
                                            <td>Failed</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="DCPanelBar">
                                <div class="Tile-Icon" style="margin-bottom: 37px">
                                    <h2 class="Title" style="font-size: 35px; word-spacing: 10px;">
                                    <i class="fa fa-check" aria-hidden="true" style="color:#89c14d"></i> |
                                    <i class="fa fa-times" aria-hidden="true" style="color:red"></i>
                                </h2>
                                </div>
                                <div id="DCPanelValue" class="Tile-Value" style="word-spacing: 50px;"></div>
                            </div>
                        </div>
                        <div id="TVPanel" class="Tile">
                            <div class="Tile-Title"> Tip Visits </div>
                            <div id="TVPanelContent" class="Tile-Content">
                                <table id="TVPanelTable" class="webaspx-table" style="table-layout:fixed; overflow:scroll;">
                                    <thead class="Tile-Table-Header">
                                        <tr>
                                            <td><!--<i class="fa fa-download" aria-hidden="true" style="font-size: 15px;color: White;margin-right: 5px;" onclick="ExportData('TVPanelTable');"></i>-->Vehicle</td>
                                            <td>Round</td>
                                            <td>Reception</td>
                                            <td>Arrival</td>
                                            <td>Visit Duration</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="TVPanelBar">
                                <div class="Tile-Icon" style="margin-bottom: 37px">
                                    <i class="fa fa-recycle" aria-hidden="true" style="font-size: 55px; color:#89c14d"></i>
                                </div>
                                <div id="TVPanelValue" class="Tile-Value"></div>
                            </div>
                        </div>
                        <div id="NGPanel" class="Tile" style="display: none">
                                <div id="nonGradingTile" class="Tile-Title"> Issues </div>
                                <div id="NGPanelContent" class="Tile-Content">
                                    <table id="NGPanelTable" class="webaspx-table" style="table-layout:fixed;">
                                        <thead class="Tile-Table-Header">
                                            <tr>
                                                <td>Vehicle</td>
                                                <td>Round</td>
                                                <td>Issue</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="NGPanelBar">
                                    <div class="Tile-Icon">
                                        <i class="ionicons ion-nuclear" style="font-size: 60px; color:#89c14d"></i>
                                    </div>
                                    <div id="NGPanelValue" class="Tile-Value"></div>
                                </div>
                            </div>
                        <div id="BNOPanel" class="Tile">
                            <div id="binNotOutTile" class="Tile-Title"> Bin Not Out </div>
                            <div id="BNOPanelContent" class="Tile-Content">
                                <table id="BNOPanelTable" class="webaspx-table" style="table-layout:fixed;">
                                    <thead class="Tile-Table-Header">
                                        <tr>
                                            <td>Vehicle</td>
                                            <td>Round</td>
                                            <td>Bin Not Out</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="BNOPanelBar">
                                <div class="Tile-Icon" style="margin-bottom: 45px">
                                    <i class="fa fa-trash" aria-hidden="true" style="color:#89c14d; margin-left: 60px">
                                    <!--<i class="material-icons Tile-Icon" style="font-size: 70px; top: 12px; position: absolute; right: 0%; color: #b91d1d">close</i>--> <!-- Color was black -->
                                        <i class="fa fa-times"              style="font-size: 60px; top: 3px; position: relative; right: 57%; color: #ff0000; display: inline-block"></i>
                                    </i>
                                </div>
                                <div id="BNOPanelValue" class="Tile-Value"></div>
                            </div>
                        </div>
                        <div id="MGPanel" class="Tile">
                            <div class="Tile-Title"> Mileage </div>
                            <div id="MGPanelContent" class="Tile-Content">
                                <table id="MGPanelTable" class="webaspx-table" style="table-layout:fixed;">
                                    <thead class="Tile-Table-Header">
                                        <tr>
                                            <td>Vehicle</td>
                                            <td>Round</td>
                                            <td>Miles Travelled</td>
                                            <td id="MGPanelTableAlerts" style="display:none; width: 0%"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="MGPanelBar">
                                <div class="Tile-Icon" style="margin-bottom: 35px">
                                    <!--<i class="material-icons" style="font-size: 60px; color:#89c14d;">local_gas_station</i>-->
                                    <i>
                                        <svg style="color: #89c14d; top: -5px; width: 48px; position: relative; right: -5px;" viewBox="0 0 512 512"><path fill="currentColor" d="M336 448H16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h320c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm157.2-340.7l-81-81c-6.2-6.2-16.4-6.2-22.6 0l-11.3 11.3c-6.2 6.2-6.2 16.4 0 22.6L416 97.9V160c0 28.1 20.9 51.3 48 55.2V376c0 13.2-10.8 24-24 24s-24-10.8-24-24v-32c0-48.6-39.4-88-88-88h-8V64c0-35.3-28.7-64-64-64H96C60.7 0 32 28.7 32 64v352h288V304h8c22.1 0 40 17.9 40 40v27.8c0 37.7 27 72 64.5 75.9 43 4.3 79.5-29.5 79.5-71.7V152.6c0-17-6.8-33.3-18.8-45.3zM256 192H96V64h160v128z" class=""></path>
                                        </svg>
                                    </i>
                                    <!--<i class="fa fa-gas-pump" style="font-size: 60px; top: 3px; position: relative; right: 57%; color: #ff0000; display: inline-block"></i>-->
                                </div>
                                <div id="MGPanelValue" class="Tile-Value"></div>
                            </div>
                        </div>
                        <div id="CTPanel" class="Tile">
                            <div id="contaminationTile" class="Tile-Title"> Contamination </div>
                            <div id="CTPanelContent" class="Tile-Content">
                                <table id="CTPanelTable" class="webaspx-table" style="table-layout:fixed;">
                                    <thead class="Tile-Table-Header">
                                        <tr>
                                            <td>Vehicle</td>
                                            <td>Round</td>
                                            <td>Contamination Issues</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="CTPanelBar">
                                <div class="Tile-Icon">
                                    <i class="ionicons ion-nuclear" style="font-size: 60px; color:#89c14d"></i>
                                </div>
                                <div id="CTPanelValue" class="Tile-Value"></div>
                            </div>
                        </div>

                        <!--<div class="Tile-Full" style="float: right">
                            <div class="Tile-Icon">
                                <i class="fa fa-question" style="font-size: 70px; color: black"></i>
                                <div style="position: absolute; left: 57%; font-size: 35px; top: 57px; width: 30px; height: 30px; background-color: gray;">
                                    <p style="color: white; margin-top: -9px; height: 30px; margin-left: 20%; position: absolute;">4</p>
                                </div>
                            </div>
                        </div>-->
                        <div id="VehiclesOutTile" class="Tile-Full" style="float: right; flex: 45%; margin-top: 10px"><!--65b6f7, a7a7a7, 89c14d-->
                            <div id="VehiclesOut" class="col Vehicle-Parent">
                            </div>
                        </div>
                    <!--</div>-->
                </div>
            </div>
            <%--<hr />--%>
            <div class="row">
                <div id="RoundCompletionPanel" class="Tile-Full" style="height: 330px; flex: 20%;">
                    <!--<i id="RoundCompletionAlert" class="ionicons ion-alert Alert-Chart" data-toggle="tooltip" title="Fleet Performance" style="display: none;"></i>-->
                    <div id="RoundCompletionChart" style="height: 110%; margin-right: 4%; margin-top: 10px"></div>
                </div>
                <div id="ChartsPanel" class="Tile-Full" style="height: 330px; flex: 58%;">
                    <div id="BinsCollectedPanel" class="col-md-4">
                        <div id="BinsCollectedChart" class="ChartScale BorderLeft2">
                            <span id="BinsCollectedIcon" class="fa fa-trash Chart-Icon" data-toggle="tooltip" title="Total Properties Collected"></span>
                        </div>
                    </div>
                    <div class="col-md-4 BorderColorPanel">
                        <div id="StreetsCollectedChart" class="ChartScale BorderLeft2">                        
                            <span id="StreetsCollectedIcon" class="glyphicon glyphicon-road Chart-Icon" data-toggle="tooltip" title="Total Streets Collected"></span>
                        </div>
                    </div>
                    <div class="col-md-4 BorderColorPanel">
                        <div id="IssueOccuranceChart" class="ChartScale BorderLeft2">
                            <span id="IssueOccuranceChartIcon" class="fa fa-exclamation-triangle Chart-Icon" data-toggle="tooltip" title="Issues By Type"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--<hr />--%>
            <div class="row">
                <div id="Alerts-Tile" class="Tile-Full" style="flex: 58%; height: 330px !important; max-height: 330px; table-layout:fixed; overflow-y: auto;">
                    <h1 id="Alerts-None" style="width: 100%; color: #979797; position: relative; top: 50%; transform: translateY(-125%);">No Problems Detected!</h1>
                    <h1 id="Alerts-Some" style="width: 100%; color: #979797; position: absolute; top: 50%; transform: translateY(-125%); display: none">Alerts</h1>
                    <ul id="Alerts-List" style="list-style: none; margin-top: 10px; max-height: unset">
                    </ul>
                </div>
                <div id="CKPanel" class="Tile-Full" style="flex: auto; height: 330px;">
                    <h1 id="LastVehicleTime" style="color: #979797; position: absolute; top: 92.5%; transform: translateY(-125%); width: 100%;"></h1>
                    <canvas id="Clock1" width="290" height="290" style="margin-left: 23px; margin-top: 2%" data-toggle="tooltip" data-placement="top" title="Estimated Last Vehicle Finish"></canvas> 
                </div>
            </div>
        </div>
    </div>
</body>
</html>
