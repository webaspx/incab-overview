﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Webaspx.Database;

namespace InCabOverview
{
    // TODO What is this called?
    public enum PortalStyle
    {
        Waste,
        Streets
    }

    public class SOTN_Unit
    {
        private readonly PortalStyle _portalStyle;
        public readonly TimeSpan StartTime;
        public readonly TimeSpan EndTime;

        private TimeSpan ProductiveTime;

        //private TimeSpan UnproductiveTime;

        private Double AverageUnproductiveSpeed;

        private Double AverageProductiveSpeed;

        private List<Property> collectedProperties;

        /* Class Properties*/

        public String DriverID;

        public String Registration;

        public Round Round;

        public TimeSpan HoursWorked;

        public Int32 BinsCollected;

        public Int32 BinsOnRound;

        public Int32 IssuesRaised;

        public Int32 BinNotOut;

        public Double MilesTravelled;

        public Int32 StreetsCollected;

        public Int32 StreetsUncollected;

        public DateTime TimeToComplete;

        public Double PickRate;

        public Int32 Completion;

        public Int32 TotalContamination;

        public Double Speed;

        public Int32[] DriverChecks;

        public String[] DriverCheckFails;

        public Int32 BinsUncollected;

        public ReceptionVisit[] TipVisits;

        public Int32 TotalYield;

        public Dictionary<String, Int32> IssueOccurances;

        public DateTime EstimatedFinish;

        public int NonGradingIssues;
        public List<string> Issues = new List<string>();

        public DateTime LoginTime;

        public SOTN_Unit(List<Facility> Facilities,
                         LogIn SignIn,
                         string Client,
                         List<LogIn> DuplicateLogIns,
                         Dictionary<string, HouseInformation> postCodes,
                         string Date,
                         PortalStyle portalStyle)
        {
            this._portalStyle = portalStyle;

            this.DriverID = SignIn.DriverID;

            var date = new DateTime(Convert.ToInt32(Date.Substring(4, 4)), Convert.ToInt32(Date.Substring(2, 2)), Convert.ToInt32(Date.Substring(0, 2)));

            this.Registration = SignIn.Registration;

            this.Round = SignIn.Round;

            this.StartTime = SignIn.StartTime;
            this.EndTime = SignIn.EndTime;

            // Gets all messages that where the [UserID] matches the SignIn.DriverID for date
            DataTable VehicleData = GetVehicleData(SignIn.DriverID, Client, date);

            // Gets the following property information from [InCabSchedule]
            // [StreetID], [PropertyID], [Lat], [Lon]
            DataTable RoundData = GetRoundData(Round, Client, portalStyle);

            if (VehicleData == null || RoundData == null)
            {
                return;
            }

            // Parse the raw VehicleData into Message classes for easier access
            List<Message> AllMessages = FuncParseMessage(VehicleData, SignIn.StartTime, SignIn.EndTime);
            List<Message> ConfirmedMessages;

            //// If there's duplicate logins for same round
            //// Find find the start time and end times
            //// So we can establish a time range we know they operated
            //// TODO refactor this
            //if (DuplicateLogIns.Count > 1)
            //{
            //    Int32 StartIndex = DuplicateLogIns.IndexOf(SignIn);
            //
            //    DateTime StartTime = DuplicateLogIns[StartIndex].Time;
            //    DateTime EndTime;
            //
            //    if (StartIndex == DuplicateLogIns.Count - 1)
            //    {
            //        EndTime = DateTime.Now;
            //    }
            //    else
            //    {
            //        Int32 EndIndex = StartIndex + 1; // Not just + 1, but Time Ranges for when he's doing a round
            //
            //        EndTime = DuplicateLogIns[EndIndex].Time;
            //
            //        // May be wrong - Quick Fix
            //        StartTime = DuplicateLogIns[EndIndex].Time;
            //
            //        EndTime = DateTime.Now;
            //    }
            //
            //    this.StartTime = StartTime;
            //
            //    this.EndTime = EndTime;
            //
            //    // Using the start time and end time, try to confirm the messages
            //    ConfirmedMessages = FuncConfirmedMessages(AllMessages, SignIn, StartTime, EndTime);
            //}
            //else
            //{
            //    // If there's no duplicate logins, then the time range is just the entire day
            //    ConfirmedMessages = FuncConfirmedMessages(AllMessages, SignIn);
            //}

            ConfirmedMessages = FuncConfirmedMessages(AllMessages, SignIn, SignIn.StartTime, SignIn.EndTime);

            // Create a GPS trail using the confirmed messages
            List<Coordinate> GPSTrail = FuncGPSTrail(ConfirmedMessages); // Doesn't stop multiple driverids

            // If there's basically no data, then stop
            //if (GPSTrail.Count < 2)
            //{
            //    return;
            //}

            this.HoursWorked = FuncHoursWorked(ConfirmedMessages);

            // Miles travelled is just a sum of crow flies distance between each point
            this.MilesTravelled = FuncMilesTravelled(GPSTrail);

            // Issues Raised is just a count of messages that have the type of "Issue"
            this.IssuesRaised = FuncIssuesRaised(AllMessages);

            // PortalStyle is used to decide whether to get Streets data or conventional Collection data
            if (portalStyle == PortalStyle.Streets)
            {
                // Work in progress
                // TODO Talk to Dave about what's needed for this
                //var xd = RoundData.AsEnumerable().Select(x => string.Join(",", x.ItemArray.StringArray()));

                // Gets all the properties on the round (returns List<Property>)
                var streetsSections = FuncPropertiesOnRound(RoundData);

                // Calculates the collected properties based off GPS trail and driver confirmation
                collectedProperties = GetCollectedProperties(AllMessages, GPSTrail, streetsSections);//, Client, date.ToString("ddMMyyyy"));

                this.BinsOnRound = streetsSections.Count;

                this.BinsCollected = GetBinsCollected(collectedProperties);

                this.StreetsUncollected = FuncStreetsOnRound(streetsSections);

                this.BinsUncollected = this.BinsOnRound - this.BinsCollected;

            }
            else
            {
                // Gets all the properties on the round (returns List<Property>)
                var PropertiesOnRound = FuncPropertiesOnRound(RoundData);

                // Calculates the collected properties based off GPS trail and driver confirmation
                collectedProperties = GetCollectedProperties(AllMessages, GPSTrail, PropertiesOnRound);//, Client, date.ToString("ddMMyyyy"));

                this.BinsCollected = GetBinsCollected(collectedProperties); // Probably not accurate

                // Not accurate, need to actually count the bins (from Collections, if they have it)
                this.BinsOnRound = PropertiesOnRound.Count;

                // Get all issues (and "unissues"/cancelled issues)
                var allIssues = GetAllIssues(SignIn.Time, Client).ToList();

                foreach (var issue in allIssues)
                {
                    // issue.PostCode and issue.Town isn't populated from the message in the [Messages] table
                    // So we need to add those details if we have them
                    if (postCodes.ContainsKey(issue.UPRN))
                    {
                        issue.PostCode = postCodes[issue.UPRN].PostCode;
                        issue.Town = postCodes[issue.UPRN].Town.TitleCase();
                    }
                    else
                    {
                        issue.PostCode = "";
                    }

                    // Update the hashcode, (ie the unique identifier for the fields in the issue class)
                    issue.GetHashCode();
                }

                // Remove duplicted issues and cancelled issues (from the the driver)
                allIssues = RemoveDuplicateIssues(allIssues);
                allIssues = RemoveCancelledIssues(allIssues);

                var round = new Round(SignIn.Round.Day, SignIn.Round.Week.Replace("Wk ", ""), SignIn.Round.Service, SignIn.Round.Crew.Replace("Cr", ""));
                var user = SignIn.DriverID.Replace(Client, "").Replace("Base", "");

                

                // Only get issues that have been sent from this driver on this round (probably redundant but too scred to change)
                var issuesForVehicle = allIssues.Where(x => x.Time.Date == SignIn.Time.Date && x.Time.TimeOfDay >= SignIn.StartTime && x.Time.TimeOfDay <= SignIn.EndTime && x.Round.Hash == round.Hash && x.User == user).ToList();

                // We only want Bin Not Out issues for the date given
                var bnoMessages = issuesForVehicle.Where(x => x.IssueCode == "BNO" || x.IssueText == "Bin Not Out").ToList();

                // Then count by the round and the driver ID
                //int count = bnoMessages.Count(x => x.Round.Hash == round.Hash && x.User == SignIn.DriverID.Replace(Client, "").Replace("Base", ""));

                this.BinNotOut = bnoMessages.Count();

                // Count total contamination issues
                this.TotalContamination = issuesForVehicle.Count(x => x.IssueCode == "Contamination");

                // Count the nummber of streets on the round
                this.StreetsUncollected = FuncStreetsOnRound(PropertiesOnRound);

                this.BinsUncollected = this.BinsOnRound - this.BinsCollected;
            }

            // Pick rate is the bins collected
            // Divided by
            // Productive time (time they've spent at less than 5mph)
            this.PickRate = FuncPickRate(BinsCollected);

            // Overall round complete as % (BinsCollected/BinsOnRound)
            this.Completion = FuncRoundCompletion();

            // Average speed of their entire work day
            this.Speed = FuncAverageSpeed(GPSTrail);

            // int[], [0] is the number of checks passed, [1] is checks failed
            this.DriverChecks = FuncDriverChecks(AllMessages, Client);

            // KeyValuePair of issue category (ie Common, Contamination, Damage, Other, etc)
            this.IssueOccurances = FuncIssueOccurences(AllMessages, Client);

            try
            {
                // Get Tip Visits, uses GPS trail.
                // It is sometimes prone to failure, email a developer with the fault
                this.TipVisits = FuncReceptionVisits(AllMessages, GPSTrail, Facilities);
            }
            catch (Exception Exception)
            {
                var ErrorMessage = $"DriverID: {this.DriverID}\r\n";

                var error = new SOTN_Error(Client, ErrorMessage, Exception, DateTime.Now, SignIn.Time.ToString("dd/MM/yyyy"));

                using (var smtpClient = new SmtpClient()
                {
                    Port = 587,
                    Host = "smtp.gmail.com",
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Timeout = 12000,
                    Credentials = new NetworkCredential("nreply@routeware.com", "qxHb5419NRz%")
                })
                {
                    using (var email = new MailMessage("nreply@routeware.com", "Calvin.StLouis@webaspx.com"))
                    {
                        email.Subject = "SOTN Error";

                        email.Body = $"Error found in {error.Client} SOTN at {error.Time}\r\n\r\n{error.Exception.Message}\r\nLine: {new StackTrace(error.Exception, true).GetFrame(0).GetFileLineNumber()}\r\n{error.DateRequested}\r\n{error.Message}";

                        smtpClient.Send(email);
                    }
                }
            }

            // If the client has opted into using TipTickets for the drivers, get the sum of the yield
            this.TotalYield = (TipVisits != null) ? TipVisits.Sum(x => x.Yield) : 0;

            // Calculate the TimeToComplete (based on bins left to collect and pickrate etc)
            if (collectedProperties.Count > 0)
            {
                this.TimeToComplete = FuncTimeToComplete(AllMessages, Facilities, /*SignIn,*/ collectedProperties.Last());
            }
            else
            {
                this.TimeToComplete = DateTime.Today;
            }

            TimeSpan TimeFromNow = DateTime.Now.TimeOfDay + this.TimeToComplete.TimeOfDay;

            this.LoginTime = SignIn.Time;

            this.EstimatedFinish = DateTime.Today + TimeFromNow;
        }

        public SOTN_Unit()
        {
        }

        // Returns all data submitted by the driver
        private DataTable GetVehicleData(string driverID, string client, DateTime date)
        {
            //CONVERT(VARCHAR(255), @User)
            //string query = @"SELECT
            //                     *
            //                 FROM
            //                     [Messages]
            //                 WHERE
            //                     [User1] = @User
            //                     AND
            //                     [Date] LIKE CONVERT(VARCHAR(255), @Date)
            //                 ORDER BY
            //                     [Time1]
            //                         DESC";

            string query = @"SELECT
                                 *
                             FROM
                                 [MessagesSplit]
                             WHERE
                                 [User1] = @User
                                 AND
                                 [Date1YYYYMMDD] = CONVERT(VARCHAR(8), @Date)
                                 AND
                                 [Time1Decimal] BETWEEN @startTime and @endTime
                             ORDER BY
                                 [Time1Decimal] DESC";

            var parameters = new SqlParameter[] {
                new SqlParameter("User", driverID),
                new SqlParameter("Date", date.ToString("yyyyMMdd")),
                new SqlParameter("startTime", StartTime.TotalHours),
                new SqlParameter("endTime", EndTime.TotalHours)
            };

            parameters[1].SqlDbType = SqlDbType.VarChar;

            return Database.Read(query, client, parameters: parameters);
        }

        // Converts database entries to Message class
        private List<Message> FuncParseMessage(DataTable vehicleData, TimeSpan startTime, TimeSpan endTime)
        {
            var AllMessages = new List<Message>();

            foreach (DataRow Row in vehicleData.Rows)
            {
                AllMessages.Add(new Message(Row));
            }

            var test = AllMessages.Where(x => x.Time.TimeOfDay >= startTime && x.Time.TimeOfDay < endTime)
                                  .ToList();

            return test;
        }

        // Returns all messages which can be uniquely verified. By round or registration
        private List<Message> FuncConfirmedMessages(List<Message> AllMessages, LogIn SignIn)
        {
            var ConfirmedMessages = new List<Message>();

            foreach (Message Message in AllMessages)
            {
                if (Message.Identifier != IdentifierType.None)
                {
                    if (Message.Identifier == IdentifierType.Registration)
                    {
                        if (Message.Registation == SignIn.Registration)
                        {
                            ConfirmedMessages.Add(Message);
                        }
                    }
                    else if (Message.Identifier == IdentifierType.Round)
                    {
                        if (SOTN_Extensions.ContainsRoundData(Message/*, SignIn.Round)*/))
                        {
                            ConfirmedMessages.Add(Message);
                        }
                    }
                    else if (Message.Identifier == IdentifierType.RegistrationRound)
                    {
                        if (Message.Registation == SignIn.Registration)
                        {
                            ConfirmedMessages.Add(Message);
                        }
                        else if (SOTN_Extensions.ContainsRoundData(Message/*, SignIn.Round*/))
                        {
                            ConfirmedMessages.Add(Message);
                        }
                    }
                }
            }

            return ConfirmedMessages;

            /* TODO 1: Find a way of confirming a driver check message with a IN message*/

            /*
            List<Message> NotConfirmed = AllMessages.Except(ConfirmedMessages).ToList();

            List<Message> DistinctDriverChecks = NotConfirmed.Where(x => x.MessageType == MessageType.DriverCheck)/*Working distinct statement*-/.GroupBy(x => x.Sent).Select(x => x.First()).ToList();

            List<Message> DistinctLogins = ConfirmedMessages.Where(x => x.MessageType == MessageType.LogIn)/*Working distinct statement*-/.GroupBy(x => x.Sent).Select(x => x.First()).ToList();

            for (int i = 0; i < DistinctLogins.Count; i++)
            {
                if (DistinctLogins[i].Sent >= DistinctDriverChecks[i].Sent.AddSeconds(-30) && DistinctLogins[i].Sent <= DistinctDriverChecks[i].Sent.AddSeconds(30))
                {

                }
            }
            */
        }

        // Returns all messages which can be uniquely verified. By round or registration
        private List<Message> FuncConfirmedMessages(List<Message> AllMessages, LogIn SignIn, TimeSpan startTime, TimeSpan endTime)
        {
            var ConfirmedMessages = new List<Message>();

            foreach (Message Message in AllMessages)
            {
                if (Message.Time.TimeOfDay >= startTime && Message.Time.TimeOfDay <= endTime)
                {
                    if (Message.Identifier != IdentifierType.None)
                    {
                        if (Message.Identifier == IdentifierType.Registration)
                        {
                            if (Message.Registation == SignIn.Registration)
                            {
                                ConfirmedMessages.Add(Message);
                            }
                        }
                        else if (Message.Identifier == IdentifierType.Round)
                        {
                            if (SOTN_Extensions.ContainsRoundData(Message/*, SignIn.Round*/))
                            {
                                ConfirmedMessages.Add(Message);
                            }
                        }
                        else if (Message.Identifier == IdentifierType.RegistrationRound)
                        {
                            if (Message.Registation == SignIn.Registration)
                            {
                                ConfirmedMessages.Add(Message);
                            }
                            else if (SOTN_Extensions.ContainsRoundData(Message/*, SignIn.Round*/))
                            {
                                ConfirmedMessages.Add(Message);
                            }
                        }
                    }
                }
            }

            return ConfirmedMessages;
        }

        // Returns all properties on the current round
        private DataTable GetRoundData(Round activeRound, string client, PortalStyle portalStyle)
        {
            string query = @"SELECT
                                 [StreetID],
                                 [PropertyID],
                                 [Lat],
                                 [Lon]
                             FROM
                                 [InCabSchedule]
                             WHERE
                                 [Service] LIKE @Service 
                             AND 
                                 [Day] LIKE @Day
                             AND 
                                 [Week] LIKE @Week 
                             AND 
                                 [Crew] = @Crew";

            string week = activeRound.Week.Replace("Wk", "Week");

            var parameters = new SqlParameter[] {
                new SqlParameter("Service", $"{activeRound.Service}%"),
                new SqlParameter("Day", $"{activeRound.Day.Substring(0, 3)}%"),
                new SqlParameter("Week", (portalStyle == PortalStyle.Waste) ? $"%{week}" : week.Replace("Week ")),
                new SqlParameter("Crew", $"{activeRound.Crew.Replace("Cr", "")}")
            };

            return Database.Read(query, client, parameters: parameters);
        }

        //Returns a list of coordinates including time and speed ~
        private List<Coordinate> FuncGPSTrail(List<Message> AllMessages)
        {
            var GPSMessages = AllMessages.Where(x => x.MessageType == MessageType.GPS).ToList();
            var GPSTrail = new List<Coordinate>();

            //foreach (Message GPS in GPSMessages)
            //{
            //    GPSTrail.Add(new Coordinate(GPS.Coordinate.Longitude, GPS.Coordinate.Latitude));
            //}

            Double SecondsProductive = 0;

            Double SecondsUnproductive = 0;

            var UnproductiveSpeeds = new List<Double>();
            var ProductiveSpeeds = new List<Double>();

            for (int i = 0; i < GPSMessages.Count - 1; i++)
            {
                Double Distance = GPSMessages[i].Coordinate.GetDistanceTo(GPSMessages[i + 1].Coordinate); // In Meters

                Double Time = (GPSMessages[i].Time - GPSMessages[i + 1].Time).Seconds; // Seconds

                Double Speed = (Distance / Time); // M/s

                Double KMh = ((Speed * 18) / 5); // Km/h

                if (KMh <= 5)
                {
                    SecondsProductive += Time;

                    ProductiveSpeeds.Add(KMh);
                }
                else
                {
                    SecondsUnproductive += Time;

                    UnproductiveSpeeds.Add(KMh);
                }

                var coordinate = new Coordinate(GPSMessages[i].Coordinate.Longitude, GPSMessages[i].Coordinate.Latitude)
                {
                    Speed = KMh,
                    Time = GPSMessages[i].Time
                };

                GPSTrail.Add(coordinate);
            }

            UnproductiveSpeeds = UnproductiveSpeeds.Where(x => !Double.IsNaN(x) && !Double.IsInfinity(x)).ToList();

            if (UnproductiveSpeeds.Count > 0)
            {
                this.AverageUnproductiveSpeed = UnproductiveSpeeds.Average();
            }

            if (ProductiveSpeeds.Count > 0)
            {
                this.AverageProductiveSpeed = ProductiveSpeeds.Average();
            }

            this.ProductiveTime = TimeSpan.FromMinutes(SecondsProductive / 60);

            //this.UnproductiveTime= TimeSpan.FromMinutes(SecondsUnproductive / 60);

            return GPSTrail;
        }

        // Returns a formatted list of all properties on the curent round
        private List<Property> FuncPropertiesOnRound(DataTable RoundData)
        {
            var PropertiesOnRound = new List<Property>();

            for (int i = 0; i < RoundData.Rows.Count; i++)
            {
                String[] Data = RoundData.Rows[i].ItemArray.StringArray();

                var property = new Property()
                {
                    StreetID = Data[0],
                    PropertyID = Data[1],
                    Coordinate = new Coordinate(Convert.ToDouble(Data[3]), Convert.ToDouble(Data[2]))
                };

                PropertiesOnRound.Add(property);
            }

            return PropertiesOnRound;
        }

        //Total Hours worked. Compensates for when the vehicle isn't online
        private TimeSpan FuncHoursWorked(List<Message> ConfirmedMessages)
        {
            var SignIns = ConfirmedMessages.Where(x => x.MessageType == MessageType.INLogin).ToList();

            Double TotalTime;

            if (ConfirmedMessages.Count >= 1 && SignIns.Count > 0)
            {
                TotalTime = (ConfirmedMessages.First().Time - SignIns.Last().Time).TotalHours;
            }
            else
            {
                TotalTime = (this.StartTime - this.EndTime).TotalHours;
            }

            if (SignIns.Count > 1)
            {
                for (int i = 0; i < SignIns.Count - 1; i++)
                {
                    if (i > 0)
                    {
                        TotalTime -= (SignIns[i].Time - (ConfirmedMessages[ConfirmedMessages.IndexOf(SignIns[i]) - 1].Time)).TotalHours;
                    }
                }
            }

            if (TotalTime < 0)
            {
                TotalTime *= -1;
            }

            return TimeSpan.FromHours(TotalTime).StripMS();
        }

        private int GetBinsCollected(List<Property> collectedProperties)
        {
            return collectedProperties.Count();
        }

        // Calculates the miles travelled by the vehicle from GPS trail
        private Double FuncMilesTravelled(List<Coordinate> GPSTrail)
        {
            Double Distance = 0;

            for (int i = 0; i < GPSTrail.Count - 1; i++)
            {
                // GetDistanceTo returns in meters
                Distance += GPSTrail[i].GetDistanceTo(GPSTrail[i + 1]);
            }

            // Converts the distance to miles
            return Math.Round(((Distance / 1000) * 0.621371), 2);
        }

        // Returns the number of issues raised
        private Int32 FuncIssuesRaised(List<Message> AllMessages)
        {
            return AllMessages.Count(x => x.MessageType == MessageType.Issue);
        }

        /// <summary>
        /// Queries the given SQL database and returns a DataTable of the result.
        /// </summary>
        public static DataTable Read(string client, string query, SqlParameter[] parameters = null, Catalog catalog = Catalog.Incab)
        {
            string callerName = new StackTrace().GetFrame(1).GetMethod().Name;

            Console.WriteLine(string.Format("{0} - {1} - {2}", DateTime.Now, client, callerName));

            using (var connection = new DatabaseConnection(client, catalog, 600))
            {
                using (var command = new SqlCommand(query))
                {
                    // Seconds
                    command.CommandTimeout = 300;

                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }

                    try
                    {
                        return connection.ExecuteDataTable(command);
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }


        public static IEnumerable<Issue> GetAllIssues(DateTime date, string client/*, Dictionary<string, List<IssueType>> issueTypes*/)
        {
            var parameters = new SqlParameter[] {
                new SqlParameter("StartDate", date.ToString("yyyy-MM-dd")),
                new SqlParameter("EndDate", date.AddDays(1).ToString("yyyy-MM-dd"))
            };

            //date.ToString();
            //var parameters = new SqlParameter[] {
            //    new SqlParameter("StartDate", new DateTime(2020, 11, 24, 00, 00, 00).Date.ToString("yyyy/MM/dd HH:mm:ss")),
            //    new SqlParameter("EndDate", new DateTime(2020, 11, 28, 00, 00, 59).Date.ToString("yyyy/MM/dd HH:mm:ss"))
            //};


            const string query = @"SELECT
                                       [OriginalID],
                                       [MessageType],
                                       [DateTimeYYYYMMDDhhmmss],
                                       [Lat],
                                       [Lon],
                                       [Registration],
                                       [DriverID],
                                       [RoundCrew],
                                       [RoundDay],
                                       [RoundWeek],
                                       [RoundService],
                                       [UPRN],
                                       [IssueType],
                                       [Issue],
                                       [IssueCode],
                                       [HouseName],
                                       [StreetName]
                                   FROM 
                                       [MessagesSplit] 
                                   WHERE 
                                       ([MessageType] = 'Issue' OR [MessageType] = 'Unissue')
                                   AND 
                                       [DateTimeYYYYMMDDhhmmss] 
                                           BETWEEN 
                                               @StartDate 
                                           AND 
                                               @EndDate";

            var result = Read(client, query, parameters);

            return result?.Rows.Cast<DataRow>().Select(x => new Issue(x)).ToArray();

            //xd1.Count();
            //
            //var xd = result?.Rows.Cast<DataRow>().Select(x => new Message(x, issueTypes, true));//.Where(x => x.Issue.IssueCode == "BNO" || x.Issue.IssueText == "Bin Not Out");
            //return xd;
            //xd.Count();
            //
            //return result?.Rows.Cast<DataRow>().Count(x => x["IssueCode"].Equals("BNO") || x["Issue"].Equals("Bin Not Out")) ?? 0;
        }

        //// Returns number of bin not out issues logged
        //private Int32 FuncBinNotOut(List<Message> AllMessages)
        //{
        //    return AllMessages.Count(x => x.Issue?.IssueCode == "BNO" || x.Issue?.IssueText == "Bin Not Out");
        //}

        // Estimated bins collected per hour
        private Double FuncPickRate(Int32 BinsCollected)
        {
            return (BinsCollected == 0) ? 0 : Math.Round((BinsCollected / ProductiveTime.TotalHours), 2);
        }

        /* WIP */

        private DateTime FuncTimeToComplete(List<Message> AllMessages, List<Facility> Facilities, /*LogIn SignIn,*/ Property LastPropertyOnRound)
        {
            /* Basically how this function works
             * _________________________________
             * |             Pickrate          |
             * | ------------------------------|
             * | Number of bins left to collect|
             * |_______________________________|
             *                 +                
             * _________________________________
             * |                               |
             * |          Time To Tip          |
             * |  (Ave. Unprod. Spd * Distance)| *Direct Line, not road distance
             * |               +               |
             * |            ~ 10m              |
             * |_______________________________|
             *                 +
             * _________________________________
             * |                               |
             * |         Time To Depot         |
             * |  (Ave. Unprod. Spd * Distance)| *Direct Line, not road distance
             * |_______________________________|
             *                 =
             *           Time To Complete
             */

            var TimeToComplete = TimeSpan.FromMinutes(0);

            //if (collectedProperties.Contains(LastPropertyOnRound)) // Last property has been collected
            //{
            //    if (this.Completion >= 95) // Fo' sure-ness
            //    {
            //    }
            //
            //    // Still don't know what time it was collected though.
            //}

            if (this.Completion >= 90) // 90% Complete with a round
            {
                // Has the vehicle been to the tip since completing the round
                //Boolean FinalTipComplete = false; 

                // Get time the last property was collected.
                DateTime LastPropertyCollected = LastPropertyOnRound.TimeCollected;

                foreach (ReceptionVisit TipVisit in this.TipVisits)
                {
                    if (TipVisit.Enter > LastPropertyCollected)
                    {
                        //FinalTipComplete = true;

                        break;
                    }
                }
            }


            if (PickRate > 0)
            {
                // Estimated time remaining collecting
                var TimeCollecting = TimeSpan.FromHours((this.BinsOnRound - this.BinsCollected) / this.PickRate);

                TimeToComplete += TimeCollecting;
            }
            else
            {
                // TODO 3: Add additional time to this.
            }

            Message MostRecentMessage = AllMessages.FirstOrDefault(x => x.Coordinate != null);

            var Receptions = Facilities.Where(x => x.FacilityType == "Reception").ToList();

            #region Recepetion Time

            // Assumes the driver will go to the nearest (from his current location) tip to empty the vehicle
            var NearestReception = Receptions.OrderBy(x => x.FacilityCoordinate.GetDistanceTo(MostRecentMessage.Coordinate)).FirstOrDefault();

            Double DistanceToNearestReception = (NearestReception.FacilityCoordinate.GetDistanceTo(MostRecentMessage.Coordinate) / 1000);

            #region Depot Nearest To The Nearest Tip
            //Facility NearestDepot = Depot.OrderByDescending(x => x.FacilityCoordinate.GetDistanceTo(NearestReception.FacilityCoordinate)).FirstOrDefault(); // Depot nearest the tip
            //Double DistanceToNearestDepot = (NearestDepot.FacilityCoordinate.GetDistanceTo(MostRecentMessage.Coordinate) / 1000);
            #endregion

            if (DistanceToNearestReception > 0.25)
            {
                if (this.AverageUnproductiveSpeed == 0)
                {
                    this.AverageUnproductiveSpeed = 1;
                }

                var TimeToReception = TimeSpan.FromHours(DistanceToNearestReception / this.AverageUnproductiveSpeed);

                // Add 15 minutes for time to complete the tip
                TimeToReception += TimeSpan.FromMinutes(15);

                // Add it to the overal TimeToComplete
                TimeToComplete += TimeToReception;
            }

            #endregion

            #region Depot Time

            // Get first login message sent (because 99% of the time, they'll be starting from the depot)
            Message LogInMessage = AllMessages.Where(x => x.Coordinate != null)
                                              .Where(y => y.MessageType == MessageType.DriverCheck)
                                              .OrderBy(x => x.Time)
                                              .FirstOrDefault();

            if (LogInMessage == null)
            {
                Message FirstLogIn = AllMessages.Last(x => x.MessageType == MessageType.INLogin);

                for (int i = AllMessages.IndexOf(FirstLogIn) - 1; i < AllMessages.Count; i++)
                {
                    if (AllMessages[i].Coordinate != null)
                    {
                        LogInMessage = AllMessages[i];

                        break;
                    }
                }
            }

            // Sets this as the starting depot based on LogInMessage
            Facility StartDepot = Facilities.Where(x => x.FacilityType == "Depot")
                                            .OrderBy(x => x.FacilityCoordinate.GetDistanceTo(LogInMessage.Coordinate))
                                            .FirstOrDefault();

            Double DistanceToStartDepot = (NearestReception.FacilityCoordinate.GetDistanceTo(StartDepot.FacilityCoordinate) / 1000);

            // If they're more than 250m away from the depot
            // Calculate time to get there and add it to TimeToComplete
            if (DistanceToStartDepot > 0.25)
            {
                TimeSpan TimeToStartDepot = (this.AverageUnproductiveSpeed <= 0) ? TimeSpan.FromHours(DistanceToStartDepot / this.AverageProductiveSpeed) : TimeSpan.FromHours(DistanceToStartDepot / this.AverageUnproductiveSpeed);

                TimeToComplete += TimeToStartDepot;
            }

            #endregion

            return DateTime.Today + TimeToComplete;
        }

        //Returns round complete percentage
        private int FuncRoundCompletion()
        {
            return (BinsCollected == 0 && BinsOnRound == 0) ? 0 : Convert.ToInt32(Math.Round((double)BinsCollected / BinsOnRound, 2) * 100);
        }

        private Double FuncAverageSpeed(List<Coordinate> GPSTrail)
        {
            var AverageSpeeds = GPSTrail.Where(x => !Double.IsInfinity(x.Speed) && !Double.IsNaN(x.Speed))
                                        .Select(x => x.Speed)
                                        .ToList();

            return Math.Round((AverageSpeeds.Count > 0) ? AverageSpeeds.Average() : 0, 2);
        }

        private Int32[] FuncDriverChecks(List<Message> AllMessages, String Client)
        {
            var driverCheckMessage = AllMessages.Where(x => x.MessageType == MessageType.DriverCheck)
                                                .OrderBy(x => x.Time)
                                                .FirstOrDefault();

            // If there's no driver checks, return empty array
            if (driverCheckMessage?.DriverCheck == null)
            {
                Int32 NoOfDriverChecks = Convert.ToInt32(Database.Read("SELECT COUNT (*) FROM [VehicleChecks]", Client).Rows[0].ItemArray[0]);

                this.DriverCheckFails = new string[] { "Did not recieve a driver's check" };

                return new Int32[] { 0, NoOfDriverChecks };
            }
            else
            {
                var Checks = new List<String>();

                // Count the number of checks in the [VehicleChecks] table.
                // TODO add error handlign
                Int32 NoOfDriverChecks = Convert.ToInt32(Database.Read("SELECT COUNT (*) FROM [VehicleChecks]", Client).Rows[0].ItemArray[0]);

                //Int32 ChecksPassed = DriverCheck.Data.Split(new[] { "Yes" }, StringSplitOptions.None).Length - 1;
                //
                //Int32 ChecksFailed = DriverCheck.Data.Split(new[] { "No" }, StringSplitOptions.None).Length - 1;

                var checkData = driverCheckMessage.DriverCheck.CheckData;
                // var checkData new String(DriverCheck.Data.Split('$')[0].Where(x => !Char.IsDigit(x)).ToArray());

                // checkData example
                // 103YesYes$$Driver Check 53.4561183333333 -2.60225166666667

                var checkIndex = checkData.IndexOf(checkData.FirstOrDefault(x => x == 'Y' || x == 'N'));
                checkData = checkData.Substring(checkIndex, checkData.Length - checkIndex);

                while (checkData != "")
                {
                    // If there are parentheses, then there should be text in there (input from the driver)
                    if (checkData.Contains("()"))
                    {
                        checkData = checkData.Replace("()");
                    }

                    // Get the first character of the string
                    var checkCharacter = checkData[0];//.Substring(0, 2);

                    var passed = checkCharacter == 'Y';

                    Checks.Add(passed ? "Yes" : "No");

                    if (!passed)
                    {
                        // Get the drive input that should be within parentheses
                        if (checkData.Length > 2 && checkData[1] == '(')
                        {
                            String FailedChecks = checkData.Substring(2, checkData.IndexOf(')') - 3).ToLower();

                            FailedChecks = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(FailedChecks);

                            checkData = checkData.Remove(0, checkData.IndexOf(')') + 1);
                        }
                        else
                        {
                            checkData = checkData.Remove(0, 1);
                        }
                    }
                    else
                    {
                        checkData = checkData.Remove(0, 1);
                    }
                }

                // If the driver said Failed any of the checks, get the checks from the table match them up
                if (Checks.Contains("No"))
                {
                    var Result = Database.Read("SELECT * FROM [VehicleChecks]", Client);

                    var NoIndexes = new List<Int32>();

                    // Tries to match indexes of 'No' to the checks in the [VehicleChecks] table.
                    for (int i = 0; i < Checks.Count; i++)
                    {
                        if (Checks[i] == "No")
                        {
                            NoIndexes.Add(i);
                        }
                    }

                    if (NoIndexes.Max() < Result.Rows.Count && NoIndexes.Count != 1)
                    {
                        //
                        this.DriverCheckFails = NoIndexes.Select(x => Result.Rows[x - 1].ItemArray[1].ToString())
                                                         .ToArray();
                    }
                    else
                    {
                        this.DriverCheckFails = Result.AsEnumerable()
                                                      .Select(x => x["checkDescription"].ToString())
                                                      .ToArray();

                        //this.DriverCheckFails = NoIndexes.Take(Result.Rows.Count)
                        //                                 .Select(x => Result.Rows[x - 1].ItemArray[1].ToString())
                        //                                 .ToArray();
                    }

                    //this.DriverCheckFails = NoIndexes.Select(x => Result.Rows[x - 1].ItemArray[1].ToString())
                    //                                 .ToArray();
                }

                if (driverCheckMessage.DriverCheck.ChecksPassed > NoOfDriverChecks)
                {
                    driverCheckMessage.DriverCheck.ChecksPassed = NoOfDriverChecks;
                }

                return new Int32[] { driverCheckMessage.DriverCheck.ChecksPassed, driverCheckMessage.DriverCheck.ChecksFailed };
            }
        }

        //private Int32 FuncContamination(List<Message> AllMessages)
        //{
        //    return AllMessages.Count(x => x.Issue?.IssueCode  == "Contamination");
        //}

        //private List<Message> GetCollectedMessages(List<Message> allMessages)
        //{
        //
        //}

        public List<Property> GetCollectedProperties_NEW(List<Message> allMessages, List<Coordinate> gpsTrail, List<Property> propertiesOnRound)
        {
            var driverMessages = allMessages.Where(x => x.MessageType == MessageType.CPY ||
                                                        x.MessageType == MessageType.CPN)
                                            .ToList();

            var lastCollectionByUPRN = driverMessages.GroupBy(x => x.UPRN)
                                                     .Select(x => x.OrderByDescending(y => y.Time).FirstOrDefault())
                                                     .ToList();

            var streetGroups = propertiesOnRound.GroupBy(x => x.StreetID)
                                                .ToDictionary(x => x.Key, x => x.Count());

            if (driverMessages.Count != lastCollectionByUPRN.Count)
            {
                // Remove all driver confirmed
                allMessages = allMessages.Except(driverMessages)
                                         .ToList();

                // Add back in last confirmation message for each UPRN (in case the driver marks a property as collected but later marks it as uncollected)
                allMessages.AddRange(lastCollectionByUPRN);
            }


            var collectedProperties = new List<Property>();
            var collectedStreets = new Dictionary<string, long[]>();

            foreach (var property in propertiesOnRound)
            {
                foreach (var gps in gpsTrail)
                {
                    if (property.Coordinate.GetDistanceTo(gps) <= 68 && gps.Speed <= 5)
                    {
                        property.TimeCollected = gps.Time;

                        collectedProperties.Add(property);

                        if (collectedStreets.ContainsKey(property.StreetID))
                        {
                            collectedStreets[property.StreetID][0]++;
                        }
                        else
                        {
                            collectedStreets.Add(property.StreetID, new long[] { 1, streetGroups[property.StreetID] });
                        }

                        break;
                    }
                }
            }

            this.StreetsCollected = collectedStreets.Where(x => ((double)x.Value[0] / (double)x.Value[1]) > 0.75).Count();

            return collectedProperties.OrderBy(x => x.TimeCollected)
                                      .ToList();
        }

        // Uses GPS trail and driver input to determine if a property has been collect or not
        public List<Property> GetCollectedProperties(List<Message> AllMessages, List<Coordinate> GPSTrail, List<Property> PropertiesOnRound)//, string client = null, string md = null) 
        {
            //GetCollectedProperties_NEW(AllMessages, GPSTrail, PropertiesOnRound);
            //if (client != null)
            //{
            //    WriteDebugData(client, md, this.Registration, AllMessages, GPSTrail, PropertiesOnRound);
            //}


            var streetCollectionData = new Dictionary<string, long[]>();
            var collectedProperties = new List<Property>();
            var collectionMessages = AllMessages.Where(x => x.MessageType == MessageType.CPY ||
                                                            x.MessageType == MessageType.CPN ||
                                                            x.MessageType == MessageType.CSN ||
                                                            x.MessageType == MessageType.CSY)
                                                .ToList();

            for (int j = 0; j < PropertiesOnRound.Count; j++)
            {
                for (int i = 0; i < GPSTrail.Count; i++)
                {
                    // 56 collected regardless of speed, 49 collected with speed (km/h)
                    if (PropertiesOnRound[j].Coordinate.GetDistanceTo(GPSTrail[i]) <= 68 && GPSTrail[i].Speed <= 5)
                    {
                        PropertiesOnRound[j].TimeCollected = GPSTrail[i].Time;

                        collectedProperties.Add(PropertiesOnRound[j]);

                        if (streetCollectionData.ContainsKey(PropertiesOnRound[j].StreetID))
                        {
                            streetCollectionData[PropertiesOnRound[j].StreetID][0]++;
                        }
                        else
                        {
                            streetCollectionData.Add(PropertiesOnRound[j].StreetID, new Int64[] { 1, PropertiesOnRound.Count(x => x.StreetID == PropertiesOnRound[j].StreetID) });
                        }

                        break;
                    }
                }
            }

            /* TODO 2: Fix or confirm this, says it's collected every property on round */

            //Message X;
            //
            //try
            //{
            foreach (Message CollectionData in collectionMessages)
            {
                //X = CollectionData;

                //if (CollectionData.Data.Contains("$.:"))
                //{
                //    continue;
                //}

                //String Identifier = "";

                if (PropertiesOnRound.Count == 0)
                {
                    continue;
                }

                // Checks the length of the StreetID, if the minimum length of all the StreetIDs is more than 4 then it's probably using the new system as opposed to having 1000's of streets on a round
                //Int32 MinLength = PropertiesOnRound.Min(x => x.StreetID.Length);

                // New system: <PostCode><Something><SomethingProbably> (10 characters)
                // Old system: <given a unique number>

                //olean UsingNewIDSystem = MinLength > 4;

                //if (UsingNewIDSystem)
                //{
                //    Identifier = CollectionData.Data.Split(':')[0].Split('.')[0].Split('$')[2];
                //}
                //else
                //{
                //    Identifier = new String(CollectionData.Data.Split(':')[0].Where(x => Char.IsDigit(x) || x == '.').ToArray());
                //}

                var streetId = CollectionData.cStreetID;// (Identifier.Contains('.')) ? Identifier.Split('.')[0] : Identifier;

                var propertyId = string.IsNullOrWhiteSpace(CollectionData.cPropertyID) ? "-1" : CollectionData.cPropertyID;// (Identifier.Contains('.')) ? Convert.ToInt64(Identifier.Split('.')[1]) : 99991;

                var propertiesOnStreet = PropertiesOnRound.Where(x => x.StreetID == streetId).ToList();
                var properties = PropertiesOnRound.Where(x => x.StreetID == streetId || x.PropertyID == propertyId).ToList();

                if (CollectionData.MessageType == MessageType.CPY || CollectionData.MessageType == MessageType.CSY)
                {
                    foreach (var property in (CollectionData.MessageType == MessageType.CPY) ? properties : propertiesOnStreet)
                    {
                        if (!collectedProperties.Contains(property))
                        {
                            Property CurrentProperty = property;

                            CurrentProperty.TimeCollected = CollectionData.Time;

                            collectedProperties.Add(CurrentProperty);
                        }

                        if (streetCollectionData.ContainsKey(property.StreetID))
                        {
                            if (CollectionData.MessageType == MessageType.CSY)
                            {
                                streetCollectionData[property.StreetID][0] = streetCollectionData[property.StreetID][1];
                            }
                        }
                    }
                }
                else
                {
                    foreach (Property Property in (CollectionData.MessageType == MessageType.CPN) ? properties : propertiesOnStreet)
                    {
                        if (collectedProperties.Contains(Property))
                        {
                            collectedProperties.Remove(Property);
                        }

                        if (streetCollectionData.ContainsKey(Property.StreetID) && CollectionData.MessageType == MessageType.CSN)
                        {
                            streetCollectionData[Property.StreetID][0] = 0;
                        }
                    }
                }
            }
            //}
            //catch (Exception ex)
            //{
            //
            //}

            this.StreetsCollected = streetCollectionData.Where(x => ((double)x.Value[0] / (double)x.Value[1]) > 0.75)
                                                        .Count();

            return collectedProperties.OrderBy(x => x.TimeCollected).ToList();
        }

        /*
        // Used for data collection for unit testing
        private void WriteDebugData(string client, string md, string registration, List<Message> allMessages, List<Coordinate> gpsTrail, List<Property> propertiesOnRound)
        {
            var messagesData = JsonConvert.SerializeObject(allMessages);
            var gpsData = JsonConvert.SerializeObject(gpsTrail);
            var propertyData = JsonConvert.SerializeObject(propertiesOnRound);

            var path = $@"F:\incab-overview\InCabOverview.Tests\data\{client}\{md}";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            File.WriteAllText(Path.Combine(path, $"AllMessages.{client}.{registration}.{md}.json"), messagesData);
            File.WriteAllText(Path.Combine(path, $"GPSTrails.{client}.{registration}.{md}.json"), gpsData);
            File.WriteAllText(Path.Combine(path, $"PropertiesOnRound.{client}.{registration}.{md}.json"), propertyData);
        }
        */

        private Int32 FuncStreetsOnRound(List<Property> PropertiesOnRound)
        {
            return PropertiesOnRound.Select(x => x.StreetID)
                                    .Distinct()
                                    .Count();
        }

        private Dictionary<String, Int32> FuncIssueOccurences(List<Message> AllMessages, string client)
        {
            //var issuesSql = "SELECT DISTINCT [issueCode], [IssueText] FROM [InCabIssues]";
            //var issuesResult = Database.Read(issuesSql, client);

            //Dictionary<string, string> issuesDictionary = null;

            //if (issuesResult != null)
            //{
            //    issuesDictionary = issuesResult.AsEnumerable()
            //                                   .ToDictionary(k => k["issueCode"].ToString(), v => v["issueText"].ToString());
            //}
            
            

            var LocalIssueOccurences = new Dictionary<string, int>();
            var gradingCounter = 0;

            var issueMessages = AllMessages.Where(x => x.MessageType == MessageType.Issue);

            foreach (Message message in issueMessages)
            {
                var issueType = message.Issue.IssueCode;

                if (message.Issue.IssueCode.Length <= 6)
                {
                    issueType = "Common";
                }

                if (_portalStyle == PortalStyle.Streets && issueType == "Grading")
                {
                    issueType = message.Issue.IssueText;
                    gradingCounter++;
                }

                //if (issueType.Any(char.IsDigit) && issuesDictionary != null)
                //{
                //    if (issuesDictionary.ContainsKey(issueType))
                //    {
                //        issueType = issuesDictionary[issueType];
                //    }
                //}

                if (LocalIssueOccurences.ContainsKey(issueType))
                {
                    LocalIssueOccurences[issueType]++;
                }
                else
                {
                    LocalIssueOccurences.Add(issueType, 1);
                }

                if (message.Issue.IssueCode != "Grading")
                {
                    Issues.Add(message.Issue.IssueText);
                }

            }

            this.NonGradingIssues = LocalIssueOccurences.Where(x => x.Key != "Grading")
                                                        .Sum(x => x.Value) - gradingCounter;

            LocalIssueOccurences = LocalIssueOccurences.OrderBy(x => x.Value)
                                                       .ToDictionary(x => x.Key, y => y.Value);

            return LocalIssueOccurences;
        }

        /*Place Holder Information*/

        //private static Int32 FuncGetYield(String[] Data)
        //{
        //    String Gross = Data[0].Replace("$TT$");

        //    String Data1 = Data[1];

        //    if (Gross.Count(x => x == ':') >= 3)
        //    {
        //        Gross = Gross.Split(':')[0];

        //        Data1 = Data[0].Split(':')[1];
        //    }

        //    if (Gross[0] == '.')
        //    {
        //        Gross = Gross.Substring(1, Gross.Length - 1);
        //    }

        //    if (Gross[Gross.Length - 1] == '.')
        //    {
        //        Gross = Gross.Substring(0, Gross.Length - 1);
        //    }


        //    // eg. "18..000"
        //    while (Data1.Contains(".."))
        //    {
        //        Data1 = Data1.Replace("..", ".");
        //    }

        //    while (Gross.Contains(".."))
        //    {
        //        Gross = Gross.Replace("..", ".");
        //    }

        //    Double CurrentYield = Convert.ToDouble(Gross) - Convert.ToDouble(Data1);

        //    return (Int32)CurrentYield;
        //}

        [Obsolete]
        private ReceptionVisit[] FuncReceptionVisits(List<Message> AllMessages, List<Coordinate> GPSTrail, List<Facility> Facilities)
        {
            // In Meters
            Int32 DistanceFromTip = 175;

            var Attendances = new List<ReceptionVisit>();
            var Receptions = Facilities.Where(x => x.FacilityType == "Reception").ToList();

            // Finds if the vehicle came within DistanceFromTip distance to any reception
            for (int i = 0; i < GPSTrail.Count; i++)
            {
                foreach (Facility Facility in Receptions)
                {
                    if (GPSTrail[i].GetDistanceTo(Facility.FacilityCoordinate) < DistanceFromTip)
                    {
                        // Time when the driver first entered that 175m boundary
                        DateTime Start = GPSTrail[i].Time;

                        for (int j = i; j < GPSTrail.Count; j++)
                        {
                            // Then find when the driver leaves the 175m boundary
                            if (GPSTrail[j].GetDistanceTo(Facility.FacilityCoordinate) > DistanceFromTip)
                            {
                                // If they stayed within that boundary for more than 2 minutes
                                // Then it was probably a valid tip visit
                                if ((Start - GPSTrail[j].Time).TotalMinutes >= 2)
                                {
                                    String ReceptionName = Facility.FacilityName;

                                    // Not sure what this did
                                    if (DriverID.Contains("Scarborough"))
                                    {
                                        if (ReceptionName.Contains('-'))
                                        {
                                            ReceptionName = ReceptionName.Split('-')[1];
                                        }
                                    }

                                    Attendances.Add(new ReceptionVisit(ReceptionName, GPSTrail[j].Time, Start));
                                }

                                // Skip the i loop to the GPS point when they left the reception
                                i = j;

                                break;
                            }
                        }
                    }
                }
            }

            // Finds any TipTicket messages
            var tipTicketsSubmitted = AllMessages.Where(x => x.MessageType == MessageType.Tip).ToList();

            if (tipTicketsSubmitted.Count > 0)
            {
                foreach (Message message in tipTicketsSubmitted)
                {
                    if (message.TipTicket == null)
                    {
                        continue;
                    }

                    // GPS data isn't sent with TipTicket messages
                    // So find the latest GPS message
                    Coordinate GPSAtReception = GPSTrail.FirstOrDefault(n => n.Time <= message.Time);

                    if (GPSAtReception == null)
                    {
                        continue;
                    }

                    // Get all the coordinates within that 175m boundary
                    Coordinate[] Visit = GPSTrail.Where(x => x.GetDistanceTo(GPSAtReception) < DistanceFromTip).ToArray();

                    //List<String> ReceptionNameArray = TipTicket.Data.Replace("$TT$").Replace(" T").Replace(" KG").Split(' ').ToList();
                    //
                    //
                    ////"$TT$" + GROSS + ":" + TARE + ":" + Unit + ":" + atTip + ":" + ttNo + ":" + latitudeGPS + ":" + longitudeGPS;
                    //
                    //
                    //String ReceptionName = "";
                    //
                    //if (TipTicket.Data.Count(x => x == ':') > 5)
                    //{
                    //    var X = TipTicket.Data.Split(':');
                    //
                    //    if (X.Length > 4)
                    //    {
                    //        ReceptionName = X[3];
                    //    }
                    //    else
                    //    {
                    //        throw new Exception("Reception Parse Error");
                    //    }
                    //}



                    //// Stupid fix
                    //if (ReceptionNameArray[0].Contains("Tancred"))
                    //{
                    //    ReceptionName = "Tancred";
                    //}
                    //else
                    //{
                    //    ReceptionNameArray.RemoveRange(0, 2);
                    //
                    //    ReceptionName = String.Join(" ", ReceptionNameArray);
                    //}

                    //foreach (Facility Reception in Facilities.Where(x => x.FacilityType == "Reception"))
                    //{
                    //    foreach (String Name in ReceptionNameArray)
                    //    {
                    //        if (Name.Length < 3)
                    //        {
                    //            continue;
                    //        }
                    //
                    //        if (Reception.FacilityName.Contains(Name))
                    //        {
                    //            ReceptionName = Reception.FacilityName;
                    //
                    //            break;
                    //        }
                    //    }
                    //}
                    //
                    //if (DriverID.Contains("Scarborough"))
                    //{
                    //    if (ReceptionName.Contains("-"))
                    //    {
                    //        ReceptionName = ReceptionName.Split('-')[1];
                    //    }
                    //}

                    var Yield = Convert.ToDouble(message.TipTicket.Gross) - Convert.ToDouble(message.TipTicket.Tare);

                    //if (TipTicket.Data.Contains(":"))
                    //{
                    //    Yield = FuncGetYield(TipTicket.Data.Split(':'));
                    //}
                    //else
                    //{
                    //    Yield = FuncGetYield(TipTicket.Data.Split(' ').StringArray());
                    //}


                    Attendances.Add(new ReceptionVisit(message.TipTicket.Location, Visit.Last().Time, Visit.First().Time, Convert.ToInt32(Yield), true));
                }
            }

            // Order all the tip visits by time they entered
            Attendances = Attendances.OrderBy(x => x.Enter).ToList();

            for (int i = 0; i < Attendances.Count - 1; i++)
            {
                TimeSpan Difference = (Attendances[i].Leave - Attendances[i + 1].Enter).Duration();

                // Remove any duplicate visists
                if (Attendances[i].Enter == Attendances[i + 1].Enter && Attendances[i].VisitLength == Attendances[i + 1].VisitLength)
                {
                    Attendances.RemoveAt((Attendances[i].Yield == 0) ? i : i + 1);
                }

                // Remove any visits that last less than 15 minutes.
                if (Difference < TimeSpan.FromMinutes(15))
                {
                    if (Attendances.Count > 1)
                    {
                        Attendances.RemoveAt(i);
                    }
                }
            }

            return Attendances.ToArray();
        }


        private List<Issue> RemoveDuplicateIssues(List<Issue> Issues)
        {
            var DuplicateIssues = Issues.GroupBy(x => x.UPRN)
                                        .Select(x => new { UPRN = x.Key, Issues = x.ToList() })
                                        .Where(x => x.Issues.Count > 1);

            var InvalidUPRNs = DuplicateIssues.Select(x => x.UPRN.ToString()).ToArray();

            var IssuesToAdd = new List<Issue>();

            foreach (var Item in DuplicateIssues)
            {
                // Group all issues with the same hash value, and get the first one.
                // eg. 
                // Before: IssuesOnUPRN = "Bin Not Out, House 1, MyTown, 15/11/2018 09:23:01, 54.0959479,-0.885205, a408md2923"
                //                        "Bin Not Out, House 1, MyTown, 15/11/2018 09:23:01, 54.0959479,-0.885205, a408md2923"
                //                        "Bin Not Out, House 1, MyTown, 15/11/2018 09:23:01, 54.0959479,-0.885205, a408md2923"

                var DuplicatesRemoved = Item.Issues.GroupBy(x => x.Hash).Select(x => x.FirstOrDefault()).ToList();

                // After: IssuesOnUPRN = "Bin Not Out, House 1, MyTown, 15/11/2018 09:23:01, 54.0959479,-0.885205, a408md2923"

                // Add it to list of issues to keep.
                IssuesToAdd.AddRange(DuplicatesRemoved);
            }

            // Remove all duplicate issues  
            Issues.RemoveAll(x => InvalidUPRNs.Contains(x.UPRN));// && x.MessageType != "Unissue");

            // Only add back distinct issues
            Issues.AddRange(IssuesToAdd);

            return Issues;
        }


        private List<Issue> RemoveCancelledIssues(List<Issue> issues)
        {
            // 1 Main Street - 1004345435345 - Issue - Bin Not Out - 12:03:00
            // 1 Main Street - 1004345435345 - Unissue - Bin Not Out - 12:04:10

            // 1. Group issues by UPRN
            var uprnsWithUnissues = issues.GroupBy(x => x.UPRN).Where(x => x.Count(y => y.MessageType == MessageType.Unissue) > 0).ToList();

            // 2. Loop through each UPRN
            uprnsWithUnissues.ForEach(x =>
            {
                // List of all issues that the current UPRN
                var issuesForUPRN = x.ToList();

                // Find any cancelled issues
                var unissues = issuesForUPRN.Where(y => y.MessageType == MessageType.Unissue);

                foreach (var unissue in unissues)
                {
                    // Find the original issue for the cancelled issue
                    var parentIssue = issuesForUPRN.FirstOrDefault(y => y.IssueText == unissue.IssueText && y.Time < unissue.Time);

                    // If we can find one remove it from the overall list
                    if (parentIssue != null)
                    {
                        issues.Remove(parentIssue);
                        issues.Remove(unissue);
                    }
                }
            });

            //var xd = issues.GroupBy(x => x.UPRN).Where(x => x.ToArray().Count(y => y.MessageType == "Unissue") > 0).ToList().ForEach(x =>
            //{
            //    
            //});

            return issues;
        }

    }
}