﻿// Add Data To Charts

function AddRoundCompletionChart(e) {
    createRoundCompletionChart('#RoundCompletionChart', e);

    /*var Series = [],
        Labels = [],
        Colors = [];

    var Below25 = 0;

    e = e.sort(function (a, b) {
        return a.Round.Service > b.Round.Service;
    });

    for (var i = 0; i < e.length; i++) {
        Series.push((e[i].Completion < 1) ? 0.5 : e[i].Completion);
        Labels.push(e[i].DriverID.toLowerCase().replace(client.toLowerCase(), "").replace("base", "") + "\r\n| " + e[i].Round.Day + " " + e[i].Round.Week + " " + e[i].Round.Service);
        Colors.push((e[i].Completion <= 33) ? '#d52b2b' : ((e[i].Completion > 33 && e[i].Completion <= 66) ? '#f2aa00' : '#2da92c')); //ColorPercentage(e[i].Completion, 0, 120)

        if (e[i].Completion < 25){
            Below25++;
        }
    }

    if (Below25 >= (e.length / 100) * 25) {
        FuncAddListAlert("Warning", "Overall Fleet Performance.");
    }

    RoundCompletionChart.series = Series;
    RoundCompletionChart.keys = Labels
    RoundCompletionChart.renderOptions.colors = Colors;
    RoundCompletionChart.update();*/
}

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

function RoundCompletionBarClick(DriverID) {
    FuncShowTableData();

    $("#" + DriverID).effect("highlight", {}, 5000);
}

function AddServiceValue(Data, Index, Service, Collected, Value) {
    if (Index != 999) {
        Data[Index].value = Data[Index].value + Value;
    } else {
        Data = AppendDictionary(Data, Service + " - " + ((Collected) ? "Collected" : "Uncollected"), Value);
    }
    return Data;
}

function AddBinsCollectedChart(e) {
    var ServiceLabels = [],
        BinInfo = [0, 0],
        StreetsInfo = [0, 0];

    var ServiceBinsCollected = [],
        ServiceBinsLeftToCollect = [],
        ServiceStreetsCollected = [],
        ServiceStreetsLeftToCollect = [];

    for (var i = 0; i < e.length; i++) {
        BinInfo[0] += e[i].BinsCollected;
        BinInfo[1] += e[i].BinsUncollected;

        StreetsInfo[0] += e[i].StreetsCollected;
        StreetsInfo[1] += e[i].StreetsUncollected - e[i].StreetsCollected;

        var Index = Find(ServiceBinsCollected, e[i].Round.Service + " - Collected");

            ServiceBinsCollected = AddServiceValue(ServiceBinsCollected, Index, e[i].Round.Service, true, e[i].BinsCollected);

        Index = Find(ServiceBinsLeftToCollect, e[i].Round.Service + " - Uncollected");

            ServiceBinsLeftToCollect = AddServiceValue(ServiceBinsLeftToCollect, Index, e[i].Round.Service, false, e[i].BinsUncollected);

        Index = Find(ServiceStreetsCollected, e[i].Round.Service + " - Collected")

            ServiceStreetsCollected = AddServiceValue(ServiceStreetsCollected, Index, e[i].Round.Service, true, e[i].StreetsCollected);

        Index = Find(ServiceStreetsLeftToCollect, e[i].Round.Service + " - Uncollected");

            ServiceStreetsLeftToCollect = AddServiceValue(ServiceStreetsLeftToCollect, Index, e[i].Round.Service, false, e[i].StreetsUncollected);

        ServiceLabels.push(e[i].Round.Service);
    }

    var BinData = ServiceBinsCollected.concat(ServiceBinsLeftToCollect);

    var Y = [], B = [];

    for (var Item in BinData) {
        if (BinData[Item].value > 0) {
            Y.push(BinData[Item].key);
        }
    }

    var StreetData = ServiceStreetsCollected.concat(ServiceStreetsLeftToCollect);

    for (var Item in StreetData) {
        if (StreetData[Item].value > 0) {
            B.push(StreetData[Item].key);
        }
    }

    ServiceLabels = ServiceLabels.filter(function (item, pos) { return ServiceLabels.indexOf(item) == pos; });

    if (Y.length == 0) {
        BinInfo[1] = 1;
    }

    BinsCollectedChart.series = BinInfo;
    BinsCollectedChart.keys = Y;
    BinsCollectedChart.renderOptions.colors = ["#2da92c", "#b91d1d"]; //Pos.concat(Neg);
    BinsCollectedChart.update();

    if (B.length == 0) {
        StreetsInfo[1] = 1;
    }

    StreetsCollectedChart.series = StreetsInfo;
    StreetsCollectedChart.keys = B;
    StreetsCollectedChart.renderOptions.colors = ["#2da92c", "#b91d1d"];//Pos.concat(Neg);
    StreetsCollectedChart.update();

    var BinsToolTipText;

    if (portalStyle == "Streets") {
        BinsToolTipText = "<div class='SeriesDiv' style='background-color: #2da92c'></div> Sections Complete - " + BinInfo[0] + "</br><div class='SeriesDiv' style='background-color: #b91d1d'></div> Sections to Complete - " + BinInfo[1];
    }
    else {
        BinsToolTipText = "<div class='SeriesDiv' style='background-color: #2da92c'></div> Properties Collected - " + BinInfo[0] + "</br><div class='SeriesDiv' style='background-color: #b91d1d'></div> Properties to Collect - " + BinInfo[1];
    }

    
    var StreetsToolTipText = "<div class='SeriesDiv' style='background-color: #2da92c'></div> Streets Complete - " + StreetsInfo[0] + "</br><div class='SeriesDiv' style='background-color: #b91d1d'></div> Streets to Complete - " + StreetsInfo[1];

    if (!$('#BinsCollectedChart').attr('data-toggle')) {
        $('#BinsCollectedChart').attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", BinsToolTipText).tooltip();
        $('#StreetsCollectedChart').attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", StreetsToolTipText).tooltip();
    }
}

function Find(Data, Item) {
    for (var j = 0; j < Data.length; j++) {
        if (Data[j].key == Item) {
            return j;
        }
    }
    return 999;
}

function AppendDictionary(Data, key, value) {
    Data.push({ key: key, value: value });

    return Data;
}

function AddValueToPanels(Data) {
    var Services = [],
        ServiceTotalYield = [],
        ServiceTotalVisits = [],
        ServiceTotalTickets = [],
        DCPanelValue = [0, 0],
        DCPanelValuePassFail = [0, 0],
        DriverIDs = [];

    var DCPassFail = false,
        HeadersAdded = false

    //for (var Item in Data) {
    //    if (Data[Item].DriverChecks[0] == 999 || Data[Item].DriverChecks[1] == 999) {
    //        DCPassFail = true;
    //
    //        $('#DCPanelTable').find("tr").find("td:eq(3)").remove();
    //
    //        break;
    //    }
    //}

    var TVPanelValue = 0,
        BNOPanelValue = 0,
        MGPanelValue = 0,
        CTPanelValue = 0,
        NGPanelValue = 0;


    var DCbody = $('#DCPanelTable > tbody'),
        TVbody = $('#TVPanelTable > tbody'),
        BNObody = $('#BNOPanelTable > tbody'),
        MGbody = $('#MGPanelTable > tbody'),
        CTbody = $('#CTPanelTable > tbody'),
        NGbody = $('#NGPanelTable > tbody');

    $('#TVPanelTable > thead > tr').each(function () {
        if (!HeadersAdded) {
            if ($(this).text().indexOf("Ticket") > -1) {
                HeadersAdded = true;
            }
        }
    });

    var TicketsSubmitted = 0;

    for (var i = 0; i < Data.length; i++) {
        if (Data[i].TipVisits != null) {
            for (var j = 0; j < Data[i].TipVisits.length; j++) {
                if (Data[i].TipVisits[j].Ticket) {
                    TicketsSubmitted++;
                }
            }
        }
    }

    if (TicketsSubmitted > 0 && !HeadersAdded) {
        $($('#TVPanelTable > thead > tr')[0]).append(FuncAddNewCell(undefined, "Yield"));
    }
    
    if (!HeadersAdded)
    {
        $($('#TVPanelTable > thead > tr')[0]).append(FuncAddNewCell(undefined, "Ticket"));
    }

    for (var Item in Data) {
        var DCrow = document.createElement('tr'),
            BNOrow = document.createElement('tr'),
            MGrow = document.createElement('tr'),
            CTrow = document.createElement('tr');
            //NGrow = document.createElement('tr');

        var DriverID = Data[Item].Registration;//Data[Item].DriverID.toLowerCase().replace(client.toLowerCase(), "").replace("base", "");

            DriverIDs.push(Data[Item].DriverID.toLowerCase().replace(client.toLowerCase(), "").replace("base", ""));

        var Round = Data[Item].Round.Crew + " " + Data[Item].Round.Day + " " + Data[Item].Round.Week + " " + Data[Item].Round.Service;

        if (Services.indexOf(Data[Item].Round.Service) < 0) {
            Services.push(Data[Item].Round.Service);
        }

        var ServicesIndex = Services.indexOf(Data[Item].Round.Service);

        if (isNaN(ServiceTotalYield[ServicesIndex])) {
            ServiceTotalYield[ServicesIndex] = 0;
        }
        if (isNaN(ServiceTotalVisits[ServicesIndex])) {
            ServiceTotalVisits[ServicesIndex] = 0;
        }
        if (isNaN(ServiceTotalTickets[ServicesIndex])) {
            ServiceTotalTickets[ServicesIndex] = 0;
        }

        //if (DCPassFail) {
        //    if (Data[Item].DriverChecks[0] == 999) {
        //        DCPanelValuePassFail[0]++;
        //    }
        //    else {
        //        DCPanelValuePassFail[1]++;
        //    }
        //}
        //else {
        DCPanelValue[0] += Data[Item].DriverChecks[0];
        DCPanelValue[1] += Data[Item].DriverChecks[1];
        //}

        TVPanelValue += (Data[Item].TipVisits != null) ? Data[Item].TipVisits.length : 0;
        BNOPanelValue += Data[Item].BinNotOut;
        MGPanelValue += Data[Item].MilesTravelled;
        CTPanelValue += Data[Item].TotalContamination;
        NGPanelValue += Data[Item].NonGradingIssues;

        //Driver Check Data
        var DCDriverID = FuncAddNewCell(undefined, DriverID);
        var DCRound = FuncAddNewCell(undefined, Round);
        var DCPass = FuncAddNewCell(undefined, Data[Item].DriverChecks[0]);
        var DCFail = FuncAddNewCell(undefined, Data[Item].DriverChecks[1]);

        //if (Data[Item].DriverChecks[0] < 999 && Data[Item].DriverChecks[1] < 999) {
        //}
        //else {
        //    var DCPass = FuncAddNewCell(undefined, (Data[Item].DriverChecks[0] == 999) ? "Passed" : "Failed");
        //}

        $(DCrow).append(DCDriverID, DCRound, DCPass, DCFail);

        if (Data[Item].DriverChecks[1] > 0) {
            $(DCFail).css("position", "relative");

            $(DCFail).append($(FuncAddTableAlert("DCPanelTable", 1, parseInt(Item), "Checks Failed: </br>" + Data[Item].DriverCheckFails.join("</br>")).children[0]).css("position", "absolute").css("top", "10%").css("margin-left", "15px").attr("data-placement", "left"));
        }

        $(DCbody).append(DCrow);

        // Bin Not Out Data
        var BNODriverID = FuncAddNewCell(undefined, DriverID),
            BNORound = FuncAddNewCell(undefined, Round),
            BNOCount = FuncAddNewCell(undefined, Data[Item].BinNotOut);

        $(BNOrow).append(BNODriverID, BNORound, BNOCount);

        if (Data[Item].BinNotOut > 0) {
            $(BNObody).append(BNOrow);
        }

        // Issues loop
        for (var i = 0; i < Data[Item].Issues.length; i++) {
            var row = document.createElement('tr');
            var driverId = FuncAddNewCell(undefined, DriverID);
            var round = FuncAddNewCell(undefined, Round);
            var issue = FuncAddNewCell(undefined, Data[Item].Issues[i]);
            $(row).append(driverId, round, issue);
            $(NGbody).append(row);
        }


        // Miles Travelled Data
        var MGDriverID = FuncAddNewCell(undefined, DriverID),
            MGRound = FuncAddNewCell(undefined, Round),
            MGMilesTravelled = FuncAddNewCell(undefined, Data[Item].MilesTravelled);

        $(MGrow).append(MGDriverID, MGRound, MGMilesTravelled);

        $(MGbody).append(MGrow);

        // Contamination Data
        var CTDriverID = FuncAddNewCell(undefined, DriverID);
        var CTRound = FuncAddNewCell(undefined, Round);
        var CTContamination = FuncAddNewCell(undefined, Data[Item].TotalContamination);

        $(CTrow).append(CTDriverID, CTRound, CTContamination);

        if (Data[Item].TotalContamination > 0) {
            $(CTbody).append(CTrow);
        }

        // Tip Visit Data
        if (Data[Item].TipVisits != null) {
            if (Data[Item].TipVisits.length > 0) {
                var TVrow = document.createElement('tr');

                var TVDriverID = FuncAddNewCell(undefined, DriverID);
                var TVRound = FuncAddNewCell(undefined, Round);

                var TVStart = document.createElement('td');
                var TVLength = document.createElement('td');
                var TVReception = document.createElement('td');
                var TVTicket = document.createElement('td');

                if (TicketsSubmitted > 0) {
                    var TVYield = document.createElement('td');
                }

                var PreviousRecepetion = "";

                for (var Visit in Data[Item].TipVisits) {
                    ServiceTotalVisits[ServicesIndex]++;

                    var Ticketed = (Data[Item].TipVisits[Visit].Ticket == true);

                    TVTicket.innerHTML += ('<i class="fa fa-' + (Ticketed ? 'check' : 'times TipVisit') + '" aria-hidden="true" style="color:' + ((Ticketed) ? "#89c14d" : "red") + '; font-size: 25px"></i><br>');
                    ServiceTotalTickets[ServicesIndex] += (Data[Item].TipVisits[Visit].Ticket) ? 1 : 0;

                    TVReception.innerHTML += ((PreviousRecepetion != Data[Item].TipVisits[Visit].ReceptionName.split(/(?=[A-Z])/).join(" ").replace("  ", " ")) ? Data[Item].TipVisits[Visit].ReceptionName.split(/(?=[A-Z])/).join(" ").replace("  ", " ") : '"') + ((Data[Item].TipVisits.length > 1) ? "<br>" : "");

                    TVStart.innerHTML += Data[Item].TipVisits[Visit].Enter.toString().slice(11, 19) + ((Data[Item].TipVisits.length > 1) ? "<br>" : "");

                    TVLength.innerHTML += Data[Item].TipVisits[Visit].VisitLength.toString().slice(0, 8) + ((Data[Item].TipVisits.length > 1) ? "<br>" : "");

                    if (TicketsSubmitted > 0) {
                        TVYield.innerHTML += ((Data[Item].TipVisits[Visit].Yield > 0) ? Data[Item].TipVisits[Visit].Yield : (((Visit < 1 && Data[Item].TipVisits[Visit].Yield > 0) ? "<br>" : "") + "-")) + ((Data[Item].TipVisits.length > 1) ? "<br>" : "");

                        if (!isNaN(Data[Item].TipVisits[Visit].Yield)) {
                            ServiceTotalYield[ServicesIndex] += Data[Item].TipVisits[Visit].Yield;
                        }
                    }
                    PreviousRecepetion = Data[Item].TipVisits[Visit].ReceptionName.split(/(?=[A-Z])/).join(" ").replace("  ", " ");
                }
                $(TVrow).append(TVDriverID, TVRound, TVReception, TVStart, TVLength, TVYield, TVTicket);
                $(TVbody).append(TVrow);
            }
        }
    }
    /*
    var Duplicates = FindDuplicates(DriverIDs);

    if (Duplicates.length > 0) {
        FuncAddListAlert("Warning", "Multiple drivers using the same Driver ID");

        ErrorCount++;

        var SOTNTable = document.getElementById('SOTN_Table');

        for (var i = 0; i < SOTNTable.rows.length; i++) {
            if (Duplicates.indexOf(SOTNTable.rows[i].cells[0].textContent) > -1) {
                $('#MainAlert').show();
                $(SOTNTable.rows[i]).append(FuncAddTableAlert("SOTN_Table", 2, i, "Multiple drivers using the same Driver ID"));
            }
        }

        $('#MGPanelTableAlerts').show();

        $('#MGPanelTableAlerts').width('10%');

        var MGTable = document.getElementById('MGPanelTable');

        for (var i = 0; i < MGTable.rows.length; i++) {
            if (Duplicates.indexOf(MGTable.rows[i].cells[0].textContent) > -1) {
                $(MGTable.rows[i]).append(FuncAddTableAlert("MGPanelTable", 0, i, "Duplicate Driver IDs - " + Duplicates[i]));
            }
            else {
                if (MGTable.rows[i].cells.length < 4) {
                    MGTable.rows[i].insertCell(3);
                }
            }
        }
    }
    */

    if (DCPassFail) {
        $('#DCPanelValue').text(DCPanelValuePassFail[0] + " " + DCPanelValuePassFail[1]);
    }
    else {
        $('#DCPanelValue').text(DCPanelValue[0] + " " + DCPanelValue[1]);
    }
    $('#TVPanelValue').text(TVPanelValue);
    $('#BNOPanelValue').text(BNOPanelValue);
    $('#MGPanelValue').text(Math.floor(MGPanelValue).toLocaleString());
    $('#CTPanelValue').text(CTPanelValue);
    $('#NGPanelValue').text(NGPanelValue);

    sortTable('#DCPanel', (DCPassFail) ? 2 : 3);
    sortTable('#TVPanel');
    sortTable('#BNOPanel', 2);
    sortTable('#MGPanel', 2);
    sortTable('#CTPanel', 2);

    var TotalRow = document.createElement('tr');

    $(TotalRow).append(FuncAddNewCell(undefined, "Total"), FuncAddNewCell(undefined, "Visits"), FuncAddNewCell(undefined, "Visit Duration"));

    $(TotalRow).addClass('td-Important');

    $(TVbody).append(TotalRow);

    for (var i = 0; i < Services.length; i++) {
        if (ServiceTotalVisits[i] < 1) {
            continue;
        }

        var TotalHeadersAdded = false;

        $('#TVPanelTable > tbody > tr').each(function () {
            if (!TotalHeadersAdded) {
                if ($(this).text().indexOf("Yield") > -1) {
                    TotalHeadersAdded = true;
                }
            }
        });

        var ColumnCount = TVbody[0].rows[0].cells.length;

        var Row = document.createElement('tr');
        Row.id = Services[i] + "-Total";

        var Service = FuncAddNewCell(undefined, Services[i]);

        $(Row).append(Service);

        var TotalVisits = FuncAddNewCell(undefined, ServiceTotalVisits[i]);

        var TotalVisitTime = FuncAddNewCell(undefined, TotalTipTicketTimes[Services[i]]);

        $(Row).append(TotalVisits, TotalVisitTime);

        if (Data[0].DriverID.indexOf("Scarb") > -1) {
            var TotalYield = FuncAddNewCell(undefined, ServiceTotalYield[i]);

            var TotalTipTickets = FuncAddNewCell(undefined, ServiceTotalTickets[i]);

            if (!TotalHeadersAdded) {
                $(TotalRow).append(FuncAddNewCell(undefined, "Yield (KG)"));
                $(TotalRow).append(FuncAddNewCell(undefined, "Tip Tickets"));
            }

            $(Row).append(TotalYield, TotalTipTickets);
        }

        while ($(Row).children().length != ColumnCount) {
            $(Row).append(FuncAddNewCell(undefined, ""));
        }
        while ($(TotalRow).children().length != ColumnCount) {
            $(TotalRow).append(FuncAddNewCell(undefined, ""));
        }

        $(TVbody).append(Row);
    }
}

function FindDuplicates(arr) {
    var len = arr.length,
        out = [],
        counts = {};

    for (var i = 0; i < len; i++) {
        var item = arr[i];
        counts[item] = counts[item] >= 1 ? counts[item] + 1 : 1;
        if (counts[item] === 2) {
            out.push(item);
        }
    }

    return out;
}




function AddIssueTypeChart(e) {
    var Data = [];

    // populates the data array with tuples showing the issue name and the quantity
    for (var item in e) {
        for (var IssueType in e[item].IssueOccurances) {
            var Index = Find(Data, IssueType);
            if (Index != 999) {
                Data[Index].value += e[item].IssueOccurances[IssueType];
            } else {
                Data.push({
                    key: IssueType,
                    value: e[item].IssueOccurances[IssueType]
                });
            }
        }
    }

    

    // Replaces issue codes with text then calls CreateIssueTypeChart
    ReplaceIssueCodesWithText(Data);
}

function CreateIssueTypeChart(Data) {    
    var Series = [];
    var Keys = [];
    var Colors = [];

    var sortedIssues = [];
    for (var temp in Data) {
        sortedIssues.push([temp, Data[temp]]);
    }

    sortedIssues.sort(function (a, b) {
        return a[1].value - b[1].value;
    }).reverse();

    if (Data.length > 0) {        
        var issueColours = ["#b91d1d", "#2da92c", "#3493e9", "#f2aa00", "#893ad8", "#e0762a", "#9665cb", "#ffb428", "#b91d1d", "#2da92c", "#3493e9", "#f2aa00", "#893ad8", "#e0762a", "#9665cb", "#ffb428"];

        for (var Item in Data) {
            Keys.push(Data[Item].key);

            if (issueColours.length != 0) {
                Colors.push(issueColours.shift());
            } else if (Data[Item].key == "Bin Empty") {
                Colors.push("#9665cb");                
            } else if (miscIssueColours.length != 0) {
                Colors.push(miscIssueColours.pop());            
            } else if (Data[Item].key == "Bin Empty") {
                Colors.push("#9665cb");                
            } else if (miscIssueColours.length != 0) {
                Colors.push(miscIssueColours.pop());            
            } else {
                Colors.push("#2adbcf");
            }

            Series.push(sortedIssues[Item][1].value);
        }
    } else {
        Colors.push('#000000');
        Series.push(999);
    }

    IssueOccuranceChart.renderOptions.colors = Colors;
    IssueOccuranceChart.series = Series;
    IssueOccuranceChart.update();

    var ToolTipText = "";

    if (Series.length > 0 && Series[0] != 999) {
        for (var i = 0; i != Series.length; i++) {
            ToolTipText += '<div class="SeriesDiv" style="background-color: ' + Colors[i] + '"></div> ' + Keys[i] + " - " + Series[i] + "</br>";
        }
    } else {
        ToolTipText += "No Issues Logged</br>";
    }


    if (!$('#IssueOccuranceChart').attr('data-toggle')) {
        $('#IssueOccuranceChart').attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", ToolTipText).tooltip();
    }
    //$('#IssueOccuranceChartIcon').attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", ToolTipText).tooltip();
}

// Previous Data Used For Comparisons

function SetLocalStorage(repData, Client, MD) {
    window.localStorage.setItem('SOTN_' + Client + '_' + MD /*(new Date().toLocaleDateString().replace(/\//g, ""))*/ + '_PastData', JSON.stringify(repData));
}

function GetLocalStorage(Client, MD) {
    return window.localStorage.getItem('SOTN_' + Client + '_' + MD /*(new Date().toLocaleDateString().replace(/\//g, ""))*/ + '_PastData');
}

function ComparePreviousPass(CurrentData, Client, MD) {
    var PreviousDataStorage = localStorage['SOTN_' + Client + '_' + MD /*(new Date().toLocaleDateString().replace(/\//g, ""))*/ + '_PastData'] || '';
    var Criteria = [];

    Criteria.push({ key: "BinsCollected", value: false },
                    { key: "BinsUncollected", value: true },
                    { key: "StreetsCollected", value: false },
                    { key: "PickRate", value: false },
                    { key: "MilesTravelled", value: false },
                    { key: "Speed", value: false },
                    { key: "TimeToComplete", value: true },
                    { key: "Completion", value: false });

    if (PreviousDataStorage != "") {
        var PreviousData = JSON.parse(PreviousDataStorage);
        for (var i = 0; i < CurrentData.length; i++) {
            if (!CurrentData[i] || !PreviousData[i]) {
                continue;
            }

            for (var key in Criteria) {
                var Current = Criteria[key].key;
                var Invert = Criteria[key].value;
                var glyphicon_span = document.createElement('span');

                var RoundID = CurrentData[i].Round.Crew + CurrentData[i].Round.Day + CurrentData[i].Round.Week + CurrentData[i].Round.Service;

                if (CurrentData[i][Current] != PreviousData[i][Current]) {
                    $(glyphicon_span).addClass((Invert) ? ((CurrentData[i][Current] < PreviousData[i][Current]) ? 'glyphicon glyphicon-arrow-down Positive' : 'glyphicon glyphicon-arrow-up Negative') : ((CurrentData[i][Current] > PreviousData[i][Current]) ? 'glyphicon glyphicon-arrow-up Positive' : 'glyphicon glyphicon-arrow-down Negative'));
                    $('#' + CurrentData[i].DriverID + "-" + RoundID + '-' + Current).append(glyphicon_span);
                }
            }
        }
    }
}

function DoubleToTime(Value) {
    Value = ((Value + "").split(".")[1] <= 5) ? Math.floor(Value) : Math.ceil(Value);
    return ((Value < 10) ? "0" + Value : Value) + ":" + (((Value + "").split(".")[0] <= 30) ? "00" : "30");
}

function AddClockData(e) {
    var LastToFinish = new Date(e[0].EstimatedFinish);
    var x = 0;

    for (var i = 0; i < e.length; i++) {
        var Current = new Date(e[i].EstimatedFinish);
        if (LastToFinish < Current) {
            x = i;
            LastToFinish = Current;
        }
    }

    var canvas = document.getElementById("Clock1");

    var ctx = canvas.getContext("2d");

    ctx.clearRect(0 - (canvas.width / 2), 0 - (canvas.height / 2), canvas.width, canvas.height);

    ctx.beginPath();

    var radius = canvas.height / 2.2;

    ctx.save();

    ctx.translate(radius, radius);

    radius = radius * 0.90

    var WillFinishToday = true;
   
    if (LastToFinish.getDate() != new Date().getDate()) {
        // If the vehicle isn't going to finish its round today, set color to red
        Color = '#c42930';

        WillFinishToday = false;
    } else if (LastToFinish.getTime() > new Date(e[x].LoginTime).getTime() + 28800) {
        // If they are going to finish today but spent more than 8 hours doing it, set color to yellow
        Color = '#f2aa00';
    } else {
        // Else they completed their round within 8 hours, set the color to green.
        Color = '#3493e9';
    }

    // Set clock label
    $('#LastVehicleTime').text(e[x].Registration + " " + FormatTime(LastToFinish).slice(0, 5));

    drawClock(ctx, radius, LastToFinish, Color);

    ctx.restore();
}

function drawClock(ctx, radius, Value, Color) {
    drawFace(ctx, radius, Color);

    drawNumbers(ctx, radius);

    drawTime(ctx, radius, Value);
}

function drawFace(ctx, radius, Color) {
    var canvas = document.getElementById("Clock1" );
    var grad;
    ctx.beginPath();
    ctx.arc(0, 0, radius, 0, 2 * Math.PI);
    ctx.fillStyle = 'white';
    ctx.fill();
    grad = ctx.createRadialGradient(0 - (canvas.width / 4), 0 - (canvas.height / 4), radius * 0.95, 0, 0, radius * 1.05);
    grad.addColorStop(0, Color);//'#3493e9'
    ctx.strokeStyle = grad;
    ctx.lineWidth = radius * 0.1;
    ctx.stroke();
    ctx.beginPath();

    ctx.arc(0, 0, radius * 0.1, 0, 2 * Math.PI);
    ctx.fillStyle = 'Black';
    ctx.fill();
}

function drawNumbers(ctx, radius) {
    var ang;
    var num;
    ctx.font = radius * 0.35 + "px arial";
    ctx.textBaseline = "middle";
    ctx.textAlign = "center";
    for (num = 1; num < 13; num++) {
        if (num % 3 == 0) {
            ang = num * Math.PI / 6;
            ctx.rotate(ang);
            ctx.translate(0, -radius * 0.85);
            ctx.rotate(-ang);
            ctx.fillText('\u2022', 0, 0);
            ctx.rotate(ang);
            ctx.translate(0, radius * 0.85);
            ctx.rotate(-ang);
        }
    }
    ctx.font = radius * 0.15 + "px arial";
}

function drawTime(ctx, radius, now) {
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    DigitalHour = (hour < 10) ? "0" + hour : hour;
    DigitalMinute = (minute < 10) ? "0" + minute : minute;
    hour = hour % 12;
    hour = (hour * Math.PI / 6) + (minute * Math.PI / (6 * 60)) + (second * Math.PI / (360 * 60));
    drawHand(ctx, hour, radius * 0.5, radius * 0.07);
    minute = (minute * Math.PI / 30) + (second * Math.PI / (30 * 60));
    drawHand(ctx, minute, radius * 0.75);
    ctx.fillStyle = '#a92026';
    ctx.font = radius * 0.35 + "px arial";
}

function drawHand(ctx, pos, length) {
    ctx.beginPath();
    ctx.lineWidth = 8;
    ctx.lineCap = "square";
    ctx.moveTo(0, 0);
    ctx.rotate(pos);
    ctx.lineTo(0, -length);
    ctx.stroke();
    ctx.rotate(-pos);
}

function DateTimeCompare(HighestValue, CurrentValue) {
    //var date1 = new Date("7/Nov/2012 20:30:00");
    //var date3 = new Date();

    var HighestValueParsed = HighestValue.slice(5, 7) + "/" + HighestValue.slice(8, 10) + "/" + HighestValue.slice(0, 4) + " " + HighestValue.slice(11, 19);

    var CurrentValueParsed = CurrentValue.slice(5, 7) + "/" + CurrentValue.slice(8, 10) + "/" + CurrentValue.slice(0, 4) + " " + CurrentValue.slice(11, 19);//(Current.getMonth() + 1) + "/" + Current.getDate() + "/" + Current.toString().slice(11, 15) + " " + Current.toString().slice(16, 24);

    var HighestValueDate = Date.parse(HighestValueParsed);

    var CurrentValueDate = Date.parse(CurrentValueParsed); //"12/7/2012 21:30:00"

    return (HighestValueDate < CurrentValueDate)
}

function isToday(date) {
    return new Date().getDate() == date.slice(8, 10);
}
// Data Export

function ExportData(TableID) {
    var csv = [];
    var rows = $('#' + TableID + ' tr');

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++) {
            row.push(cols[j].innerText.replace("\r\n", " ").replace("↵", " ").replace(/(\r\n|\n|\r)/gm, " "));
        }

        csv.push(row.join(","));
    }

    var csvData = csv.join("\n");

    var csvFile = new Blob([csvData], { type: "text/csv" });

    var TimeFormat = ((Time.getHours() < 10) ? "0" + Time.getHours() : Time.getHours()) + ":" + ((Time.getMinutes() < 10) ? "0" + Time.getMinutes() : Time.getMinutes()) + ":" + ((Time.getSeconds() < 10) ? "0" + Time.getSeconds() : Time.getSeconds());
    var DateFormat = ((Time.getDate() < 10) ? "0" + Time.getDate() : Time.getDate()) + "." + ((Time.getMonth() < 10) ? "0" + (parseInt(Time.getMonth()) + 1) : parseInt(Time.getMonth() + 1)) + "." + Time.getFullYear();

    var ExportName = client + "-" + DateFormat + "-" + TimeFormat + "-data.csv";

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(csvFile, ExportName);
    } else {
        var downloadLink = document.createElement("a");

        downloadLink.download = ExportName;

        // Create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Hide download link
        downloadLink.style.display = "none";

        // Add the link to DOM
        document.body.appendChild(downloadLink);

        // Click download link
        downloadLink.click();
    }


    return;

    /*
    var ColumnCount = document.getElementById(TableID).rows[1].cells.length;

    //if (TableID == 'SOTN_Table') { ColumnCount--; }

    var HeadersSource = [];
    $('#' + TableID + ' th').each(function () { HeadersSource.push($(this).text()) });

    var Headers = HeadersSource.splice((HeadersSource.length - ColumnCount), ColumnCount); // Remove Header Headers

    if (TableID == 'SOTN_Table') {
        if (Headers.indexOf("Alerts") < 0) {
            Headers.unshift("Driver ID");
        }
        else {
            ColumnCount--;
        }
        Headers.splice(Headers.indexOf("Alerts"), 1);
    }

    for (var Item in Headers) {
        if (Headers[Item].toString().indexOf("    ") > -1) {
            Headers[Item] = Headers[Item].replace(/\n|\r/g, "");
        }
    }

    //Gets Row Data
    var RowDataSource = [];

    $('#' + TableID + ' td').each(function () {
        if ($(this).text() != "") {
            RowDataSource.push($(this).text());
        }
    });

    var CSVString = Headers.toString() + "\r\n" + prepCSVRow(Separate(RowDataSource, ColumnCount), ColumnCount, '');
    

    /*
    var downloadLink = document.createElement("a");
    downloadLink.download = client + "-" + DateFormat + "-" + TimeFormat + "-data.csv";
    downloadLink.href = 'data:text/csv;charset=utf-8;base64,' + window.btoa(CSVString);

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);* /

    

    var Data = new Blob([CSVString],{type: "text/csv;charset=utf-8;"});

    if (navigator.msSaveBlob) { 
        navigator.msSaveBlob(Data, ExportName)
    } else {
        var link = document.createElement("a");

        if (link.download !== undefined) {
            var url = URL.createObjectURL(Data);
            link.setAttribute("href", url);
            link.setAttribute("download", ExportName);
            link.style = "visibility:hidden";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }           
    }

    */
}

function FormatTime(Time) {
    return ((Time.getHours() < 10) ? "0" + Time.getHours() : Time.getHours()) + ":" + ((Time.getMinutes() < 10) ? "0" + Time.getMinutes() : Time.getMinutes()) + ":" + ((Time.getSeconds() < 10) ? "0" + Time.getSeconds() : Time.getSeconds());
}

function prepCSVRow(arr, columnCount, initial) {
    var row = ''; // this will hold data
    var delimeter = ','; // data slice separator, in excel it's `;`, in usual CSv it's `,`
    var newLine = '\r\n'; // newline separator for CSV row

    /*
     * Convert [1,2,3,4] into [[1,2], [3,4]] while count is 2
     * @param _arr {Array} - the actual array to split
     * @param _count {Number} - the amount to split
     * return {Array} - splitted array
     */
    function splitArray(_arr, _count) {
        var splitted = [];
        var result = [];
        _arr.forEach(function (item, idx) {
            if ((idx + 1) % _count === 0) {
                splitted.push(item);
                result.push(splitted);
                splitted = [];
            } else {
                splitted.push(item);
            }
        });
        return result;
    }
    //var plainArr = splitArray(arr, columnCount);
    // don't know how to explain this
    // you just have to like follow the code
    // and you understand, it's pretty simple
    // it converts `['a', 'b', 'c']` to `a,b,c` string
    arr.forEach(function (arrItem) {
        arrItem.forEach(function (item, idx) {
            row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
        });
        row = row + newLine;
    });
    return initial + row;
}

function Separate(RowDataSource, ColumnCount) {
    var results = [];
    while (RowDataSource.length) {
        results.push(RowDataSource.splice(0, ColumnCount));
    }
    return results;
};

function EmailData() {
    //window.open('mailto:test@example.com');
    var email = 'sample@gmail.com';
    var subject = 'Test';
    var emailBody = 'Hi Sample,';
    var attach = 'path';
    document.location = "mailto:" + email + "?subject=" + subject + "&body=" + emailBody +
    "?attach=" + attach;
}

function FuncAddListAlert(Title, Text) {
    var NewAlert = document.createElement('li');

    $(NewAlert).addClass('Alert-List');

    NewAlert.innerHTML = '<div class="alert alert-danger alert-dismissable">' +
                            '<a type="button" href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' +
                            '<strong>' + Title + ':</strong> ' + Text +
                        '</div>';

    $('#Alerts-List').append(NewAlert);

    $('#Alerts-None').hide();

    $('#Alerts-Some').show();
}

/*
function FuncAddTableAlert(TableID, RowOffset, RowIndex, Text) {

    var NewAlert = document.createElement('td'),
        Icon = document.createElement('i');

    $(Icon).addClass("ionicons ion-alert Alert-Table").attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", Text).tooltip();

    $(NewAlert).append(Icon);

    var Table = document.getElementById(TableID),
        ColumnCount = $($("#" + TableID + " thead tr")[RowOffset - 1]).children().length;

    for (var i = RowOffset; i < Table.rows.length; i++) {
        if (i - RowOffset != RowIndex) {
            if (Table.rows[i].cells.length <= ColumnCount - 1) {
                //Table.rows[i].insertCell(ColumnCount - 1);
                $(Table.rows[i]).append(NewAlert);
            }
        }
    }

    return NewAlert;
}*/
function FuncAddTableAlert(TableID, RowOffset, RowIndex, Text) {

    var NewAlert = document.createElement('td'),
        Icon = document.createElement('i');

    $(Icon).addClass("ionicons ion-alert Alert-Table").attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", Text).tooltip();;

    $(NewAlert).append(Icon);

    var Table = document.getElementById(TableID),
        ColumnCount = $($("#" + TableID + " thead tr")[RowOffset - 1]).children().length;

    for (var i = RowOffset; i < Table.rows.length; i++) {
        if (i - RowOffset != RowIndex) {
            if (Table.rows[i].cells.length <= RowOffset - 1) {
                Table.rows[i].insertCell(RowOffset - 1);
            }
        }
    }

    return NewAlert;
}

function FuncAddNewCell(ID, Text) {
    var NewCell = document.createElement('td');

    if (ID != undefined) {
        NewCell.id = ID;
    }

    NewCell.textContent = Text;

    return NewCell;
}


/*
function PositiveColours() {
    return ["#51a9f7", "#3493e9", "#1e88ea", "#0873d6", "#1b5e9b", ];

    //return ["#2da92c", "#3493e9", "#1e88ea", "#0873d6", "#1b5e9b", ];
}

function NegativeColours() {
    return ["#c42930", "#a5262b", "#841116", "#700b0f", "#ad0c12", ];
}
*/

function sortTable(ID, ColumnIndex) {
    ColumnIndex = (ColumnIndex == undefined) ? 1 : ColumnIndex;

    var rows, switching, i, x, y, shouldSwitch;

    switching = true;

    while (switching) {
        switching = false;
        rows = $(ID + 'Table > tbody > tr');
        for (i = 0; i < (rows.length - 1) ; i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[ColumnIndex];
            y = rows[i + 1].getElementsByTagName("TD")[ColumnIndex];
            if (parseFloat(x.innerHTML.toLowerCase()) < parseFloat(y.innerHTML.toLowerCase())) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function ServiceData(e) {
    var Services = [];
    var VehiclesOut = [];
    var Colors = [];

    for (var i = 0; i < e.length; i++) {
        var ServiceIndex = Services.indexOf(e[i].Round.Service);

        if (ServiceIndex < 0) {
            Services.push(e[i].Round.Service);

            ServiceIndex = Services.indexOf(e[i].Round.Service);

            VehiclesOut.push(0);
        }
        VehiclesOut[ServiceIndex]++;
    }

    if (Services < 1) {
        $('#VehiclesOutTile').hide();
    } else {
        for (var i = 0; i < Services.length; i++) {
            var Color = "";
            var Name = "";
            var FontColor = "#424242";

            if (Services[i] == "Refuse") {
                Name = "Refuse";
                Color = "#a7a7a7";
            }
            else if (Services[i] == "Recycling") {
                Name = "Recycling";
                Color = "#65b6f7";
            }
            else if (Services[i] == "Garden") {
                Name = "Garden";
            }
            else if (Services[i] == "Food") {
                Name = "Food";
                Color = '#705e4d';
            }
            else if (Services[i] == "Glass") {
                Name = "Glass";
                Color = '#b3b3b391;';
            }
            else if (Services[i] == "Delivery") {
                Name = "Delivery";
            }
            else if (Services[i] == "Organic") {
                Name = "Organic";
                Color = '#89c14d'
            }
            else if (Services[i] == "AHP") {
                Name = "AHP";
                Color = "#e0762a";
            }
            else if (Services[i] == "Trade") {
                Name = "Trade";
                Color = '#b91d1d';
            }
            else if (Services[i] == "Trade Refuse") {
                Name = "TradeRefuse";
                Color = '#b91d1d';
            }
            else if (Services[i] == "Trade Recycling") {
                Name = "TradeRecycling";
                Color = '#b91d1d';
            }
            else if (Services[i] == "Trade Garden") {
                Name = "TradeGarden";
            }
            else if (Services[i] == "Box") {
                Name = "Box";
            }
            else if (Services[i] == "Green") {
                Name = "Green";
                Color = '#548224';
            }
            else if (Services[i] == "Bulky") {
                Name = "Bulky";
                Color = '#f5b21c';
            }
            else if (Services[i] == "Plastic") {
                Name = "Plastic";
                Color = '#78437c';
                FontColor = "#ffffff";
            }
            else if (Services[i] == "Small Mech") {
                Name = "SmallMech";
                Color = '#5578dc';
            }
            else if (Services[i] == "Large Mech") {
                Name = "LargeMech";
                Color = '#dc5568';
            }
            else if (Services[i] == "Gan") {
                Name = "Gang";
                Color = '#b8b8b8';
            } else {
                FuncAddListAlert("Unknown Service", "Unrecognised service - " + Services[i] + ".");
            }

            var AddVehicle = document.createElement('div');
            $(AddVehicle).addClass('Vehicle-Div');

            $(AddVehicle).css("margin-right", "0px");

            var Multipler = 5;

            if (VehiclesOut.length > 3) {
                Multipler = 5.5;
                //$(AddVehicle).css("margin-top", "3%");
            }

            $('#VehiclesOutTile').css("width", (VehiclesOut.length * 100 - (VehiclesOut.length * 10)) + "px");

            var LeftMargin = (VehiclesOut[i] < 10) ? 38 : 27;

            $(AddVehicle).append('<i class="fa fa-truck Vehicle-Icon" style="color:' + Color + '"></i><p class="Vehicle-Text" id="' + Services[i] + '" style="margin-left: ' + LeftMargin + 'px; color: '+ FontColor + '">' + VehiclesOut[i] + '</p>');

            $(AddVehicle).attr("data-toggle", "tooltip").attr("data-html", "true").attr("data-placement", "bottom").attr("title", VehiclesOut[i] + " " + Name + " vehicle" + ((VehiclesOut[i] > 1) ? "s" : "") + " out").tooltip();
            $('#VehiclesOut').append(AddVehicle);
        }
    }
}

Number.prototype.roundTo = function (num) {
    var resto = this % num;
    if (resto <= (num / 2)) {
        return this - resto;
    } else {
        return this + num - resto;
    }
}


function XOffset() {
    var canvas = document.getElementById("RoundCompletionChartCanvas").getContext("2d");

    canvas.fillStyle = '#f442b9';

    for (var i = 0; i < 10; i++) {

        var X = XPoint + ((BarWidth) * i);

        canvas.fillRect(X, YPoint, 2, 2);
    }
}
