﻿var RoundCompletionChart;
//var PickRateChart;
var BinsCollectedChart;
var StreetsCollectedChart;
//var IssuesChart;
//var VehicleChart;
var IssueOccuranceChart;

var colors = ['rgba(191, 0, 0, 0.7)',
              'rgba(232, 31, 109, 0.7)',
              'rgba(145, 52, 162, 0.7)',
              'rgba(74, 88, 170, 0.7)',
              'rgba(52, 147, 223, 0.7)',
              'rgba(13, 136, 123, 0.7)',
              'rgba(137, 193, 77, 0.7)',
              'rgba(231, 196, 30, 0.7)',
              'rgba(231, 146, 23, 0.7)',
              'rgba(160, 105, 70, 0.7)',
              'rgba(190,198,124,0.7)',
              'rgba(176,205,206,0.7)',
              'rgba(197,191,163,0.7)'];

$(document).ready(function () {
    initCharts();
});

function createRoundCompletionChart(container, data) {
    var labels = [];
    var series = [];
    var colors = [];
    var Below25 = 0;

    for (var i = 0, len = data.length; i < len; i++) {
        labels.push(data[i].Round.Crew + " | " + data[i].Round.Day + " | " + data[i].Round.Week + " | " + data[i].Round.Service + " | " + data[i].Completion + "%")
        series.push((data[i].Completion < 1) ? 2 : data[i].Completion);
        colors.push((data[i].Completion <= 33) ? '#d52b2b' : ((data[i].Completion > 33 && data[i].Completion <= 66) ? '#f2aa00' : '#2da92c'));

        if (data[i].Completion < 25) {
            Below25++;
        }
    }

    if (Below25 >= (data.length / 100) * 25) {
        FuncAddListAlert("Warning", "Overall Fleet Performance.");
    }

    var canvas = document.createElement('canvas');
    $(container).append(canvas);

    var barData = {
        labels: labels,
        datasets: [{
            backgroundColor: colors,
            data: series,
        }],
    };

    RoundCompletionChart = new Chart(canvas.getContext('2d'), {
        type: 'bar',
        data: barData,
        options: {
            legend: {
                display: false,
            },
            maintainAspectRatio: false,
            responsive: true,
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: roundCompletionChartToolTips
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        display: false,
                        tickMarkLength: 50,
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true,
                        min: 0,
                        max: 100,
                    },
                    gridLines: {
                        drawBorder: false,
                    }
                }]
            }
        }
    });
}

function roundCompletionChartToolTips(item, data) {
    var rcc = repData[item.index];

    return rcc.DriverID.replace(/\D/g, '') + " | " + rcc.Registration;
};

function initCharts() { 
    /*RoundCompletionChart = new BarChart("#RoundCompletionChart");
    RoundCompletionChart.title = "";
    RoundCompletionChart.renderOptions.showKeys = false;
    RoundCompletionChart.renderOptions.showValues = false;
    RoundCompletionChart.renderOptions.decimals = 0;
    RoundCompletionChart.maximum = 100;
    RoundCompletionChart.width = 300;
    RoundCompletionChart.height = 340;
    RoundCompletionChart.step = 25;
    RoundCompletionChart.renderOptions.axisColor = "#efefef";
    RoundCompletionChart.renderOptions.font = "0px Arial";
    RoundCompletionChart.renderOptions.gridX = false;
    RoundCompletionChart.renderOptions.gridY = true;
    RoundCompletionChart.renderOptions.gridColor = "#979797";*/

    BinsCollectedChart = new DoughnutChart("#BinsCollectedChart");
    BinsCollectedChart.title = "";
    BinsCollectedChart.width = 300;
    BinsCollectedChart.height = 300;
    BinsCollectedChart.renderOptions.showKeys = false;
    BinsCollectedChart.renderOptions.showValues = false;
    BinsCollectedChart.renderOptions.showAsPercentage = false;
    
    StreetsCollectedChart = new DoughnutChart("#StreetsCollectedChart");
    StreetsCollectedChart.title = "";
    StreetsCollectedChart.width = 300;
    StreetsCollectedChart.height = 300;
    StreetsCollectedChart.renderOptions.showKeys = false;
    StreetsCollectedChart.renderOptions.showValues = false;
    StreetsCollectedChart.renderOptions.showAsPercentage = false;
    
    IssueOccuranceChart = new DoughnutChart('#IssueOccuranceChart');
    IssueOccuranceChart.title = "";
    IssueOccuranceChart.width = 300;
    IssueOccuranceChart.height = 300;
    IssueOccuranceChart.renderOptions.showValues = false;
    IssueOccuranceChart.renderOptions.showKeys = false;
}