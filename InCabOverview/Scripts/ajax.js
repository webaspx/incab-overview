﻿function GetAddressList(params)
{
    $.ajax({
        type: "POST",
        url: "ManageTask.aspx/SearchAddresses",
        data: "{addressText : " + params.addressText + ", streetnameText: " + params.streetnameText + ", townText: " + params.townText + ", postcodeText: " + params.postcodeText + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data)
        {
            $('#outputtext').text(data.d);
        },
        failure: function (response) { alert("FAIL"); }
    });
}

//var pendingAjaxRequests = new Array();
var pendingAjaxRequests = [];

function ajaxReqCompleted(obj, status)
{
    //  var index = $.inArray(obj, pendingAjaxRequests);

    //Creates a new array with the items that aren't the passed object
    pendingAjaxRequests = $.grep(pendingAjaxRequests, function (x)
    {
        return x !== obj;
    });
    
}

function cancelAjaxRequests()
{
    $.each(pendingAjaxRequests, function (index, value)
    {
        value.abort();
    });

    //pendingAjaxRequests = new Array(); //reset the array
    pendingAjaxRequests = []; //reset the array
}


function ajax(page, func, inputVarsJson, successCallback, failCallback, timeout)
{//returns json object
    if (typeof timeout === 'undefined') { timeout = 30000; }

    pendingAjaxRequests.push(
            $.ajax({
            type: "POST",
            url: page + "/" + func,
            data: JSON.stringify(inputVarsJson),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg)
            {
                if (successCallback)
                    successCallback(JSON.parse(msg.d));
            },
            error: function (error)
            {
                if (failCallback)
                    failCallback(error);
            },
            complete: ajaxReqCompleted,
            timeout: timeout
            })
        );
}


function OnSuccess(response)
{
    alert(response.d);
}
