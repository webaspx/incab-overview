var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var BarWidth = 0;
var XPoint, YPoint;
var Height;

var Extension = /** @class */ (function () {
    function Extension() {
    }
    /**
     * Used by array.reduce to find the longest string.
     * @param a String a
     * @param b String b
     */
    Extension.longestLength = function (a, b) {
        return a.length > b.length ? a : b;
    };
    /* tslint:disable */
    Extension.shadeBlendConvert = function (p, from, to) {
        if (to === void 0) { to = ""; }
        if (typeof (p) != "number" || p < -1 || p > 1 || typeof (from) != "string" || (from[0] != 'r' && from[0] != '#') || (typeof (to) != "string" && typeof (to) != "undefined"))
            return null; //ErrorCheck
        if (!this.sbcRip)
            this.sbcRip = function (d) {
                var l = d.length, RGB = new Object();
                if (l > 9) {
                    d = d.split(",");
                    if (d.length < 3 || d.length > 4)
                        return null; //ErrorCheck
                    RGB[0] = i(d[0].slice(4)), RGB[1] = i(d[1]), RGB[2] = i(d[2]), RGB[3] = d[3] ? parseFloat(d[3]) : -1;
                }
                else {
                    if (l == 8 || l == 6 || l < 4)
                        return null; //ErrorCheck
                    if (l < 6)
                        d = "#" + d[1] + d[1] + d[2] + d[2] + d[3] + d[3] + (l > 4 ? d[4] + "" + d[4] : ""); //3 digit
                    d = i(d.slice(1), 16), RGB[0] = d >> 16 & 255, RGB[1] = d >> 8 & 255, RGB[2] = d & 255, RGB[3] = l == 9 || l == 5 ? r(((d >> 24 & 255) / 255) * 10000) / 10000 : -1;
                }
                return RGB;
            };
        var i = parseInt, r = Math.round, h = from.length > 9, h = typeof (to) == "string" ? to.length > 9 ? true : to == "c" ? !h : false : h, b = p < 0, p = b ? p * -1 : p, to = to && to != "c" ? to : b ? "#000000" : "#FFFFFF", f = this.sbcRip(from), t = this.sbcRip(to);
        if (!f || !t)
            return null; //ErrorCheck
        if (h)
            return "rgb(" + r((t[0] - f[0]) * p + f[0]) + "," + r((t[1] - f[1]) * p + f[1]) + "," + r((t[2] - f[2]) * p + f[2]) + (f[3] < 0 && t[3] < 0 ? ")" : "," + (f[3] > -1 && t[3] > -1 ? r(((t[3] - f[3]) * p + f[3]) * 10000) / 10000 : t[3] < 0 ? f[3] : t[3]) + ")");
        else
            return "#" + (0x100000000 + (f[3] > -1 && t[3] > -1 ? r(((t[3] - f[3]) * p + f[3]) * 255) : t[3] > -1 ? r(t[3] * 255) : f[3] > -1 ? r(f[3] * 255) : 255) * 0x1000000 + r((t[0] - f[0]) * p + f[0]) * 0x10000 + r((t[1] - f[1]) * p + f[1]) * 0x100 + r((t[2] - f[2]) * p + f[2])).toString(16).slice(f[3] > -1 || t[3] > -1 ? 1 : 3);
    };
    return Extension;
}());
/**
 * Add ellipse to the context - For internet explorer.
 */
if (CanvasRenderingContext2D.prototype.ellipse === undefined) {
    CanvasRenderingContext2D.prototype.ellipse = function (x, y, radiusX, radiusY, rotation, startAngle, endAngle, antiClockwise) {
        this.save();
        this.translate(x, y);
        this.rotate(rotation);
        this.scale(radiusX, radiusY);
        this.arc(0, 0, 1, startAngle, endAngle, antiClockwise);
        this.restore();
    };
}
var Maths = /** @class */ (function () {
    function Maths() {
    }
    Maths.radians = function (degrees) {
        return (Math.PI * 2) / 360 * degrees;
    };
    /**
     * Adds a series of numbers together with array.reduce, ensuring the numbers
     * are absolute values.
     * @param total The total so far.
     * @param number The number to add to the total.
     */
    Maths.abs_sum = function (total, number) {
        return Math.abs(total) + Math.abs(number);
    };
    Maths.getArcPoint = function (c1, c2, radius, angle) {
        return [c1 + Math.cos(angle) * radius, c2 + Math.sin(angle) * radius];
    };
    Maths.getEllipsePoint = function (cx, cy, ra, rb, t) {
        var x = cx + ra * Math.cos(t);
        var y = cy + rb * Math.sin(t);
        return [x, y];
    };
    /**
     * Array.Reduce to the largest number in the array.
     * @param a The first number.
     * @param b The second number.
     */
    Maths.reduce_largest = function (a, b) {
        return (a > b) ? a : b;
    };
    Maths.reduce_lowest = function (a, b) {
        return (a < b) ? a : b;
    };
    Maths.getStep = function (minimum, maximum, divisor) {
        var diff = maximum - minimum;
        if (diff % 1 === 0 || diff >= 2) {
            var numbers = Math.abs(Math.ceil(diff)).toString().length;
            var pow = Math.pow(10, numbers);
            if (pow > diff * 2) {
                pow /= 2;
            }
            return Math.abs(Math.ceil(diff / pow) * pow) / divisor;
        }
        else {
            if (diff >= 1) {
                return 2 / divisor;
            }
            else {
                // Decimal
                var decimal = (diff.toString().indexOf(".") !== -1) ? diff.toString().split(".")[1] : "0";
                var decimalStart = 1;
                while (decimal.length > 0 && decimal.indexOf("0") === 0) {
                    decimal = decimal.substring(1);
                    decimalStart++;
                }
                var pow = Number(Math.pow(0.1, decimalStart).toFixed(decimalStart));
                return Math.abs(Math.ceil(diff / pow) * pow) / (divisor / 2);
            }
        }
    };
    /**
     * Removes floating numbers from the number provided.
     * @param num The number to fix.
     * @param step The step amount.
     */
    Maths.strip = function (num, step) {
        if (step.toString().indexOf(".") !== -1) {
            var decimals = step.toString().substring(step.toString().indexOf(".") + 1).length;
            return num.toFixed(decimals);
        }
        else {
            return num.toFixed(0);
        }
    };
    Maths.drawCurve = function (ctx, ptsa, tension, resolution) {
        var res = this.drawLines(ctx, this.getCurvePoints(ptsa, tension, resolution));
        //drawPoints (ctx, res);
    };
    Maths.drawLines = function (ctx, pts) {
        ctx.beginPath();
        ctx.moveTo(pts[0][0], pts[0][1]);
        for (var i = 1, len = pts.length; i < len; i++) {
            ctx.lineTo(pts[i][0], pts[i][1]);
        }
        ctx.stroke();
        return pts;
    };
    // https://codepen.io/viglino/pen/PGEgwW
    Maths.getCurvePoints = function (line, tension, resolution) {
        // use input value if provided, or use a default value	 
        tension = typeof tension === "number" ? tension : 0.5;
        resolution = resolution || 10;
        tension = Math.abs(tension);
        var pts, res = [], // clone array
        x, y, // our x,y coords
        t1x, t2x, t1y, t2y, // tension vectors
        c1, c2, c3, c4, // cardinal points
        st, t, i; // steps based on num. of segments
        // clone array so we don't change the original
        //
        pts = line.slice(0);
        // The algorithm require a previous and next point to the actual point array.
        // Check if we will draw closed or open curve.
        // If closed, copy end points to beginning and first points to end
        // If open, duplicate first points to befinning, end points to end
        if (line.length > 2 && line[0][0] === line[line.length - 1][0] && line[0][1] === line[line.length - 1][1]) {
            pts.unshift(line[line.length - 2]);
            pts.push(line[1]);
        }
        else {
            pts.unshift(line[0]);
            pts.push(line[line.length - 1]);
        }
        // ok, lets start..
        // 1. loop goes through point array
        // 2. loop goes through each segment between the 2 pts + 1e point before and after
        for (var i_1 = 1, len = pts.length - 2; i_1 < len; i_1++) {
            var dx = pts[i_1 + 1][0] - pts[i_1][0];
            var dy = pts[i_1 + 1][1] - pts[i_1][1];
            var numOfSegments = Math.round(Math.sqrt(dx * dx + dy * dy) / resolution);
            for (t = 0; t <= numOfSegments; t++) {
                t1x = (pts[i_1 + 1][0] - pts[i_1 - 1][0]) * tension;
                t2x = (pts[i_1 + 2][0] - pts[i_1][0]) * tension;
                t1y = (pts[i_1 + 1][1] - pts[i_1 - 1][1]) * tension;
                t2y = (pts[i_1 + 2][1] - pts[i_1][1]) * tension;
                // calc step
                st = t / numOfSegments;
                // calc cardinals
                c1 = 2 * Math.pow(st, 3) - 3 * Math.pow(st, 2) + 1;
                c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2);
                c3 = Math.pow(st, 3) - 2 * Math.pow(st, 2) + st;
                c4 = Math.pow(st, 3) - Math.pow(st, 2);
                // calc x and y cords with common control vectors
                x = c1 * pts[i_1][0] + c2 * pts[i_1 + 1][0] + c3 * t1x + c4 * t2x;
                y = c1 * pts[i_1][1] + c2 * pts[i_1 + 1][1] + c3 * t1y + c4 * t2y;
                //store points in array
                res.push([x, y]);
            }
        }
        return res;
    };
    return Maths;
}());
/// <reference path="../extensions/Extension.ts"/>
/// <reference path="../extensions/Maths.ts"/>
/// <reference path="../extensions/Styles.ts"/>
/**
 * An abstract class for axis charts (Bar Charts and Line Charts).
 */
var AxisChart = /** @class */ (function () {
    function AxisChart(container) {
        this.title = "Chart";
        this.xlabel = "X Axis";
        this.ylabel = "Y Axis";
        this.width = 600;
        this.height = 400;
        this.adjustHeight = true;
        /**
         * A set of option determining how the chart contents will be rendered.
         */
        this.renderOptions = new RenderOptions();
        this.container = document.getElementById(container.replace("#", ""));
        this.canvas = document.createElement("canvas");
        this.context = this.canvas.getContext("2d");
        this.container.appendChild(this.canvas);
    }
    /**
     * Draws the key definitions to the right of the pie chart.
     * Returns: The amount of space used to the right.
     */
    AxisChart.prototype.drawKeyDefinitions = function (top) {
        if (this.keys && this.keys.length > 0 && this.renderOptions.showKeys) {
            var keyWidth = this.context.measureText(this.keys.reduce(Extension.longestLength)).width;
            for (var i in this.keys) {
                if (this.keys.hasOwnProperty(i)) {
                    // Draw the key color.
                    this.context.fillStyle = this.renderOptions.colors[i];
                    this.context.fillRect(this.width - keyWidth - 20, Number(i) * 20 + top, 18, 10);
                    // Draw the key value.
                    this.context.fillStyle = this.renderOptions.keyColor;
                    this.context.fillText(this.keys[i], this.width - keyWidth, Number(i) * 20 + 10 + top);
                }
            }
            return keyWidth + 30;
        }
        return 0;
    };
    /**
     * Draws the Y axis label.
     * @param y1 YAxis start Y.
     * @param y2 YAxis end Y.
     * @param textHeight The height of the text.
     */
    AxisChart.prototype.drawYAxisLabel = function (y1, y2, textHeight) {
        this.context.save();
        this.context.translate(this.width / 2, y1 + (y2 - y1) / 2);
        this.context.rotate(Maths.radians(-90));
        // Axis is inverted this is actually x.
        var y = -(this.width) / 2 + textHeight * 0.75;
        var textWidth = this.context.measureText(this.ylabel).width;
        this.context.textAlign = "center";
        this.context.fillStyle = "black";
        this.context.fillText(this.ylabel, 0, y);
        this.context.restore();
    };
    /**
     * Draws the X axis label.
     * @param x1 XAxis start X.
     * @param x2 XAxis end X.
     * @param textHeight The height of the text.
     */
    AxisChart.prototype.drawXAxisLabel = function (x1, x2, textHeight) {
        var x = x1 + (x2 - x1) / 2;
        this.context.fillStyle = "black";
        this.context.textAlign = "center";
        // this.height should work alone as the label point is at the bottom, center of the text, but the text for some reason
        // clips slightly, -2 to prevent the clip.
        this.context.fillText(this.xlabel, x, this.height - 2);
    };
    /**
     * Draws the X axis.
     * @param x1 Start X of the axis.
     * @param y1 Start Y of the axis.
     * @param x2 End X of the axis.
     * @param y2 End Y of the axis.
     * @param xlabels An array of labels for the X axis.
     * @param textHeight The height of text.
     * @param spacing The spacing between the bars (for bar charts). Use zero otherwise.
     */
    AxisChart.prototype.drawXAxis = function (x1, y1, x2, y2, xlabels, textHeight, spacing, center, grid) {
        if (grid === void 0) { grid = true; }
        this.context.strokeStyle = this.renderOptions.axisColor;
        this.context.moveTo(x1, y2);
        this.context.lineTo(x2, y2);
        this.context.stroke();
        var increment = (x2 - x1) / ((center) ? xlabels.length : xlabels.length - 1);
        var textWidth = this.context.measureText(xlabels.reduce(Extension.longestLength)).width;
        // Draw the labels.
        for (var i in xlabels) {
            if (xlabels.hasOwnProperty(i)) {
                var x = 0;
                if (center) {
                    x = Math.round((x1 + increment * Number(i) + increment / 2 - spacing / 2));
                }
                else {
                    x = Math.round((x1 + increment * Number(i) - spacing / 2));
                }
                this.context.strokeStyle = this.renderOptions.axisColor;
                // Positions are rounded to the nearest whole number to disable antialiasing. 
                // translate(0.5, 0.5) is also part of the fix.
                // Center the text and markers + increment / 2.
                this.context.beginPath();
                this.context.moveTo(x, y2);
                this.context.lineTo(x, y2 + this.renderOptions.markerLength);
                this.context.stroke();
                // Draws the X axis grid.
                if (grid && this.renderOptions.gridX && Number(i) > 0) {
                    this.context.strokeStyle = this.renderOptions.gridColor;
                    this.context.beginPath();
                    this.context.moveTo(x, y1);
                    this.context.lineTo(x, y2);
                    this.context.stroke();
                }
                var y = y2 + textHeight + this.renderOptions.markerLength;
                if (this.renderOptions.xAngled) {
                    x += textHeight / 2;
                    this.context.save();
                    this.context.translate(x, y);
                    this.context.rotate(Maths.radians(300));
                    this.context.textAlign = "right";
                    this.context.fillText(xlabels[i], 0, 0);
                    this.context.restore();
                }
                else {
                    this.context.textAlign = "center";
                    this.context.fillText(xlabels[i], x, y);
                }
            }
        }
        return increment;
    };
    /**
     * Draws the Y Axis.
     * @param x1 Start X of the axis.
     * @param y1 Start Y of the axis.
     * @param x2 End X of the axis.
     * @param y2 End Y of the axis.
     * @param ylabels An array of labels for the Y axis.
     * @param textHeight The height of the label text.
     */
    AxisChart.prototype.drawYAxis = function (x1, y1, x2, y2, ylabels, textHeight, grid) {
        if (grid === void 0) { grid = true; }
        this.context.strokeStyle = this.renderOptions.axisColor;
        this.context.moveTo(x1, y1);
        this.context.lineTo(x1, y2);
        this.context.stroke();
        var height = y2 - y1;
        var increment = height / (ylabels.length - 1);
        ylabels.reverse();
        for (var i in ylabels) {
            if (ylabels.hasOwnProperty(i)) {
                this.context.strokeStyle = this.renderOptions.axisColor;
                this.context.beginPath();
                this.context.moveTo(Math.round(x1 - this.renderOptions.markerLength), Math.round(y1 + increment * Number(i)));
                this.context.lineTo(Math.round(x1), Math.round(y1 + increment * Number(i)));
                this.context.stroke();
                // Draw the Y axis grid.
                if (grid && this.renderOptions.gridY && Number(i) < ylabels.length - 1) {
                    this.context.strokeStyle = this.renderOptions.gridColor;
                    this.context.beginPath();
                    this.context.moveTo(x1, Math.round(y1 + increment * Number(i)));
                    this.context.lineTo(x2, Math.round(y1 + increment * Number(i)));
                    this.context.stroke();
                }
                this.context.textAlign = "right";
                this.context.fillText(ylabels[i], Math.round(x1 - this.renderOptions.markerLength - 5), Math.round(y1 + increment * Number(i) + textHeight / 2) - 2);
            }
        }
    };
    return AxisChart;
}());
/// <reference path="AxisChart.ts" />
var AdvancedLineChart = /** @class */ (function (_super) {
    __extends(AdvancedLineChart, _super);
    /**
    * Constructs a new BarChart.
    * @param container The container to append the chart.
    */
    function AdvancedLineChart(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0; // Step is always Y by default. Although the advanced line chart may also have a step for X.
        _this.stepX = 0;
        _this.lineWidth = 2;
        _this.cardinalSpline = new CardinalSplineOptions();
        _this.title = "Advanced Line Chart";
        _this.renderOptions.gridX = true; // Default.
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    /**
     * Updates the BarChart adding the new or updated data.
     */
    AdvancedLineChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var numbers = [].concat.apply([], this.series);
        var minimum = this.getMinimum(false);
        var maximum = this.getMaximum(false);
        var minimumX = this.getMinimum(true);
        var maximumX = this.getMaximum(true);
        // Automatically generate a step if zero.
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        var xstep = (this.stepX === 0) ? Maths.getStep(minimumX, maximumX, divisor) : this.stepX;
        var isDateTime = this.isDateTime();
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        if (!isDateTime) {
            maximumX = Math.ceil(maximumX / xstep) * xstep;
            minimumX = Math.floor(minimumX / xstep) * xstep;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        var segments;
        var xlabels = [];
        if (isDateTime) {
            // Get the data needed for a date and time x axis.
            var data = this.getDateTimeData(minimumX, maximumX);
            segments = data.segments;
            xlabels = data.xlabels;
        }
        else {
            // Calculate the labels using the step.
            for (var i = minimumX; i <= maximumX; i += xstep) {
                xlabels.push(Maths.strip(i, xstep));
            }
        }
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width;
        // Recalculate the text width and text size for the labels not the title.
        var textWidth = this.context.measureText(xlabels.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        if (this.renderOptions.xAngled) {
            bottom += Math.round(textWidth);
        }
        else {
            bottom += Math.round(textHeight);
        }
        if (this.xlabel) {
            bottom += textHeight + 10;
        }
        if (this.ylabel) {
            left += textHeight + 10;
        }
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        textWidth = this.context.measureText(xlabels[xlabels.length - 1]).width;
        right += textWidth / 2;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        this.drawYAxis(x1, y1, x2, y2, ylabels, textHeight);
        var xincrement = this.drawXAxis(x1, y1, x2, y2, xlabels, textHeight, 0, false);
        if (isDateTime) {
            this.drawDateTimeLines(x1, y1, x2, y2, step, minimum, maximum, segments);
        }
        else {
            this.drawLines(x1, y1, x2, y2, minimumX, maximumX, xstep, step, minimum, maximum);
        }
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2, textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1, y2, textHeight);
        }
        // Draw the title.
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };
    AdvancedLineChart.prototype.drawLines = function (x1, y1, x2, y2, minimumX, maximumX, xstep, step, minimum, maximum) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        var xincrement = (x2 - x1) / (maximumX - minimumX);
        var yzero = y2 - yincrement * Math.abs(minimum);
        var xzero = (minimumX < 0) ? x1 + xincrement * Math.abs(minimumX) : x1 - xincrement * minimumX;
        var pointSize = this.renderOptions.pointSize;
        this.context.lineWidth = this.lineWidth;
        // Draw the lines
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i) && this.keys.length > Number(i)) {
                var data = this.series[i];
                var points = [];
                this.context.beginPath();
                this.context.fillStyle = this.renderOptions.colors[i];
                this.context.strokeStyle = this.renderOptions.colors[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var value = data[j]; // Value is X,Y
                        var x = 0;
                        var y = 0;
                        if (value[0] < 0) {
                            x = xzero - xincrement * Math.abs(value[0]);
                        }
                        else {
                            x = xzero + xincrement * value[0];
                        }
                        if (value[1] < 0) {
                            y = yzero + yincrement * Math.abs(value[1]);
                        }
                        else {
                            y = yzero - yincrement * value[1];
                        }
                        points.push([x, y]);
                        this.context.fillRect(x - pointSize / 2, y - pointSize / 2, pointSize, pointSize);
                    }
                }
                if (this.cardinalSpline.visible) {
                    Maths.drawCurve(this.context, points, this.cardinalSpline.tension, this.cardinalSpline.resolution);
                }
                else {
                    for (var j in points) {
                        if (points.hasOwnProperty(j)) {
                            this.context.lineTo(points[j][0], points[j][1]);
                        }
                    }
                }
                this.context.stroke();
            }
        }
    };
    AdvancedLineChart.prototype.drawDateTimeLines = function (x1, y1, x2, y2, step, minimum, maximum, segments) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        var yzero = y2 - yincrement * Math.abs(minimum);
        var pointSize = this.renderOptions.pointSize;
        var xincrement = (x2 - x1) / (segments.length - 1);
        this.context.lineWidth = this.lineWidth;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i) && this.keys.length > Number(i)) {
                var data = this.series[i];
                var points = [];
                this.context.beginPath();
                this.context.fillStyle = this.renderOptions.colors[i];
                this.context.strokeStyle = this.renderOptions.colors[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var value = data[j]; // Value is X,Y
                        var time = value[0].getTime();
                        var x = 0;
                        var y = 0;
                        for (var k = 1; k < segments.length; k++) {
                            if (time >= segments[k - 1].time && time <= segments[k].time) {
                                var timeIncrement = segments[k].time - segments[k - 1].time;
                                var increment = xincrement / timeIncrement;
                                x = increment * (time - segments[k - 1].time) + xincrement * (k - 1) + x1;
                                if (value[1] < 0) {
                                    y = yzero + yincrement * Math.abs(value[1]);
                                }
                                else {
                                    y = yzero - yincrement * value[1];
                                }
                                points.push([x, y]);
                                this.context.fillRect(x - pointSize / 2, y - pointSize / 2, pointSize, pointSize);
                            }
                        }
                    }
                }
                if (this.cardinalSpline.visible) {
                    Maths.drawCurve(this.context, points, this.cardinalSpline.tension, this.cardinalSpline.resolution);
                }
                else {
                    for (var j in points) {
                        if (points.hasOwnProperty(j)) {
                            this.context.lineTo(points[j][0], points[j][1]);
                        }
                    }
                }
                this.context.stroke();
            }
        }
    };
    AdvancedLineChart.prototype.getMinimum = function (x) {
        var minimum = 0;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var data = this.series[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var values = data[j];
                        if (x && typeof values[0].getMonth === "function") {
                            // X value is a date.
                            var ms = values[0].getTime();
                            if (minimum > ms || minimum === 0) {
                                minimum = ms;
                            }
                        }
                        else {
                            if (x && (values[0] < minimum || minimum === 0)) {
                                minimum = values[0];
                            }
                            else if (!x && (values[1] < minimum || minimum === 0)) {
                                minimum = values[1];
                            }
                        }
                    }
                }
            }
        }
        return minimum;
    };
    AdvancedLineChart.prototype.getMaximum = function (x) {
        var maximum = 0;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var data = this.series[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var values = data[j];
                        if (x && typeof values[0].getMonth === "function") {
                            //X value is a date.
                            var ms = values[0].getTime();
                            if (maximum < ms) {
                                maximum = ms;
                            }
                        }
                        else {
                            if (x && values[0] > maximum) {
                                maximum = values[0];
                            }
                            else if (!x && values[1] > maximum) {
                                maximum = values[1];
                            }
                        }
                    }
                }
            }
        }
        return maximum;
    };
    AdvancedLineChart.prototype.isDateTime = function () {
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var data = this.series[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var values = data[j];
                        if (typeof values[0].getMonth === "function") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
            }
        }
    };
    AdvancedLineChart.prototype.getDateTimeData = function (minimum, maximum) {
        var start = new Date(minimum);
        var end = new Date(maximum);
        var diff = maximum - minimum;
        var years = Math.ceil(diff / 31556952000);
        var months = Math.ceil(diff / 2629746000);
        var increment = 0;
        var total = 0;
        if (years > 4) {
            var len = years.toString().length - 1;
            var pow = Math.pow(10, len);
            increment = 12 * pow;
            total = Math.ceil(years / pow);
            start = new Date(Math.floor(start.getFullYear() / pow) * pow, 0);
        }
        else if (months > 24) {
            increment = 6; // 6 Months
            total = Math.ceil(months / 6);
            start = new Date(start.getFullYear(), start.getMonth());
        }
        else if (months > 12) {
            increment = 3; // 3 Months
            total = Math.ceil(months / 3);
            start = new Date(start.getFullYear(), start.getMonth());
        }
        else if (months > 1) {
            increment = 1; // 1 Month
            total = months;
            start = new Date(start.getFullYear(), start.getMonth());
        }
        var segments = [];
        segments.push(new DateTimeAxisSegment(this.getMonth(start.getMonth()) + " " + start.getFullYear(), start.getTime()));
        var xlabels = [];
        xlabels.push(this.getMonth(start.getMonth()) + " " + start.getFullYear());
        for (var i = 0; i < total; i++) {
            start.setMonth(start.getMonth() + increment);
            segments.push(new DateTimeAxisSegment(this.getMonth(start.getMonth()) + " " + start.getFullYear(), start.getTime()));
            xlabels.push(this.getMonth(start.getMonth()) + " " + start.getFullYear());
        }
        return { segments: segments, xlabels: xlabels };
    };
    AdvancedLineChart.prototype.getMonth = function (month) {
        switch (month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
        }
    };
    return AdvancedLineChart;
}(AxisChart));
/// <reference path="AxisChart.ts" />
var BarChart = /** @class */ (function (_super) {
    __extends(BarChart, _super);
    /**
    * Constructs a new BarChart.
    * @param container The container to append the chart.
    */
    function BarChart(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0;
        _this.minimum = 0;
        _this.maximum = 0;
        _this.title = "Bar Chart";
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    /**
     * Updates the BarChart adding the new or updated data.
     */
    BarChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var minimum = (this.minimum === 0) ? this.series.reduce(Maths.reduce_lowest) : this.minimum;
        if (minimum > 0) {
            minimum = 0;
        }
        var maximum = (this.maximum === 0) ? this.series.reduce(Maths.reduce_largest) : this.maximum;
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        // Automatically set the step if it is zero.
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width;
        // Set the font.
        this.context.font = this.renderOptions.font;
        // Recalculate the text width and text size for the labels not the title.
        var textWidth = this.context.measureText(this.keys.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        if (this.renderOptions.xAngled) {
            bottom += Math.round(textWidth);
        }
        else {
            bottom += Math.round(textHeight);
        }
        if (this.xlabel) {
            bottom += textHeight + 10;
        }
        if (this.ylabel) {
            left += textHeight + 10;
        }
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        textWidth = this.context.measureText(this.keys[this.keys.length - 1]).width;
        right += textWidth / 4;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        this.drawYAxis(x1, y1, x2, y2, ylabels, textHeight);
        var xincrement = this.drawXAxis(x1, y1, x2, y2, this.keys, textHeight, this.renderOptions.barSpacing, true);
        BarWidth = xincrement;
        this.drawBars(x1, y1, y2, xincrement, step, minimum, maximum, textHeight);
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2, textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1, y2, textHeight);
        }
        // Draw the title.
        if (this.title) {
            // Text height was changed to measure labels.    
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };

    BarChart.prototype.drawBars = function (x1, y1, y2, xincrement, step, minimum, maximum, textHeight) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        // Where is zero?
        var zero = y2 - yincrement * Math.abs(minimum);
        var spacing = this.renderOptions.barSpacing;

        XPoint = x1 + xincrement * Number("0");
        YPoint = zero;
        Height = yincrement;
        // Draw the bars
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var value = this.series[i];
                if (value <= maximum && value >= minimum) {
                    if (this.keys.length > Number(i)) {
                        this.context.fillStyle = this.renderOptions.colors[Number(i)];
                        if (value < 0) {
                            this.context.fillRect(x1 + xincrement * Number(i), zero, xincrement - spacing, yincrement * Math.abs(value));
                        }
                        else {
                            this.context.fillRect(x1 + xincrement * Number(i), zero - yincrement * value, xincrement - spacing, yincrement * value);
                        }
                        if (this.renderOptions.showValues) {
                            var text = (this.renderOptions.showAsPercentage) ? (100 / maximum * value).toFixed(this.renderOptions.decimals) + "%" : value;
                            var textWidth = this.context.measureText(text.toString()).width;
                            this.context.fillStyle = "white";
                            this.context.textAlign = "center";
                            // Draw the value / percentage.
                            if (textWidth <= xincrement && textHeight <= Math.abs(yincrement * value)) {
                                var x = x1 + xincrement * Number(i) + xincrement / 2 - spacing / 2;
                                // Minus 2 from y for better alignment.
                                var y = zero - yincrement / 2 * value + textHeight / 2 - 2;
                                this.context.fillText(text.toString(), x, y);
                            }
                        }
                    }
                }
            }
        }
    };
    return BarChart;
}(AxisChart));
/// <reference path="AxisChart.ts" />
var BarChart3D = /** @class */ (function (_super) {
    __extends(BarChart3D, _super);
    /**
    * Constructs a new BarChart.
    * @param container The container to append the chart.
    */
    function BarChart3D(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0;
        _this.title = "Bar Chart 3D";
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    /**
     * Updates the BarChart adding the new or updated data.
     */
    BarChart3D.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var minimum = this.series.reduce(Maths.reduce_lowest);
        if (minimum > 0) {
            minimum = 0;
        }
        var maximum = this.series.reduce(Maths.reduce_largest);
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        // Automatically set the step if it is zero.
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width;
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Set the font.
        this.context.font = this.renderOptions.font;
        // Recalculate the text width and text size for the labels not the title.
        var textWidth = this.context.measureText(this.keys.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        if (this.renderOptions.xAngled) {
            bottom += Math.round(textWidth);
        }
        else {
            bottom += Math.round(textHeight);
        }
        if (this.xlabel) {
            bottom += textHeight + 10;
        }
        if (this.ylabel) {
            left += textHeight + 10;
        }
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        textWidth = this.context.measureText(this.keys[this.keys.length - 1]).width;
        //right += textWidth / 2;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        var perc = 0.2;
        // draw the 3d axis lines.
        this.context.beginPath();
        this.context.moveTo(x1 + (x2 - x1) * perc, y2 * (1 - perc));
        this.context.lineTo(x1 + (x2 - x1) * perc, y1 - y1 * perc);
        this.context.lineTo(x1, y1 + (y2 - y1) * perc);
        this.context.moveTo(x2 * (1 - perc), y2);
        this.context.lineTo(x2 - x1 * perc, y2 * (1 - perc));
        this.context.lineTo(x1 + (x2 - x1) * perc, y2 * (1 - perc));
        this.context.lineTo(x1, y2);
        this.context.strokeStyle = "lightgray";
        this.context.stroke();
        // Draw the axis
        this.context.beginPath();
        this.drawYAxis(x1, y1 + (y2 - y1) * perc, x2, y2, ylabels, textHeight, false);
        var xincrement = this.drawXAxis(x1, y1, x2 * (1 - perc), y2, this.keys, textHeight, this.renderOptions.barSpacing, false);
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2 * (1 - perc), textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1 + (y2 - y1) * perc, y2, textHeight);
        }
        if (this.renderOptions.gridY) {
            this.drawYGrid(x1, y1, x2, y2, step, minimum, maximum, perc);
        }
        // Draw negative bars first.
        this.drawBars(x1, y1, x2, y2, xincrement, step, minimum, maximum, textHeight, perc, true);
        this.drawSeperator(x1, y1, x2, y2, step, minimum, maximum, perc);
        this.drawBars(x1, y1, x2, y2, xincrement, step, minimum, maximum, textHeight, perc, false);
        // Draw the title.
        if (this.title) {
            // Text height was changed to measure labels.            
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };
    /**
    * Draws the X axis.
    * @param x1 Start X of the axis.
    * @param y1 Start Y of the axis.
    * @param x2 End X of the axis.
    * @param y2 End Y of the axis.
    * @param xlabels An array of labels for the X axis.
    * @param textHeight The height of text.
    * @param spacing The spacing between the bars (for bar charts). Use zero otherwise.
    */
    BarChart3D.prototype.drawXAxis = function (x1, y1, x2, y2, xlabels, textHeight, spacing, grid) {
        if (grid === void 0) { grid = true; }
        this.context.strokeStyle = this.renderOptions.axisColor;
        this.context.moveTo(x1, y2);
        this.context.lineTo(x2, y2);
        this.context.stroke();
        var increment = (x2 - x1) / xlabels.length;
        var textWidth = this.context.measureText(xlabels.reduce(Extension.longestLength)).width;
        // Draw the labels.
        for (var i in xlabels) {
            if (xlabels.hasOwnProperty(i)) {
                var x = Math.round((x1 + increment * Number(i) + increment / 2 - spacing / 2));
                this.context.strokeStyle = this.renderOptions.axisColor;
                // Positions are rounded to the nearest whole number to disable antialiasing. 
                // translate(0.5, 0.5) is also part of the fix.
                // Center the text and markers + increment / 2.
                this.context.beginPath();
                this.context.moveTo(x, y2);
                this.context.lineTo(x, y2 + this.renderOptions.markerLength);
                this.context.stroke();
                // Draws the X axis grid.
                if (grid && this.renderOptions.gridX && Number(i) > 0) {
                    this.context.strokeStyle = this.renderOptions.gridColor;
                    this.context.beginPath();
                    this.context.moveTo(x, y1);
                    this.context.lineTo(x, y2);
                    this.context.stroke();
                }
                var y = y2 + textHeight + this.renderOptions.markerLength;
                if (this.renderOptions.xAngled) {
                    x += textHeight / 2;
                    this.context.save();
                    this.context.translate(x, y);
                    this.context.rotate(Maths.radians(300));
                    this.context.textAlign = "right";
                    this.context.fillText(xlabels[i], 0, 0);
                    this.context.restore();
                }
                else {
                    this.context.textAlign = "center";
                    this.context.fillText(xlabels[i], x, y);
                }
            }
        }
        return increment;
    };
    BarChart3D.prototype.drawYGrid = function (x1, y1, x2, y2, step, minimum, maximum, perc) {
        var yincrement = ((y2 - y1) * (1 - perc)) / (maximum - minimum);
        var xval = Number(1) + 1;
        var ystart = 0;
        var yend = 10;
        for (var i = minimum; i <= maximum; i += step) {
            var xI = x1;
            var yI = Math.round(y2 - yincrement * (Math.abs(minimum) + i));
            var xII = Math.round(x1 + (x2 - x1) * perc);
            var yII = Math.round(y2 * (1 - perc) - yincrement * (Math.abs(minimum) + i));
            var xIII = Math.round(x2 - x1 * perc);
            var yIII = yII;
            this.context.beginPath();
            this.context.strokeStyle = this.renderOptions.gridColor;
            this.context.moveTo(xI, yI);
            this.context.lineTo(xII, yII);
            this.context.lineTo(xIII, yIII);
            this.context.stroke();
        }
        this.context.strokeStyle = this.renderOptions.axisColor;
    };
    BarChart3D.prototype.drawSeperator = function (x1, y1, x2, y2, step, minimum, maximum, perc) {
        var yincrement = ((y2 - y1) * (1 - perc)) / (maximum - minimum);
        var zero = Math.round(y2 - yincrement * Math.abs(minimum));
        var y3 = Math.round(y2 * (1 - perc) - yincrement * Math.abs(minimum));
        var x3 = Math.round(x1 + (x2 - x1) * perc);
        var x4 = Math.round(x2 - x1 * perc);
        var y4 = Math.round(y3);
        this.context.beginPath();
        this.context.fillStyle = "rgba(255, 255, 255, 0.5)";
        this.context.moveTo(x3, y3);
        this.context.lineTo(x1, zero);
        this.context.lineTo(Math.round(x2 * (1 - perc)), zero);
        this.context.lineTo(x4, y4);
        this.context.lineTo(x3, y3);
        this.context.stroke();
        this.context.fill();
    };
    /* tslint:disable:variable-name */
    BarChart3D.prototype.drawBars = function (x1, y1, x2, y2, xincrement, step, minimum, maximum, textHeight, perc, lower) {
        var spacing = this.renderOptions.barSpacing;
        var yincrement = ((y2 - y1) * (1 - perc)) / (maximum - minimum);
        var zero = y2 - yincrement * Math.abs(minimum);
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var value = this.series[i];
                if (this.keys.length > Number(i) && ((value < 0 && lower) || (!lower && value > 0))) {
                    var xval = Number(i) + 1;
                    var ystart = (value > 0) ? value : 0;
                    var yend = (value > 0) ? 0 : value;
                    var b_x1 = x1 + (x2 - x1) * perc;
                    var b_y1 = y2 * (1 - perc);
                    var b_x2 = x1;
                    var b_y2 = y2;
                    var b_x3 = x1 + (x2 - x1) * perc;
                    var b_y3 = y2 * (1 - perc);
                    var b_x4 = x1;
                    var b_y4 = y2;
                    b_x1 += (xval - 1) * xincrement + spacing;
                    b_x2 += (xval - 1) * xincrement + spacing;
                    b_x3 += xval * xincrement - spacing;
                    b_x4 += xval * xincrement - spacing;
                    var angle = Math.atan2(b_y4 - b_y3, b_x4 - b_x3);
                    var x3 = x1 + (x2 - x1) * perc;
                    var y3 = y2 * (1 - perc);
                    var xlen = (x1 - x3);
                    var ylen = (y2 - y3);
                    var len = Math.sqrt(xlen * xlen + ylen * ylen);
                    xlen = b_x4 - b_x2;
                    ylen = b_y4 - b_y2;
                    var barWidth = Math.sqrt(xlen * xlen + ylen * ylen);
                    //Make the length same as the width.
                    b_x1 += Math.cos(angle) * (len - barWidth) / 2;
                    b_y1 += Math.sin(angle) * (len - barWidth) / 2;
                    b_x2 -= Math.cos(angle) * (len - barWidth) / 2;
                    b_y2 -= Math.sin(angle) * (len - barWidth) / 2;
                    b_x3 += Math.cos(angle) * (len - barWidth) / 2;
                    b_y3 += Math.sin(angle) * (len - barWidth) / 2;
                    b_x4 -= Math.cos(angle) * (len - barWidth) / 2;
                    b_y4 -= Math.sin(angle) * (len - barWidth) / 2;
                    var t_x1 = b_x1;
                    var t_y1 = b_y1;
                    var t_x2 = b_x2;
                    var t_y2 = b_y2;
                    var t_x3 = b_x3;
                    var t_y3 = b_y3;
                    var t_x4 = b_x4;
                    var t_y4 = b_y4;
                    // Value.
                    // Minimum is zero position.
                    b_y1 -= yincrement * (Math.abs(minimum) + ystart);
                    b_y2 -= yincrement * (Math.abs(minimum) + ystart);
                    b_y3 -= yincrement * (Math.abs(minimum) + ystart);
                    b_y4 -= yincrement * (Math.abs(minimum) + ystart);
                    t_y1 -= yincrement * (Math.abs(minimum) + yend);
                    t_y2 -= yincrement * (Math.abs(minimum) + yend);
                    t_y3 -= yincrement * (Math.abs(minimum) + yend);
                    t_y4 -= yincrement * (Math.abs(minimum) + yend);
                    //this.context.fillRect(b_x1 - 1, b_y1 - 1, 2, 2);
                    // this.context.fillRect(b_x2 - 1, b_y2 - 1, 2, 2);
                    //this.context.fillRect(b_x3 - 1, b_y3 - 1, 2, 2);
                    // this.context.fillRect(b_x4 - 1, b_y4 - 1, 2, 2);
                    //this.context.fillRect(t_x1 - 1, t_y1 - 1, 2, 2);
                    //this.context.fillRect(t_x2 - 1, t_y2 - 1, 2, 2);
                    //this.context.fillRect(t_x3 - 1, t_y3 - 1, 2, 2);
                    //this.context.fillRect(t_x4 - 1, t_y4 - 1, 2, 2);
                    // front
                    this.context.beginPath();
                    this.context.fillStyle = this.renderOptions.colors[i];
                    this.context.moveTo(b_x2, b_y2);
                    this.context.lineTo(b_x4, b_y4);
                    this.context.lineTo(t_x4, t_y4);
                    this.context.lineTo(t_x2, t_y2);
                    this.context.lineTo(b_x2, b_y2);
                    this.context.fill();
                    if (this.renderOptions.showValues) {
                        var text = (this.renderOptions.showAsPercentage) ? (100 / maximum * value).toFixed(this.renderOptions.decimals) + "%" : value;
                        var textWidth = this.context.measureText(text.toString()).width;
                        this.context.fillStyle = "white";
                        this.context.textAlign = "center";
                        //const x =
                        var tx1 = b_x2;
                        var ty1 = b_y2;
                        var tx2 = t_x4;
                        var ty2 = t_y4;
                        var width = tx2 - tx1;
                        var height = ty2 - ty1;
                        var tx = tx1 + (tx2 - tx1) / 2;
                        // Minus 2 from y for better alignment.
                        var ty = ty1 + (ty2 - ty1) / 2 + textHeight / 2 - 2;
                        if (textWidth <= width && textHeight <= Math.abs(height)) {
                            this.context.fillText(text.toString(), tx, ty);
                        }
                    }
                    // top
                    this.context.beginPath();
                    this.context.fillStyle = Extension.shadeBlendConvert(-0.1, this.renderOptions.colors[i]);
                    this.context.moveTo(b_x2, b_y2);
                    this.context.lineTo(b_x1, b_y1);
                    this.context.lineTo(b_x3, b_y3);
                    this.context.lineTo(b_x4, b_y4);
                    this.context.fill();
                    // right
                    this.context.beginPath();
                    this.context.fillStyle = Extension.shadeBlendConvert(-0.2, this.renderOptions.colors[i]);
                    this.context.moveTo(b_x4, b_y4);
                    this.context.lineTo(b_x3, b_y3);
                    this.context.lineTo(t_x3, t_y3);
                    this.context.lineTo(t_x4, t_y4);
                    this.context.fill();
                    this.context.fillStyle = "black";
                }
            }
        }
    };
    return BarChart3D;
}(AxisChart));
/// <reference path="../extensions/Extension.ts"/>
/// <reference path="../extensions/Maths.ts"/>
/// <reference path="../extensions/Styles.ts"/>
var DoughnutChart = /** @class */ (function () {
    /**
     * Constructs a new PieChart.
     * @param container The container to append the chart.
     */
    function DoughnutChart(container) {
        /**
         * A set of option determining how the chart contents will be rendered.
         */
        this.renderOptions = new RenderOptions();
        this.title = "Doughnut Chart";
        this.width = 400;
        this.height = 400;
        this.thickness = 50;
        this.adjustHeight = false;
        this.canvas = document.createElement("canvas");
        this._context = this.canvas.getContext("2d");
        document.getElementById(container.replace("#", "")).appendChild(this.canvas);
        var self = this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
    }
    DoughnutChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.size = (this.width > this.height) ? this.height : this.width;
        this.canvas.width = this.size;
        this.canvas.height = this.size;
        // Clear the area.
        this._context.clearRect(0, 0, this.size, this.size);
        // Boundary adjustments.
        var right = 0;
        var top = 0;
        // Find the title text height and adjust the top.
        var textHeight = 0;
        if (this.title) {
            textHeight = Number(this.renderOptions.titleFont.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Draw the key definitions.
        this._context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Draw the title.
        if (this.title) {
            this._context.font = this.renderOptions.titleFont;
            this._context.textAlign = "center";
            this._context.fillText(this.title, this.size / 2 - right / 2, textHeight);
        }
        // Set the text font for other text.
        this._context.textAlign = "left";
        this._context.font = this.renderOptions.font;
        this._context.lineWidth = this.thickness;
        var circumference = Math.PI * 2 * (this.size / 2 - right / 2);
        var circIncrement = circumference / 360;
        var sum = this.series.reduce(Maths.abs_sum);
        var increment = 360 / sum;
        if (this.series) {
            var total = 0;
            for (var i in this.series) {
                if (this.series.hasOwnProperty(i)) {
                    var value = Math.abs(this.series[i]);
                    this._context.strokeStyle = this.renderOptions.colors[Number(i)];
                    // Draw the value.
                    this.drawDoughnutSegment(top, right, increment, circIncrement, total, value, sum);
                    total += value;
                }
            }
        }
    };
    DoughnutChart.prototype.drawDoughnutSegment = function (top, right, increment, circIncrement, total, value, sum) {
        // Draw the value pie.
        this._context.beginPath();
        var x = this.size / 2 - right / 2;
        var y = this.size / 2 - right / 2 + top;
        var radius = this.size / 2 - right / 2 - this.thickness / 2;
        var startAngle = Maths.radians(increment * total - 90);
        var endAngle = Maths.radians(increment * (total + value) - 90);
        if (right <= 0) {
            // Due to the right been zero the size now exceeds the height, fix it by removing the top.
            x = this.size / 2;
            y = this.size / 2 + top / 2;
            radius = this.size / 2 - top / 2 - this.thickness / 2;
        }
        if (radius > -1) {
            this._context.arc(x, y, radius, startAngle, endAngle);
            this._context.stroke();
        }
        // Add the value or percentage to the pie chart.
        if (this.renderOptions.showValues) {
            // Find the center of the value segment.
            var angle = increment * (total + value / 2);
            // Position 70% between the center and the end.
            var position = 0.7;
            var text = (this.renderOptions.showAsPercentage) ? (100 / sum * value).toFixed(this.renderOptions.decimals) + "%" : value.toString();
            var textHeight = Number(this._context.font.replace(/[^0-9.]/g, ""));
            var textWidth = this._context.measureText(text).width;
            // Get the text position point.
            var textPoint = Maths.getArcPoint(x, y + textHeight / 2, radius, endAngle - Maths.radians(value / 2 * increment));
            // Will the text fit
            if (textWidth < circIncrement * value) {
                this._context.fillStyle = "white";
                this._context.textAlign = "center";
                this._context.fillText(text, textPoint[0], textPoint[1]);
                this._context.fillStyle = "black";
            }
        }
    };
    /**
     * Draws the key definitions to the right of the pie chart.
     * Returns: The amount of space used to the right.
     */
    DoughnutChart.prototype.drawKeyDefinitions = function (top) {
        if (this.keys && this.keys.length > 0 && this.renderOptions.showKeys) {
            var keyWidth = this._context.measureText(this.keys.reduce(Extension.longestLength)).width;
            for (var i in this.keys) {
                if (this.keys.hasOwnProperty(i)) {
                    // Draw the key color.
                    this._context.fillStyle = this.renderOptions.colors[i];
                    this._context.fillRect(this.size - keyWidth - 20, Number(i) * 20 + top, 18, 10);
                    // Draw the key value.
                    this._context.fillStyle = this.renderOptions.keyColor;
                    this._context.fillText(this.keys[i], this.size - keyWidth, Number(i) * 20 + 10 + top);
                }
            }
            return keyWidth + 20 + 5; // + 5 for padding between the chart and the keys.
        }
        return 0;
    };
    return DoughnutChart;
}());
/// <reference path="AxisChart.ts"/>
var GroupedBarChart = /** @class */ (function (_super) {
    __extends(GroupedBarChart, _super);
    function GroupedBarChart(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0;
        _this.title = "Grouped Bar Chart";
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    GroupedBarChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var numbers = [].concat.apply([], this.series);
        var minimum = numbers.reduce(Maths.reduce_lowest);
        if (minimum > 0) {
            minimum = 0;
        }
        var maximum = numbers.reduce(Maths.reduce_largest);
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        // Automatically generate the step if it is zero.
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        if (ylabels.indexOf("0") === -1) {
            var lowestWhole = ylabels.reduce(function (a, b) { return (Number(a) < Number(b) && Number(a) > 0) ? a : b; });
            ylabels.splice(ylabels.indexOf(lowestWhole), 0, "0");
        }
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Set the font.
        this.context.font = this.renderOptions.font;
        var textWidth = this.context.measureText(this.keys.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        if (this.renderOptions.xAngled) {
            bottom += Math.round(textWidth);
        }
        else {
            bottom += Math.round(textHeight);
        }
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width + this.renderOptions.markerLength;
        if (this.xlabel) {
            bottom += textHeight + 15;
        }
        if (this.ylabel) {
            left += textHeight + 15;
        }
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        textWidth = this.context.measureText(this.keys[this.keys.length - 1]).width;
        right += textWidth / 4;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        this.drawYAxis(x1, y1, x2, y2, ylabels, textHeight);
        var xincrement = this.drawXAxis(x1, y1, x2, y2, this.keys, textHeight, this.renderOptions.barSpacing, true);
        this.drawBars(x1, y1, y2, xincrement, step, minimum, maximum, textHeight);
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2, textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1, y2, textHeight);
        }
        // Draw the title.
        if (this.title) {
            // Text height was changed to measure labels.            
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };
    GroupedBarChart.prototype.drawBars = function (x1, y1, y2, xincrement, step, minimum, maximum, textHeight) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        // Where is zero?
        var zero = y2 - yincrement * Math.abs(minimum);
        var spacing = this.renderOptions.barSpacing;
        var barIncrement = (xincrement - spacing) / this.series.reduce(function (a, b) { return (a.length > b.length) ? a : b; }).length;
        // Itterate through each set of bars.
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var values = this.series[i];
                // Only draw if there is a key for this bar.
                if (this.keys.length > Number(i)) {
                    var x = x1 + xincrement * Number(i);
                    // Go through each bar.
                    for (var j in values) {
                        if (values.hasOwnProperty(j)) {
                            this.context.fillStyle = this.renderOptions.colors[j];
                            if (values[j] < 0) {
                                this.context.fillRect(x + barIncrement * Number(j), zero, barIncrement, yincrement * Math.abs(values[j]));
                            }
                            else {
                                this.context.fillRect(x + barIncrement * Number(j), zero - yincrement * values[j], barIncrement, yincrement * values[j]);
                            }
                            // Draw the values or percentages
                            if (this.renderOptions.showValues) {
                                var text = (this.renderOptions.showAsPercentage) ? (100 / maximum * values[j]).toFixed(this.renderOptions.decimals) + "%" : values[j];
                                var textWidth = this.context.measureText(text.toString()).width;
                                this.context.fillStyle = "white";
                                this.context.textAlign = "center";
                                // Draw the value / percentage.
                                if (textWidth <= barIncrement) {
                                    var y = zero - yincrement / 2 * values[j] + textHeight / 2;
                                    this.context.fillText(text.toString(), x + barIncrement * Number(j) + barIncrement / 2, y);
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    return GroupedBarChart;
}(AxisChart));
/// <reference path="../extensions/Extension.ts"/>
/// <reference path="../extensions/Maths.ts"/>
/// <reference path="../extensions/Styles.ts"/>
var PieChart = /** @class */ (function () {
    /**
     * Constructs a new PieChart.
     * @param container The container to append the chart.
     */
    function PieChart(container) {
        /**
         * A set of option determining how the chart contents will be rendered.
         */
        this.renderOptions = new RenderOptions();
        this.title = "Pie Chart";
        this.width = 400;
        this.height = 400;
        this.adjustHeight = false;
        this.canvas = document.createElement("canvas");
        this._context = this.canvas.getContext("2d");
        document.getElementById(container.replace("#", "")).appendChild(this.canvas);
        var self = this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
    }
    /**
     * Updates the PieChart adding the new or updated data.
     */
    PieChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.size = (this.width > this.height) ? this.height : this.width;
        this.canvas.width = this.size;
        this.canvas.height = this.size;
        // Clear the area.
        this._context.clearRect(0, 0, this.size, this.size);
        // Boundary adjustments.
        var right = 0;
        var top = 0;
        // Find the title text height and adjust the top.
        var textHeight = 0;
        if (this.title) {
            textHeight = Number(this.renderOptions.titleFont.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Draw the key definitions.
        this._context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Draw the title.
        if (this.title) {
            this._context.font = this.renderOptions.titleFont;
            this._context.textAlign = "center";
            this._context.fillText(this.title, this.size / 2 - right / 2, textHeight);
        }
        // Set the text font for other text.
        this._context.textAlign = "left";
        this._context.font = this.renderOptions.font;
        this._context.lineWidth = this.size / 2 - right / 2;
        if (right <= 0) {
            // Due to right been zero the chart now exceeds the height fix it by replacing right with top.
            this._context.lineWidth = this.size / 2 - top / 2;
        }
        var circumference = Math.PI * 2 * (this.size / 2 - (right / 2));
        var circIncrement = circumference / 360;
        var sum = this.series.reduce(Maths.abs_sum);
        var increment = 360 / sum;
        if (this.series) {
            var total = 0;
            for (var i in this.series) {
                if (this.series.hasOwnProperty(i)) {
                    var value = Math.abs(this.series[i]);
                    this._context.strokeStyle = this.renderOptions.colors[i];
                    // Draw the value.
                    this.drawPieSegment(top, right, increment, circIncrement, total, value, sum);
                    total += value;
                }
            }
        }
    };
    PieChart.prototype.drawPieSegment = function (top, right, increment, circIncrement, total, value, sum) {
        // Draw the value pie.
        this._context.beginPath();
        var x = this.size / 2 - right / 2;
        var y = this.size / 2 - right / 2 + top;
        var radius = this.size / 4 - right / 4; //right / 2
        var startAngle = Maths.radians(increment * total - 90);
        var endAngle = Maths.radians(increment * (total + value) - 90);
        if (right <= 0) {
            // Due to the right been zero the size now exceeds the height, fix it by removing the top.
            x = this.size / 2;
            y = this.size / 2 + top / 2;
            radius = this.size / 4 - top / 4; //right / 2
        }
        this._context.arc(x, y, radius, startAngle, endAngle);
        this._context.stroke();
        // Add the value or percentage to the pie chart.
        if (this.renderOptions.showValues) {
            // Find the center of the value segment.
            var angle = increment * (total + value / 2);
            // Position 70% between the center and the end.
            var position = 0.7;
            var text = (this.renderOptions.showAsPercentage) ? (100 / sum * value).toFixed(this.renderOptions.decimals) + "%" : value.toString();
            // Get the text position point.
            var textPoint = Maths.getArcPoint(x, y - top / 2, (this.size / 2 - right / 2) * position, endAngle - Maths.radians(value / 2 * increment));
            var textWidth = this._context.measureText(text).width;
            // Will the text fit
            if (textWidth < circIncrement * value) {
                this._context.fillStyle = "white";
                this._context.textAlign = "center";
                this._context.fillText(text, textPoint[0], textPoint[1] + textWidth / 2);
                this._context.fillStyle = "black";
            }
        }
    };
    /**
     * Draws the key definitions to the right of the pie chart.
     * Returns: The amount of space used to the right.
     */
    PieChart.prototype.drawKeyDefinitions = function (top) {
        if (this.keys && this.keys.length > 0 && this.renderOptions.showKeys) {
            var keyWidth = this._context.measureText(this.keys.reduce(Extension.longestLength)).width;
            for (var i in this.keys) {
                if (this.keys.hasOwnProperty(i)) {
                    // Draw the key color.
                    this._context.fillStyle = this.renderOptions.colors[i];
                    this._context.fillRect(this.size - keyWidth - 20, Number(i) * 20 + top, 18, 10);
                    // Draw the key value.
                    this._context.fillStyle = this.renderOptions.keyColor;
                    this._context.fillText(this.keys[i], this.size - keyWidth, Number(i) * 20 + 10 + top);
                }
            }
            return keyWidth + 20 + 5; // + 5 for padding between the chart and the keys.
        }
        return 0;
    };
    return PieChart;
}());
/// <reference path="../extensions/Extension.ts"/>
/// <reference path="../extensions/Maths.ts"/>
/// <reference path="../extensions/Styles.ts"/>
var PieChart3D = /** @class */ (function () {
    /**
     * Constructs a new PieChart.
     * @param container The container to append the chart.
     */
    function PieChart3D(container) {
        /**
         * A set of option determining how the chart contents will be rendered.
         */
        this.renderOptions = new RenderOptions();
        this.title = "Pie Chart 3D";
        this.width = 400;
        this.height = 400;
        this.adjustHeight = false;
        this.canvas = document.createElement("canvas");
        this._context = this.canvas.getContext("2d");
        document.getElementById(container.replace("#", "")).appendChild(this.canvas);
        var self = this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
    }
    /**
     * Updates the PieChart adding the new or updated data.
     */
    PieChart3D.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.size = (this.width > this.height) ? this.height : this.width;
        this.canvas.width = this.size;
        this.canvas.height = this.size;
        // Clear the area.
        this._context.clearRect(0, 0, this.size, this.size);
        // Boundary adjustments.
        var right = 0;
        var top = 0;
        // Find the title text height and adjust the top.
        var textHeight = 0;
        if (this.title) {
            textHeight = Number(this.renderOptions.titleFont.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Draw the key definitions.
        this._context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Draw the title.
        if (this.title) {
            this._context.font = this.renderOptions.titleFont;
            this._context.textAlign = "center";
            this._context.fillText(this.title, this.size / 2 - right / 2, textHeight);
        }
        // Set the text font for other text.
        this._context.textAlign = "left";
        this._context.font = this.renderOptions.font;
        this._context.lineWidth = this.size / 2 - right / 2;
        var circumference = Math.PI * 2 * (this.size / 2 - right / 2);
        var circIncrement = circumference / 360;
        var sum = this.series.reduce(Maths.abs_sum);
        var increment = 360 / sum;
        if (this.series) {
            var total = 0;
            for (var i in this.series) {
                if (this.series.hasOwnProperty(i)) {
                    var value = Math.abs(this.series[i]);
                    var angle = increment * value;
                    this._context.fillStyle = this.renderOptions.colors[i];
                    // Draw the value.
                    this.drawPieSegment(top, right, increment, circIncrement, total, angle, sum, value, this.renderOptions.colors[i]);
                    total += angle;
                }
            }
        }
    };
    PieChart3D.prototype.drawPieSegment = function (top, right, increment, circIncrement, total, angle, sum, value, color) {
        // Draw the value pie.
        this._context.beginPath();
        var x = this.size / 2 - right / 2;
        var radiusX = this.size / 2 - right / 2;
        var y = this.size / 2 - right / 2 + top - (this.size / 2 - right / 2) * 0.3;
        var radiusY = (this.size / 2 - right / 2) * 0.7;
        var startAngle = Maths.radians(total - 90);
        var endAngle = Maths.radians(total + angle - 90);
        this._context.moveTo(x, y);
        this._context.ellipse(x, y, radiusX, radiusY, 0, startAngle, endAngle);
        //this._context.arc(x, y, radius, startAngle, endAngle);
        this._context.fill();
        // Add the value or percentage to the pie chart.
        if (this.renderOptions.showValues) {
            // Position 70% between the center and the end.
            var position = 0.7;
            var text = (this.renderOptions.showAsPercentage) ? (100 / sum * value).toFixed(this.renderOptions.decimals) + "%" : value.toString();
            // Get the text position point.
            var textPoint = Maths.getEllipsePoint(x, y - (radiusY * (1 - position)) / 2, radiusX * position, radiusY * position, endAngle - Maths.radians(value / 2 * increment));
            var textWidth = this._context.measureText(text).width;
            // Will the text fit
            if (textWidth < circIncrement * value) {
                this._context.fillStyle = "white";
                this._context.textAlign = "center";
                this._context.fillText(text, textPoint[0], textPoint[1] + textWidth / 2);
                this._context.fillStyle = "black";
            }
        }
        // Draw the front.
        if (total + angle > 90 && total < 270) {
            this._context.beginPath();
            // Don't go beyond the front.
            var start = (total < 90) ? Maths.radians(90) : Maths.radians(total);
            var end = (total + angle > 270) ? Maths.radians(270) : Maths.radians(total + angle);
            this._context.ellipse(x, y, radiusX, radiusY, 0, start - Math.PI / 2, end - Math.PI / 2);
            // Height of the front.
            y += 40;
            this._context.ellipse(x, y, radiusX, radiusY, 0, end - Math.PI / 2, start - Math.PI / 2, true);
            this._context.fillStyle = Extension.shadeBlendConvert(-0.3, color);
            this._context.fill();
        }
    };
    /**
      * Draws the key definitions to the right of the pie chart.
      * Returns: The amount of space used to the right.
      */
    PieChart3D.prototype.drawKeyDefinitions = function (top) {
        if (this.keys && this.keys.length > 0 && this.renderOptions.showKeys) {
            var keyWidth = this._context.measureText(this.keys.reduce(Extension.longestLength)).width;
            for (var i in this.keys) {
                if (this.keys.hasOwnProperty(i)) {
                    // Draw the key color.
                    this._context.fillStyle = this.renderOptions.colors[i];
                    this._context.fillRect(this.size - keyWidth - 20, Number(i) * 20 + top, 18, 10);
                    // Draw the key value.
                    this._context.fillStyle = this.renderOptions.keyColor;
                    this._context.fillText(this.keys[i], this.size - keyWidth, Number(i) * 20 + 10 + top);
                }
            }
            return keyWidth + 20 + 5; // + 5 for padding between the chart and the keys.
        }
        return 0;
    };
    return PieChart3D;
}());
var Responsive = /** @class */ (function () {
    function Responsive() {
    }
    Responsive.HandleResize = function (element) {
        var parent = element.canvas.parentElement;
        element.width = parent.offsetWidth;
        if (element.adjustHeight) {
            element.height = element.width * 0.75;
        }
        element.update();
        // alert(parent.offsetWidth + "," + parent.offsetHeight);
        //alert("Test");
    };
    return Responsive;
}());
/// <reference path="AxisChart.ts" />
var ScatterGraph = /** @class */ (function (_super) {
    __extends(ScatterGraph, _super);
    /**
    * Constructs a new BarChart.
    * @param container The container to append the chart.
    */
    function ScatterGraph(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0; // Step is always Y by default. Although the advanced line chart may also have a step for X.
        _this.stepX = 0;
        _this.lineWidth = 1;
        _this.title = "Scatter Graph";
        _this.renderOptions.gridX = true; // Default.
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    /**
     * Updates the BarChart adding the new or updated data.
     */
    ScatterGraph.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var numbers = [].concat.apply([], this.series);
        var minimum = this.getMinimum(false);
        var maximum = this.getMaximum(false);
        var minimumX = this.getMinimum(true);
        var maximumX = this.getMaximum(true);
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        // Automatically generate a step if zero.
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        var xstep = (this.stepX === 0) ? Maths.getStep(minimumX, maximumX, divisor) : this.stepX;
        var isDateTime = this.isDateTime();
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        if (!isDateTime) {
            maximumX = Math.ceil(maximumX / xstep) * xstep;
            minimumX = Math.floor(minimumX / xstep) * xstep;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        var segments;
        var xlabels = [];
        if (isDateTime) {
            // Get the data needed for a date and time x axis.
            var data = this.getDateTimeData(minimumX, maximumX);
            segments = data.segments;
            xlabels = data.xlabels;
        }
        else {
            // Calculate the labels using the step.
            for (var i = minimumX; i <= maximumX; i += xstep) {
                xlabels.push(Maths.strip(i, xstep));
            }
        }
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width;
        // Recalculate the text width and text size for the labels not the title.
        var textWidth = this.context.measureText(xlabels.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        if (this.renderOptions.xAngled) {
            bottom += Math.round(textWidth);
        }
        else {
            bottom += Math.round(textHeight);
        }
        if (this.xlabel) {
            bottom += textHeight + 10;
        }
        if (this.ylabel) {
            left += textHeight + 10;
        }
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        textWidth = this.context.measureText(xlabels[xlabels.length - 1]).width;
        right += textWidth / 2;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        this.drawYAxis(x1, y1, x2, y2, ylabels, textHeight);
        var xincrement = this.drawXAxis(x1, y1, x2, y2, xlabels, textHeight, 0, false);
        if (isDateTime) {
            this.drawDateTimeLines(x1, y1, x2, y2, step, minimum, maximum, segments);
        }
        else {
            this.drawLines(x1, y1, x2, y2, minimumX, maximumX, xstep, step, minimum, maximum);
        }
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2, textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1, y2, textHeight);
        }
        // Draw the title.
        if (this.title) {
            // Text height was changed to measure labels.            
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };
    ScatterGraph.prototype.drawLines = function (x1, y1, x2, y2, minimumX, maximumX, xstep, step, minimum, maximum) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        var xincrement = (x2 - x1) / (maximumX - minimumX);
        var yzero = y2 - yincrement * Math.abs(minimum);
        var xzero = (minimumX < 0) ? x1 + xincrement * Math.abs(minimumX) : x1 - xincrement * minimumX;
        var pointSize = this.renderOptions.pointSize;
        this.context.lineWidth = this.lineWidth;
        // Draw the lines
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i) && this.keys.length > Number(i)) {
                var data = this.series[i];
                var points = [];
                this.context.beginPath();
                this.context.fillStyle = this.renderOptions.colors[i];
                this.context.strokeStyle = this.renderOptions.colors[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var value = data[j]; // Value is X,Y
                        var x = 0;
                        var y = 0;
                        if (value[0] < 0) {
                            x = xzero - xincrement * Math.abs(value[0]);
                        }
                        else {
                            x = xzero + xincrement * value[0];
                        }
                        if (value[1] < 0) {
                            y = yzero + yincrement * Math.abs(value[1]);
                        }
                        else {
                            y = yzero - yincrement * value[1];
                        }
                        points.push([x, y]);
                        this.context.fillRect(x - pointSize / 2, y - pointSize / 2, pointSize, pointSize);
                    }
                }
                // Draw a trendline.
                var trendLine = this.getTrendLine(points);
                for (var j in points) {
                    if (points.hasOwnProperty(j)) {
                        var x = Math.round(points[j][0]);
                        var y = Math.round(x * trendLine[0] + trendLine[1]);
                        this.context.lineTo(x, y);
                    }
                }
                this.context.stroke();
                /*if (this.cardinalSpline.visible) {
                    Maths.drawCurve(this.context, points, this.cardinalSpline.tension, this.cardinalSpline.resolution);
                } else {
                    for (const j in points) {
                        if (points.hasOwnProperty(j)) {
                            this.context.lineTo(points[j][0], points[j][1]);
                        }
                    }
                }
                
                this.context.stroke(); */
            }
        }
    };
    ScatterGraph.prototype.drawDateTimeLines = function (x1, y1, x2, y2, step, minimum, maximum, segments) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        var yzero = y2 - yincrement * Math.abs(minimum);
        var pointSize = this.renderOptions.pointSize;
        var xincrement = (x2 - x1) / (segments.length - 1);
        this.context.lineWidth = this.lineWidth;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i) && this.keys.length > Number(i)) {
                var data = this.series[i];
                var points = [];
                this.context.beginPath();
                this.context.fillStyle = this.renderOptions.colors[i];
                this.context.strokeStyle = this.renderOptions.colors[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var value = data[j]; // Value is X,Y
                        var time = value[0].getTime();
                        var x = 0;
                        var y = 0;
                        for (var k = 1; k < segments.length; k++) {
                            if (time >= segments[k - 1].time && time <= segments[k].time) {
                                var timeIncrement = segments[k].time - segments[k - 1].time;
                                var increment = xincrement / timeIncrement;
                                x = increment * (time - segments[k - 1].time) + xincrement * (k - 1) + x1;
                                if (value[1] < 0) {
                                    y = yzero + yincrement * Math.abs(value[1]);
                                }
                                else {
                                    y = yzero - yincrement * value[1];
                                }
                                points.push([x, y]);
                                this.context.fillRect(x - pointSize / 2, y - pointSize / 2, pointSize, pointSize);
                            }
                        }
                    }
                }
                /* if (this.cardinalSpline.visible) {
                     Maths.drawCurve(this.context, points, this.cardinalSpline.tension, this.cardinalSpline.resolution);
                 } else {
                     for (const j in points) {
                         if (points.hasOwnProperty(j)) {
                             this.context.lineTo(points[j][0], points[j][1]);
                         }
                     }
                 }
 
                 this.context.stroke();*/
            }
        }
    };
    ScatterGraph.prototype.getTrendLine = function (points) {
        var xy = 0;
        var sumX = 0;
        var sumY = 0;
        var sqrtX = 0;
        for (var i in points) {
            if (points.hasOwnProperty(i)) {
                var x = points[i][0];
                var y = points[i][1];
                xy += x * y;
                sumX += x;
                sumY += y;
                sqrtX += x * x;
            }
        }
        var a = points.length * xy;
        var b = sumX * sumY;
        var c = points.length * sqrtX;
        var d = sumX * sumX;
        var slope = (a - b) / (c - d);
        var e = sumY;
        var f = slope * sumX;
        var yintercept = (e - f) / points.length;
        return [slope, yintercept];
    };
    ScatterGraph.prototype.getMinimum = function (x) {
        var minimum = 0;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var data = this.series[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var values = data[j];
                        if (x && typeof values[0].getMonth === "function") {
                            // X value is a date.
                            var ms = values[0].getTime();
                            if (minimum > ms || minimum === 0) {
                                minimum = ms;
                            }
                        }
                        else {
                            if (x && (values[0] < minimum || minimum === 0)) {
                                minimum = values[0];
                            }
                            else if (!x && (values[1] < minimum || minimum === 0)) {
                                minimum = values[1];
                            }
                        }
                    }
                }
            }
        }
        return minimum;
    };
    ScatterGraph.prototype.getMaximum = function (x) {
        var maximum = 0;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var data = this.series[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var values = data[j];
                        if (x && typeof values[0].getMonth === "function") {
                            //X value is a date.
                            var ms = values[0].getTime();
                            if (maximum < ms) {
                                maximum = ms;
                            }
                        }
                        else {
                            if (x && values[0] > maximum) {
                                maximum = values[0];
                            }
                            else if (!x && values[1] > maximum) {
                                maximum = values[1];
                            }
                        }
                    }
                }
            }
        }
        return maximum;
    };
    ScatterGraph.prototype.isDateTime = function () {
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var data = this.series[i];
                for (var j in data) {
                    if (data.hasOwnProperty(j)) {
                        var values = data[j];
                        if (typeof values[0].getMonth === "function") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
            }
        }
    };
    ScatterGraph.prototype.getDateTimeData = function (minimum, maximum) {
        var start = new Date(minimum);
        var end = new Date(maximum);
        var diff = maximum - minimum;
        var years = Math.ceil(diff / 31556952000);
        var months = Math.ceil(diff / 2629746000);
        var increment = 0;
        var total = 0;
        if (years > 4) {
            var len = years.toString().length - 1;
            var pow = Math.pow(10, len);
            increment = 12 * pow;
            total = Math.ceil(years / pow);
            start = new Date(Math.floor(start.getFullYear() / pow) * pow, 0);
        }
        else if (months > 24) {
            increment = 6; // 6 Months
            total = Math.ceil(months / 6);
            start = new Date(start.getFullYear(), start.getMonth());
        }
        else if (months > 12) {
            increment = 3; // 3 Months
            total = Math.ceil(months / 3);
            start = new Date(start.getFullYear(), start.getMonth());
        }
        else if (months > 1) {
            increment = 1; // 1 Month
            total = months;
            start = new Date(start.getFullYear(), start.getMonth());
        }
        var segments = [];
        segments.push(new DateTimeAxisSegment(this.getMonth(start.getMonth()) + " " + start.getFullYear(), start.getTime()));
        var xlabels = [];
        xlabels.push(this.getMonth(start.getMonth()) + " " + start.getFullYear());
        for (var i = 0; i < total; i++) {
            start.setMonth(start.getMonth() + increment);
            segments.push(new DateTimeAxisSegment(this.getMonth(start.getMonth()) + " " + start.getFullYear(), start.getTime()));
            xlabels.push(this.getMonth(start.getMonth()) + " " + start.getFullYear());
        }
        return { segments: segments, xlabels: xlabels };
    };
    ScatterGraph.prototype.getMonth = function (month) {
        switch (month) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
        }
    };
    return ScatterGraph;
}(AxisChart));
/// <reference path="AxisChart.ts" />
var SimpleLineChart = /** @class */ (function (_super) {
    __extends(SimpleLineChart, _super);
    /**
    * Constructs a new BarChart.
    * @param container The container to append the chart.
    */
    function SimpleLineChart(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0;
        _this.lineWidth = 2;
        _this.cardinalSpline = new CardinalSplineOptions();
        _this.title = "Simple Line Chart";
        _this.renderOptions.gridX = true; // Default.
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    /**
     * Updates the BarChart adding the new or updated data.
     */
    SimpleLineChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var numbers = [].concat.apply([], this.series);
        var minimum = numbers.reduce(Maths.reduce_lowest);
        if (minimum > 0) {
            minimum = 0;
        }
        var maximum = numbers.reduce(Maths.reduce_largest);
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        // Automatically generate the step if it is zero.
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        var textWidth = this.context.measureText(this.data.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        if (this.renderOptions.xAngled) {
            bottom += Math.round(textWidth);
        }
        else {
            bottom += Math.round(textHeight);
        }
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width + this.renderOptions.markerLength;
        if (this.xlabel) {
            bottom += textHeight + 15;
        }
        if (this.ylabel) {
            left += textHeight + 15;
        }
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        textWidth = this.context.measureText(this.data[this.data.length - 1]).width;
        right += textWidth / 2;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        this.drawYAxis(x1, y1, x2, y2, ylabels, textHeight);
        var xincrement = this.drawXAxis(x1, y1, x2, y2, this.data, textHeight, 0, false);
        this.drawLines(x1, y1, y2, xincrement, step, minimum, maximum);
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2, textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1, y2, textHeight);
        }
        // Draw the title.
        if (this.title) {
            // Text height was changed to measure labels.            
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };
    SimpleLineChart.prototype.drawLines = function (x1, y1, y2, xincrement, step, minimum, maximum) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        // Where is zero?
        var zero = y2 - yincrement * Math.abs(minimum);
        var pointSize = this.renderOptions.pointSize;
        this.context.lineWidth = this.lineWidth;
        // Start by drawing the line.
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                this.context.beginPath();
                var points = [];
                var values = this.series[i];
                this.context.strokeStyle = this.renderOptions.colors[Number(i)];
                this.context.fillStyle = this.renderOptions.colors[Number(i)];
                for (var j in values) {
                    if (values.hasOwnProperty(j)) {
                        if (this.data.length > Number(j)) {
                            if (values[j] < 0) {
                                points.push([x1 + xincrement * Number(j), zero + yincrement * Math.abs(values[j])]);
                                this.context.fillRect(x1 + xincrement * Number(j) - pointSize / 2, zero + yincrement * Math.abs(values[j]) - pointSize / 2, pointSize, pointSize);
                            }
                            else {
                                points.push([x1 + xincrement * Number(j), zero - yincrement * values[j]]);
                                this.context.fillRect(x1 + xincrement * Number(j) - pointSize / 2, zero - yincrement * values[j] - pointSize / 2, pointSize, pointSize);
                            }
                        }
                    }
                }
                if (this.cardinalSpline.visible) {
                    Maths.drawCurve(this.context, points, this.cardinalSpline.tension, this.cardinalSpline.resolution);
                }
                else {
                    for (var j in points) {
                        if (points.hasOwnProperty(j)) {
                            this.context.lineTo(points[j][0], points[j][1]);
                        }
                    }
                }
                this.context.stroke();
            }
        }
    };
    return SimpleLineChart;
}(AxisChart));
/// <reference path="AxisChart.ts"/>
var StackedBarChart = /** @class */ (function (_super) {
    __extends(StackedBarChart, _super);
    function StackedBarChart(container) {
        var _this = _super.call(this, container) || this;
        _this.step = 0;
        _this.title = "Stacked Bar Chart";
        var self = _this;
        window.addEventListener("resize", function () {
            Responsive.HandleResize(self);
        }, false);
        Responsive.HandleResize(self);
        return _this;
    }
    StackedBarChart.prototype.update = function () {
        if (!this.series) {
            return;
        }
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        // https://stackoverflow.com/questions/195262/can-i-turn-off-antialiasing-on-an-html-canvas-element - allan
        // Turn off antialiasing. Other numbers must be a whole.
        this.context.translate(0.5, 0.5);
        // Clear the area.
        this.context.clearRect(0, 0, this.width, this.height);
        var top = 0;
        var bottom = 5 + this.renderOptions.markerLength;
        var left = 10 + this.renderOptions.markerLength;
        var right = 0;
        var minimum = this.getMinimum();
        if (minimum > 0) {
            minimum = 0;
        }
        var maximum = this.getMaximum(minimum);
        var divisor = (this.canvas.width > 400) ? 10 : 5;
        // Automatically generate the step if it is zero.
        var step = (this.step === 0) ? Maths.getStep(minimum, maximum, divisor) : this.step;
        maximum = Math.ceil(maximum / step) * step;
        minimum = Math.floor(minimum / step) * step;
        if (minimum > 0) {
            minimum = 0;
        }
        // Calculate the y-axis labels.
        var ylabels = [];
        for (var i = minimum; i <= maximum; i += step) {
            ylabels.push(Maths.strip(i, step));
        }
        if (ylabels.indexOf("0") === -1) {
            var lowestWhole = ylabels.reduce(function (a, b) { return (Number(a) < Number(b) && Number(a) > 0) ? a : b; });
            ylabels.splice(ylabels.indexOf(lowestWhole), 0, "0");
        }
        // Adjust the top for the title.
        var textHeight = 0;
        if (this.title) {
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            top += textHeight + 15;
        }
        // Key definitions.
        this.context.textAlign = "left";
        this.context.font = this.renderOptions.font;
        right += this.drawKeyDefinitions(top);
        // Adjust the left to fit the y-axis text.
        left += this.context.measureText(ylabels.reduce(Extension.longestLength)).width + this.renderOptions.markerLength;
        // Set the font.
        this.context.font = this.renderOptions.font;
        // Adjust the right, text is centered therefore the end marker is going to exceed the right by half it's width. It will always,
        // be the last x-axis value.
        var textWidth = this.context.measureText(this.data.reduce(Extension.longestLength)).width;
        textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
        bottom += Math.round(textWidth);
        if (this.xlabel) {
            bottom += textHeight + 15;
        }
        if (this.ylabel) {
            left += textHeight + 15;
        }
        // Adjust the right to fit the text for the end label.
        textWidth = this.context.measureText(this.data[this.data.length - 1]).width;
        right += textWidth / 4;
        // Set the axis style.
        this.context.lineWidth = this.renderOptions.thickness;
        this.context.strokeStyle = this.renderOptions.axisColor;
        // Draw the axis.
        var x1 = Math.round(left);
        var y1 = Math.round(top);
        var y2 = this.height - bottom;
        var x2 = this.width - right;
        this.drawYAxis(x1, y1, x2, y2, ylabels, textHeight);
        var xincrement = this.drawXAxis(x1, y1, x2, y2, this.data, textHeight, this.renderOptions.barSpacing, true);
        this.drawBars(x1, y1, y2, xincrement, step, minimum, maximum, textHeight);
        if (this.xlabel) {
            this.drawXAxisLabel(x1, x2, textHeight);
        }
        if (this.ylabel) {
            this.drawYAxisLabel(y1, y2, textHeight);
        }
        // Draw the title.
        if (this.title) {
            // Text height was changed to measure labels.     
            this.context.font = this.renderOptions.titleFont;
            textHeight = Number(this.context.font.replace(/[^0-9.]/g, ""));
            this.context.textAlign = "center";
            this.context.fillText(this.title, x1 + (x2 - x1) / 2, textHeight);
        }
    };
    StackedBarChart.prototype.drawBars = function (x1, y1, y2, xincrement, step, minimum, maximum, textHeight) {
        var yincrement = (y2 - y1) / (maximum - minimum);
        // Where is zero?
        var zero = y2 - yincrement * Math.abs(minimum);
        var spacing = this.renderOptions.barSpacing;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var total = 0;
                var values = this.series[i];
                var negative = (values[0] >= 0) ? false : true;
                if (this.keys.length > Number(i)) {
                    for (var j in values) {
                        if (values.hasOwnProperty(j)) {
                            this.context.fillStyle = this.renderOptions.colors[j];
                            var value = Math.abs(values[j]);
                            var y = 0;
                            if (negative) {
                                y = zero + yincrement * Math.abs(total);
                                this.context.fillRect(x1 + xincrement * Number(i), y, xincrement - spacing, yincrement * Math.abs(value));
                            }
                            else {
                                y = zero - yincrement * (value + total);
                                this.context.fillRect(x1 + xincrement * Number(i), y, xincrement - spacing, yincrement * value);
                            }
                            total += values[j];
                            // Draw the values or percentages
                            if (this.renderOptions.showValues) {
                                var text = (this.renderOptions.showAsPercentage) ? (100 / maximum * values[j]).toFixed(this.renderOptions.decimals) + "%" : values[j];
                                var textWidth = this.context.measureText(text.toString()).width;
                                this.context.fillStyle = "white";
                                this.context.textAlign = "center";
                                // Draw the value / percentage.
                                if (textWidth <= xincrement && textHeight <= yincrement * Math.abs(value)) {
                                    // -2 to from x and y for better alignment.
                                    y += value * yincrement / 2 + textHeight / 2 - 2;
                                    this.context.fillText(text.toString(), x1 + xincrement * Number(i) + xincrement / 2 - 2, y);
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    StackedBarChart.prototype.getMinimum = function () {
        var minimum = 0;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var total = 0;
                var negative = false;
                for (var j in this.series[i]) {
                    if (this.series[i].hasOwnProperty(j)) {
                        var value = this.series[i][j];
                        // Determine if the stack if negative or positive.
                        if (Number(j) === 0 && value < 0) {
                            negative = true;
                        }
                        total += (negative) ? -Math.abs(value) : Math.abs(value);
                    }
                }
                if (total < minimum) {
                    minimum = total;
                }
            }
        }
        return (minimum < 0) ? minimum : 0;
    };
    StackedBarChart.prototype.getMaximum = function (minimum) {
        var maximum = minimum;
        for (var i in this.series) {
            if (this.series.hasOwnProperty(i)) {
                var total = 0;
                var negative = false;
                for (var j in this.series[i]) {
                    if (this.series[i].hasOwnProperty(j)) {
                        var value = this.series[i][j];
                        // Determine if the stack if negative or positive.
                        if (Number(j) === 0 && value < 0) {
                            negative = true;
                        }
                        total += (negative) ? -Math.abs(value) : Math.abs(value);
                    }
                }
                if (total > maximum) {
                    maximum = total;
                }
            }
        }
        return (maximum > 0) ? maximum : 0;
    };
    return StackedBarChart;
}(AxisChart));
var DateTimeAxisSegment = /** @class */ (function () {
    /**
     * The end time() of the axis segment.
     */
    function DateTimeAxisSegment(label, time) {
        this.label = label;
        this.time = time;
    }
    return DateTimeAxisSegment;
}());
var CardinalSplineOptions = /** @class */ (function () {
    function CardinalSplineOptions() {
        this.visible = false;
        this.tension = 0.5;
        this.resolution = 10;
    }
    return CardinalSplineOptions;
}());
var RenderOptions = /** @class */ (function () {
    function RenderOptions() {
        /**
         * The colors to be used by the data series.
        */
        this.colors = ["#bf0000", "#e81f6d", "#9134a2", "#4a58aa", "#3493df", "#0d887b", "#89c14d", "#e7c41e", "#e79217", "#a06946",
            "CornflowerBlue", "DarkOrange ", "Red", "Purple", "LimeGreen", "CadetBlue", "DeepPink", "Gold", "Aquamarine", "Crimson", "Beige",
            "Bisque", "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "AliceBlue", "Chartreuse", "Chocolate", "Coral", "Cornsilk", "YellowGreen",
            "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "DarkOrchid", "DarkRed",
            "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "AntiqueWhite", "DeepSkyBlue", "DimGray", "DimGrey",
            "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Aqua", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow",
            "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan",
            "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey",
            "LightSteelBlue", "LightYellow", "Lime", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen",
            "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace",
            "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum",
            "PowderBlue", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray",
            "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "Azure"];
        /**
         * The chart text font.
         */
        this.font = "14px Arial";
        this.titleFont = "bold 15px Arial";
        /**
         * Should the values be displayed on the chart.
         */
        this.showValues = true;
        /**
         * Should the values be displayed in a percentage.
         */
        this.showAsPercentage = true;
        /**
         * The text color for the chart key.
         */
        this.keyColor = "black";
        this.axisColor = "black";
        this.gridColor = "lightGray";
        this.gridX = false;
        this.gridY = true;
        /**
         * The axis thickness.
         */
        this.thickness = 1;
        this.barSpacing = 5;
        this.markerLength = 5;
        this.pointSize = 5;
        this.showKeys = true;
        this.decimals = 1;
        this.xAngled = true;
    }
    return RenderOptions;
}());
