﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Webaspx.Database;
using System.Data;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using System.Globalization;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;
using System.Text;
//using Portal.Shared;
using System.Text.RegularExpressions;
using incabOverview;
using Newtonsoft.Json;
using System.Runtime.InteropServices.WindowsRuntime;

namespace InCabOverview
{
    public partial class StateOfTheNation : Page
    {
        protected string Client = "";

        protected string MD = DateTime.Today.ToString("ddMMyyyy");

        protected string PortalStyle = "";

        protected static bool InternalClient;

        private static Guid _secret = Guid.NewGuid();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Debugger.IsAttached)
            {
                //var test = GetAllLoggedInUnits("York", "19112021");
            }

            // Gets client name from URL
            //Client = Request.QueryString["client"];
            var client = Request.QueryString["client"];
            Client = Encryption.Encrypt(Request.QueryString["client"], _secret.ToString());
            //_client = client;

            InternalClient = string.Equals(client, "Webaspx", StringComparison.OrdinalIgnoreCase) ||
                             string.Equals(client, "RWTestDemo", StringComparison.OrdinalIgnoreCase);

            // Checks if client is fully authenticated, not fully implemented
            var authenticated = CheckAuthenticatedUser(client);

            // Needs changing to 'Authenticated' once T3 is done.
            if (authenticated || Request.Url.AbsoluteUri.StartsWith("http://localhost"))
            {
                // Set Portal Style
                PortalStyle = GetPortalStyle(client);

                // Gets selected date from URL.
                string Date = Request.QueryString["MD"];

                if (Date == null)
                {
                    Date = DateTime.Today.ToString("ddMMyyyy");
                }

                MD = DateTime.TryParseExact(Date, "ddMMyyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _)
                    ? Date
                    : "Invalid Date Selected";
            }
            else
            {
                //client = "<strong>Error: Unauthenticated</strong>.<br>It looks like you're not logged in to the InCab Portal, if you are, please try logging out and back in again.";
                Client = "Unauthenticated";
            }
        }

        // Handle multiple requests at the same time.
        static Dictionary<string, List<LogIn>> LogIns = new Dictionary<string, List<LogIn>>(); 

        static Dictionary<string, Dictionary<string, List<IssueType>>> IssueTypes = new Dictionary<string, Dictionary<string, List<IssueType>>>();

        static Dictionary<string, Dictionary<string, TimeSpan>> TipVisitTime = new Dictionary<string, Dictionary<string, TimeSpan>>();

        static Dictionary<string, List<Facility>> Facilities = new Dictionary<string, List<Facility>>();

        static Dictionary<string, Dictionary<string, HouseInformation>> PostCodes = new Dictionary<string, Dictionary<string, HouseInformation>>();


        [WebMethod]
        public static string FuncInCabData(string Client, string CurrentIndex, string MD, string PortalStyle)
        {
            // When I wrote this, only God and I understood what I was doing
            // Now, God only knows

            var erroredDriverID = "";
            var client =  Encryption.Decrypt(Client, _secret.ToString());

            try
            {
                // Check the client doesn't pass invalid data as the index
                if (!int.TryParse(CurrentIndex, out int index))
                {
                    throw new ArgumentException($"Unable to convert {CurrentIndex} to int.", CurrentIndex);
                }

                if (!Enum.TryParse(PortalStyle, out PortalStyle portalStyle))
                {
                    portalStyle = InCabOverview.PortalStyle.Waste;
                }

                if (LogIns.ContainsKey(client))
                {
                    if (LogIns[client].Count < 1)
                    {
                        LogIns[client] = GetAllLoggedInUnits(client, MD);
                    }
                }
                else
                {
                    LogIns.Add(client, GetAllLoggedInUnits(client, MD));

                    IssueTypes.Add(client, FuncGetIssueTypes(client));

                    TipVisitTime.Add(client, new Dictionary<string, TimeSpan>());
                }

                if (!Facilities.ContainsKey(client))
                {
                    Facilities.Add(client, GetFacilities(client));
                }
                else if (Facilities[client].Count == 0)
                {
                    Facilities[client] = GetFacilities(client);
                }

                if (!PostCodes.ContainsKey(client))
                {
                    DataTable PostCodesSource = Database.Read(@"SELECT 
                                                                   DISTINCT 
                                                                       [UPRN], 
                                                                       [Town], 
                                                                       [Postcode] 
                                                                FROM 
                                                                   [InCabSchedule]", client);

                    PostCodes[client] = FormatPostCodes(PostCodesSource);
                }

                if (Facilities[client].Count == 0)
                {
                    throw new Exception("Unable to find any facilities (depot, receptions), we need this to calculate your information. Webaspx will be in contact shortly to offer assistance.");
                    //return JObject.FromObject(new { data = "</br>Sorry! We were unable to find any of your facilities (depots, receptions).<br>We need this information to calculate your data.<br>Please contact <b>support@webaspx.com</b> for assistance.", success = false, alertDeveloper = false }).ToString();
                }

                SOTN_Unit unitData = null;

                if (index < LogIns[client].Count)
                {
                    LogIn CurrentLogIn = LogIns[client][index];

                    erroredDriverID = CurrentLogIn.DriverID;

                    List<LogIn> Duplicates = LogIns[client].Where(x => x.DriverID == CurrentLogIn.DriverID && x.Registration == CurrentLogIn.Registration).ToList();

                    try
                    {
                        unitData = new SOTN_Unit(Facilities[client], CurrentLogIn, client, Duplicates, PostCodes[client], MD, portalStyle);
                    }
                    catch (Exception ex)
                    {
                        var errorMessage = $"DriverID: {erroredDriverID}\r\n";

                        EmailDeveloper(client, errorMessage, ex, DateTime.Now, MD);
                    }
                }
        
                if (unitData != null)
                {
                    var service = unitData.Round.Service;

                    if (!TipVisitTime[client].ContainsKey(service))
                    {
                        TipVisitTime[client].Add(service, TimeSpan.FromMinutes(0));
                    }

                    double totalMins = unitData.TipVisits == null ? 0 : unitData.TipVisits.Sum(x => x.VisitLength.TotalMinutes);

                    TipVisitTime[client][service] = TipVisitTime[client][service].Add(TimeSpan.FromMinutes(totalMins)).StripMS();
                }

                // Filter out logins that have been seen for
                // Less than 20 minutes for normal clients
                // Or less than 5 minutes for Webaspx/RWTestDemo
                //var minWorkedThreshold = (InternalClient) ? TimeSpan.FromMinutes(5) : TimeSpan.FromMinutes(20);
                //
                //if (unitData.Count > 0 && unitData[0].HoursWorked < minWorkedThreshold)
                //{
                //    unitData[0] = null;
                //}

                if (index >= LogIns[client].Count)
                {
                    LogIns[client].Clear();

                    TipVisitTime[client].Clear();
                }

                var json = JObject.FromObject(new { data = unitData, success = true, totalVehicles = LogIns[client].Count, tipvisittimes = TipVisitTime[client] }).ToString();

                return json.ToString();
            }
            catch (Exception exception)
            {
                if (!HttpContext.Current.Request.Url.AbsoluteUri.StartsWith("http://localhost"))
                {
                    var errorMessage = $"DriverID: {erroredDriverID}\r\n";

                    //var error = new SOTN_Error(Client, errorMessage, exception, DateTime.Now, MD);

                    EmailDeveloper(client, errorMessage, exception, DateTime.Now, MD);

                    ////If error hasn't been reported, email developer with the error.
                    //if (Errors.Count(x => x.Message == exception.Message) < 1)
                    //{
                    //    Errors.Add(exception);

                    //}
                }

                return JObject.FromObject(new { data = exception.Message, success = false, alertDeveloper = true }).ToString();
            }
        }

        // Checking for duplicate Driver ID Logins
        [WebMethod]
        public static string CheckLoggedInDrivers(string clientName, string date_ddMMyyyy)
        {
            return JObject.FromObject(new { code = "200", driverOverlapErrors = new List<string>() }).ToString();

            //var client = Encryption.Decrypt(clientName, _secret.ToString());

            //var validClients = DatabaseConnection.GetCouncilList();

            //if (!validClients.Any(x => x == client))
            //{
            //    return "";
            //}

            //if (!DateTime.TryParseExact(date_ddMMyyyy, "ddMMyyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
            //{
            //    return "";
            //}

            //string incabConnection = Misc.getDbnameDbSourceDbUser(client, "incab");
            //List<string> DriverOverlapErrorMessages = DriverDuplicateOverlapsToday(clientName, date_ddMMyyyy, incabConnection);            

            //return JObject.FromObject(new { code = "200", driverOverlapErrors = DriverOverlapErrorMessages }).ToString();
        }
        
        private static List<string> DriverDuplicateOverlapsToday(string clientName, string date_ddMMyyyy, string incabConnection)
        {
            return new List<string>();
            ////string returnErrorString = "";
            //string getRegSection = "substring(MessageID,10+" + clientName.Length + ",10)";


            //var regAndUsersForDateSql = "select distinct substring(MessageID, 10 + @length, 10) as reg,user1 from messages where messageID Like 'GPS%' and date like @date and user1 not like @client order by user1";
            //var regAndUsersForDateParameters = new SqlParameter[]
            //{
            //    new SqlParameter("length", clientName.Length),
            //    new SqlParameter("date", date_ddMMyyyy + "%"),
            //    new SqlParameter("client", clientName + "Base")
            //};
            //var regAndUsersForDate = Database.Read(regAndUsersForDateSql, clientName, Catalog.Incab, regAndUsersForDateParameters);

            ////string sqlGetRegAndUsersForToday = "select distinct " + getRegSection + " as reg,user1 from messages where messageID Like 'GPS%' and date like '" + date_ddMMyyyy + "%' and user1 not like'" + clientName + "Base' order by user1;";
            ////string[,] regsAndUsers = Misc.getDataArrayExactFromDB(sqlGetRegAndUsersForToday, clientName, incabConnection);
            ////regsAndUsers = Misc.ResizeArray(regsAndUsers, 0, 2);

            //if (regAndUsersForDate == null)
            //{
            //    return null;
            //}

            //string sqlFirst = "";
            //string sqlLast = "";

            //for (int i = 1; i < regAndUsersForDate.GetLength(0); i++)
            //{
                

            //    sqlFirst = "select top 1 id," + getRegSection + " as reg,user1, time1 from messages where messageID Like 'GPS%' and date like '" + date_ddMMyyyy + "%' and user1='" + regsAndUsers[i, 1] + "' and " + getRegSection + "='" + regsAndUsers[i, 0] + "' order by id";
            //    sqlLast = "select top 1 id," + getRegSection + " as reg,user1, time1 from messages where messageID Like 'GPS%' and date like '" + date_ddMMyyyy + "%' and user1='" + regsAndUsers[i, 1] + "' and " + getRegSection + "='" + regsAndUsers[i, 0] + "' order by id desc";
            //    string[,] resultFirst = Misc.getDataArrayExactFromDB(sqlFirst, clientName, incabConnection);
            //    string[,] resultLast = Misc.getDataArrayExactFromDB(sqlLast, clientName, incabConnection);
            //    regAndUsersForDate[i, 2] = resultFirst[1, 3];
            //    regAndUsersForDate[i, 3] = resultLast[1, 3];
            //}
            
            //// check for duplicate driver ids
            //// get list of duplicates
            ////for each entry within the duplicates, check if the times overlap
            //List<string[]> regsAndUsersList = new List<string[]>();

            //List<string> driverOverlapErrorMessages = new List<string>();

            //for (int i = 1; i < regAndUsersForDate.GetLength(0); i++)
            //{
            //    regsAndUsersList.Add(new string[] { regAndUsersForDate[i, 0], regAndUsersForDate[i, 1], regAndUsersForDate[i, 2], regAndUsersForDate[i, 3] });
            //}

            //List<string> driverIdCheckHistory = new List<string>();

            //foreach (string[] item in regsAndUsersList) {
            //    List<string[]> matchingUsers = regsAndUsersList.FindAll(x => x[1] == item[1]);
            //    if (matchingUsers.Count > 1 && !driverIdCheckHistory.Contains(item[1]))
            //    {
            //        driverIdCheckHistory.Add(item[1]);
                    
            //        List<string[]> comparisonHistory = new List<string[]>();

            //        foreach (string[] entry1 in matchingUsers)
            //        {
            //            foreach (string[] entry2 in matchingUsers)
            //            {                            
            //                if (entry1 != entry2 && !comparisonHistory.Exists(x => x[0] == entry1[0] && x[1] == entry2[0]) && !comparisonHistory.Exists(x => x[0] == entry2[0] && x[1] == entry1[0]))
            //                {
            //                    comparisonHistory.Add(new string[] { entry1[0], entry2[0] });

            //                    float entry1StartTime = float.Parse(entry1[2]);
            //                    float entry1EndTime = float.Parse(entry1[3]);
            //                    float entry2StartTime = float.Parse(entry2[2]);
            //                    float entry2EndTime = float.Parse(entry2[3]);

            //                    bool entry1StartTimeWithinEntry2 = entry1StartTime >= entry2StartTime && entry1StartTime <= entry2EndTime;
            //                    bool entry1EndTimeWithinEntry2 = entry1EndTime >= entry2StartTime && entry1EndTime <= entry2EndTime;
            //                    bool entry2StartTimeWithinEntry1 = entry2StartTime >= entry1StartTime && entry2StartTime <= entry1EndTime;
            //                    bool entry2EndTimeWithinEntry1 = entry2EndTime >= entry1StartTime && entry2EndTime <= entry1EndTime;

            //                    bool entry1AndEntry2Overlap = entry1StartTimeWithinEntry2 || entry1EndTimeWithinEntry2 || entry2StartTimeWithinEntry1 || entry2EndTimeWithinEntry1;

            //                    if (entry1AndEntry2Overlap)
            //                    {
            //                        float overlapStartTime = 0;
            //                        float overlapEndTime = 0;

            //                        if (entry1StartTimeWithinEntry2)
            //                        {
            //                            overlapStartTime = entry1StartTime;
            //                        }
            //                        else if (entry2StartTimeWithinEntry1)
            //                        {
            //                            overlapStartTime = entry2StartTime;
            //                        }

            //                        if (entry1EndTimeWithinEntry2)
            //                        {
            //                            overlapEndTime = entry1EndTime;
            //                        }
            //                        else if (entry2EndTimeWithinEntry1)
            //                        {
            //                            overlapEndTime = entry2EndTime;
            //                        }                                    
                                    
            //                        string driverId = Regex.Split(entry1[1], @"\D+")[1];
            //                        string entry1Reg = entry1[0].Replace("$$", "");
            //                        string entry2Reg = entry2[0].Replace("$$", "");
            //                        string startTime_hh_mm_ss = TimeSpan.FromHours(overlapStartTime).ToString(@"hh\:mm\:ss");
            //                        string endTime_hh_mm_ss = TimeSpan.FromHours(overlapEndTime).ToString(@"hh\:mm\:ss");
            //                        driverOverlapErrorMessages.Add($"Driver {driverId} logged into both <b>{entry1Reg}</b> and <b>{entry2Reg}</b> during times {startTime_hh_mm_ss} and {endTime_hh_mm_ss}");
            //                    }
            //                }
            //            }
            //        }

            //    }
            //}


            //return driverOverlapErrorMessages;
        }

        //-------------------------------------------------------------------------------------------
        // Checking for duplicate Driver ID Logins
        [WebMethod]
        public static string ReplaceIssueCodesWithIssueText(string clientName, List<string[]> data)
        {
            return JObject.FromObject(new { code = "200", convertedData = data }).ToString();

            //// List [key, value]. Example: List ["BGDLV0", "28"]

            //// database call to incab issues to 
            //string issueCodes = "";

            //SqlParameter[] parameters = null;

            //foreach(string[] item in data)
            //{
            //    issueCodes += "'" + item[0] + "',";
            //}

            //if (issueCodes.Length > 0)
            //{
            //    issueCodes = Misc.removeLastChar(issueCodes);
            //}

            //string incabConnection = Misc.getDbnameDbSourceDbUser(clientName, "incab");
            //string sqlIncabIssueData = "SELECT issueCode, issueText FROM inCabIssues WHERE issueCouncil = '" + clientName + "' AND issueCode IN(" + issueCodes + ") ORDER BY uploadDateYYYYMMDD";
            //string[,] incabIssueData = Misc.getDataArrayExactFromDB(sqlIncabIssueData, clientName, incabConnection);

            //foreach(string[] item in data)
            //{
            //    for (int i = 1; i < incabIssueData.GetLength(0); i++)
            //    {
            //        if (item[0].Trim() == incabIssueData[i, 0].Trim())
            //        {
            //            item[0] = incabIssueData[i, 1].Trim();
            //        }
            //    }
            //}

            //return JObject.FromObject(new { code = "200", convertedData = data }).ToString();
        }     
        //-------------------------------------------------------------------------------------------
        private static Dictionary<string, HouseInformation> FormatPostCodes(DataTable postCodesSource)
        {
            // Groups together the UPRN and Postcode fields, discarding any rows with an invalid postcode.
            var postCodesFormatted = postCodesSource.AsEnumerable()
                .GroupBy(x => new { 
                    URPN = x["UPRN"].ToString(),
                    PostCode = x["PostCode"].ToString(),
                    Town = x["PostCode"].ToString(),
                })
                .Where(x => !string.IsNullOrEmpty(x.Key.PostCode));

            Dictionary<string, HouseInformation> PostCodesSorted = new Dictionary<string, HouseInformation>();

            foreach (var Item in postCodesFormatted)
            {
                // If the dictionary does have an entry for the given UPRN.
                if (PostCodesSorted.ContainsKey(Item.Key.URPN))
                {
                    // If the existing value is invalid, the new value must be valid, set it.
                    if (string.IsNullOrEmpty(PostCodesSorted[Item.Key.URPN].PostCode))
                    {
                        PostCodesSorted[Item.Key.URPN] = new HouseInformation(Item.Key.PostCode, Item.Key.Town);
                    }
                    //// If the existing value is valid and the new value is also valid, means that one UPRN has been assigned to multiple properties
                    //else if (PostCodesSorted[Item.Key.URPN].PostCode != Item.Key.PostCode)
                    //{
                    //    // If the towns mismatch alter the user
                    //    if (PostCodesSorted[Item.Key.URPN].Town != Item.Key.Town)
                    //    {
                    //        DuplicateUPRNs.Add(String.Format("{0},{1}", Item.Key.URPN, String.Join(Environment.NewLine, PostCodesSorted[Item.Key.URPN].Town, Item.Key.Town)));
                    //    }
                    //}
                }
                else
                {
                    PostCodesSorted.Add(Item.Key.URPN, new HouseInformation(Item.Key.PostCode, Item.Key.Town));
                }
            }

            /*
            if (DuplicateUPRNs.Count > 0)
            {
                DuplicateUPRNs.Insert(0, "UPRN:,Town:");

                _Excel.WriteToSpreadSheet(DuplicateUPRNs, Client, String.Format("{0}-UPRN Issues", Client), _Extensions.OutputFolder(StartDate, Client));
            }
            */

            return PostCodesSorted;
        }

        static List<SOTN_Error> Errors = new List<SOTN_Error>();

        public static List<LogIn> GetAllLoggedInUnits(string client, string date)
        {
            var LogIns = new List<LogIn>();
            var yyymmddDate = $"{date.Substring(4, 4)}{date.Substring(2, 2)}{date.Substring(0, 2)}";

            //string query = @"SELECT 
            //                     DISTINCT 
            //                         [MessageID], 
            //                         [Date], 
            //                         [Time1], 
            //                         [User1]
            //                 FROM
            //                     [Messages] 
            //                 WHERE 
            //                     [MessageID] LIKE '$IN%' 
            //                     AND 
            //                     [Date] = CONVERT(VARCHAR(255), @Date)";
            //
            //var parameters = new SqlParameter[] {
            //    new SqlParameter("Date", date)
            //};

            var query = @"SELECT 
                              DISTINCT 
                                  [Registration],
                                  [DateTimeYYYYMMDDhhmmss],
                                  [User1],
                                  [RoundDay],
                                  [RoundWeek],
                                  [RoundService],
                                  [RoundCrew]
                          FROM
                              [MessagesSplit]
                          WHERE 
                              ([Date1YYYYMMDD] = @date AND [MessageType]= 'INLogin')";
                          //ORDER BY
                          //    [DateTimeYYYYMMDDhhmmss]";

            var parameters = new SqlParameter[] {
                new SqlParameter("date", yyymmddDate)
            };

            var result = Database.Read(query, client, parameters: parameters);

            if (result != null && result.Rows.Count > 0)
            {
                var sorted = result.Select()
                                   .OrderBy(x => DateTime.Parse(x["DateTimeYYYYMMDDhhmmss"].ToString()))
                                   .ToArray();

                result = sorted.CopyToDataTable();
            }

            foreach (DataRow row in result.Rows)
            {
                var currentRegistration = row["Registration"].ToString();

                var allLoginsForVehicle = result.AsEnumerable()
                                                .Where(x => x["Registration"].ToString() == currentRegistration && !x.Equals(row))
                                                .ToArray();

                var current = new LogIn(row, allLoginsForVehicle);

                LogIns.Add(current);
            }

            LogIns = LogIns.OrderByDescending(x => x.Time).ToList();

            /* Filters out vehicles with less than 15 minutes of activity - remove 16/09/2021

            Log(client, date, $"Login - {LogIns.Count}");

            var driverIDList = LogIns.Select(x => x.DriverID).Distinct().ToArray();

            foreach (var driverID in driverIDList)
            {
                List<LogIn> Logins = LogIns.Where(x => x.DriverID == driverID).ToList();

                for (int i = 0; i < Logins.Count - 1; i++)
                {
                    TimeSpan Difference = Logins[i + 1].Time - Logins[i].Time;

                    if (Difference < TimeSpan.FromMinutes(15))
                    {
                        Log(client, date, $"Removed {Logins[i]} for less than 15 minutes of activity");
                        LogIns.Remove(Logins[i]);
                    }
                }
            }

            Log(client, date, $"LogIns Removed Low - {LogIns.Count}");
            */

            //var uniqueLogIns = new Dictionary<String, Dictionary<Round, List<Int32>>>();

            //for (int i = 0; i < LogIns.Count; i++)
            //{
            //    if (uniqueLogIns.ContainsKey(LogIns[i].DriverID))
            //    {
            //        if (uniqueLogIns[LogIns[i].DriverID].ContainsKey(LogIns[i].Round))
            //        {
            //            uniqueLogIns[LogIns[i].DriverID][LogIns[i].Round].Add(i);
            //        }
            //        else
            //        {
            //            uniqueLogIns[LogIns[i].DriverID].Add(LogIns[i].Round, new List<int>() {i});
            //        }
            //    }
            //    else
            //    {
            //        uniqueLogIns.Add(LogIns[i].DriverID, new Dictionary<Round, List<int>>() { 
            //            { LogIns[i].Round, new List<int>() {i} }
            //        });
            //    }
            //}

            //foreach (var drivers in uniqueLogIns)
            //{
            //    foreach (var Rounds in drivers.Value)
            //    {
            //        if (Rounds.Value.Count > 1)
            //        {
            //        }
            //    }
            //}

            Log(client, date, $"LogIns - {LogIns.Count}");

            
            // Remove consecutive logins to the same round

            //for (int i = 0; i < LogIns.ToList().Count; i++)
            //{
            //    try
            //    {
            //        var loginsForUnit = LogIns.Where(x => x.DriverID == LogIns[i].DriverID && x.Registration == LogIns[i].Registration)
            //                                  .ToList();
            
            //        if (loginsForUnit.Count() > 1)
            //        {
            //            var currentLoginIndex = loginsForUnit.IndexOf(LogIns[i]);
            
            //            if (currentLoginIndex != -1)
            //            {
            //                var isLastElement = currentLoginIndex + 1 == loginsForUnit.Count;
            
            //                var nextLogin = loginsForUnit[isLastElement ? currentLoginIndex : currentLoginIndex + 1];
            
            //                if (!nextLogin.Equals(LogIns[i]) && nextLogin.Round.Equals(LogIns[i].Round))
            //                {
            //                    LogIns.Remove(LogIns[i]);
            //                    Log(client, date, $"Removed {LogIns[i]}");
            //                    i = 0;
            //                }
            //            }
            //            else
            //            {
            
            //            }
            //        }
            
            //        //var loginsTemp = new List<LogIn>();
            
            //        //var loginForUnitGrouped = loginsForUnit.GroupBy(x => x.Round);
            
            //        //foreach (var item in loginForUnitGrouped)
            //        //{
            //        //    var first = item.First();
                  
            //        //    foreach (var t in item.ToList())
            //        //    {
            //        //        //var range = new DateRange() {
            //        //        //    Start = t.Time,
            //        //        //    End = t.EndTime
            //        //        //};
                  
            //        //        //first.DateRanges.Add(range);
            //        //    }
                  
            //        //    loginsTemp.Add(first);
            //        //}
            //    }
            //    catch (Exception ex)
            //    {
            //    }
            //}
            
            //Log(client, date, $"LogIns - Removed duplicates - now {LogIns.Count}");

            var vehicleLogins = LogIns.GroupBy(x => x.Registration)
                                      .SelectMany(x => x.OrderBy(y => y.StartTime))
                                      .ToList();

            return vehicleLogins;

            //return (InternalClient) ? LogIns : LogIns.Where(x => !x.DriverID.Contains("1111")).ToList();
        }

        private static void Log(string client, string date, string message)
        {
            Debug.WriteLine($"{client}, {date}, {message}");
        }

        /*
        private string DebugFormat(string client, string date, string message)
        {
            return $"{DateTime.Now} - {client} - {date} - {message}";
        }
        */

        public static Dictionary<String, List<IssueType>> FuncGetIssueTypes(String Client) // Gets a list of Issues by type( contamination, hazard, issue, other)
        {
            Dictionary<String, List<IssueType>> IssueTypes = new Dictionary<String, List<IssueType>>();

            String Query = @"SELECT 
                                 DISTINCT 
                                     [issueCode], 
                                     [issueText], 
                                     [issueType] 
                             FROM 
                                 [InCabIssues]";

            DataTable Result = Database.Read(Query, Client);

            foreach (DataRow Row in Result.Rows)
            {
                String[] Data = Row.ItemArray.StringArray();

                if (IssueTypes.ContainsKey(Data[2]))
                {
                    IssueTypes[Data[2]].Add(new IssueType() { IssueCode = Data[0], IssueText = Data[1] });
                }
                else
                {
                    IssueTypes.Add(Data[2], new List<IssueType>() { new IssueType() { IssueCode = Data[0], IssueText = Data[1] } });
                }
            }

            return IssueTypes;
        }

        private static List<Facility> GetFacilities(string client) // Gets a list of all facilities available to that client
        {
            List<Facility> ClientFacilities = new List<Facility>();

            string query = @"SELECT 
                                 * 
                             FROM 
                                 [Facilities] 
                             WHERE 
                                 [FacilityCouncil] = @Client";


            var parameter = new SqlParameter[] {
                new SqlParameter("Client", client)
            };

            DataTable Result = Database.Read(query, client, parameters: parameter);

            foreach (DataRow Row in Result.Rows)
            {
                String[] Data = Row.ItemArray.StringArray();

                if (Data[2][0] == ' ')
                {
                    Data[2] = Data[2].TrimStart(' ');
                }

                ClientFacilities.Add(new Facility(Data[2], Data[3], new Coordinate(Convert.ToDouble(Data[7]), Convert.ToDouble(Data[8]))));
            }

            return ClientFacilities;
        }

        private bool CheckAuthenticatedUser(string client)
        {
            var useDigitalDepot = CheckDigitalDepot(client);

            if (useDigitalDepot)
            {
                //https://incab-overview.azurewebsites.net/StateOfTheNation.aspx?client=Webaspx&MD=25022021&SessionID=DigitalDepotxxjamie.edwards@webaspx.com$Collections$

                var sessionID = Request.QueryString["SessionID"];

                if (sessionID != null)
                {
                    var email = sessionID.Replace("DigitalDepotxx").Replace("$Collections$");

                    var query = @"SELECT
                                      *
                                  FROM
                                      [DDUsers]
                                  LEFT JOIN 
	                                  [DDProductAccess]
	                                  ON
	                                  [DDProductAccess].[UserID] = [DDUsers].[ID]
                                  WHERE
                                      [DDUsers].[Email] = @email
	                                  AND
	                                  [DDProductAccess].[Name] = 'InCab'";

                    var parameters = new SqlParameter[]
                    {
                        new SqlParameter("email", email)
                    };

                    var result = Database.Read(query, client, Catalog.Collections, parameters);

                    if (result?.Rows.Count == 1)
                    {
                        // Add better authentication here in time
                        var lastLogin = (DateTime)result.Rows[0]["LastLogin"];

                        return (lastLogin.Date == DateTime.Today);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                var sessionKey = Request.QueryString.AllKeys.Where(x => x != null).Where(x => x.Contains("SessionID")).FirstOrDefault();
                var sessionID = Request.QueryString[sessionKey];

                if (sessionID != null)
                {
                    var query = @"SELECT
                                      COUNT(*) 
                                  FROM 
                                      [Users]
                                  WHERE 
                                      [Session] = @SessionID";

                    //var result = SOTN_Extensions.Read(query, client, new Dictionary<String, String>() { { "@SessionID", sessionID } });

                    var parameters = new SqlParameter[]
                    {
                        new SqlParameter("SessionID", sessionID)
                    };

                    var result = Database.Scalar(query, client, Catalog.Incab, parameters);

                    var validUsers = Convert.ToInt32(result);

                    return (validUsers > 0);
                }
                else
                {
                    return false;
                }
            }
        }

        private bool CheckDigitalDepot(string client)
        {
            if (string.IsNullOrWhiteSpace(client))
            {
                return false;
            }

            var query = @"SELECT
                              [useDigitalDepot]
                          FROM
                              [InCabPortalConfig]";

            var result = Database.Scalar(query, client, Catalog.Incab);

            return (result != DBNull.Value && result != null && result.ToString() == "Y");
        }

        private static void EmailDeveloper(string client, string message, Exception exception, DateTime date, string MD)
        {
            var password = GetPassword();

            if (string.IsNullOrWhiteSpace(password))
            {
                return;
            }

            using (var smtpClient = new SmtpClient()
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Timeout = 12000,
                Credentials = new NetworkCredential("no-reply@webaspx.com", password)
            })
            {
                using (var email = new MailMessage("no-reply@webaspx.com", "jamie.edwards@webaspx.com"))
                {
                    email.Subject = "SOTN Error";

                    var lineNumber = new StackTrace(exception, true).GetFrame(0).GetFileLineNumber();

                    email.Body = $"Error found in {client} SOTN at {date}\r\n\r\n{exception.Message}\r\nLine: {lineNumber}\r\n{MD}\r\n{message}";

                    smtpClient.Send(email);
                }
            }
        }

        private static string GetPassword()
        {
            var query = @"SELECT
                              [Value]
                          FROM
                              [WorkflowConfig]
                          WHERE
                              [Key] = 'defaultEmailPW'";

            return (string)Database.Scalar(query, "Webaspx", Catalog.Collections);
        }

        public string GetPortalStyle(string client)
        {
            if (string.IsNullOrWhiteSpace(client))
            {
                return "";
            }

            var query = @"SELECT
                              [portalStyle]
                          FROM
                              [InCabPortalConfig]";

            var result = Database.Scalar(query, client);

            if (result == DBNull.Value)
            {
                return "Waste";
            }

            return (string)result;
        }
    }
}
