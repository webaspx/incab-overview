﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Webaspx.Database;

namespace tests
{
    [TestClass]
    public class SimpleTest
    {
        [TestMethod]
        public void Test()
        {
            Assert.AreEqual("a", "a", "same");
        }

        [TestMethod]
        public void GetBinNotOutCount()
        {
            // int _23 = incabOverview.SOTN_Unit.GetBinNotOutCount(new DateTime(2020, 11, 23), "Middlesbrough");
            // int _24 = incabOverview.SOTN_Unit.GetBinNotOutCount(new DateTime(2020, 11, 24), "Middlesbrough");
            // int _25 = incabOverview.SOTN_Unit.GetBinNotOutCount(new DateTime(2020, 11, 25), "Middlesbrough");
            // int _26 = incabOverview.SOTN_Unit.GetBinNotOutCount(new DateTime(2020, 11, 26), "Middlesbrough");
            // int _27 = incabOverview.SOTN_Unit.GetBinNotOutCount(new DateTime(2020, 11, 27), "Middlesbrough");
            // int total = _23 + _24 + _25 + _26 + _27;
            // 
            // Assert.AreEqual(1366, total);

            string[] councils = new[]
            {
                "ArdsAndNorthDown",
                "BlaenauGwent",  "Bournemouth",
                "Carlisle",      "Copeland",
                "EastAyrshire",  "EastRenfrewshire",
                "Falkirk",       "FalkStreets",
                "Glamorgan",
                "Halton",        "Hambleton",
                "Middlesbrough", "Monmouthshire",
                "OadbyAndWigston",
                "Pembrokeshire",
                "Rhondda",       "Richmondshire",    "Rugby",        "Rutland",
                "Scarborough",   
                "Stirling",      "Swansea",          "Swindon",
                "Torridge",
                "Webaspx",       "WestLancs",    "Wrexham"
            };

            councils = DatabaseConnection.GetInCabClientsList();

            int total = 0;
            DateTime end = new DateTime(2021, 1, 8);

            var counts = new Dictionary<string, int>();

            foreach (string council in councils)
            {
                counts[council] = 0;
                //var issuesTypes = incabOverview.StateOfTheNation.FuncGetIssueTypes(council);

                Console.WriteLine(council);

                for (DateTime start = new DateTime(2020, 11, 23); start < end; start = start.AddDays(7))
                {
                    for (int i = 0; i < 5; i++)
                    {
                        //int count = incabOverview.SOTN_Unit.GetBinNotOutCount(start.AddDays(i), council, issuesTypes);
                        //counts[council] += count;
                        //total += count;
                    }
                }
            }
            Assert.AreEqual(152974, total);
        }
    }
}