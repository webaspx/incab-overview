﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Device.Location;
using System.Globalization;
using System.Linq;

namespace InCabOverview
{
    public static class SOTN_Extensions
    {
        public static String TitleCase(this String Before)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Before.ToLower());
        }

        public static Boolean IssueCheck(Issue I1, Issue I2)
        {
            return (I1.House == I2.House && I1.Street == I2.Street && I1.UPRN == I2.UPRN && I1.Round == I2.Round && I1.User == I2.User);
        }

        public static Boolean Contains(this DataRow DTR, String Find)
        {
            return DTR.ItemArray[1].ToString().Contains(Find);
        }

        public static Boolean TimeComparison(DateTime D1, DateTime D2)
        {
            return (D1 - D2) > TimeSpan.FromMilliseconds(1000);
        }


        public static DateTime TimeSent(this Object[] Before)
        {
            String[] Data = Before.StringArray();

            if (Data[1].Contains("GPS:"))
            {
                if (Data[2].Length == 9)
                {
                    return DateTime.ParseExact(((Data[2].Length == 9) ? Data[2].RemoveLast(1) : Data[2]), "ddMMyyyy", CultureInfo.InvariantCulture) + TimeSpan.FromHours(Data[3].ToDouble());
                }
                else
                {
                    return DateTime.ParseExact(((Data[2].Length == 10) ? Data[2].RemoveLast(2) : Data[2]), "ddMMyyyy", CultureInfo.InvariantCulture) + TimeSpan.FromHours(Data[3].ToDouble());
                }
            }
            else
            {
                return DateTime.ParseExact((Data[2].Length <= 8) ? Data[2] : (Data[2].RemoveLast(2)), "ddMMyyyy", CultureInfo.InvariantCulture) + TimeSpan.FromHours(Data[3].ToDouble());
            }
        }   

        public static Double ToDouble(this String Before)
        {
            return Convert.ToDouble(Before);
        }

        public static Boolean ContainsRoundData(Message Message/*, Round Round*/)
        {
            //String[] RoundFormats = new String[] {
            //                String.Format("{0}:{1}:Week {2}", Round.Service, Round.Day, Round.Week),
            //                String.Format("{0}:{1}:Week{2}", Round.Service, Round.Day, Round.Week),
            //                String.Format("{0} Week{1} {2}", Round.Day, Round.Week, Round.Service)
            //            };

            return Message.Round != null; //RoundFormats.Any(x => Message.Data.Contains(x));
        }

        public static GeoCoordinate Coordinate(this Object[] Source)
        {
            String[] RawData = Source.StringArray();

            if (RawData[0].Contains("$NoGPSFix$"))
            {

            }
            else if (RawData[1].Contains("GPS:"))
            {
                return new GeoCoordinate(RawData[1].Split(':')[4].ToDouble(), RawData[1].Split(':')[5].ToDouble()) { Speed = 5 };
            }
            else if (RawData[1].Contains("$Driver Check"))
            {
                String[] MsgData = RawData[1].Split('$').Last().Replace("Driver Check ").Split(' ');

                return new GeoCoordinate(MsgData[0].ToDouble(), MsgData[1].ToDouble());
            }
            else
            {
                String MsgData = (RawData[1].Split(':').Last()[0] == ' ') ? RawData[1].Split(':').Last().Substring(1) : RawData[1].Split(':').Last();

                return new GeoCoordinate(MsgData.Split(' ')[0].ToDouble(), MsgData.Split(' ')[1].ToDouble());
            }

            return null;
        }

        public static Coordinate Coordinate2(this Object[] Source)
        {
            String[] RawData = Source.StringArray();

            DateTime TimeSent = RawData.TimeSent();

            if (RawData[1].Contains("$NoGPSFix$"))
            {
                String Coordinate = RawData[1].Split(':')[3];

                return new Coordinate(Convert.ToDouble(Coordinate.Split(' ')[1]), Convert.ToDouble(Coordinate.Split(' ')[0]), TimeSent);
            }
            else if (RawData[1].Contains("GPS:"))
            {
                return new Coordinate(RawData[1].Split(':')[5].ToDouble(), RawData[1].Split(':')[4].ToDouble(), TimeSent) { Speed = 5 };
            }
            else if (RawData[1].Contains("$Driver Check"))
            {
                String[] MessageData = RawData[1].Split('$').Last().Replace("Driver Check ").Split(' ');

                if (MessageData.Length > 2)
                {
                    MessageData = MessageData.Skip(1).ToArray();
                }
                
                return new Coordinate(Convert.ToDouble(MessageData[1]), Convert.ToDouble(MessageData[0]), TimeSent);
            }
            else
            {
                String MsgData = (RawData[1].Split(':').Last()[0] == ' ') ? RawData[1].Split(':').Last().Substring(1) : RawData[1].Split(':').Last();

                return new Coordinate(MsgData.Split(' ')[0].ToDouble(), MsgData.Split(' ')[1].ToDouble(), TimeSent);
            }
        }

        public static String RemoveLast(this String Before, int TakeX)
        {
            return Before.Substring(0, Before.Length - TakeX);
        }

        public static String Replace(this String Str, String Replace)
        {
            return Str.Replace(Replace, "");
        }

        public static String[] StringArray(this Object[] Before)
        {
            return Before.Select(x => x.ToString()).ToArray();
        }

        public static TimeSpan StripMS(this TimeSpan time)
        {
            return new TimeSpan(time.Days, time.Hours, time.Minutes, time.Seconds);
        }

        public static Dictionary<Double, Int32> DailyDictionary(Decimal Freq)
        {
            Freq = Math.Round(Freq, 1);
            Dictionary<Double, Int32> DailyDictionary = new Dictionary<Double, Int32>();

            for (Double i = 0; i < 24; i += Convert.ToDouble(Freq))
            {
                DailyDictionary.Add(i, 0);
            }
            return DailyDictionary;
        }

        public static Double TimeToDouble(this DateTime Value, Decimal Freq)
        {
            //Freq = Math.Round(Freq, 1);
            //Double FreqMinutes = Convert.ToDouble(TimeSpan.FromHours(Decimal.ToDouble(Freq)).Minutes);
            //var nearestMultiple = (int)Math.Round(Convert.ToDouble((Value.Minute / FreqMinutes)), MidpointRounding.AwayFromZero) * FreqMinutes;

            if (Convert.ToDouble(Freq) == 0.5)
                return Value.Hour + ((Value.Minute < 30) ? 0 : 0.5);
            else//else if (Freq == 0.25)
                return Value.Hour + ((Value.Minute < 30) ? (Value.Minute < 15) ? 0 : 0.25 : (Value.Minute < 45) ? 0.5 : 0.75);
        }

        public static Double GetDistance(this GeoCoordinate Point1, Coordinate Point2)
        {
            Double phi1, phi2, theta1, theta2, rad, thetaprime, costhetaprime,
                   xx1, yy1, zz1,
                   xx2, yy2, zz2,
                   TwoPi = Math.PI / 180.0;

            //if start point is end point then do nothing
            if (Math.Abs(Point1.Latitude - Point2.Latitude) < 0.00000001)
            {
                if (Math.Abs(Point1.Longitude - Point2.Longitude) < 0.00000001)
                {
                    return 0;
                }
            }

            //do a crows flies calculation
            phi1 = (90.0 - Point1.Latitude) * TwoPi;
            phi2 = (90.0 - Point2.Latitude) * TwoPi;
            theta1 = Point1.Longitude * TwoPi;
            theta2 = Point1.Longitude * TwoPi;
            rad = 12450.8539 / (Math.PI);
            //24859.82miles is the earths circumference through the poles
            xx1 = Math.Cos(theta1) * Math.Sin(phi1);
            yy1 = Math.Sin(theta1) * Math.Sin(phi1);
            zz1 = Math.Cos(phi1);
            xx2 = Math.Cos(theta2) * Math.Sin(phi2);
            yy2 = Math.Sin(theta2) * Math.Sin(phi2);
            zz2 = Math.Cos(phi2);

            costhetaprime = (xx1 * xx2 + yy1 * yy2 + zz1 * zz2);

            if (costhetaprime > 1.0 - 0.00000000000000000001)
            {
                thetaprime = 0.0;
            }
            else
            {
                thetaprime = Math.Sqrt(1.0 - costhetaprime * costhetaprime) / costhetaprime;
            }

            thetaprime = Math.Atan(thetaprime);

            return Math.Abs(rad * thetaprime) * 1609.334;
        }

        public static Double ToMiles(this Double Before)
        {
            return ((Before / 1000) * 0.621371);
        }

        public static Dictionary<Double, Int32> DictionaryData(this Dictionary<Double, Int32> Data, DateTime SignIn, Decimal Freq)
        {
            Double First = Data.FirstOrDefault(x => x.Value != 0).Key,
                   Last = Data.LastOrDefault(x => x.Value != 0).Key;

            if (First == 0)
            {
                First = SignIn.TimeToDouble(Freq);

                Last = DateTime.Now.TimeToDouble(Freq);
            }

            return Data.Where(x => x.Key >= First && x.Key <= Last).ToDictionary(x => x.Key, x => x.Value);
        }

        public static Dictionary<String, Int32> AddDictionary(ref Dictionary<String, Int32> Data, String Key)
        {
            if (Data.ContainsKey(Key))
            {
                Data[Key]++;
            }
            else
            {
                Data.Add(Key, 1);
            }

            return Data;
        }

        public static Double GetDistanceTo(Coordinate C1, Coordinate C2)
        {
            Int32 R = 6371;

            Double Latitude = DegreesToRad(C2.Latitude - C1.Latitude);

            Double Longitude = DegreesToRad(C2.Longitude - C1.Longitude);

            Double A = Math.Sin(Latitude / 2) * Math.Sin(Latitude / 2) + Math.Cos(DegreesToRad(C1.Longitude)) * Math.Cos(DegreesToRad(C2.Longitude)) * Math.Sin(Longitude / 2) * Math.Sin(Longitude / 2);

            Double C = 2 * Math.Atan2(Math.Sqrt(A), Math.Sqrt(1 - A));

            return R * C;
        }

        public static Double DegreesToRad(Double Degrees)
        {
            return Degrees * (Math.PI / 180);
        }

        public static GeoCoordinate RoundCoordinate(Double Lat, Double Lon)
        {
            String LatString = Lat.ToString();

            String LonString = Lon.ToString();

            Int32 LatIndex = LatString.IndexOf('.') + 1;

            Int32 LonIndex = LonString.IndexOf('.') + 1;

            if ((LatIndex == -1) && (LonIndex == -1))
            {
                return new GeoCoordinate(Lat, Lon);
            }
            else
            {
                Int32 LatRound = LatString.Substring(LatIndex, LatString.Length - LatIndex).Length - 1;

                Int32 LonRound = LonString.Substring(LonIndex, LonString.Length - LonIndex).Length - 1;

                return new GeoCoordinate(Math.Round(Lat, LatRound), Math.Round(Lon, LonRound));
            }
        }
    }
}

