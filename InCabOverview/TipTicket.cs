﻿namespace InCabOverview
{
    public class TipTicket
    {
        public int Gross { get; set; }

        public int Tare { get; set; }

        public string Unit { get; set; }

        public string Location { get; set; }

    }
}