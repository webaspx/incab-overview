﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Webaspx.Database;

namespace InCabOverview
{
    public class Database
    {
        public static DataTable Read(string query, string client, Catalog catalog = Catalog.Incab, SqlParameter[] parameters = null)
        {
            using (var connection = new DatabaseConnection(client, catalog))
            {
                if (connection.Connection == null)
                {
                    return null;
                }

                using (var sqlCommand = new SqlCommand(query)
                {
                    CommandTimeout = 30
                })
                {
                    if (parameters != null)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    try
                    {
                        return connection.ExecuteDataTable(sqlCommand);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
        }

        public static object Scalar(string query, string client, Catalog catalog = Catalog.Incab, SqlParameter[] parameters = null)
        {
            using (var connection = new DatabaseConnection(client, catalog))
            {
                if (connection.Connection == null)
                {
                    return null;
                }

                using (var sqlCommand = new SqlCommand(query)
                {
                    CommandTimeout = 30
                })
                {
                    if (parameters != null)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    try
                    {
                        return connection.ExecuteScalar(sqlCommand);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
        }
    }
}